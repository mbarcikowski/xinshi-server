/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.server.spi;

import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.core.stanza.Stanza;

/**
 * Define a service as a specialized local service of the XMPP server.
 * The full address of the service is determined according to the service's sub-domain and the XMPP server's domain.
 *
 * @author mathieu.barcikowski@gmail.com
 */
public interface Service
{

    /**
     * Notify that the service is registered and provides its fully qualified domain name.
     * Called when the XMPP server register the service as a specialized local service.
     *
     * @param aServiceFQDN service's fully qualified domain name
     */
    void bind( Address aServiceFQDN );

    /**
     * Handle stanza whose destination is the service.
     * The XMPP server route the stanza to the service according to the service's sub-domain
     * (see {@link  Services#SERVICE_SUBDOMAIN  Services#SERVICE_SUBDOMAIN property})
     *
     * @param aStanza stanza to handle.
     */
    void handle( Stanza aStanza );

    /**
     * Notify that the service is unregistered.
     * Called when the XMPP server unregister the service from its list of specialized local services.
     */
    void unbind();
}
