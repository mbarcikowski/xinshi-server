/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax.internal.core.delegate.element;

import org.junit.Test;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import uk.org.retep.niosax.internal.core.delegate.BaseSaxTest;

/**
 * Test elements with namespace declarations
 *
 * @author peter
 */
public class NamespaceTest
    extends BaseSaxTest
{

    /**
     * XML Based on RFC3920 4.8
     */
    private static final String STREAM_START =
        "<stream:stream\n       to='example.com'\n       xmlns='jabber:client'\n       xmlns:stream='http://etherx.jabber.org/streams'\n       version='1.0'>";

    //private static final String STREAM_START = "<stream:stream\n       to='example.com'\n       xmlns:stream='http://etherx.jabber.org/streams'\n       version='1.0'>";
    private static final String MESSAGE =
        "<message from='juliet@example.com'to='romeo@example.net'xml:lang='en'><body>Art thou not Romeo, and a Montague?</body></message>";

    private static final String STREAM_END = "</stream:stream>";

    private static final String NS_XML = STREAM_START + MESSAGE + STREAM_END;

    /**
     * Tests SIMPLE_XML without splitting
     *
     * @throws Exception
     */
    @Test
    public void testNSElement()
        throws Exception
    {
        nsElement( false );
    }

    /**
     * Tests SIMPLE_XML with splitting
     *
     * @throws Exception
     */
    @Test
    public void testNSElementSplit()
        throws Exception
    {
        nsElement( true );
    }

    private void nsElement( final boolean split )
        throws Exception
    {
        parseDecl( new NSElement(), split, NS_XML );
    }

    /**
     * TestHandler for simple tests
     */
    private class NSElement
        extends TestHandler
    {


        @Override
        public void resetHandler()
            throws Exception
        {
            super.resetHandler();
        }

        @Override
        public void assertHandler()
            throws Exception
        {
        }

        @Override
        public void startPrefixMapping( String prefix, String uri )
            throws SAXException
        {
        }

        @Override
        public void endPrefixMapping( String prefix )
            throws SAXException
        {
        }

        @Override
        public void startElement( String uri, String localName, String qName, Attributes attributes )
            throws SAXException
        {

        }
    }
}
