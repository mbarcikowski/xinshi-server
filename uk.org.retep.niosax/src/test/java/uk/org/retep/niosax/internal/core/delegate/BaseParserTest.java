/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax.internal.core.delegate;

import org.junit.Test;

/**
 * This generates two tests, parseWhole() and parseSplit(). These then call an
 * abstract test parseDocument() with a single parameter with false and true respectively.
 * <p/>
 * The reason for this is that the class then implements two tests. When that
 * parameter is false, then the xml is passed to the parser in its entirety.
 * <p/>
 * When true, it is split in two before testing. This allows us to test partial
 * content easily.
 *
 * @author peter
 */
public abstract class BaseParserTest
    extends BaseSaxTest
{

    @Test
    public final void parseWhole()
        throws Exception
    {
        parseDocument( false );
    }

    @Test
    public final void parseSplit()
        throws Exception
    {
        parseDocument( false );
    }

    /**
     * Implement the actual test. JUnit will run this twice, one for parseWhole()
     * with split==false, and once for parseSplit() with split==true.
     *
     * @param split Should the xml in the test be broken up to simulate
     *              incomplete content?
     * @throws Exception on failure
     */
    protected abstract void parseDocument( boolean split )
        throws Exception;

}
