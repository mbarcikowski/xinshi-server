/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax.internal.core;

import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.ext.LexicalHandler;
import uk.org.retep.niosax.NioSaxParser;
import uk.org.retep.niosax.NioSaxParserHandler;
import uk.org.retep.niosax.UndeclaredNamespaceException;
import uk.org.retep.niosax.internal.helper.NamespaceSupport;
import uk.org.retep.niosax.internal.helper.XmlSpec;

/**
 * Base implementation of {@link NioSaxParser}
 *
 * @author peter
 * @since 9.10
 */
public abstract class AbstractNioSaxParser
    implements NioSaxParser
{

    private final NamespaceSupport namespaceSupport;

    private ContentHandler handler;

    private LexicalHandler lexicalHandler;

    private NioSaxParserHandler nioSaxParserHandler;

    // The current ParserDelegate in use.
    private ParserDelegate parserState;

    public AbstractNioSaxParser()
    {
        namespaceSupport = new NamespaceSupport();
        namespaceSupport.setNamespaceDeclUris( true );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final ContentHandler getHandler()
    {
        return handler;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void setHandler( final ContentHandler handler )
    {
        if ( handler == null )
        {
            throw new NullPointerException();
        }

        this.handler = handler;

        if ( handler instanceof LexicalHandler )
        {
            lexicalHandler = (LexicalHandler) handler;
        }

        if ( handler instanceof NioSaxParserHandler )
        {
            nioSaxParserHandler = (NioSaxParserHandler) handler;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final LexicalHandler getLexicalHandler()
    {
        return lexicalHandler;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void setLexicalHandler( final LexicalHandler lexicalHandler )
    {
        this.lexicalHandler = lexicalHandler;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final NioSaxParserHandler getNioSaxParserHandler()
    {
        return nioSaxParserHandler;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void setNioSaxParserHandler( final NioSaxParserHandler nioSaxParserHandler )
    {
        this.nioSaxParserHandler = nioSaxParserHandler;
    }

    /**
     * The current {@link ParserDelegate}
     *
     * @param <T> type of {@link ParserDelegate}
     * @return the current {@link ParserDelegate}
     */
    @SuppressWarnings("unchecked")
    protected final <T extends ParserDelegate> T getParserState()
    {
        return (T) parserState;
    }

    /**
     * Set the current {@link ParserDelegate} that this instance will delegate
     * parsing to. This is usually called by the delegates constructor
     *
     * @param parserState {@link ParserDelegate} that will handle parsing.
     */
    final void setParserState( final ParserDelegate parserState )
    {
        this.parserState = parserState;
    }

    /**
     * Finishes the current {@link ParserDelegate}, cleaning up any resources
     * it may have and makes its parent the active one.
     *
     * @param <T> type of {@link ParserDelegate}
     * @return the new active {@link ParserDelegate}
     */
    public final <T extends ParserDelegate> T finish()
    {
        parserState.cleanup();
        parserState = parserState.getParent();
        return this.getParserState();
    }

    /**
     * Delegates the parser to {@link Prolog} instance compatible with this parser
     *
     * @return {@link Prolog}
     * @throws org.xml.sax.SAXException on failure
     */
    public abstract Prolog delegateProlog()
        throws SAXException;

    /**
     * {@inheritDoc }
     */
    @Override
    public final void startDocument()
        throws SAXException
    {
        if ( parserState != null )
        {
            throw new SAXException( "Document already started" );
        }

        // Reset the NamespaceSupport
        namespaceSupport.reset();
        namespaceSupport.setNamespaceDeclUris( true );
        namespaceSupport.pushContext();

        try
        {
            delegateProlog();
            getHandler().startDocument();
        }
        catch ( SAXException sex )
        {
            // Remove the document then rethrow
            finish();
            throw sex;
        }
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final void endDocument()
        throws SAXException
    {
        try
        {
            getHandler().endDocument();
        }
        finally
        {
            // Regardless of how endDocument finished, cleanup
            while ( parserState != null )
            {
                finish();
            }
        }
    }


    /**
     * Declare a Namespace prefix. All prefixes must be declared before they are
     * referenced.
     * <p/>
     * <p>
     * This is similar to {@code getNamespaceSupport().declarePrefix(prefix, uri)}
     * and throwing SAXException if that method returned false.
     * </p>
     *
     * @param prefix The prefix to declare, or the empty string to indicate the
     *               default element namespace. This may never have the value "xml" or "xmlns".
     * @param uri    The Namespace URI to associate with the prefix.
     * @throws org.xml.sax.SAXException if the prefix was not legal.
     * @see org.xml.sax.helpers.NamespaceSupport#declarePrefix(String, String)
     */
    public final void declarePrefix( final String prefix, final String uri )
        throws SAXException
    {
        handler.startPrefixMapping( prefix, uri );

        if ( !namespaceSupport.declarePrefix( prefix, uri ) )
        {
            throw new SAXException( "Illegal namespace declaration " + prefix + "=\"" + uri + "\"" );
        }

        handler.endPrefixMapping( prefix );
    }

    /**
     * Convenience method to process a raw XML qualified name, after all
     * declarations in the current context have been handled by
     *
     * @param name      qName to process
     * @param attribute true if the name is for an attribute, false for an element
     * @return String[] containing the processed qName
     * @throws org.xml.sax.SAXException
     * @see org.xml.sax.helpers.NamespaceSupport#processName(String, String[], boolean)
     * @see XmlSpec#NAMESPACEURI
     * @see XmlSpec#LOCALNAME
     * @see XmlSpec#QNAME
     */
    public final String[] processName( final String name, final boolean attribute )
        throws SAXException
    {
        final String[] qName = new String[XmlSpec.PROCESSED_NAME_SIZE];

        if ( namespaceSupport.processName( name, qName, attribute ) == null )
        {
            throw new UndeclaredNamespaceException( name );
        }

        return qName;
    }

    /**
     * Convenience method for {@link org.xml.sax.helpers.NamespaceSupport#pushContext() }
     */
    public final void pushNamespaceSupportContext()
    {
        namespaceSupport.pushContext();
    }

    /**
     * Convenience method for {@link org.xml.sax.helpers.NamespaceSupport#pushContext() }
     */
    public final void popNamespaceSupportContext()
    {
        namespaceSupport.popContext();
    }
}
