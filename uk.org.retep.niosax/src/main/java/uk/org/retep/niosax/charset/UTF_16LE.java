/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax.charset;

import java.nio.ByteBuffer;

/**
 * Our own implementation of the UTF-16 charset. This simply delegages the
 * encoding to {@link java.nio.ByteBuffer#getChar()} and {@link java.nio.ByteBuffer#putChar(char)}
 *
 * @author peter
 */
@Encoding(
    { "UTF-16LE", "UTF_16LE", "X-UTF-16LE", "UnicodeLittleUnmarked" })
public class UTF_16LE
    extends AbstractUTF_16
{

    /**
     * {@inheritDoc}
     */
    @Override
    public Charset getInstance()
    {
        return new UTF_16LE();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public char decode( final ByteBuffer buffer )
    {
        if ( buffer.remaining() < 2 )
        {
            return NOT_ENOUGH_DATA;
        }
        else
        {
            return decodeLittle( buffer.get(), buffer.get() );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean encode( final ByteBuffer buffer, final char c )
    {
        return encodeLittle( buffer, c );
    }
}
