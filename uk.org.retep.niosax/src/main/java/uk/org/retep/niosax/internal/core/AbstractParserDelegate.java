/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax.internal.core;

import org.xml.sax.SAXException;
import uk.org.retep.niosax.NioSaxSource;

/**
 * Abstract class for all {@link ParserDelegate} implementations that have a
 * parent.
 * <p/>
 * <p>
 * By convention it's usual for implementations to have either private or
 * protected constructors and have a public static method to obtain a new
 * instance. For example:
 * </p>
 * <p/>
 * <code><pre>
 *  public static &lt;P extends {@linkplain ParserDelegate ParserDelegate}&gt; Comment&lt;P&gt; delegate( final P parent )
 *      {
 *          return new Comment&lt;P&gt;( parent );
 *      }
 * </pre></code>
 * <p/>
 * <p>
 * The reason for this is to allow for custom implementations to be provided
 * based on the parent. For example the {@link uk.org.retep.niosax.internal.core.delegate.ProcessingInstruction} class
 * provides two delegate methods, one for normal use the other specifically
 * for use by {@link uk.org.retep.niosax.internal.core.Prolog}. The latter instance allows for xml and DOCTYPE
 * declarations whilst the former prohibits them.
 * </p>
 *
 * @param <P> The parent {@link ParserDelegate} type
 * @author peter
 * @since 9.10
 */
public abstract class AbstractParserDelegate<P extends ParserDelegate>
    extends ParserDelegate
{

    private final P parent;

    /**
     * Constructor used by all {@link ParserDelegate} implementations.
     *
     * @param parent The parent {@link ParserDelegate}
     * @throws NullPointerException if parent is null
     */
    public AbstractParserDelegate( final P parent )
    {
        super( parent.getParser() );
        this.parent = parent;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final P getParent()
    {
        return parent;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void parse( final NioSaxSource source )
        throws SAXException
    {
        char c = source.decode();
        while ( source.isValid( c ) && parse( source, c ) )
        {
            c = source.decode();
        }
    }

    /**
     * Parse a character provided by the main {@link #parse(uk.org.retep.niosax.NioSaxSource) }
     * method.
     *
     * @param source {@link NioSaxSource}
     * @param c      char read
     * @return true if processing should continue, false to abort the loop
     * @throws org.xml.sax.SAXException if the parse fails
     */
    public abstract boolean parse( final NioSaxSource source, final char c )
        throws SAXException;
}
