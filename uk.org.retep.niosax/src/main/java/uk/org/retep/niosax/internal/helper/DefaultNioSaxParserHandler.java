/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax.internal.helper;

import org.xml.sax.ext.DefaultHandler2;
import uk.org.retep.niosax.NioSaxParserHandler;

/**
 * A subclass of the SAX {@link org.xml.sax.ext.DefaultHandler2} which implements the
 * {@link NioSaxParserHandler} interface.
 * <p/>
 * <p>
 * Except for overriding the original SAX1
 * {@link #resolveEntity(String, String) } method the
 * added handler methods just return. Subclassers may override everything on
 * a method-by-method basis.
 * </p>
 *
 * @author peter
 * @see org.xml.sax.ext.DefaultHandler2
 * @see NioSaxParserHandler
 * @since 9.10
 */
public class DefaultNioSaxParserHandler
    extends DefaultHandler2
    implements NioSaxParserHandler
{

    /**
     * {@inheritDoc}
     */
    @Override
    public void xmlDeclaration( String versionInfo, String encoding, boolean standalone )
    {
    }
}
