/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax.internal.core.engine;

import org.xml.sax.SAXException;
import org.xml.sax.ext.LexicalHandler;
import uk.org.retep.niosax.IllegalCharacterException;
import uk.org.retep.niosax.NioSaxSource;
import uk.org.retep.niosax.internal.core.StateEngine;
import uk.org.retep.niosax.internal.core.StateEngineDelegate;
import uk.org.retep.niosax.internal.helper.Appendable;

import static uk.org.retep.niosax.internal.helper.XmlSpec.stateCompleted;

/**
 * {@link StateEngine} implementation which parses an xml comment
 *
 * @author peter
 * @since 9.10
 */
public enum CommentStateEngine
    implements StateEngine<StateEngineDelegate>
{

    /**
     * If the char is '-' then switch to {@link #END1} otherwise append to
     * the buffer
     */
    NORMAL
        {
            @Override
            public StateEngine parse( final StateEngineDelegate e, final NioSaxSource source, final char c )
                throws SAXException
            {
                if ( c == '-' )
                {
                    return END1;
                }
                else
                {
                    e.append( c );
                    return this;
                }
            }
        },
    /**
     * State when the first '-' is found. If the next char is also '-' then we
     * switch to {@link #END2}, otherwise simply but both '-' and the char into the
     * buffer and resume {@link #NORMAL}
     */
    END1
        {
            @Override
            public StateEngine parse( final StateEngineDelegate e, final NioSaxSource source, final char c )
                throws SAXException
            {
                if ( c == '-' )
                {
                    return END2;
                }
                else
                {
                    // Push back the preceding '-' and return back to NORMAL.

                    // Note: if c=='>' here the XML 1.1 grammar allows for -&gt;
                    // just not --&gt; so this is still valid.

                    e.append( '-' ).append( c );
                    return NORMAL;
                }
            }
        },
    /**
     * State when a '-' is found whilst in {@link #END1}. The next char must then
     * be '&gt;'
     */
    END2
        {
            @Override
            public StateEngine parse( final StateEngineDelegate e, final NioSaxSource source, final char c )
                throws SAXException
            {
                if ( c == '>' )
                {
                    // Notify the LexicalHandler (if supported),
                    // finish this state and change to COMPLETED

                    final LexicalHandler lh = e.getLexicalHandler();
                    if ( lh != null )
                    {
                        final Appendable a = e.getAppendable();
                        lh.comment( a.getCharBuffer(), 0, a.size() );
                    }

                    e.finish();

                    return stateCompleted();
                }
                else
                {
                    throw new IllegalCharacterException();
                }
            }
        };

    /**
     * {@inheritDoc }
     */
    @Override
    public final boolean continueLoop()
    {
        return true;
    }
}
