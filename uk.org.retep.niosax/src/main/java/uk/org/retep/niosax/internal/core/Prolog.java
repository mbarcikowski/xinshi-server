/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax.internal.core;

/**
 * {@link ParserDelegate} implementation for parsing the document's prolog.
 *
 * @author peter
 * @since 9.10
 */
public abstract class Prolog
    extends ParserDelegate
{

    protected Prolog( final AbstractNioSaxParser parser )
    {
        super( parser );
    }

    /**
     * <p>
     * This method always returns null as the Prolog is the root
     * {@link ParserDelegate}.
     * </p>
     * <p/>
     * {@inheritDoc }
     */
    @Override
    public final ParserDelegate getParent()
    {
        return null;
    }
}
