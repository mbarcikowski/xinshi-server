/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax;

/**
 * Optional extension handler for reporting events not supported by the SAX2
 * API, such has the xml declaration.
 *
 * @author peter
 * @since 9.10
 */
public interface NioSaxParserHandler
{

    /**
     * Called when the xmlDeclaration has been parsed. If the document has no
     * declaration element then this method will not be called.
     *
     * @param versionInfo the xml version if supplied
     * @param encoding    the charset encoding of the document
     * @param standalone  Is the document standalone. {@link Boolean#FALSE}
     *                    indicates the value was not declared
     */
    void xmlDeclaration( String versionInfo, String encoding, boolean standalone );
}
