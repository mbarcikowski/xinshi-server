/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax.internal.helper.dom;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;

/**
 * A {@link AbstractNioDomParser} implementation which will generate a DOM tree
 * which is populated by a {@link uk.org.retep.niosax.NioSaxParser}.
 *
 * @author peter
 * @see NioDomStreamParser
 * @since 9.10
 */
public class NioDomParser
    extends AbstractNioDomParser
{

    /**
     * Construct a handler using the supplied listener.
     * This will create a standard namespace aware {@link org.w3c.dom.Document}.
     *
     * @throws javax.xml.parsers.ParserConfigurationException
     *          if the {@link org.w3c.dom.Document} could not
     *          be created
     */
    public NioDomParser()
        throws ParserConfigurationException
    {
        this( new ListenerAdapter() );
    }

    /**
     * Construct a handler using the supplied listener and document
     *
     * @param document {@link org.w3c.dom.Document} to use
     */
    public NioDomParser( final Document document )
    {
        this( document, new ListenerAdapter() );
    }

    /**
     * Construct a handler using the supplied listener.
     * This will create a standard namespace aware {@link org.w3c.dom.Document}.
     *
     * @param listener {@link uk.org.retep.niosax.internal.helper.dom.AbstractNioDomParser.Listener} to receive events
     * @throws javax.xml.parsers.ParserConfigurationException
     *          if the {@link org.w3c.dom.Document} could not
     *          be created
     */
    public NioDomParser( final Listener listener )
        throws ParserConfigurationException
    {
        super( listener );
    }

    /**
     * Construct a handler using the supplied listener and document
     *
     * @param document {@link org.w3c.dom.Document} to use
     * @param listener {@link uk.org.retep.niosax.internal.helper.dom.AbstractNioDomParser.Listener} to receive events
     */
    public NioDomParser( final Document document, final Listener listener )
    {
        super( document, listener );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void appendElement( final Node child )
    {
        if ( node == null )
        {
            document.appendChild( child );
        }
        else
        {
            node.appendChild( child );
        }
        node = child;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void endElement( final String uri, final String localName, final String qName )
        throws SAXException
    {
        node = node.getParentNode();
    }
}
