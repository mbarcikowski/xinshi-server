/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax;

import org.xml.sax.ContentHandler;

/**
 * A factory of {@link NioSaxParser}'s. It is advised to use this class so that
 * the underlying implementation can be changed with little code changes.
 * <p/>
 * <p>
 * This will use the default implementation unless the system property
 * "retep.niosax.factory" is set. If so then it will use the class
 * defined by that property as the factory instance.
 * </p>
 *
 * @author peter
 * @since 9.10
 */
public abstract class NioSaxParserFactory
{

    private static final String FACTORY_KEY = "retep.nioparser.factory";

    private static final String DEFAULT_FACTORY = "uk.org.retep.niosax.internal.core.DefaultNioSaxFactory";

    /**
     * Get the current {@link uk.org.retep.niosax.NioSaxParserFactory} implementation.
     *
     * @return {@link uk.org.retep.niosax.NioSaxParserFactory} implementation.
     */
    public static NioSaxParserFactory getInstance()
    {
        return FactoryHolder.getInstance();
    }

    /**
     * Create a new {@link NioSaxParser}
     *
     * @return {@link NioSaxParser}
     */
    public abstract NioSaxParser newInstance();

    /**
     * Create a new {@link NioSaxParser}
     *
     * @param handler {@link org.xml.sax.ContentHandler} to receive events
     * @return {@link NioSaxParser}
     */
    public final NioSaxParser newInstance( final ContentHandler handler )
    {
        final NioSaxParser parser = newInstance();
        parser.setHandler( handler );
        return parser;
    }

    /**
     * Manages the instantiation of the factory using a initialise-on-demand
     * holder pattern. This ensures that the factory is only created when it is
     * first used, not when the main class is loaded.
     */
    private static class FactoryHolder
    {

        private static NioSaxParserFactory instance;

        static
        {
            final String className = System.getProperty( FACTORY_KEY, DEFAULT_FACTORY );

            try
            {
                instance = (NioSaxParserFactory) Class.forName( className ).newInstance();
            }
            catch ( Exception ex )
            {
                throw new RuntimeException( "Unable to load NioSaxParserFactory " + className, ex );
            }
        }

        private FactoryHolder()
        {
        }

        static NioSaxParserFactory getInstance()
        {
            return instance;
        }
    }
}
