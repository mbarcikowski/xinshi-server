/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax.internal.helper;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;
import uk.org.retep.niosax.internal.core.AbstractNioSaxParser;

import java.util.Arrays;

import static uk.org.retep.niosax.internal.helper.XmlSpec.*;

/**
 * A collection of qName/value pairs. This collection is used to hold attributes
 * whilst they are being parsed. Once parsing is complete, it parses the qNames
 * for namespace declarations, declares them in the current scope
 *
 * @author peter
 */
public class AttributeList
{

    private final AbstractNioSaxParser parser;

    private String[][] buf;

    private int count;

    public AttributeList( final AbstractNioSaxParser parser )
    {
        this.parser = parser;
        buf = new String[10][];
        count = 0;
    }

    public void addAttribute( final String qName, final String value )
    {
        final int newcount = count + 1;
        if ( newcount > buf.length )
        {
            buf = Arrays.copyOf( buf, Math.max( buf.length << 1, newcount ) );
        }

        buf[count] = new String[]{ qName, value };

        count = newcount;
    }

    public int size()
    {
        return count;
    }

    public boolean isEmpty()
    {
        return count == 0;
    }

    public boolean processNames()
        throws SAXException
    {
        boolean newContext = false;

        for ( int i = 0; i < count; i++ )
        {
            final String qName = buf[i][0];
            if ( qName.startsWith( XMLNS ) )
            {
                if ( !newContext )
                {
                    parser.pushNamespaceSupportContext();
                    newContext = true;
                }

                final int idx = qName.indexOf( ':' ) + 1;
                if ( idx == 0 )
                {
                    // The default namespace
                    parser.declarePrefix( "", buf[i][1] );
                }
                else
                {
                    parser.declarePrefix( qName.substring( idx ), buf[i][1] );
                }
            }
        }

        return newContext;
    }

    public Attributes getAttributes()
        throws SAXException
    {
        final AttributesImpl ai = new AttributesImpl();

        for ( int i = 0; i < count; i++ )
        {
            final String qName[] = parser.processName( buf[i][0], true );
            ai.addAttribute( qName[NAMESPACEURI], qName[LOCALNAME], qName[QNAME],
                             // CDATA" as stated in the XML 1.0 Recommentation
                             // clause 3.3.3, Attribute-Value Normalization
                             TYPE_CDATA, buf[i][1] );
        }

        return ai;
    }
}
