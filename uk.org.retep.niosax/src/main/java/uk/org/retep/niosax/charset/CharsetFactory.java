/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax.charset;

import com.google.common.collect.Lists;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Factory of {@link uk.org.retep.niosax.charset.Charset} instances. This factory allows for the lookup of
 * Charset instances based on their names.
 *
 * @author peter
 */
public class CharsetFactory
{

    private final static List<Charset> CHARSETS =
        Lists.<Charset>newArrayList( new ISO_8859_1(), new US_ASCII(), new UTF_16(), new UTF_16BE(), new UTF_16LE(),
                                     new UTF_8() );

    private final static Map<String, Charset> CHARSET_MAP;

    static
    {
        CHARSET_MAP = new HashMap<>();

        for ( Charset c : CHARSETS )
        {
            final Encoding e = c.getClass().getAnnotation( Encoding.class );
            if ( e != null )
            {
                for ( String enc : e.value() )
                {
                    if ( !CHARSET_MAP.containsKey( enc ) )
                    {
                        CHARSET_MAP.put( enc, c );
                    }
                }
            }
        }
    }

    private CharsetFactory()
    {
    }


    /**
     * Returns a {@link uk.org.retep.niosax.charset.Charset} implementation for the named encoding
     *
     * @param encoding Encoding to lookup
     * @return {@link uk.org.retep.niosax.charset.Charset} or null if not supported.
     */
    public static Charset getCharset( final String encoding )
    {
        final Charset c = CHARSET_MAP.get( encoding );
        return c == null ? null : c.getInstance();
    }
}
