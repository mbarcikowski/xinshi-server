/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax;

import org.xml.sax.SAXException;

/**
 * {@link org.xml.sax.SAXException} thrown by {@link NioSaxParser} when a namespace is
 * referenced but has not been declared within the current
 * {@link org.xml.sax.helpers.NamespaceSupport} context.
 *
 * @author peter
 * @since 9.10
 */
public class UndeclaredNamespaceException
    extends SAXException
{

    private static final String msg = "Undeclared namespace";

    private final String qName;

    public UndeclaredNamespaceException( final String qName )
    {
        super( msg + " " + qName );
        this.qName = qName;
    }

    public UndeclaredNamespaceException( final String qName, final Exception e )
    {
        super( msg + " " + qName, e );
        this.qName = qName;
    }

    /**
     * The QName that caused the failure
     *
     * @return the QName that caused the failure
     */
    public final String getQName()
    {
        return qName;
    }
}
