/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax.internal.core;

import uk.org.retep.niosax.internal.helper.Appendable;
import uk.org.retep.niosax.internal.helper.CharAppendable;

/**
 * Abstract implementation for {@link ParserDelegate}'s that need to store
 * parsed characters using the {@link Appendable} api.
 *
 * @param <P> The parent {@link ParserDelegate} type
 * @author peter
 * @since 9.10
 */
public abstract class AppendableParserDelegate<P extends ParserDelegate>
    extends AbstractParserDelegate<P>
{

    private Appendable appendable;

    /**
     * Constructor used by all {@link StateEngineDelegate} implementations.
     *
     * @param parent The parent {@link ParserDelegate}
     */
    public AppendableParserDelegate( final P parent )
    {
        super( parent );
        appendable = new CharAppendable();
    }

    /**
     * The underlying {@link Appendable} used to store parsed characters
     *
     * @param <T> type of {@link Appendable}
     * @return underlying {@link Appendable}
     */
    @SuppressWarnings("unchecked")
    public final <T extends Appendable> T getAppendable()
    {
        return (T) appendable;
    }

    /**
     * Replaces the underlying appendable with a new one. The new one will have
     * it's parent set to the original one.
     *
     * @param <T>        type of {@link Appendable}
     * @param appendable {@link Appendable} to replace the current one with
     * @return appendable
     */
    public final <T extends Appendable> T setAppendable( final T appendable )
    {
        this.appendable = appendable.setParent( this.appendable );
        return appendable;
    }

    /**
     * Replaces the underlying appendable with it's parent.
     *
     * @param <T> type of {@link Appendable}
     * @return the new underlying {@link Appendable}
     */
    public final <T extends Appendable> T restoreParentAppendable()
    {
        if ( appendable != null )
        {
            appendable = appendable.getParent();
        }
        return this.<T>getAppendable();
    }

    /**
     * Convert the content of the writer into a string, then reset it so any
     * new data begins a new string.
     * <p/>
     * <p>
     * This is the equivalent to {@code getAppendable().toString(); getAppendable().reset();}
     * </p>
     *
     * @return String of data in the writer
     */
    public final String getAppendableString()
    {
        final String s = appendable.toString();
        appendable.reset();
        return s;
    }

    /**
     * Convert the content of the writer into a char[], then reset it so any
     * new data begins a new string.
     * <p/>
     * <p>
     * This is the equivalent to {@code getAppendable().toCharArray(); getAppendable().reset();}
     * </p>
     *
     * @return String of data in the writer
     */
    public final char[] getAppendableChars()
    {
        final char[] c = appendable.toCharArray();
        appendable.reset();
        return c;
    }

    /**
     * Append a char to the current {@link Appendable}.
     * <p/>
     * <p>
     * This is the equivalent of {@code getAppendable().append(c);}
     * </p>
     *
     * @param c character to append
     * @return {@link Appendable} to allow chaining
     */
    public final Appendable append( final char c )
    {
        return getAppendable().append( c );
    }
}
