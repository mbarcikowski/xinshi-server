/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax.internal.helper;

import java.util.Arrays;

/**
 * A base {@link Appendable} implementation which stores an extensible char
 * array for storing parsed content.
 * <p/>
 * <p>
 * It has a similar operation to the {@link java.io.CharAppendable} without the
 * added baggage of ThreadSafety, IO etc as these are not necessary
 * </p>
 *
 * @author peter
 * @since 9.10
 */
public class CharAppendable
    implements Appendable
{

    private Appendable parent;

    /**
     * The buffer where data is stored.
     */
    protected char buf[];

    /**
     * The number of chars in the buffer.
     */
    protected int count;

    /**
     * Creates a new CharAppendable.
     */
    public CharAppendable()
    {
        this( null );
    }

    /**
     * Creates a new CharAppendable.
     *
     * @param parent The parent {@link Appendable} or null
     */
    public CharAppendable( final Appendable parent )
    {
        this( null, 32 );
    }

    /**
     * Creates a new CharAppendable with the specified initial size.
     *
     * @param initialSize an int specifying the initial buffer size.
     * @throws IllegalArgumentException if initialSize is negative
     */
    public CharAppendable( final int initialSize )
    {
        this( null, initialSize );
    }

    /**
     * Creates a new CharAppendable with the specified initial size.
     *
     * @param parent      The parent {@link Appendable} or null
     * @param initialSize an int specifying the initial buffer size.
     * @throws IllegalArgumentException if initialSize is negative
     */
    public CharAppendable( final Appendable parent, final int initialSize )
    {
        if ( initialSize < 0 )
        {
            throw new IllegalArgumentException( "Negative initial size: " + initialSize );
        }
        this.parent = parent;
        buf = new char[initialSize];
    }

    /**
     * {@inheritDoc }
     */
    @Override
    @SuppressWarnings("unchecked")
    public final <T extends Appendable> T getParent()
    {
        return (T) parent;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final <T extends Appendable> T setParent( final T parent )
    {
        this.parent = parent;
        return parent;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final CharAppendable append( final char c )
    {
        final int newcount = count + 1;
        if ( newcount > buf.length )
        {
            buf = Arrays.copyOf( buf, Math.max( buf.length << 1, newcount ) );
        }

        buf[count] = c;
        count = newcount;

        return this;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final CharAppendable append( final char[] c, final int off, final int len )
    {
        if ( ( off < 0 ) || ( off > c.length ) || ( len < 0 ) || ( ( off + len ) > c.length ) || ( ( off + len ) < 0 ) )
        {
            throw new IndexOutOfBoundsException();
        }
        else if ( len > 0 )
        {
            final int newcount = count + len;

            if ( newcount > buf.length )
            {
                buf = Arrays.copyOf( buf, Math.max( buf.length << 1, newcount ) );
            }

            System.arraycopy( c, off, buf, count, len );
            count = newcount;
        }

        return this;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final char[] toCharArray()
    {
        return Arrays.copyOf( buf, count );
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final char[] getCharBuffer()
    {
        return buf;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public CharAppendable reset()
    {
        count = 0;
        return this;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final int size()
    {
        return count;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final String toString()
    {
        return String.valueOf( buf, 0, count );
    }
}
