/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax;

import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.ext.LexicalHandler;

/**
 * A SAX style XML Parser that takes its input from a {@link java.nio.ByteBuffer} and
 * passes events to a {@link org.xml.sax.ContentHandler}.
 *
 * @author peter
 * @see NioSaxParserFactory
 * @see org.xml.sax.ContentHandler
 * @see org.xml.sax.ext.LexicalHandler
 * @see uk.org.retep.niosax.NioSaxParserHandler
 * @since 9.10
 */
public interface NioSaxParser
{

    /**
     * The SAX {@link org.xml.sax.ContentHandler} to receive events
     *
     * @return SAX {@link org.xml.sax.ContentHandler} to receive events
     */
    ContentHandler getHandler();

    /**
     * Set the SAX {@link org.xml.sax.ContentHandler} to receive events.
     * <p/>
     * <p>
     * If the handler also implements {@link org.xml.sax.ext.LexicalHandler} then this method
     * will also call {@link #setLexicalHandler(org.xml.sax.ext.LexicalHandler)}.
     * </p>
     * <p/>
     * <p>
     * If the handler also implements {@link NioSaxParserHandler} then this method
     * will also call {@link #setNioSaxParserHandler(uk.org.retep.niosax.NioSaxParserHandler)}.
     * </p>
     *
     * @param handler SAX {@link org.xml.sax.ContentHandler} to receive events
     * @throws NullPointerException if handler is null
     */
    void setHandler( ContentHandler handler );

    /**
     * The SAX {@link org.xml.sax.ext.LexicalHandler} to receive events.
     *
     * @return {@link org.xml.sax.ext.LexicalHandler} or null if not in use.
     */
    LexicalHandler getLexicalHandler();

    /**
     * Set the SAX {@link org.xml.sax.ext.LexicalHandler} to receive events.
     *
     * @param lexicalHandler the SAX {@link org.xml.sax.ext.LexicalHandler} to receive events
     */
    void setLexicalHandler( LexicalHandler lexicalHandler );

    /**
     * The {@link NioSaxParserHandler} to receive events.
     *
     * @return {@link NioSaxParserHandler} or null if not in use.
     */
    NioSaxParserHandler getNioSaxParserHandler();

    /**
     * Set the {@link NioSaxParserHandler} to receive events.
     *
     * @param nioSaxParserHandler {@link NioSaxParserHandler} or null if none.
     */
    void setNioSaxParserHandler( NioSaxParserHandler nioSaxParserHandler );

    /**
     * This must be called by client code before passing any data to this
     * {@link uk.org.retep.niosax.NioSaxParser}. It initialises the parser for a new document and
     * notifies the attached {@link org.xml.sax.ContentHandler}.
     *
     * @throws org.xml.sax.SAXException on failure
     */
    void startDocument()
        throws SAXException;

    /**
     * Client code should call this once the document has been completed. It
     * notifies the attached {@link org.xml.sax.ContentHandler} and cleans up the parser.
     *
     * @throws org.xml.sax.SAXException on failure
     */
    void endDocument()
        throws SAXException;

    /**
     * Parse the content of a {@link java.nio.ByteBuffer} for SAX events
     *
     * @param input {@link java.nio.ByteBuffer} containing data to parse
     * @throws org.xml.sax.SAXException if the content is invalid
     */
    void parse( NioSaxSource input )
        throws SAXException;

}
