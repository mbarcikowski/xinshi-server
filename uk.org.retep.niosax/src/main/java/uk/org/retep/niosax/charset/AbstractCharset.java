/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax.charset;

import java.nio.ByteBuffer;

/**
 * Abstract implementation of {@link Charset} implementing methods that are
 * identical to all {@link Charset} implementations.
 *
 * @author peter
 */
public abstract class AbstractCharset
    implements Charset
{

    /**
     * {@inheritDoc}
     * <p/>
     * <p>
     * Most implementations would not need to override this method as the
     * default implementation returns the same instance.
     * </p>
     */
    @Override
    public Charset getInstance()
    {
        return this;
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>
     * The default implementation will test for {@link #NOT_ENOUGH_DATA}
     * and {@link #INVALID_CHAR} only, returning true for all other values.
     * </p>
     */
    @Override
    public boolean isValid( final char c )
    {
        return c != NOT_ENOUGH_DATA && c != INVALID_CHAR;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean hasCharacter( final ByteBuffer buffer )
    {
        // TODO determine if INVALID_CHAR should be part of the result here
        return isValid( peek( buffer ) );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean hasCharacters( final ByteBuffer buffer, final int count )
    {
        final int pos = buffer.position();
        try
        {
            // Return on the first invalid char found
            for ( int i = 0; i < count; i++ )
            {
                // TODO determine if INVALID_CHAR should be part of the result here
                if ( !isValid( decode( buffer ) ) )
                {
                    return false;
                }
            }

            return true;
        }
        finally
        {
            // Always reset the buffer
            buffer.position( pos );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final char peek( final ByteBuffer buffer )
    {
        final int pos = buffer.position();
        try
        {
            return decode( buffer );
        }
        finally
        {
            // Always reset the buffer
            buffer.position( pos );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final int size( final char[] c )
    {
        return size( c, 0, c.length );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final int size( final char[] c, final int length )
    {
        return size( c, 0, length );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final int size( final char[] c, final int offset, final int length )
    {
        int size = 0;
        for ( int i = offset, j = 0; j < length; j++ )
        {
            size += size( c[i++] );
        }
        return size;
    }

    private boolean skip( final ByteBuffer buffer, final int length )
    {
        if ( buffer.remaining() >= length )
        {
            buffer.position( buffer.position() + length );
            return true;
        }
        {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean skip( final ByteBuffer buffer, final char[] c )
    {
        return skip( buffer, size( c ) );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean skip( final ByteBuffer buffer, final char[] c, final int length )
    {
        return skip( buffer, size( c, length ) );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean skip( final ByteBuffer buffer, final char[] c, final int offset, final int length )
    {
        return skip( buffer, size( c, offset, length ) );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean peek( final ByteBuffer buffer, final char[] c )
    {
        return peek( buffer, c, 0, c.length );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean peek( final ByteBuffer buffer, final char[] c, final int length )
    {
        return peek( buffer, c, 0, length );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean peek( final ByteBuffer buffer, final char[] c, final int offset, final int length )
    {
        final int pos = buffer.position();

        boolean valid = true;
        int p = offset;
        for ( int l = 0; l < length && valid; l++ )
        {
            final char d = decode( buffer );
            if ( d < 0 )
            {
                // not enough data
                valid = false;
                break;
            }
            c[p++] = d;
            l--;
        }

        // Always reset the buffer
        buffer.position( pos );
        return valid;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean read( final ByteBuffer buffer, final char[] c )
    {
        return read( buffer, c, 0, c.length );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean read( final ByteBuffer buffer, final char[] c, final int length )
    {
        return read( buffer, c, 0, length );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean read( final ByteBuffer buffer, final char[] c, final int offset, final int length )
    {
        final int pos = buffer.position();

        int p = offset;
        for ( int l = 0; l < length; l++ )
        {
            final char d = decode( buffer );
            if ( d < 0 )
            {
                // not enough data so restore the buffer and bomb out
                buffer.position( pos );
                return false;
            }
            c[p++] = d;
            l--;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final int write( final ByteBuffer buffer, final char[] c )
    {
        return write( buffer, c, 0, c.length );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final int write( final ByteBuffer buffer, final char[] c, int length )
    {
        return write( buffer, c, 0, length );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final int write( final ByteBuffer buffer, final char[] c, final int offset, final int length )
    {
        int s = 0;
        for ( int i = 0; i < length; i++ )
        {
            if ( encode( buffer, c[offset + i] ) )
            {
                s++;
            }
            else
            {
                break;
            }
        }
        return s;
    }
}
