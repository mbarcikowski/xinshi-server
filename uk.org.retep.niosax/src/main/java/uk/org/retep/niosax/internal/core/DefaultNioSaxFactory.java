/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax.internal.core;

import uk.org.retep.niosax.NioSaxParser;
import uk.org.retep.niosax.NioSaxParserFactory;

/**
 * {@link NioSaxParserFactory} implementation which can create instances of
 * {@link NioSaxParser} using the default implementation.
 * <p/>
 * <p>
 * In normal operation you should use the {@link NioSaxParserFactory#getInstance() }
 * method to obtain a reference to this factory.
 * </p>
 *
 * @author peter
 * @see NioSaxParserFactory#getInstance()
 * @since 9.10
 */
public class DefaultNioSaxFactory
    extends NioSaxParserFactory
{

    @Override
    public NioSaxParser newInstance()
    {
        return new DefaultNioSaxParser();
    }
}
