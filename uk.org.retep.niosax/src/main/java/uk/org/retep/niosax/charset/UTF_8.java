/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax.charset;

import java.nio.ByteBuffer;

/**
 * Our own implementation of the UTF-8 charset. Unlike
 * {@link java.nio.charset.Charset} this does not support UCS characters, just
 * 16 bit Unicode characters.
 * <p/>
 * # Bits   Bit pattern
 * 1    7   0xxxxxxx
 * 2   11   110xxxxx 10xxxxxx
 * 3   16   1110xxxx 10xxxxxx 10xxxxxx
 * 4   21   11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
 * 5   26   111110xx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx
 * 6   31   1111110x 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx
 * <p/>
 * UCS-2 uses 1-3, UTF-16 uses 1-4, UCS-4 uses 1-6
 *
 * @author peter
 */
@Encoding(
    { "UTF-8", "UTF_8", "UTF8", // JDK historical
        "unicode-1-1-utf-8" })
public class UTF_8
    extends AbstractCharset
{

    /**
     * Is the character a continuation char
     *
     * @param b inbound byte
     * @return true if this is a continuation byte
     */
    public final boolean isContinuation( final int b )
    {
        return ( ( b & 0xc0 ) == 0x80 );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public char decode( final ByteBuffer buffer )
    {
        if ( !buffer.hasRemaining() )
        {
            return NOT_ENOUGH_DATA;
        }

        final int pos = buffer.position();

        int b1 = buffer.get();
        switch ( ( b1 >> 4 ) & 0x0f )
        {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                // 1 byte, 7 bits: 0xxxxxxx
                return (char) ( b1 & 0x7f );

            case 12:
            case 13:
                // 2 bytes, 11 bits: 110xxxxx 10xxxxxx
                if ( buffer.hasRemaining() )
                {
                    int b2 = buffer.get();
                    if ( isContinuation( b2 ) )
                    {
                        return (char) ( ( ( b1 & 0x1f ) << 6 ) | ( ( b2 & 0x3f ) << 0 ) );
                    }
                }
                break;

            case 14:
                // 3 bytes, 16 bits: 1110xxxx 10xxxxxx 10xxxxxx
                if ( buffer.remaining() > 2 )
                {
                    int b2 = buffer.get();
                    int b3 = buffer.get();
                    if ( isContinuation( b2 ) && isContinuation( b3 ) )
                    {
                        return (char) ( ( ( b1 & 0x0f ) << 12 ) | ( ( b2 & 0x3f ) << 06 ) | ( ( b3 & 0x3f ) << 0 ) );
                    }
                }
                break;

            default:
                return INVALID_CHAR;
        }

        // Not enough data so reset the buffer and return
        buffer.position( pos );
        return NOT_ENOUGH_DATA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean encode( final ByteBuffer buffer, final char c )
    {
        if ( c < 0x80 )
        {
            // Have at most seven bits
            if ( buffer.hasRemaining() )
            {
                buffer.put( (byte) c );
                return true;
            }
        } // should do a Surrogate check here
        else if ( c < 0x800 )
        {
            // 2 bytes, 11 bits
            if ( buffer.remaining() > 1 )
            {
                buffer.put( (byte) ( 0xc0 | ( ( c >> 06 ) ) ) );
                buffer.put( (byte) ( 0x80 | ( ( c >> 00 ) & 0x3f ) ) );
                return true;
            }
        }
        else if ( c <= '\uFFFF' )
        {
            // 3 bytes, 16 bits
            if ( buffer.remaining() > 2 )
            {
                buffer.put( (byte) ( 0xe0 | ( ( c >> 12 ) ) ) );
                buffer.put( (byte) ( 0x80 | ( ( c >> 06 ) & 0x3f ) ) );
                buffer.put( (byte) ( 0x80 | ( ( c >> 00 ) & 0x3f ) ) );

            }
        }

        // Not enough room or unsupported char
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size( final char c )
    {
        if ( c < 0x80 )
        {
            // Have at most seven bits
            return 1;
        } // should do a Surrogate check here
        else if ( c < 0x800 )
        {
            // 2 bytes, 11 bits
            return 2;
        }
        else if ( c <= '\uFFFF' )
        {
            // 3 bytes, 16 bits
            return 3;
        }

        // Illegal char, should do something correct here
        return 0;
    }
}
