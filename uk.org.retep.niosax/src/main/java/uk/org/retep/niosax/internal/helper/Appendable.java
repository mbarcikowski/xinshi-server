/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax.internal.helper;

/**
 * A class which stores an extensible char array for storing parsed content.
 *
 * @author peter
 * @since 9.10
 */
public interface Appendable
{

    /**
     * Appends the specified character to this writer.
     *
     * @param c character to append
     * @return this to allow method chaining
     */
    Appendable append( final char c );

    /**
     * Writes characters to the buffer.
     *
     * @param c   the data to be written
     * @param off the start offset in the data
     * @param len the number of chars that are written
     * @return this to allow method chaining
     */
    Appendable append( final char[] c, final int off, final int len );

    /**
     * Returns a copy of the input data.
     *
     * @return an array of chars copied from the input data.
     * @see #getCharBuffer()
     */
    char[] toCharArray();

    /**
     * Returns the underlying char array. This method is used for performance
     * purposes where the array is only needed momentarily. The array may be
     * larger than the size of the Appendable.
     * <p/>
     * <p>
     * Be warned, the char array may be modified by further updates to the
     * Appendable, so should be used only where a copy is made and
     * {@link #toCharArray() } would be expensive in making an unwanted copy
     * of data thats just thrown away.
     * </p>
     *
     * @return underlying volatile char array
     * @see #toCharArray()
     */
    char[] getCharBuffer();

    /**
     * Resets the buffer so that you can use it again without
     * throwing away the already allocated buffer.
     *
     * @return this to allow method chaining
     */
    Appendable reset();

    /**
     * Returns the current size of the buffer.
     *
     * @return an int representing the current size of the buffer.
     */
    int size();

    /**
     * Converts input data to a string.
     *
     * @return the string.
     */
    @Override
    String toString();

    /**
     * Returns the parent {@link uk.org.retep.niosax.internal.helper.Appendable} instance or null if the root.
     * This allows for Appendable's to be temporarily replaced by another one
     * during parsing.
     *
     * @param <T> type of {@link uk.org.retep.niosax.internal.helper.Appendable}
     * @return the parent {@link uk.org.retep.niosax.internal.helper.Appendable}
     */
    <T extends Appendable> T getParent();

    /**
     * Sets the parent {@link uk.org.retep.niosax.internal.helper.Appendable} instance or null if the root.
     *
     * @param <T>    type of {@link uk.org.retep.niosax.internal.helper.Appendable}
     * @param parent {@link uk.org.retep.niosax.internal.helper.Appendable}
     * @return parent to allow method chaining
     */
    <T extends Appendable> Appendable setParent( T parent );
}
