/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax.internal.core.delegate;

import org.xml.sax.SAXException;
import uk.org.retep.niosax.IllegalCharacterException;
import uk.org.retep.niosax.NioSaxSource;
import uk.org.retep.niosax.internal.core.AbstractNioSaxParser;
import uk.org.retep.niosax.internal.core.ParserDelegate;
import uk.org.retep.niosax.internal.core.Prolog;
import uk.org.retep.niosax.internal.core.engine.CommentCDataStateEngine;

import static uk.org.retep.niosax.internal.helper.XmlSpec.isWhitespace;

/**
 * {@link ParserDelegate} implementation for parsing the document's prolog.
 *
 * @author peter
 * @since 9.10
 */
public class DefaultProlog
    extends Prolog
{

    private State state;

    /**
     * Creates a new instance of {@link uk.org.retep.niosax.internal.core.delegate.DefaultProlog}
     *
     * @param parser The containing parser
     * @return instance
     */
    public static Prolog delegate( final AbstractNioSaxParser parser )
    {
        return new DefaultProlog( parser );
    }

    private DefaultProlog( final AbstractNioSaxParser parser )
    {
        super( parser );
        state = State.PROLOG;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void parse( final NioSaxSource source )
        throws SAXException
    {
        final char c = source.decode();
        if ( source.isValid( c ) )
        {
            state = state.parse( this, source, c );
        }
    }

    /**
     * State engine to handle the prolog.
     * <p/>
     * This is not a {@link StateEngine} because we dont extend {@link StateEngineDelegate}
     */
    private enum State
    {

        /**
         * Running within the prolog, followed by ELEMENT only.
         */
        PROLOG
            {
                @Override
                public State parse( final DefaultProlog p, final NioSaxSource s, final char c )
                    throws SAXException
                {
                    if ( c == '<' )
                    {
                        return ELEMENT;
                    }
                    else if ( isWhitespace( c ) )
                    {
                        // Ignore
                        return PROLOG;
                    }
                    else
                    {
                        throw new IllegalCharacterException();
                    }
                }
            },
        /**
         * Start of an element, comment or processing instruction.
         * Followed by PROLOG or COMMENT1
         */
        ELEMENT
            {
                @Override
                public State parse( final DefaultProlog p, final NioSaxSource s, final char c )
                    throws SAXException
                {
                    if ( c == '!' )
                    {
                        // A comment or cdata
                        CommentCDataStateEngine.delegate( p );
                    }
                    else if ( c == '?' )
                    {
                        // processing instruction
                        ProcessingInstruction.delegate( p );
                    }
                    else
                    {
                        // Start a root element, forwarding the char as it's the
                        // first char of the element
                        Element.delegate( p ).parse( s, c );
                    }

                    // Back to prolog
                    return PROLOG;
                }
            };

        /**
         * Called by {@link uk.org.retep.niosax.internal.core.delegate.DefaultProlog}
         *
         * @param p {@link uk.org.retep.niosax.internal.core.delegate.DefaultProlog}
         * @param c char to parse
         * @return The next state
         * @throws org.xml.sax.SAXException if c is invalid
         */
        public abstract State parse( final DefaultProlog p, final NioSaxSource s, final char c )
            throws SAXException;
    }
}
