/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax;

import org.xml.sax.SAXException;

/**
 * {@link org.xml.sax.SAXException} thrown by {@link NioSaxParser} when an illegal character
 * has been found in the xml.
 *
 * @author peter
 * @since 9.10
 */
public class IllegalCharacterException
    extends SAXException
{

    private static final String msg = "Illegal character";

    public IllegalCharacterException()
    {
        super( msg );
    }

    public IllegalCharacterException( final Exception e )
    {
        super( msg, e );
    }

    public IllegalCharacterException( final char c )
    {
        super( msg + " " + c );
    }

    public IllegalCharacterException( final char c, final Exception e )
    {
        super( msg + " " + c, e );
    }
}
