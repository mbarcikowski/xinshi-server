/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax.internal.core;

import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.ext.LexicalHandler;
import uk.org.retep.niosax.NioSaxParserHandler;
import uk.org.retep.niosax.NioSaxSource;

/**
 * Abstract class for the individual parsers
 *
 * @param <P> The parent AbstractParserDelegate type
 * @author peter
 * @since 9.10
 */
public abstract class ParserDelegate<P extends ParserDelegate>
{

    protected final AbstractNioSaxParser parser;

    /**
     * @param parser The parser to attach to
     */
    public ParserDelegate( final AbstractNioSaxParser parser )
    {
        this.parser = parser;
        parser.setParserState( this );
    }

    /**
     * Hook called by the parser when the parsers {@link AbstractNioSaxParser#finish()}
     * method is invoked. Subclasses should use this method to release resources
     * and must call {@code super.cleanup()} at the end of their call.
     */
    public void cleanup()
    {
    }

    /**
     * The parent {@link uk.org.retep.niosax.internal.core.ParserDelegate} to this one.
     *
     * @return Parent or null if this is the root. i.e. {@link Prolog}.
     */
    public abstract P getParent();

    /**
     * The {@link AbstractNioSaxParser} this {@link AbstractParserDelegate} is
     * attached to.
     *
     * @return {@link AbstractNioSaxParser} this {@link AbstractParserDelegate}
     *         is attached to.
     */
    public final AbstractNioSaxParser getParser()
    {
        return parser;
    }

    /**
     * Finish this state and return to the parent.
     * Equivalent to {@code getParser().finish()}.
     * <p/>
     * <p>
     * Normally this is used in a subclass as {@code return finish();} so that
     * the main loop in {@link #parse(uk.org.retep.niosax.NioSaxSource)} will terminate.
     * </p>
     *
     * @return false
     */
    public final boolean finish()
    {
        parser.finish();
        return false;
    }

    /**
     * Convenience method, identical to {@code getParser().getHandler()}.
     * <p/>
     * <p>
     * You must not cache the {@link org.xml.sax.ContentHandler} outside of the scope of
     * the method it is used in as it can change during the lifetime of the document being parsed.
     * </p>
     *
     * @return {@link org.xml.sax.ContentHandler} currently in use
     */
    public final ContentHandler getHandler()
    {
        return parser.getHandler();
    }

    /**
     * Convenience method, identical to {@code getParser().getLexicalHandler()}.
     * <p/>
     * <p>
     * You must not cache the {@link org.xml.sax.ext.LexicalHandler} outside of the scope of
     * the method it is used in as it can change during the lifetime of the document being parsed.
     * </p>
     *
     * @return {@link org.xml.sax.ext.LexicalHandler} or null if the handler does not implement
     *         the {@link org.xml.sax.ext.LexicalHandler} interface.
     */
    public final LexicalHandler getLexicalHandler()
    {
        return parser.getLexicalHandler();
    }

    /**
     * Convenience method, identical to {@code getParser().getNioSaxParserHandler()}.
     * <p/>
     * <p>
     * You must not cache the {@link NioSaxParserHandler} outside of the scope of
     * the method it is used in as it can change during the lifetime of the document being parsed.
     * </p>
     *
     * @return {@link NioSaxParserHandler} or null if the handler does not implement
     *         the {@link NioSaxParserHandler} interface.
     */
    public final NioSaxParserHandler getNioSaxParserHandler()
    {
        return parser.getNioSaxParserHandler();
    }

    /**
     * Called by {@link AbstractNioSaxParser} to parse the input.
     *
     * @param source {@link NioSaxSource}
     * @throws org.xml.sax.SAXException if the parse fails
     */
    public abstract void parse( final NioSaxSource source )
        throws SAXException;

}
