/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax.internal.core;

import org.xml.sax.SAXException;
import uk.org.retep.niosax.NioSaxSource;

/**
 * A state engine based parser used by {@link StateEngineDelegate}
 * implementations to handle complex parsing.
 *
 * @param <T> The implementing AbstractParserDelegate type
 */
public interface StateEngine<T extends StateEngineDelegate>
{

    /**
     * parse a character
     *
     * @param instance The containing instance calling the engine
     * @param s        {@link NioSaxSource} containing the input
     * @param c        character to parse
     * @return The state to use after this call
     * @throws org.xml.sax.SAXException if the character is illegal for this state
     */
    StateEngine parse( T instance, NioSaxSource s, char c )
        throws SAXException;

    /**
     * Should the parser continue looping whilst in this state?
     * <p/>
     * <p>
     * The {@link ParserDelegate} will loop whilst this method returns true
     * so when the state engine indicates it's finished it should switch to
     * a state where this returns false, then the loop will terminate.
     * </p>
     *
     * @return true to continue, false to exit
     */
    boolean continueLoop();
}
