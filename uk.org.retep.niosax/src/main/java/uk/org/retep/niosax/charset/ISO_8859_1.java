/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax.charset;

import java.nio.ByteBuffer;

/**
 * Our own implementation of the US_ASCII charset.
 *
 * @author peter
 */
@Encoding(
    { "ISO-8859-1",
        // IANA aliases
        "iso-ir-100", "ISO_8859-1", "latin1", "l1", "IBM819", "cp819", "csISOLatin1",
        // Other aliases
        "819", "IBM-819", "ISO8859_1", "ISO_8859-1:1987", "ISO_8859_1", "8859_1", "ISO8859-1" })
public class ISO_8859_1
    extends AbstractCharset
{

    /**
     * {@inheritDoc}
     */
    @Override
    public char decode( final ByteBuffer buffer )
    {
        if ( buffer.hasRemaining() )
        {
            return (char) buffer.get();
        }
        else
        {
            return NOT_ENOUGH_DATA;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean encode( final ByteBuffer buffer, final char c )
    {
        if ( buffer.hasRemaining() )
        {
            buffer.put( (byte) ( c < 0xff ? c : ' ' ) );
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size( char c )
    {
        return 1;
    }
}
