/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax.internal.core.engine;

import org.xml.sax.SAXException;
import org.xml.sax.ext.LexicalHandler;
import uk.org.retep.niosax.NioSaxSource;
import uk.org.retep.niosax.internal.core.StateEngine;
import uk.org.retep.niosax.internal.core.StateEngineDelegate;
import uk.org.retep.niosax.internal.helper.Appendable;

import static uk.org.retep.niosax.internal.helper.XmlSpec.stateCompleted;

/**
 * A {@link StateEngine} implementation which parses a CData section.
 *
 * @author peter
 * @since 9.10
 */
public enum CDataStateEngine
    implements StateEngine<StateEngineDelegate>
{

    /**
     * The entry point. On the first char, call {@link org.xml.sax.ext.LexicalHandler#startCDATA()}
     * append the caracter and then switch to {@link #NORMAL}
     */
    START
        {
            @Override
            public StateEngine parse( final StateEngineDelegate e, final NioSaxSource source, final char c )
                throws SAXException
            {
                // Notify the start of the CDATA section
                final LexicalHandler lh = e.getLexicalHandler();
                if ( lh != null )
                {
                    lh.startCDATA();
                }

                return NORMAL.parse( e, source, c );
            }
        },
    /**
     * If the char is ']' switch to {@link #END1} otherwise append to the buffer.
     */
    NORMAL
        {
            @Override
            public StateEngine parse( final StateEngineDelegate e, final NioSaxSource source, final char c )
                throws SAXException
            {
                if ( c == ']' )
                {
                    return END1;
                }
                else
                {
                    e.append( c );
                    return this;
                }
            }
        },
    /**
     * State when the first ']' is found. If the next char is also ']' then
     * switch to {@link #END1} otherwise append the previous ']' and the char
     * to the buffer and switch to {@link #NORMAL}
     */
    END1
        {
            @Override
            public StateEngine parse( final StateEngineDelegate e, final NioSaxSource source, final char c )
                throws SAXException
            {
                if ( c == ']' )
                {
                    return END2;
                }
                else
                {
                    // Push back the preceding ']' and return back to NORMAL.
                    e.append( ']' ).append( c );
                    return NORMAL;
                }
            }
        },
    /**
     * State when two ']' have been found.
     * <p/>
     * <p>
     * If the next char is '&gt;' then we call
     * {@link org.xml.sax.ContentHandler#characters(char[], int, int) }
     * with the buffer content, call {@link org.xml.sax.ext.LexicalHandler#endCDATA() } to
     * notify the end of the CDATA section, tell the {@link StateEngineDelegate}
     * to finish and return {@link uk.org.retep.niosax.internal.helper.XmlSpec#stateCompleted()}.
     * </p>
     * <p/>
     * <p>
     * If the next char is not '&gt;' then we append the previous two ']'
     * characters and switch back to {@link #NORMAL}
     * </p>
     */
    END2
        {
            @Override
            public StateEngine parse( final StateEngineDelegate e, final NioSaxSource source, final char c )
                throws SAXException
            {
                if ( c == '>' )
                {
                    // Notify the handler of the characters
                    final Appendable a = e.getAppendable();
                    e.getHandler().characters( a.getCharBuffer(), 0, a.size() );

                    // Notify the end of the CDATA section
                    final LexicalHandler lh = e.getLexicalHandler();
                    if ( lh != null )
                    {
                        lh.endCDATA();
                    }

                    e.finish();

                    return stateCompleted();
                }
                else
                {
                    // Push back the preceding ']]' and return back to NORMAL.
                    e.append( ']' ).append( ']' ).append( c );
                    return NORMAL;
                }
            }
        };

    /**
     * {@inheritDoc }
     */
    @Override
    public final boolean continueLoop()
    {
        return true;
    }
}
