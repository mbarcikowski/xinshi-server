/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax.charset;

import java.nio.ByteBuffer;

/**
 * Base implementation of UTF16 Charsets
 *
 * @author peter
 */
public abstract class AbstractUTF_16
    extends AbstractCharset
{

    /**
     * Byte order mark used to indicate a BIG endian stream
     */
    public static final char BYTE_ORDER_MARK = (char) 0xfeff;

    /**
     * Byte order mark used to indicate a LITTLE endian stream
     */
    public static final char REVERSED_MARK = (char) 0xfffe;

    /**
     * {@inheritDoc}
     */
    @Override
    public abstract Charset getInstance();

    /**
     * Decode a Big endian utf16 character
     *
     * @param b1 byte1 from stream
     * @param b2 byte2 from stream
     * @return decoded char
     */
    protected final char decodeBig( final int b1, final int b2 )
    {
        return (char) ( ( b1 << 8 ) | b2 );
    }

    /**
     * Decode a Little endian utf16 character
     *
     * @param b1 byte1 from stream
     * @param b2 byte2 from stream
     * @return decoded char
     */
    protected final char decodeLittle( final int b1, final int b2 )
    {
        return (char) ( ( b2 << 8 ) | b1 );
    }

    /**
     * Encode the character into the specified {@link java.nio.ByteBuffer} at the current
     * position using big endian byte ordering.
     * <p/>
     * <p>
     * The buffer's position will be incremented accordingly.
     * </p>
     *
     * @param buffer {@link java.nio.ByteBuffer} to append to.
     * @param c      char to append
     * @return true if the append succeded, false if the buffer does not have
     *         enough capacity to hold this character.
     */
    protected final boolean encodeBig( final ByteBuffer buffer, final char c )
    {
        if ( buffer.remaining() < 2 )
        {
            return false;
        }
        else
        {
            buffer.put( (byte) ( c >> 8 ) );
            buffer.put( (byte) ( c & 0xff ) );
            return true;
        }
    }

    /**
     * Encode the character into the specified {@link java.nio.ByteBuffer} at the current
     * position using little endian byte ordering.
     * <p/>
     * <p>
     * The buffer's position will be incremented accordingly.
     * </p>
     *
     * @param buffer {@link java.nio.ByteBuffer} to append to.
     * @param c      char to append
     * @return true if the append succeded, false if the buffer does not have
     *         enough capacity to hold this character.
     */
    protected final boolean encodeLittle( final ByteBuffer buffer, final char c )
    {
        if ( buffer.remaining() < 2 )
        {
            return false;
        }
        else
        {
            buffer.put( (byte) ( c & 0xff ) );
            buffer.put( (byte) ( c >> 8 ) );
            return true;
        }
    }

    @Override
    public int size( char c )
    {
        return 2;
    }
}
