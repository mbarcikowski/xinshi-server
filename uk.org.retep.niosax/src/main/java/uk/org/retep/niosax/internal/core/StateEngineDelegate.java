/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax.internal.core;

import org.xml.sax.SAXException;
import uk.org.retep.niosax.NioSaxSource;

/**
 * Abstract class for {@link ParserDelegate}'s that have a parent and which use
 * a state engine for managing parsing.
 * <p/>
 * <p>
 * The state engine is usually an enum which implements this interface, so a
 * minimum implementation of this class would be:
 * </p>
 * <p/>
 * <code><pre>
 *  // StateEngineDelegate that captures all text until '*' is found
 *  public class MyParser extends StateEngineDelegate&lt;ParentParser&gt; {
 *      public MyParser( ParentParser parent ) {
 *          super(parent);
 *      }
 * <p/>
 *      // The state to use when this instance starts for the first time
 *      protected {@linkplain StateEngine StateEngine} getInitialState() {
 *          return MyState.INITIAL;
 *      }
 * <p/>
 *      private enum MyState implements {@linkplain StateEngine StateEngine}&lt;MyParser&gt; {
 * <p/>
 *          INITIAL {
 *              public {@linkplain StateEngine StateEngine} parse( MyParser instance, {@linkplain uk.org.retep.niosax.internal.helper.Appendable uk.org.retep.niosax.internal.helper.Appendable} w, char c )
 *                  throws SAXException
 *              {
 *                  if( c=='*' ) {
 * <p/>
 *                      // get the captured data
 *                      String s = instance.{@linkplain #getAppendableString() getAppendableString}();
 *                      // do something with it
 * <p/>
 *                      // change to the COMPLETE state
 *                      return COMPLETE;
 *                  } else {
 *                      // Append to the buffer
 *                      w.append(c);
 * <p/>
 *                      // Stay in this state
 *                      return this;
 *                  }
 *              }
 *          },
 * <p/>
 *          COMPLETE {
 *              // This breaks the parsing loop
 *              public boolean continueLoop() {
 *                  return false;
 *              }
 *          };
 * <p/>
 *          public {@linkplain StateEngine StateEngine} parse( MyParser instance, {@linkplain uk.org.retep.niosax.internal.helper.Appendable uk.org.retep.niosax.internal.helper.Appendable} w, char c )
 *              throws SAXException
 *          {
 *              throw new UnsupportedOperationException();
 *          }
 * <p/>
 *          public boolean continueLoop() {
 *              return true;
 *          }
 *      }
 *  }
 * </pre></code>
 *
 * @param <P> The parent {@link ParserDelegate} type
 * @author peter
 * @since 9.10
 */
public abstract class StateEngineDelegate<P extends ParserDelegate>
    extends AppendableParserDelegate<P>
{

    private StateEngine state;

    /**
     * Returns a {@link uk.org.retep.niosax.internal.core.StateEngineDelegate} which utilises a standalone
     * {@link StateEngine} implementation that does nothing with parsed content.
     * <p/>
     * <p>
     * This is usually used for specific {@link StateEngine} implementation's
     * which parse content to determine one or more {@link ParserDelegate}'s
     * should parse content.
     * </p>
     * <p/>
     * <p>
     * For example, comments start with &lt;!-- and CDATA with &lt;[CDATA[
     * so there's a unique state engine which uses this method to delegate
     * parsing, and delegates control depending if -- or CDATA[ is found.
     * </p>
     *
     * @param <P>          The parent {@link ParserDelegate} type
     * @param parent       the parent {@link ParserDelegate}
     * @param initialState The initial state to use
     * @return {@link uk.org.retep.niosax.internal.core.StateEngineDelegate} instance
     */
    public static <P extends ParserDelegate> StateEngineDelegate delegate( final P parent,
                                                                           final StateEngine<StateEngineDelegate> initialState )
    {
        return new StateEngineDelegate<P>( parent )
        {

            @Override
            protected StateEngine getInitialState()
            {
                return initialState;
            }
        };
    }

    /**
     * Constructor used by all {@link uk.org.retep.niosax.internal.core.StateEngineDelegate} implementations.
     *
     * @param parent The parent {@link ParserDelegate}
     */
    public StateEngineDelegate( final P parent )
    {
        super( parent );
        state = getInitialState();
    }

    /**
     * The initial state for this instance
     *
     * @return initial State value
     */
    protected abstract StateEngine getInitialState();

    /**
     * Parses content by using the current {@link StateEngine}. This method will loop
     * until the {@link StateEngine#continueLoop()} returns false or no more content
     * is available in the buffer.
     * <p/>
     * {@inheritDoc }
     */
    @Override
    @SuppressWarnings("unchecked")
    public final boolean parse( final NioSaxSource source, final char c )
        throws SAXException
    {
        state = state.parse( this, source, c );
        return state.continueLoop();
    }

    /**
     * The current {@link StateEngine}
     *
     * @return current {@link StateEngine}
     */
    protected final StateEngine getState()
    {
        return state;
    }
}
