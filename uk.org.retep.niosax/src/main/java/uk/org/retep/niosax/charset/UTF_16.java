/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax.charset;

import java.nio.ByteBuffer;

/**
 * Our own implementation of the UTF-16 charset. This simply delegages the
 * encoding to {@link java.nio.ByteBuffer#getChar()} and {@link java.nio.ByteBuffer#putChar(char)}
 *
 * @author peter
 */
@Encoding(
    { "UTF-16", "UTF_16", // JDK historical
        "utf16", "unicode", "UnicodeBig" })
public class UTF_16
    extends AbstractUTF_16
{

    private Endian endian = Endian.NONE;

    private boolean requiresBOM;

    public boolean isRequiresBOM()
    {
        return requiresBOM;
    }

    public void setRequiresBOM( final boolean requiresBOM )
    {
        if ( endian == Endian.NONE )
        {
            this.requiresBOM = requiresBOM;
        }
        else
        {
            throw new IllegalStateException( "Cannot set requiresDOM once the stream has been used" );
        }
    }

    public static enum Endian
    {

        /**
         * No endian, assume little endian until told otherwise
         */
        NONE,
        /**
         * Big endian, so order is b[0],b[1]
         */
        BIG,
        /**
         * Little endian, so order is b[1],b[0]
         */
        LITTLE
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Charset getInstance()
    {
        return new UTF_16();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public char decode( final ByteBuffer buffer )
    {
        if ( buffer.remaining() < 2 )
        {
            return NOT_ENOUGH_DATA;
        }
        else if ( endian == Endian.BIG )
        {
            return decodeBig( buffer.get(), buffer.get() );
        }
        else
        {
            return decodeLittle( buffer.get(), buffer.get() );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean encode( final ByteBuffer buffer, final char c )
    {
        if ( endian == Endian.BIG )
        {
            return encodeBig( buffer, c );
        }
        else if ( endian == Endian.LITTLE )
        {
            return encodeLittle( buffer, c );
        }
        else
        {
            // Check for a BOM... Note BOM's are always encoded BIG_ENDIAN in
            // the stream
            if ( c == REVERSED_MARK )
            {
                // Switch into Little endian mode but record the BOM in Big endian...
                endian = Endian.LITTLE;
            }
            else if ( c == BYTE_ORDER_MARK )
            {
                // Switch into Big endian mode
                endian = Endian.BIG;
            }
            else
            {
                // The default is to run in BigEndian so switch and encode
                // however if a BOM is required then try to send that first
                // Then switch.
                if ( requiresBOM && !encodeBig( buffer, BYTE_ORDER_MARK ) )
                {
                    return false;
                }
            }
            endian = Endian.BIG;
        }

        return encodeBig( buffer, c );
    }
}
