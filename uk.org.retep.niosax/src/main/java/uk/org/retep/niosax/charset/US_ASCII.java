/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax.charset;

import java.nio.ByteBuffer;

/**
 * Our own implementation of the US_ASCII charset.
 *
 * @author peter
 */
@Encoding(
    { "US-ASCII", "US_ASCII", // JDK Historical?
        // IANA aliases
        "iso-ir-6", "ANSI_X3.4-1986", "ISO_646.irv:1991", "ASCII", "ISO646-US", "us", "IBM367", "cp367", "csASCII",
        "default",
        // Other aliases
        "646", // Solaris POSIX locale
        "iso_646.irv:1983", "ANSI_X3.4-1968", // Linux POSIX locale (RedHat)
        "ascii7" })
public class US_ASCII
    extends AbstractCharset
{

    /**
     * {@inheritDoc}
     */
    @Override
    public char decode( final ByteBuffer buffer )
    {
        if ( buffer.hasRemaining() )
        {
            final byte b = buffer.get();
            if ( b <= 127 )
            {
                return (char) b;
            }
            else
            {
                return INVALID_CHAR;
            }
        }
        else
        {
            return NOT_ENOUGH_DATA;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean encode( final ByteBuffer buffer, final char c )
    {
        if ( buffer.hasRemaining() )
        {
            buffer.put( (byte) ( c < 0x80 ? c : ' ' ) );
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size( char c )
    {
        return 1;
    }
}
