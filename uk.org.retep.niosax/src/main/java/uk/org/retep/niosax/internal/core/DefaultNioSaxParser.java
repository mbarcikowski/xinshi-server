/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.retep.niosax.internal.core;

import org.xml.sax.SAXException;
import uk.org.retep.niosax.NioSaxSource;
import uk.org.retep.niosax.internal.core.delegate.DefaultProlog;

/**
 * A {@link uk.org.retep.niosax.NioSaxParser} implementation that uses the
 * {@link uk.org.retep.niosax.internal.core.AbstractParserDelegate } API to delegate
 * the actual parsing.
 *
 * @author peter
 * @since 9.10
 */
public class DefaultNioSaxParser
    extends AbstractNioSaxParser
{

    DefaultNioSaxParser()
    {
    }

    @Override
    public Prolog delegateProlog()
        throws SAXException
    {
        return DefaultProlog.delegate( this );
    }

    @Override
    public void parse( final NioSaxSource source )
        throws SAXException
    {
        // Loop until we run out of available data
        while ( source.hasCharacter() )
        {
            getParserState().parse( source );
        }
    }
}
