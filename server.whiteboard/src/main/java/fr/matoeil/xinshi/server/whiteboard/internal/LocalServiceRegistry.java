/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.server.whiteboard.internal;

import fr.matoeil.xinshi.server.api.Router;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static com.google.common.base.Preconditions.checkState;

public class LocalServiceRegistry
    implements RouterListener
{

    private static final Logger LOGGER = LoggerFactory.getLogger( LocalServiceRegistry.class );

    private static final String DIFFERENT_ROUTERS_MESSAGE =
        "Unavailable router [{}] is not equal with prior available router [{}]";

    private static final String ERROR_DURING_REGISTRATION_MESSAGE =
        "Registration skipped for [%s] due to error during registration";

    private final List<ServiceRegistration> serviceRegistrations_;

    private final ReadWriteLock serviceRegistrationsLock_;

    private final ReadWriteLock routerLock_;

    private Router router_;

    public LocalServiceRegistry()
    {
        serviceRegistrations_ = new ArrayList<>();
        routerLock_ = new ReentrantReadWriteLock();
        serviceRegistrationsLock_ = new ReentrantReadWriteLock();
    }

    public void addServiceRegistration( ServiceRegistration aServiceRegistration )
    {
        serviceRegistrationsLock_.writeLock().lock();
        try
        {
            serviceRegistrations_.add( aServiceRegistration );
        }
        finally
        {
            serviceRegistrationsLock_.writeLock().unlock();
        }
        routerLock_.readLock().lock();
        try
        {
            registerServiceRegistration( aServiceRegistration );
        }
        finally
        {
            routerLock_.readLock().unlock();
        }
    }

    public void removeServiceRegistration( ServiceRegistration aServiceRegistration )
    {
        serviceRegistrationsLock_.writeLock().lock();
        try
        {
            serviceRegistrations_.remove( aServiceRegistration );
        }
        finally
        {
            serviceRegistrationsLock_.writeLock().unlock();
        }
        routerLock_.readLock().lock();
        try
        {
            unregisterServiceRegistration( aServiceRegistration );
        }
        finally
        {
            routerLock_.readLock().unlock();
        }
    }

    @Override
    public void available( Router aRouter )
    {
        routerLock_.writeLock().lock();
        try
        {
            if ( router_ != null )
            {
                unavailable( router_ );
            }
            router_ = aRouter;
            registerServiceRegistrations();
        }
        finally
        {
            routerLock_.writeLock().unlock();
        }
    }

    @Override
    public void unavailable( Router aRouter )
    {
        routerLock_.writeLock().lock();
        try
        {
            checkState( aRouter == router_, DIFFERENT_ROUTERS_MESSAGE, aRouter, router_ );
            if ( aRouter != router_ )
            {
                throw new IllegalStateException(

                );
            }
            unregisterServiceRegistrations();
            router_ = null;
        }
        finally
        {
            routerLock_.writeLock().unlock();
        }
    }

    private void registerServiceRegistration( final ServiceRegistration aServiceRegistration )
    {
        try
        {
            if ( router_ != null )
            {
                aServiceRegistration.register( router_ );
            }
        }
        catch ( Exception e )
        {
            LOGGER.error( String.format( ERROR_DURING_REGISTRATION_MESSAGE, aServiceRegistration ), e );
        }
    }

    private void unregisterServiceRegistration( final ServiceRegistration aServiceRegistration )
    {
        if ( router_ != null )
        {
            aServiceRegistration.unregister( router_ );
        }
    }

    private void registerServiceRegistrations()
    {
        serviceRegistrationsLock_.readLock().lock();
        try
        {
            if ( router_ != null )
            {
                for ( ServiceRegistration serviceRegistration : serviceRegistrations_ )
                {
                    registerServiceRegistration( serviceRegistration );
                }
            }
        }
        finally
        {
            serviceRegistrationsLock_.readLock().unlock();
        }
    }

    private void unregisterServiceRegistrations()
    {
        serviceRegistrationsLock_.readLock().lock();
        try
        {
            if ( router_ != null )
            {
                for ( ServiceRegistration serviceRegistration : serviceRegistrations_ )
                {
                    unregisterServiceRegistration( serviceRegistration );
                }
            }
        }
        finally
        {
            serviceRegistrationsLock_.readLock().unlock();
        }
    }


}
