/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.server.whiteboard.internal;

import fr.matoeil.xinshi.server.spi.Service;
import fr.matoeil.xinshi.server.spi.Services;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Filter;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class LocalServiceTracker
    extends ServiceTracker
{

    private static final Logger LOGGER = LoggerFactory.getLogger( LocalServiceTracker.class );

    private static final String FILTER =
        "(&(" + org.osgi.framework.Constants.OBJECTCLASS + "=" + Service.class.getName() + ")(" + Services.SERVICE_SUBDOMAIN
            + "=*))";

    private static final String SERVICE_AVAILABLE_MESSAGE = "Service available {}";

    private static final String SERVICE_REMOVED_MESSAGE = "Service removed {}";

    private static final String INVALID_SERVICE_SUBDOMAIN_MESSAGE =
        "Registered service [{}] did not contain a valid value for the '" + Services.SERVICE_SUBDOMAIN + "' property.";

    private final LocalServiceRegistry localServiceRegistry_;

    @Inject
    public LocalServiceTracker( final BundleContext aBundleContext, LocalServiceRegistry aLocalServiceRegistry )
    {
        super( aBundleContext, createFilter( aBundleContext ), null );
        localServiceRegistry_ = aLocalServiceRegistry;
    }

    private static Filter createFilter( final BundleContext bundleContext )
    {
        try
        {
            return bundleContext.createFilter( FILTER );
        }
        catch ( InvalidSyntaxException e )
        {
            throw new IllegalArgumentException( e.getMessage(), e );
        }
    }

    @Override
    public Object addingService( final ServiceReference serviceReference )
    {
        LOGGER.debug( SERVICE_AVAILABLE_MESSAGE, serviceReference );
        Service service = (Service) super.addingService( serviceReference );
        ServiceRegistration serviceRegistration = createServiceRegistration( serviceReference, service );
        if ( serviceRegistration != null )
        {
            localServiceRegistry_.addServiceRegistration( serviceRegistration );
            return serviceRegistration;
        }
        else
        {
            super.remove( serviceReference );
            return null;
        }
    }

    @Override
    public void removedService( final ServiceReference serviceReference, final Object aServiceRegistration )
    {
        LOGGER.debug( SERVICE_REMOVED_MESSAGE, serviceReference );
        super.removedService( serviceReference, aServiceRegistration );
        localServiceRegistry_.removeServiceRegistration( (ServiceRegistration) aServiceRegistration );
    }

    private ServiceRegistration createServiceRegistration( final ServiceReference serviceReference,
                                                           final Service aService )
    {
        final Object subDomain = serviceReference.getProperty( Services.SERVICE_SUBDOMAIN );
        if ( subDomain instanceof String )
        {
            return new ServiceRegistration( aService, (String) subDomain );

        }
        else
        {
            LOGGER.warn( INVALID_SERVICE_SUBDOMAIN_MESSAGE, aService );
            return null;
        }
    }
}
