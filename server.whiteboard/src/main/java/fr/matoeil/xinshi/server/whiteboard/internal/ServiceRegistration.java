/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.server.whiteboard.internal;

import fr.matoeil.xinshi.server.api.Router;
import fr.matoeil.xinshi.server.spi.Service;
import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.core.stanza.Stanza;

public class ServiceRegistration
    implements Service
{
    private final Service service_;

    private final String label_;

    public ServiceRegistration( Service aService, String aLabel )
    {
        service_ = aService;
        label_ = aLabel;
    }

    public void register( Router aRouter )
    {
        aRouter.registerService( service_, label_ );
    }

    public void unregister( Router aRouter )
    {
        aRouter.unregisterService( label_ );
    }

    @Override
    public void bind( final Address aServiceFQDN )
    {
        service_.bind( aServiceFQDN );
    }

    @Override
    public void handle( final Stanza aStanza )
    {
        service_.handle( aStanza );
    }

    @Override
    public void unbind()
    {
        service_.unbind();
    }
}
