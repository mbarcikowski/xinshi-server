/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.server.whiteboard.internal.activation;

import com.google.inject.Guice;
import fr.matoeil.xinshi.server.whiteboard.internal.LocalServiceTracker;
import fr.matoeil.xinshi.server.whiteboard.internal.RouterTracker;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import javax.inject.Inject;

import static org.ops4j.peaberry.Peaberry.osgiModule;

public class Activator
    implements BundleActivator
{


    private RouterTracker routerTracker_;

    private LocalServiceTracker localServiceTracker_;

    @Inject
    public void setRouterTracker( RouterTracker aRouterTracker )
    {
        routerTracker_ = aRouterTracker;
    }

    @Inject
    public void setLocalServiceTracker( LocalServiceTracker aLocalServiceTracker )
    {
        localServiceTracker_ = aLocalServiceTracker;
    }

    @Override
    public void start( BundleContext context )
        throws Exception
    {
        Guice.createInjector( osgiModule( context ), new BundleModule() ).injectMembers( this );
        routerTracker_.open();
        localServiceTracker_.open();
    }

    @Override
    public void stop( BundleContext context )
        throws Exception
    {
        routerTracker_.close();
        localServiceTracker_.close();
    }
}
