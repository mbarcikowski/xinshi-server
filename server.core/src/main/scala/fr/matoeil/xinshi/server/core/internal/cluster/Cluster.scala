/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.server.core.internal.cluster

import org.jgroups.{View, ReceiverAdapter, JChannel}

class Cluster
  extends ReceiverAdapter
{
  private val CLUSTER_NAME: String = "xinshi"

  private var channel_ : JChannel = null

  def start()
  {
    channel_ = new
        JChannel
    channel_.setReceiver(this)
    channel_.connect(CLUSTER_NAME)
  }

  def stop()
  {
    channel_.close()
  }

  override def viewAccepted(view: View)
  {
    System.out.println("** view: " + view)
  }
}
