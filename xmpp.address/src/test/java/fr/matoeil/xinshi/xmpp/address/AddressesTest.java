package fr.matoeil.xinshi.xmpp.address;

import org.junit.Test;

import java.lang.reflect.Constructor;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class AddressesTest
{

    @Test
    public void testConstructor()
        throws Exception
    {
        Constructor target = Addresses.class.getDeclaredConstructor();

        assertThat( target.isAccessible(), is( equalTo( false ) ) );

        target.setAccessible( true );
        target.newInstance();
    }

    @Test
    public void testFromString_with_no_node()
        throws Exception
    {
        Address actual = Addresses.fromString( "domain.com/resource" );

        assertThat( actual.node(), isEmptyString() );
        assertThat( actual.hasNode(), is( equalTo( false ) ) );
        assertThat( actual.domain(), is( equalTo( "domain.com" ) ) );
        assertThat( actual.resource(), is( equalTo( "resource" ) ) );
        assertThat( actual.hasResource(), is( equalTo( true ) ) );

    }

    @Test
    public void testFromString_with_no_node_but_with_at()
        throws Exception
    {
        Address actual = Addresses.fromString( "@domain.com/resource" );

        assertThat( actual.node(), isEmptyString() );
        assertThat( actual.hasNode(), is( equalTo( false ) ) );
        assertThat( actual.domain(), is( equalTo( "domain.com" ) ) );
        assertThat( actual.resource(), is( equalTo( "resource" ) ) );
        assertThat( actual.hasResource(), is( equalTo( true ) ) );

    }

    @Test
    public void testFromString_with_no_resource()
        throws Exception
    {
        Address actual = Addresses.fromString( "me@domain.com" );

        assertThat( actual.node(), is( equalTo( "me" ) ) );
        assertThat( actual.hasNode(), is( equalTo( true ) ) );
        assertThat( actual.domain(), is( equalTo( "domain.com" ) ) );
        assertThat( actual.resource(), isEmptyString() );
        assertThat( actual.hasResource(), is( equalTo( false ) ) );
    }

    @Test
    public void testFromString_with_no_resource_but_with_slash()
        throws Exception
    {
        Address actual = Addresses.fromString( "me@domain.com/" );

        assertThat( actual.node(), is( equalTo( "me" ) ) );
        assertThat( actual.hasNode(), is( equalTo( true ) ) );
        assertThat( actual.domain(), is( equalTo( "domain.com" ) ) );
        assertThat( actual.resource(), isEmptyString() );
        assertThat( actual.hasResource(), is( equalTo( false ) ) );
    }

    @Test
    public void testFromString_with_no_node_and_no_resource()
        throws Exception
    {
        Address actual = Addresses.fromString( "domain.com" );

        assertThat( actual.node(), isEmptyString() );
        assertThat( actual.hasNode(), is( equalTo( false ) ) );
        assertThat( actual.domain(), is( equalTo( "domain.com" ) ) );
        assertThat( actual.resource(), isEmptyString() );
        assertThat( actual.hasResource(), is( equalTo( false ) ) );
    }

    @Test
    public void testFromString_with_no_node_and_no_resource_but_with_at_and_slash()
        throws Exception
    {
        Address actual = Addresses.fromString( "@domain.com/" );

        assertThat( actual.node(), isEmptyString() );
        assertThat( actual.hasNode(), is( equalTo( false ) ) );
        assertThat( actual.domain(), is( equalTo( "domain.com" ) ) );
        assertThat( actual.resource(), isEmptyString() );
        assertThat( actual.hasResource(), is( equalTo( false ) ) );
    }

    @Test( expected = IllegalArgumentException.class )
    public void testFromString_with_missing_domain_but_with_node()
        throws Exception
    {
        Addresses.fromString( "me@" );
    }

    @Test( expected = IllegalArgumentException.class )
    public void testFromString_with_missing_domain_but_with_resource()
        throws Exception
    {
        Addresses.fromString( "/resource" );
    }

    @Test( expected = IllegalArgumentException.class )
    public void testFromString_with_missing_domain_but_with_node_and_resource()
        throws Exception
    {
        Addresses.fromString( "me@/resource" );
    }

    @Test( expected = IllegalArgumentException.class )
    public void testFromString_with_invalid_node()
        throws Exception
    {
        Addresses.fromString( '\u0221' + "me@/resource" );
    }


    @Test
    public void testFromString()
        throws Exception
    {
        final Address expected = new Address( "me", "domain.com", "resource" );

        Address actual = Addresses.fromString( "me@domain.com/resource" );

        assertThat( actual, is( equalTo( expected ) ) );
    }
}
