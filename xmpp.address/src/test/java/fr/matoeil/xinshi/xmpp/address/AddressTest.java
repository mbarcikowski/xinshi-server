package fr.matoeil.xinshi.xmpp.address;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class AddressTest
{

    private Address target_;

    private final String expectedNode_ = "me";

    private final String expectedDomain_ = "domain.com";

    private final String expectedResource_ = "resource";

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new Address( expectedNode_, expectedDomain_, expectedResource_ );
    }

    @Test
    public void testNode()
        throws Exception
    {
        String actual = target_.node();

        assertThat( actual, is( equalTo( expectedNode_ ) ) );
    }

    @Test
    public void testDomain()
        throws Exception
    {
        String actual = target_.domain();

        assertThat( actual, is( equalTo( expectedDomain_ ) ) );
    }

    @Test
    public void testResource()
        throws Exception
    {
        String actual = target_.resource();

        assertThat( actual, is( equalTo( expectedResource_ ) ) );
    }

    @Test
    public void testBareAddress()
        throws Exception
    {
        String actual = target_.bareAddress();

        assertThat( actual, is( equalTo( expectedNode_ + "@" + expectedDomain_ ) ) );
    }

    @Test
    public void testFullAddress()
        throws Exception
    {
        String actual = target_.fullAddress();

        assertThat( actual, is( equalTo( expectedNode_ + "@" + expectedDomain_ + "/" + expectedResource_ ) ) );
    }

    @Test
    public void testHasNode()
        throws Exception
    {
        boolean actual = target_.hasNode();

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testHasNode_with_no_node()
        throws Exception
    {
        target_ = new Address( "", expectedDomain_, expectedResource_ );

        boolean actual = target_.hasNode();

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Test
    public void testHasResource()
        throws Exception
    {
        boolean actual = target_.hasResource();

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testHasResource_with_no_node()
        throws Exception
    {
        target_ = new Address( expectedNode_, expectedDomain_, "" );

        boolean actual = target_.hasResource();

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Test
    public void testEquals_with_same()
        throws Exception
    {
        boolean actual = target_.equals( target_ );

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testEquals_with_equals()
        throws Exception
    {
        final Address version = new Address( expectedNode_, expectedDomain_, expectedResource_ );
        boolean actual = target_.equals( version );

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testEquals_with_null()
        throws Exception
    {
        boolean actual = target_.equals( null );

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Test
    public void testEquals_with_different_address()
        throws Exception
    {
        final Address address = new Address( "", expectedDomain_, "" );

        boolean actual = target_.equals( address );

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Test
    public void testHashCode()
        throws Exception
    {
        final Address address = new Address( expectedNode_, expectedDomain_, expectedResource_ );
        int expected = address.hashCode();

        int actual = target_.hashCode();

        assertThat( actual, is( equalTo( expected ) ) );
    }

    @Test
    public void testToString()
        throws Exception
    {
        final Address target_ = new Address( expectedNode_, expectedDomain_, expectedResource_ );
        final Object expected = expectedNode_ + "@" + expectedDomain_ + "/" + expectedResource_;

        String actual = target_.toString();

        assertThat( actual, is( equalTo( expected ) ) );

    }
}
