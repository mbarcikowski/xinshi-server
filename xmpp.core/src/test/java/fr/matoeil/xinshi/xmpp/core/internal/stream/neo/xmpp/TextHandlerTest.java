/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.Xmls;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.Text;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.TextBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.Attributes;

import javax.annotation.Nullable;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class TextHandlerTest
{
    private TextHandler target_;

    private final String expectedNamespaceURI = "namespace";

    private final ImmutableNode mockedParentNode_ = mock( ImmutableNode.class );

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new TestTextHandler();
    }

    @Test
    public void testGetTextBuilder()
        throws Exception
    {
        TextBuilder actual = target_.getTextBuilder();

        assertThat( actual, is( notNullValue() ) );
    }

    @Test(expected = StreamException.class)
    public void testHandleElementOpening()
        throws Exception
    {
        target_.handleElementOpening( "namespace", "name", "name" );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testStartPrefixMapping()
        throws Exception
    {
        target_.startPrefixMapping( "prefix", "namespace" );
    }

    @Test
    public void testStartElement_without_attributes()
        throws Exception
    {
        final Attributes mockedAttributes = mock( Attributes.class );

        target_.startElement( "namespace", "name", "name", mockedAttributes );

        Text actual = target_.getTextBuilder().isChildOf( mockedParentNode_ ).atIndex( 0 ).build();
        assertThat( actual.getAttributes().getLength(), is( equalTo( 0 ) ) );
    }

    @Test
    public void testStartElement_with_xml_lang()
        throws Exception
    {
        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( eq( 0 ) ) ).thenReturn( "xml:lang" );
        String expectedLang = "fr";
        when( mockedAttributes.getValue( eq( 0 ) ) ).thenReturn( expectedLang );

        target_.startElement( "namespace", "name", "name", mockedAttributes );

        Text actual = target_.getTextBuilder().isChildOf( mockedParentNode_ ).atIndex( 0 ).build();
        assertThat( actual.getAttributes().getLength(), is( equalTo( 1 ) ) );
        assertThat( actual.getAttributeNS( Xmls.XML_NS_URI, Xmls.LANG ), is( equalTo( expectedLang ) ) );
    }

    @Test
    public void testStartElement_with_correct_xmlns()
        throws Exception
    {
        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( eq( 0 ) ) ).thenReturn( "xmlns" );
        when( mockedAttributes.getValue( eq( 0 ) ) ).thenReturn( expectedNamespaceURI );
        target_.startElement( "namespace", "name", "name", mockedAttributes );

        Text actual = target_.getTextBuilder().isChildOf( mockedParentNode_ ).atIndex( 0 ).build();
        assertThat( actual, is( notNullValue() ) );
    }

    @Test(expected = StreamException.class)
    public void testStartElement_with_incorrect_xmlns()
        throws Exception
    {
        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( eq( 0 ) ) ).thenReturn( "xmlns" );
        when( mockedAttributes.getValue( eq( 0 ) ) ).thenReturn( "other_namespace" );
        target_.startElement( "namespace", "name", "name", mockedAttributes );
    }

    @Test(expected = StreamException.class)
    public void testStartElement_with_other_attributes()
        throws Exception
    {
        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( eq( 0 ) ) ).thenReturn( "any" );
        target_.startElement( "namespace", "name", "name", mockedAttributes );
    }

    @Test(expected = StreamException.class)
    public void testStartCDATA()
        throws Exception
    {
        target_.startCDATA();
    }

    @Test
    public void testCharacters()
        throws Exception
    {
        String expectedContent = "content";
        char[] chars = expectedContent.toCharArray();

        target_.characters( chars, 0, chars.length );

        Text actual = target_.getTextBuilder().isChildOf( mockedParentNode_ ).atIndex( 0 ).build();
        assertThat( actual.getContent(), is( equalTo( expectedContent ) ) );
    }

    @Test(expected = StreamException.class)
    public void testEndCDATA()
        throws Exception
    {
        target_.endCDATA();
    }

    private class TestText
        extends Text
    {
        public TestText( final ImmutableNode aParentNode, final Integer aElementIndex, final String aLanguage,
                         final String aContent )
        {
            super( aParentNode, aElementIndex, expectedNamespaceURI, aLanguage, aContent );
        }
    }

    private class TestTextHandler
        extends TextHandler
    {
        public TestTextHandler()
        {
            super( new TextBuilder()
            {
                @Override
                protected Text newText( final ImmutableNode aParentNode, final Integer aElementIndex,
                                        @Nullable final String aLanguage, @Nullable final String aContent )
                {
                    return new TestText( aParentNode, aElementIndex, aLanguage, aContent );
                }
            }, TextHandlerTest.this.expectedNamespaceURI );
        }


    }
}
