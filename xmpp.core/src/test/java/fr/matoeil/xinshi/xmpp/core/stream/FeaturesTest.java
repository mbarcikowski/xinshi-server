/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream;

import com.google.common.collect.ImmutableList;
import fr.matoeil.xinshi.xmpp.core.internal.dom.EmptyImmutableNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableArrayNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableOneNodeList;
import fr.matoeil.xinshi.xmpp.core.stream.feature.Feature;
import fr.matoeil.xinshi.xmpp.core.stream.feature.FeatureBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class FeaturesTest
{
    private Features target_;

    private final String expectedNamespaceURI = "http://etherx.jabber.org/streams";

    private final String expectedPrefix = "prefix";

    private final String expectedLocalName = "features";

    private final FeatureBuilder expectedFeature =
        new FeatureBuilder().inNamespace( "extension-namespace" ).named( "extension" );

    private final List<FeatureBuilder> expectedFeatures =
        new ImmutableList.Builder<FeatureBuilder>().add( expectedFeature ).build();

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new Features( expectedPrefix, expectedFeatures );
    }

    @After
    public void tearDown()
        throws Exception
    {
    }

    @Test
    public void testGetFeatures()
        throws Exception
    {
        List<Feature> actual = target_.getFeatures();

        assertThat( actual.size(), is( equalTo( expectedFeatures.size() ) ) );
    }


    @Test
    public void testGetChildNodes()
        throws Exception
    {
        NodeList actualChildNodes = target_.getChildNodes();

        assertThat( actualChildNodes, is( notNullValue() ) );
        assertThat( actualChildNodes.getLength(), is( equalTo( expectedFeatures.size() ) ) );
    }


    @Test
    public void testConstructor_with_no_child()
        throws Exception
    {
        List<FeatureBuilder> featureBuilders = new ArrayList<>();

        target_ = new Features( expectedPrefix, featureBuilders );

        NodeList actualChildNodes = target_.getChildNodes();
        assertThat( actualChildNodes, is( instanceOf( EmptyImmutableNodeList.class ) ) );
    }

    @Test
    public void testConstructor_with_one_child()
        throws Exception
    {
        List<FeatureBuilder> featureBuilders = new ArrayList<>();

        FeatureBuilder expectedFeature = new FeatureBuilder().inNamespace( "extension-namespace" ).named( "extension" );
        featureBuilders.add( expectedFeature );

        target_ = new Features( expectedPrefix, featureBuilders );

        NodeList actualChildNodes = target_.getChildNodes();
        assertThat( actualChildNodes, is( instanceOf( ImmutableOneNodeList.class ) ) );
    }

    @Test
    public void testConstructor_with_more_than_one_child()
        throws Exception
    {
        List<FeatureBuilder> featureBuilders = new ArrayList<>();

        FeatureBuilder first = new FeatureBuilder().inNamespace( "extension-namespace" ).named( "extension" );
        featureBuilders.add( first );
        FeatureBuilder second = new FeatureBuilder().inNamespace( "extension-namespace" ).named( "extension" );
        featureBuilders.add( second );

        target_ = new Features( expectedPrefix, featureBuilders );

        NodeList actualChildNodes = target_.getChildNodes();
        assertThat( actualChildNodes, is( instanceOf( ImmutableArrayNodeList.class ) ) );
    }
}
