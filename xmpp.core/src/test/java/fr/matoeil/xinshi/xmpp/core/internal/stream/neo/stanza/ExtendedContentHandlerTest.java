/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.stanza.ExtendedContent;
import fr.matoeil.xinshi.xmpp.core.stanza.ExtendedContentBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class ExtendedContentHandlerTest
{
    private ExtendedContentHandler target_;

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new ExtendedContentHandler();
    }

    @Test
    public void testgetBuilder()
        throws Exception
    {
        ExtendedContentBuilder actual = target_.getBuilder();

        assertThat( actual, is( instanceOf( ExtendedContentBuilder.class ) ) );
    }

    @Test( expected = StreamException.class )
    public void testStartPrefixMapping_with_forbidden_mapping()
        throws Exception
    {
        target_.startPrefixMapping( "prefix", "jabber:client" );
    }

    @Test
    public void testStartPrefixMapping_with_allowed_mapping()
        throws Exception
    {
        target_.startPrefixMapping( "prefix", "namespace" );

        ExtendedContent actual =
            target_.getBuilder().isChildOf( mock( ImmutableNode.class ) ).atIndex( 0 ).inNamespace( "namespace" ).named(
                "name" ).build();
        assertThat( actual, is( notNullValue() ) );
    }
}
