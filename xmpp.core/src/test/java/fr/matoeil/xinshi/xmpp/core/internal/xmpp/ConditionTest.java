/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.xmpp;

import fr.matoeil.xinshi.xmpp.core.internal.dom.EmptyImmutableNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableOneImmutableAttributeMap;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableOneNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.Xmls;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;

public class ConditionTest
{
    private Condition<TestDefinedCondition> target_;

    private final ImmutableNode mockedImmutableNode_ = mock( ImmutableNode.class );

    private final int expectedElementIndex = 0;

    private final String expectedNamespaceURI = "namespace";

    private final TestDefinedCondition expectedDefinedCondition_ = TestDefinedCondition.TEST;

    private final String expectedLocalName = "test";

    private final String expectedContent = "a content";

    @Before
    public void setUp()
        throws Exception
    {
        target_ =
            new Condition( mockedImmutableNode_, expectedElementIndex, expectedNamespaceURI, expectedDefinedCondition_,
                           expectedContent )
            {
            };
    }

    @Test
    public void testGetDefinedCondition()
        throws Exception
    {
        TestDefinedCondition actualDefinedCondition = target_.getDefinedCondition();

        assertThat( actualDefinedCondition, is( equalTo( expectedDefinedCondition_ ) ) );
    }

    @Test
    public void testGetContent()
        throws Exception
    {
        String actualContent = target_.getContent();

        assertThat( actualContent, is( equalTo( expectedContent ) ) );
    }

    @Test
    public void testGetTagName()
        throws Exception
    {
        String actual = target_.getTagName();

        assertThat( actual, is( equalTo( expectedLocalName ) ) );
    }


    @Test
    public void testGetNodeName()
        throws Exception
    {
        String actual = target_.getNodeName();

        assertThat( actual, is( equalTo( expectedLocalName ) ) );
    }

    @Test
    public void testGetNamespaceURI()
        throws Exception
    {
        String actual = target_.getNamespaceURI();

        assertThat( actual, is( equalTo( expectedNamespaceURI ) ) );
    }


    @Test
    public void testGetLocalName()
        throws Exception
    {
        String actual = target_.getLocalName();

        assertThat( actual, is( equalTo( expectedLocalName ) ) );
    }

    @Test
    public void testGetChildNodes()
        throws Exception
    {
        NodeList actual = target_.getChildNodes();

        assertThat( actual, is( instanceOf( ImmutableOneNodeList.class ) ) );

        Node item = actual.item( 0 );

        assertThat( item, is( notNullValue() ) );
        assertThat( item.getTextContent(), is( equalTo( expectedContent ) ) );
    }

    @Test
    public void testGetAttributes()
        throws Exception
    {
        NamedNodeMap actual = target_.getAttributes();

        assertThat( actual, is( instanceOf( ImmutableOneImmutableAttributeMap.class ) ) );
    }

    @Test
    public void testGetAttribute_with_unknow_attribute()
        throws Exception
    {
        String actual = target_.getAttribute( "name" );

        assertThat( actual, isEmptyString() );
    }

    @Test
    public void testGetAttribute_with_know_attribute()
        throws Exception
    {
        String actual = target_.getAttribute( Xmls.XMLNS_ATTRIBUTE );

        assertThat( actual, is( equalTo( expectedNamespaceURI ) ) );
    }


    @Test
    public void testGetAttributeNode_with_unknow_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNode( "name" );

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetAttributeNode_with_know_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNode( Xmls.XMLNS_ATTRIBUTE );

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getNodeName(), is( equalTo( Xmls.XMLNS_ATTRIBUTE ) ) );
        assertThat( actual.getNodeValue(), is( equalTo( expectedNamespaceURI ) ) );
    }

    @Test
    public void testGetAttributeNS_with_unknow_attribute()
        throws Exception
    {
        String actual = target_.getAttributeNS( "namespace", "localName" );

        assertThat( actual, isEmptyString() );
    }

    @Test
    public void testGetAttributeNS_with_know_attribute()
        throws Exception
    {
        String actual = target_.getAttributeNS( Xmls.XMLNS_ATTRIBUTE_NS_URI, Xmls.XMLNS_ATTRIBUTE );

        assertThat( actual, is( equalTo( expectedNamespaceURI ) ) );
    }

    @Test
    public void testGetAttributeNodeNS_with_unknow_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNodeNS( "namespace", "localName" );

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetAttributeNodeNS_with_know_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNodeNS( Xmls.XMLNS_ATTRIBUTE_NS_URI, Xmls.XMLNS_ATTRIBUTE );

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getNodeName(), is( equalTo( Xmls.XMLNS_ATTRIBUTE ) ) );
        assertThat( actual.getNodeValue(), is( equalTo( expectedNamespaceURI ) ) );
    }


    @Test
    public void testHasAttribute_with_unknow_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttribute( "name" );

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Test
    public void testHasAttribute_with_know_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttribute( Xmls.XMLNS_ATTRIBUTE );

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testHasAttributeNS_with_unknow_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttributeNS( "namespace", "localName" );

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Test
    public void testHasAttributeNS_with_know_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttributeNS( Xmls.XMLNS_ATTRIBUTE_NS_URI, Xmls.XMLNS_ATTRIBUTE );

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testGetTextContent()
        throws Exception
    {
        String actual = target_.getTextContent();

        assertThat( actual, is( equalTo( expectedContent ) ) );
    }

    @Test
    public void testConstructor_without_content()
        throws Exception
    {
        target_ =
            new Condition( mockedImmutableNode_, expectedElementIndex, expectedNamespaceURI, expectedDefinedCondition_,
                           null )
            {
            };

        assertThat( target_.getTextContent(), is( nullValue() ) );
        assertThat( target_.getContent(), is( nullValue() ) );
        assertThat( target_.getChildNodes(), is( instanceOf( EmptyImmutableNodeList.class ) ) );

    }

    private static enum TestDefinedCondition
        implements DefinedCondition
    {
        TEST
            {
                @Override
                public String asString()
                {
                    return "test";
                }
            }
    }
}
