/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.error;

import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.address.Addresses;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp.ImmutableChildElementHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp.XmppHandler;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaError;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorCondition;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.Attributes;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StanzaErrorHandlerTest
{
    private final String expectedNamespaceURI_ = "urn:ietf:params:xml:ns:xmpp-stanzas";

    private StanzaErrorHandler target_;

    private final ImmutableNode mockedParentNode_ = mock( ImmutableNode.class );

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new StanzaErrorHandler();
    }

    @Test
    public void testGetStanzaErrorBuilder()
        throws Exception
    {
        StanzaErrorBuilder actual = target_.getStanzaErrorBuilder();

        assertThat( actual, is( notNullValue() ) );
    }

    @Test( expected = StreamException.class )
    public void testHandleElementOpening_without_namespace()
        throws Exception
    {
        target_.handleElementOpening( null, "name", "name" );
    }

    @Test
    public void testHandleElementOpening_with_text()
        throws Exception
    {
        XmppHandler actual = target_.handleElementOpening( expectedNamespaceURI_, "text", "text" );

        assertThat( actual, is( instanceOf( StanzaErrorTextHandler.class ) ) );
    }

    @Test
    public void testHandleElementOpening_with_condition()
        throws Exception
    {
        StanzaErrorCondition.DefinedCondition expectedDefinedCondition =
            StanzaErrorCondition.DefinedCondition.BAD_REQUEST;

        XmppHandler actual = target_.handleElementOpening( expectedNamespaceURI_, expectedDefinedCondition.asString(),
                                                           expectedDefinedCondition.asString() );

        assertThat( actual, is( instanceOf( StanzaErrorConditionHandler.class ) ) );
    }

    @Test
    public void testHandleElementOpening_with_element_not_in_stanza_namespace()
        throws Exception
    {
        XmppHandler actual = target_.handleElementOpening( "namespace", "name", "name" );

        assertThat( actual, is( instanceOf( ImmutableChildElementHandler.class ) ) );
    }

    @Test( expected = StreamException.class )
    public void testStartPrefixMapping_with_client_namespace_prefixed()
        throws Exception
    {
        target_.startPrefixMapping( "prefix", "jabber:client" );
    }

    @Test
    public void testStartPrefixMapping_with_client_namespace_not_prefixed()
        throws Exception
    {
        target_.startPrefixMapping( null, "jabber:client" );

        assertStanzaErrorBuilt();
    }

    @Test
    public void testStartPrefixMapping_with_other_mapping()
        throws Exception
    {
        target_.startPrefixMapping( "prefix", "namespace" );

        assertStanzaErrorBuilt();
    }

    @Test
    public void testStartElement_with_type_attribute()
        throws Exception
    {
        StanzaError.Type expectedType = StanzaError.Type.CONTINUE;

        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( eq( 0 ) ) ).thenReturn( "type" );
        when( mockedAttributes.getValue( eq( 0 ) ) ).thenReturn( expectedType.asString() );

        target_.startElement( "jabber:client", "error", "error", mockedAttributes );

        StanzaErrorCondition.DefinedCondition expectedDefinedCondition =
            StanzaErrorCondition.DefinedCondition.BAD_REQUEST;
        XmppHandler xmppHandler =
            target_.handleElementOpening( expectedNamespaceURI_, expectedDefinedCondition.asString(),
                                          expectedDefinedCondition.asString() );
        Attributes mockedConditionAttributes = mock( Attributes.class );
        when( mockedConditionAttributes.getLength() ).thenReturn( 1 );
        when( mockedConditionAttributes.getQName( eq( 0 ) ) ).thenReturn( "xmlns" );
        when( mockedConditionAttributes.getValue( eq( 0 ) ) ).thenReturn( expectedNamespaceURI_ );
        xmppHandler.startElement( expectedNamespaceURI_, expectedDefinedCondition.asString(),
                                  expectedDefinedCondition.asString(), mockedConditionAttributes );

        StanzaError actual = target_.getStanzaErrorBuilder().isChildOf( mockedParentNode_ ).atIndex( 0 ).build();
        assertThat( actual.getType(), is( equalTo( expectedType ) ) );
    }


    @Test
    public void testStartElement_with_by_attribute()
        throws Exception
    {
        Address expectedBy = Addresses.fromString( "me@domain.com" );

        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 2 );
        when( mockedAttributes.getQName( eq( 0 ) ) ).thenReturn( "by" );
        when( mockedAttributes.getValue( eq( 0 ) ) ).thenReturn( expectedBy.fullAddress() );
        when( mockedAttributes.getQName( eq( 1 ) ) ).thenReturn( "type" );
        when( mockedAttributes.getValue( eq( 1 ) ) ).thenReturn( StanzaError.Type.CONTINUE.asString() );

        target_.startElement( "jabber:client", "error", "error", mockedAttributes );

        StanzaErrorCondition.DefinedCondition expectedDefinedCondition =
            StanzaErrorCondition.DefinedCondition.BAD_REQUEST;
        XmppHandler xmppHandler =
            target_.handleElementOpening( expectedNamespaceURI_, expectedDefinedCondition.asString(),
                                          expectedDefinedCondition.asString() );
        Attributes mockedConditionAttributes = mock( Attributes.class );
        when( mockedConditionAttributes.getLength() ).thenReturn( 1 );
        when( mockedConditionAttributes.getQName( eq( 0 ) ) ).thenReturn( "xmlns" );
        when( mockedConditionAttributes.getValue( eq( 0 ) ) ).thenReturn( expectedNamespaceURI_ );
        xmppHandler.startElement( expectedNamespaceURI_, expectedDefinedCondition.asString(),
                                  expectedDefinedCondition.asString(), mockedConditionAttributes );

        StanzaError actual = target_.getStanzaErrorBuilder().isChildOf( mockedParentNode_ ).atIndex( 0 ).build();
        assertThat( actual.getBy(), is( equalTo( expectedBy ) ) );
    }


    @Test( expected = StreamException.class )
    public void testStartElement_with_other_attribute()
        throws Exception
    {
        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( eq( 0 ) ) ).thenReturn( "attribute" );
        when( mockedAttributes.getValue( eq( 0 ) ) ).thenReturn( "value" );
        target_.startElement( "jabber:client", "error", "error", mockedAttributes );
    }

    @Test( expected = StreamException.class )
    public void testStartCDATA()
        throws Exception
    {
        target_.startCDATA();
    }

    @Test
    public void testCharacters_with_whitespaces()
        throws Exception
    {
        char[] characters = "        ".toCharArray();

        target_.characters( characters, 0, characters.length );

        assertStanzaErrorBuilt();
    }

    @Test( expected = StreamException.class )
    public void testCharacters_without_whitespaces_only()
        throws Exception
    {
        char[] characters = "    a    ".toCharArray();
        target_.characters( characters, 0, characters.length );
    }

    @Test( expected = StreamException.class )
    public void testEndCDATA()
        throws Exception
    {
        target_.endCDATA();
    }

    private void assertStanzaErrorBuilt()
    {
        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( eq( 0 ) ) ).thenReturn( "type" );
        when( mockedAttributes.getValue( eq( 0 ) ) ).thenReturn( StanzaError.Type.CANCEL.asString() );
        target_.startElement( "jabber:client", "error", "error", mockedAttributes );
        StanzaErrorCondition.DefinedCondition expectedDefinedCondition =
            StanzaErrorCondition.DefinedCondition.BAD_REQUEST;
        XmppHandler xmppHandler =
            target_.handleElementOpening( expectedNamespaceURI_, expectedDefinedCondition.asString(),
                                          expectedDefinedCondition.asString() );
        Attributes mockedConditionAttributes = mock( Attributes.class );
        when( mockedConditionAttributes.getLength() ).thenReturn( 1 );
        when( mockedConditionAttributes.getQName( eq( 0 ) ) ).thenReturn( "xmlns" );
        when( mockedConditionAttributes.getValue( eq( 0 ) ) ).thenReturn( expectedNamespaceURI_ );
        xmppHandler.startElement( expectedNamespaceURI_, expectedDefinedCondition.asString(),
                                  expectedDefinedCondition.asString(), mockedConditionAttributes );
        StanzaError actual = target_.getStanzaErrorBuilder().isChildOf( mockedParentNode_ ).atIndex( 0 ).build();
        assertThat( actual, is( notNullValue() ) );
    }
}
