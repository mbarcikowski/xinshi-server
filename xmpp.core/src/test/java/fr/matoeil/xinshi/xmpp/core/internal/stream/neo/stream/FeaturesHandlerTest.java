/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stream;

import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp.XmppHandler;
import fr.matoeil.xinshi.xmpp.core.stream.Features;
import fr.matoeil.xinshi.xmpp.core.stream.FeaturesBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.Attributes;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class FeaturesHandlerTest
{
    private FeaturesHandler target_;

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new FeaturesHandler();
    }

    @Test
    public void testGetBuilder()
        throws Exception
    {
        FeaturesBuilder actual = target_.getBuilder();

        assertThat( actual, is( instanceOf( FeaturesBuilder.class ) ) );

    }

    @Test(expected = StreamException.class)
    public void testHandleElementOpening_without_namespace()
        throws Exception
    {
        target_.handleElementOpening( null, "name", "name:prefix" );
    }

    @Test
    public void testHandleElementOpening_with_namespace()
        throws Exception
    {
        XmppHandler xmppHandler = target_.handleElementOpening( "namespace", "name", "name:prefix" );

        xmppHandler.startElement( "namespace", "name", "prefix:name", mock( Attributes.class ) );
        assertBuilt();
    }

    @Test(expected = StreamException.class)
    public void testStartPrefixMapping_with_forbidden_namespace()
        throws Exception
    {
        target_.startPrefixMapping( "prefix", "http://etherx.jabber.org/streams" );
    }

    @Test
    public void testStartPrefixMapping_with_allowed_namespace()
        throws Exception
    {
        target_.startPrefixMapping( "prefix", "namespce" );

        assertBuilt();
    }

    @Test(expected = StreamException.class)
    public void testStartElement_with_attributes()
        throws Exception
    {
        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );

        target_.startElement( "namespace", "name", "prefix:name", mockedAttributes );
    }

    @Test
    public void testStartElement_without_attribute()
        throws Exception
    {
        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 0 );

        target_.startElement( "namespace", "name", "prefix:name", mockedAttributes );

        assertBuilt();
    }

    @Test(expected = StreamException.class)
    public void testStartCDATA()
        throws Exception
    {
        target_.startCDATA();
    }

    @Test
    public void testCharacters_with_whitespaces()
        throws Exception
    {
        char[] characters = "        ".toCharArray();

        target_.characters( characters, 0, characters.length );

        assertBuilt();
    }

    @Test(expected = StreamException.class)
    public void testCharacters_without_whitespaces_only()
        throws Exception
    {
        char[] characters = "    a    ".toCharArray();
        target_.characters( characters, 0, characters.length );
    }

    @Test(expected = StreamException.class)
    public void testEndCDATA()
        throws Exception
    {
        target_.endCDATA();
    }

    private void assertBuilt()
    {
        Features actual = target_.getBuilder().build();
        assertThat( actual, is( notNullValue() ) );
    }
}
