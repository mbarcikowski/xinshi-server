/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.dom;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableAttributeBuilder;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.Show;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.ShowBuilder;
import org.junit.Test;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.lang.reflect.Constructor;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

public class ElementsTest
{
    private final ImmutableBaseElement expectedOwnerElement = mock( ImmutableBaseElement.class );

    private final ImmutableNode expectedParentNode = mock( ImmutableNode.class );

    @Test
    public void testConstructor()
        throws Exception
    {
        Constructor target_ = Elements.class.getDeclaredConstructor();

        assertThat( target_.isAccessible(), is( equalTo( false ) ) );

        target_.setAccessible( true );
        target_.newInstance();
    }

    @Test
    public void testEMPTY_ATTRIBUTES()
        throws Exception
    {
        ImmutableAttributeMap actual = Elements.EMPTY_ATTRIBUTES;

        assertThat( actual, is( instanceOf( EmptyImmutableAttributeMap.class ) ) );
    }

    @Test
    public void testBuild_with_null()
        throws Exception
    {
        Function<ShowBuilder, Show> target = Elements.build();

        ImmutableBaseChildElement actual = target.apply( null );

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testBuild_with_a_builder()
        throws Exception
    {
        ImmutableNode mockedImmutableNode = mock( ImmutableNode.class );
        ShowBuilder builder =
            new ShowBuilder().isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( "namespace" ).withState(
                Show.State.AWAY );
        Function<ShowBuilder, Show> target = Elements.build();

        ImmutableBaseChildElement actual = target.apply( builder );

        assertThat( actual, is( notNullValue() ) );
    }

    @Test
    public void testGenerateAttributes_with_zero_attribute()
        throws Exception
    {
        final List<ImmutableAttributeBuilder> expectedAttributeBuilders = Collections.emptyList();

        NamedNodeMap actual = Elements.generateAttributes( expectedOwnerElement, expectedAttributeBuilders );

        assertThat( actual, is( instanceOf( EmptyImmutableAttributeMap.class ) ) );
        assertThat( actual.getLength(), is( equalTo( 0 ) ) );
        assertThat_ownerElement_of_attributes_is_equal_to_expected_owner_element( actual );
    }

    @Test
    public void testGenerateAttributes_with_one_attribute()
        throws Exception
    {
        ImmutableAttributeBuilder attributeBuilder =
            new ImmutableAttributeBuilder().named( "attribute" ).withValue( "value" );

        final List<ImmutableAttributeBuilder> expectedAttributeBuilders = Lists.newArrayList( attributeBuilder );

        NamedNodeMap actual = Elements.generateAttributes( expectedOwnerElement, expectedAttributeBuilders );

        assertThat( actual, is( instanceOf( ImmutableOneImmutableAttributeMap.class ) ) );
        assertThat( actual.getLength(), is( equalTo( expectedAttributeBuilders.size() ) ) );
        assertThat_ownerElement_of_attributes_is_equal_to_expected_owner_element( actual );
    }

    @Test
    public void testGenerateAttributes_with_more_than_one_attribute()
        throws Exception
    {

        ImmutableAttributeBuilder firstAttributeBuilder =
            new ImmutableAttributeBuilder().named( "first" ).withValue( "value" );

        ImmutableAttributeBuilder secondAttributeBuilder =
            new ImmutableAttributeBuilder().named( "second" ).withValue( "value" );

        final List<ImmutableAttributeBuilder> expectedAttributeBuilders =
            Lists.newArrayList( firstAttributeBuilder, secondAttributeBuilder );

        NamedNodeMap actual = Elements.generateAttributes( expectedOwnerElement, expectedAttributeBuilders );

        assertThat( actual, is( instanceOf( ImmutableArrayImmutableAttributeMap.class ) ) );
        assertThat( actual.getLength(), is( equalTo( expectedAttributeBuilders.size() ) ) );
        assertThat_ownerElement_of_attributes_is_equal_to_expected_owner_element( actual );
    }

    @Test
    public void testConstructor_with_zero_child_nodes()
        throws Exception
    {
        final List<ImmutableChildNodeBuilder> expectedChildNodeBuilders = Collections.emptyList();

        NodeList actual = Elements.generateChildNodes( expectedParentNode, expectedChildNodeBuilders );

        assertThat( actual, is( instanceOf( EmptyImmutableNodeList.class ) ) );
        assertThat( actual.getLength(), is( equalTo( expectedChildNodeBuilders.size() ) ) );
        assertThat_parentNode_of_child_nodes_is_equal_to_expected_parent_node( actual );
    }

    @Test
    public void testConstructor_with_one_child_node()
        throws Exception
    {

        ImmutableChildNodeBuilder immutableChildElementBuilder = new ImmutableChildElementBuilder().named( "child" );

        final List<ImmutableChildNodeBuilder> expectedChildNodeBuilders =
            Lists.newArrayList( immutableChildElementBuilder );

        NodeList actual = Elements.generateChildNodes( expectedParentNode, expectedChildNodeBuilders );

        assertThat( actual, is( instanceOf( ImmutableOneNodeList.class ) ) );
        assertThat( actual.getLength(), is( equalTo( expectedChildNodeBuilders.size() ) ) );
        assertThat_parentNode_of_child_nodes_is_equal_to_expected_parent_node( actual );
    }

    @Test
    public void testConstructor_with_more_than_one_child_nodes()
        throws Exception
    {

        ImmutableChildNodeBuilder first = new ImmutableChildElementBuilder().named( "first" );

        ImmutableChildNodeBuilder second = new ImmutableChildElementBuilder().named( "second" );

        final List<ImmutableChildNodeBuilder> expectedChildNodeBuilders = Lists.newArrayList( first, second );

        NodeList actual = Elements.generateChildNodes( expectedParentNode, expectedChildNodeBuilders );

        assertThat( actual, is( instanceOf( ImmutableArrayNodeList.class ) ) );
        assertThat( actual.getLength(), is( equalTo( expectedChildNodeBuilders.size() ) ) );
        assertThat_parentNode_of_child_nodes_is_equal_to_expected_parent_node( actual );
    }

    private void assertThat_ownerElement_of_attributes_is_equal_to_expected_owner_element( final NamedNodeMap aActual )
    {
        for ( int index = 0, length = aActual.getLength(); index < length; index++ )
        {
            Attr attributed = (Attr) aActual.item( index );
            assertThat( attributed.getOwnerElement(), is( equalTo( (Element) expectedOwnerElement ) ) );
        }
    }

    private void assertThat_parentNode_of_child_nodes_is_equal_to_expected_parent_node( final NodeList aActual )
    {
        for ( int index = 0, length = aActual.getLength(); index < length; index++ )
        {
            Node node = aActual.item( index );
            assertThat( node.getParentNode(), is( equalTo( (Node) expectedParentNode ) ) );
        }
    }
}
