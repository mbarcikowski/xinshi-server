/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stream;

import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp.XmppHandler;
import fr.matoeil.xinshi.xmpp.core.stream.StreamElement;
import fr.matoeil.xinshi.xmpp.core.stream.StreamElementBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.Attributes;

import javax.annotation.Nullable;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StreamElementHandlerTest
{

    private StreamElementHandler target_;

    private String expectedNamespaceURI_ = "namespace";

    private String expectedLocalName_ = "name";

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new TestStreamElementHandler( new TestStreamElementBuilder() );

    }

    @Test
    public void testGetBuilder()
        throws Exception
    {
        StreamElementBuilder actual = target_.getBuilder();

        assertThat( actual, is( instanceOf( TestStreamElementBuilder.class ) ) );
    }

    @Test
    public void testBuild()
        throws Exception
    {
        StreamElement actual = target_.build();

        assertThat( actual, is( notNullValue() ) );
    }


    @Test
    public void testCheckStreamNamespaceURI_whit_valid_namespace()
        throws Exception
    {
        StreamElementHandler.checkStreamNamespaceURI( "namespace" );
    }

    @Test(expected = StreamException.class)
    public void testCheckStreamNamespaceURI_whit_forbidden_namespace()
        throws Exception
    {
        StreamElementHandler.checkStreamNamespaceURI( "http://etherx.jabber.org/streams" );
    }

    @Test
    public void testInitializeBuilderWithoutAttribute_without_attribute()
        throws Exception
    {
        TestStreamElementBuilder builder = new TestStreamElementBuilder();
        String expectedQualifiedName = "prefix:name";

        StreamElementHandler.initializeBuilderWithoutAttribute( expectedQualifiedName, mock( Attributes.class ),
                                                                builder );

        StreamElement actual = builder.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getNodeName(), is( equalTo( expectedQualifiedName ) ) );
    }

    @Test(expected = StreamException.class)
    public void testInitializeBuilderWithoutAttribute_with_attributes()
        throws Exception
    {
        TestStreamElementBuilder builder = new TestStreamElementBuilder();

        String expectedQualifiedName = "prefix:name";
        Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );

        StreamElementHandler.initializeBuilderWithoutAttribute( expectedQualifiedName, mockedAttributes, builder );

        StreamElement actual = builder.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getNodeName(), is( equalTo( expectedQualifiedName ) ) );
        assertThat( actual.getAttributes().getLength(), is( equalTo( 0 ) ) );
    }

    private class TestStreamElement
        extends StreamElement
    {


        public TestStreamElement( final String aPrefix )
        {
            super( expectedNamespaceURI_, aPrefix, expectedLocalName_ );
        }
    }

    private class TestStreamElementBuilder
        extends StreamElementBuilder<TestStreamElement, TestStreamElementBuilder>
    {

        @Override
        protected TestStreamElement newStreamElement( @Nullable final String aPrefix )
        {
            return new TestStreamElement( aPrefix );
        }
    }

    private static class TestStreamElementHandler
        extends StreamElementHandler<TestStreamElement, TestStreamElementBuilder>
    {
        protected TestStreamElementHandler( final TestStreamElementBuilder aBuilder )
        {
            super( aBuilder );
        }

        @Override
        public XmppHandler handleElementOpening( final String aNamespaceURI, final String aLocalName,
                                                 final String aQualifiedName )
        {
            throw new UnsupportedOperationException( "#handleElementOpening not implemented" );
        }

        @Override
        public void startPrefixMapping( final String aPrefix, final String aNamespaceURI )
        {
            throw new UnsupportedOperationException( "#startPrefixMapping not implemented" );
        }

        @Override
        public void startElement( final String aNamespaceURI, final String aLocalName, final String aQualifiedName,
                                  final Attributes aAttributes )
        {
            throw new UnsupportedOperationException( "#startElement not implemented" );
        }

        @Override
        public void startCDATA()
        {
            throw new UnsupportedOperationException( "TestStreamElementHandler#startCDATA not implemented" );
        }

        @Override
        public void characters( final char[] aChars, final int aStart, final int aLength )
        {
            throw new UnsupportedOperationException( "TestStreamElementHandler#characters not implemented" );
        }

        @Override
        public void endCDATA()
        {
            throw new UnsupportedOperationException( "TestStreamElementHandler#endCDATA not implemented" );
        }
    }
}
