/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.ImmutableLocalizedContentLeaf;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.ImmutableLocalizedContentLeafBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.Attributes;

import javax.annotation.Nullable;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ImmutableLocalizedContentLeafHandlerTest
{

    private TestImmutableLocalizedContentLeafHandler target_;

    private final String expectedLocalName_ = "name";

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new TestImmutableLocalizedContentLeafHandler();
    }

    @Test
    public void testGetBuilder()
        throws Exception
    {
        TestImmutableLocalizedContentLeafBuilder actual = target_.getBuilder();

        assertThat( actual, is( instanceOf( TestImmutableLocalizedContentLeafBuilder.class ) ) );
    }

    @Test(expected = StreamException.class)
    public void testHandleElementOpening()
        throws Exception
    {
        target_.handleElementOpening( "namespace", "name", "prefix:name" );
    }

    @Test(expected = StreamException.class)
    public void testStartPrefixMapping()
        throws Exception
    {
        target_.startPrefixMapping( "prefix", "namespace" );
    }


    @Test
    public void testStartElement_with_xml_langattribute()
        throws Exception
    {
        String expectedLanguage = "fr";

        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( 0 ) ).thenReturn( "xml:lang" );
        when( mockedAttributes.getValue( 0 ) ).thenReturn( expectedLanguage );

        target_.startElement( "namespace", "name", "prefix:name", mockedAttributes );

        TestImmutableLocalizedContentLeaf actual =
            target_.getBuilder().isChildOf( mock( ImmutableNode.class ) ).atIndex( 0 ).build();

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getLanguage(), is( equalTo( expectedLanguage ) ) );
    }

    @Test(expected = StreamException.class)
    public void testStartElement_with_invalid_attribute()
        throws Exception
    {
        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( 0 ) ).thenReturn( "any" );
        when( mockedAttributes.getValue( 0 ) ).thenReturn( "value" );

        target_.startElement( "namespace", "name", "prefix:name", mockedAttributes );
    }

    @Test(expected = StreamException.class)
    public void testStartCDATA()
        throws Exception
    {
        target_.startCDATA();
    }

    @Test
    public void testCharacters()
        throws Exception
    {
        String expectedContent = "content";
        final char[] chars = expectedContent.toCharArray();

        target_.characters( chars, 0, chars.length );

        TestImmutableLocalizedContentLeaf actual =
            target_.getBuilder().isChildOf( mock( ImmutableNode.class ) ).atIndex( 0 ).build();

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getContent(), is( equalTo( expectedContent ) ) );
    }

    @Test(expected = StreamException.class)
    public void testEndCDATA()
        throws Exception
    {
        target_.endCDATA();
    }

    private class TestImmutableLocalizedContentLeaf
        extends ImmutableLocalizedContentLeaf
    {
        public TestImmutableLocalizedContentLeaf( final ImmutableNode aParentNode, final Integer aElementIndex,
                                                  final String aNamespaceURI, final String aPrefix,
                                                  final String aLanguage, final String aContent )
        {
            super( aParentNode, aElementIndex, aNamespaceURI, expectedLocalName_, aLanguage, aContent );
        }
    }

    private class TestImmutableLocalizedContentLeafBuilder
        extends
        ImmutableLocalizedContentLeafBuilder<TestImmutableLocalizedContentLeaf, TestImmutableLocalizedContentLeafBuilder>
    {
        @Override
        protected TestImmutableLocalizedContentLeaf newImmutableLocalizedContentLeaf( final ImmutableNode aParentNode,
                                                                                      final Integer aElementIndex,
                                                                                      final String aNamespaceURI,
                                                                                      @Nullable final String aLanguage,
                                                                                      @Nullable final String aContent )
        {
            return new TestImmutableLocalizedContentLeaf( aParentNode, aElementIndex, aNamespaceURI, null, aLanguage,
                                                          aContent );
        }


    }

    private class TestImmutableLocalizedContentLeafHandler
        extends
        ImmutableLocalizedContentLeafHandler<TestImmutableLocalizedContentLeaf, TestImmutableLocalizedContentLeafBuilder>
    {
        public TestImmutableLocalizedContentLeafHandler()
        {
            super( new TestImmutableLocalizedContentLeafBuilder() );
        }
    }
}
