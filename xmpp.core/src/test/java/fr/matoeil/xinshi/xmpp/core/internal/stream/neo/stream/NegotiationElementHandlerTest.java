/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stream;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableCDATASection;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableText;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp.XmppHandler;
import fr.matoeil.xinshi.xmpp.core.stream.NegotiationElement;
import fr.matoeil.xinshi.xmpp.core.stream.NegotiationElementBuilder;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Attr;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class NegotiationElementHandlerTest
{

    private NegotiationElementHandler target_;

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new NegotiationElementHandler();
    }

    @Test
    public void testgetBuilder()
        throws Exception
    {

        NegotiationElementBuilder actual = target_.getBuilder();

        assertThat( actual, is( instanceOf( NegotiationElementBuilder.class ) ) );
    }

    @Test
    public void testHandleElementOpening()
        throws Exception
    {
        String expectedChildNamespaceURI = "namespace";
        String expectedChildName = "name";
        String expectedChildPrefix = "prefix";

        XmppHandler xmppHandler = target_.handleElementOpening( expectedChildNamespaceURI, expectedChildName,
                                                                expectedChildPrefix + ":" + expectedChildName );

        xmppHandler.startElement( expectedChildNamespaceURI, expectedChildName,
                                  expectedChildPrefix + ":" + expectedChildName, mock( Attributes.class ) );
        NegotiationElement actual = assertBuilt();
        assertThat( actual.getChildNodes().getLength(), is( equalTo( 1 ) ) );
        Node item = actual.getChildNodes().item( 0 );
        assertThat( item.getNamespaceURI(), is( equalTo( expectedChildNamespaceURI ) ) );
        assertThat( item.getLocalName(), is( equalTo( expectedChildName ) ) );
        assertThat( item.getNodeName(), is( equalTo( expectedChildPrefix + ":" + expectedChildName ) ) );
    }

    @Test
    public void testStartPrefixMapping()
        throws Exception
    {
        target_.startPrefixMapping( "prefix", "namespace" );

        assertBuilt();
    }

    @Test
    public void testStartElement()
        throws Exception
    {
        String expectedNamespaceURI = "http://etherx.jabber.org/streams";
        String expectedPrefix = "prefix";
        String expectedName = "name";

        String expectedAttributeNamespaceURI = "attribute-namespace";
        String expectedAttributePrefix = "attribute-prefix";
        String expectedAttributeName = "attribute-name";
        String expectedAttributeValue = "value";

        Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getURI( 0 ) ).thenReturn( expectedAttributeNamespaceURI );
        when( mockedAttributes.getQName( 0 ) ).thenReturn( expectedAttributePrefix + ":" + expectedAttributeName );
        when( mockedAttributes.getValue( 0 ) ).thenReturn( expectedAttributeValue );

        target_.startElement( expectedNamespaceURI, expectedName, expectedPrefix + ":" + expectedName,
                              mockedAttributes );

        NegotiationElement actual = target_.getBuilder().build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getNamespaceURI(), is( equalTo( expectedNamespaceURI ) ) );
        assertThat( actual.getLocalName(), is( equalTo( expectedName ) ) );
        assertThat( actual.getNodeName(), is( equalTo( expectedPrefix + ":" + expectedName ) ) );

        assertThat( actual.getAttributes().getLength(), is( equalTo( 1 ) ) );
        Attr attributes = (Attr) actual.getAttributes().item( 0 );
        assertThat( attributes.getNamespaceURI(), is( equalTo( expectedAttributeNamespaceURI ) ) );
        assertThat( attributes.getLocalName(), is( equalTo( expectedAttributeName ) ) );
        assertThat( attributes.getNodeName(), is( equalTo( expectedAttributePrefix + ":" + expectedAttributeName ) ) );
        assertThat( attributes.getValue(), is( equalTo( expectedAttributeValue ) ) );
    }

    @Test
    public void testStartCDATA()
        throws Exception
    {
        target_.startCDATA();

        assertBuilt();
    }

    @Test
    public void testCharacters_not_in_cdata_section()
        throws Exception
    {

        char[] characters = "        ".toCharArray();

        target_.characters( characters, 0, characters.length );

        NegotiationElement actual = assertBuilt();
        NodeList childNodes = actual.getChildNodes();
        assertThat( childNodes.getLength(), is( equalTo( 1 ) ) );
        assertThat( childNodes.item( 0 ), instanceOf( ImmutableText.class ) );
    }

    @Test
    public void testCharacters_in_cdata_section()
        throws Exception
    {

        char[] characters = "        ".toCharArray();

        target_.startCDATA();
        target_.characters( characters, 0, characters.length );
        target_.endCDATA();

        NegotiationElement actual = assertBuilt();
        NodeList childNodes = actual.getChildNodes();
        assertThat( childNodes.getLength(), is( equalTo( 1 ) ) );
        assertThat( childNodes.item( 0 ), instanceOf( ImmutableCDATASection.class ) );
    }

    @Test
    public void testEndCDATA()
        throws Exception
    {
        target_.endCDATA();

        assertBuilt();
    }


    private NegotiationElement assertBuilt()
    {
        target_.startElement( "http://etherx.jabber.org/streams", "name", "prefix:name", mock( Attributes.class ) );
        NegotiationElement actual = target_.getBuilder().build();
        assertThat( actual, is( notNullValue() ) );
        return actual;
    }
}
