/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream.feature;

import com.google.common.collect.ImmutableList;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableAttributeBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableChildNodeBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.stanza.message.BodyBuilder;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class FeatureTest
{

    private Feature target_;

    private final ImmutableNode mockedImmutableNode_ = mock( ImmutableNode.class );

    private final int expectedElementIndex = 0;

    private final String expectedNamespaceURI = "expectedNamespace";

    private final String expectedPrefix = "prefix";

    private final String expectedLocalName = "priority";

    private final String expectedAttributeNamespaceURI = "attribute-namespace";

    private final String expectedAttributePrefix = "attribute-prefix";

    private final String expectedAttributeLocalName = "attribute-local-name";

    private final String expectedAttributeValue = "attribute-value";

    private final ImmutableAttributeBuilder expectedAttributeBuilder_ =
        new ImmutableAttributeBuilder().inNamespace( expectedAttributeNamespaceURI ).withPrefix(
            expectedAttributePrefix ).named( expectedAttributeLocalName ).withValue( expectedAttributeValue );

    private final List<ImmutableAttributeBuilder> expectedAttributeBuilders_ =
        new ImmutableList.Builder<ImmutableAttributeBuilder>().add( expectedAttributeBuilder_ ).build();

    private final ImmutableChildNodeBuilder expectedChildNodeBuilder_ = new BodyBuilder();

    private final List<ImmutableChildNodeBuilder> expectedChildNodeBuilders_ =
        new ImmutableList.Builder<ImmutableChildNodeBuilder>().add( expectedChildNodeBuilder_ ).build();


    @Before
    public void setUp()
        throws Exception
    {
        target_ = new Feature( mockedImmutableNode_, expectedElementIndex, expectedNamespaceURI, expectedPrefix,
                               expectedLocalName, expectedAttributeBuilders_, expectedChildNodeBuilders_ );
    }

    @Test
    public void testGetTagName()
        throws Exception
    {
        String actual = target_.getTagName();

        assertThat( actual, is( equalTo( expectedPrefix + ":" + expectedLocalName ) ) );
    }

    @Test
    public void testGetAttribute_with_unknow_attribute()
        throws Exception
    {
        String actual = target_.getAttribute( "name" );

        assertThat( actual, isEmptyString() );
    }

    @Test
    public void testGetAttribute_with_know_attribute()
        throws Exception
    {
        String actual = target_.getAttribute( "attribute-prefix:attribute-local-name" );

        assertThat( actual, is( equalTo( expectedAttributeValue ) ) );
    }

    @Test
    public void testGetAttributeNode_with_unknow_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNode( "name" );

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetAttributeNode_with_know_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNode( "attribute-prefix:attribute-local-name" );

        assertThat( actual, is( notNullValue() ) );
    }

    @Test
    public void testGetAttributeNS_with_unknow_attribute()
        throws Exception
    {
        String actual = target_.getAttributeNS( "namespace", "localName" );

        assertThat( actual, isEmptyString() );
    }

    @Test
    public void testGetAttributeNS_with_know_attribute()
        throws Exception
    {

        String actual = target_.getAttributeNS( "attribute-namespace", "attribute-local-name" );

        assertThat( actual, is( equalTo( expectedAttributeValue ) ) );
    }

    @Test
    public void testGetAttributeNodeNS_with_unknow_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNodeNS( "namespace", "localName" );

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetAttributeNodeNS_with_know_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNodeNS( "attribute-namespace", "attribute-local-name" );

        assertThat( actual, is( notNullValue() ) );
    }

    @Test
    public void testHasAttribute_with_unknow_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttribute( "name" );

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Test
    public void testHasAttribute_with_know_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttribute( "attribute-prefix:attribute-local-name" );

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testHasAttributeNS_with_unknow_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttributeNS( "namespace", "localName" );

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Test
    public void testHasAttributeNS_with_know_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttributeNS( "attribute-namespace", "attribute-local-name" );

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testGetChildNodes()
        throws Exception
    {
        NodeList actual = target_.getChildNodes();

        assertThat( actual, is( notNullValue() ) );
    }

    @Test
    public void testGetFirstChild()
        throws Exception
    {
        Node actual = target_.getFirstChild();

        assertThat( actual, is( notNullValue() ) );
    }

    @Test
    public void testGetLastChild()
        throws Exception
    {
        Node actual = target_.getLastChild();

        assertThat( actual, is( notNullValue() ) );
    }

    @Test
    public void testGetAttributes()
        throws Exception
    {
        NamedNodeMap actual = target_.getAttributes();

        assertThat( actual, is( notNullValue() ) );
    }

    @Test
    public void testHasChildNodes()
        throws Exception
    {
        boolean actual = target_.hasChildNodes();

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testGetNamespaceURI()
        throws Exception
    {
        String actual = target_.getNamespaceURI();

        assertThat( actual, is( equalTo( expectedNamespaceURI ) ) );
    }

    @Test
    public void testGetLocalName()
        throws Exception
    {
        String actual = target_.getLocalName();

        assertThat( actual, is( equalTo( expectedLocalName ) ) );
    }

    @Test
    public void testHasAttributes()
        throws Exception
    {
        boolean actual = target_.hasAttributes();

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test( expected = UnsupportedOperationException.class )
    public void testGetTextContent()
        throws Exception
    {
        target_.getTextContent();
    }
}
