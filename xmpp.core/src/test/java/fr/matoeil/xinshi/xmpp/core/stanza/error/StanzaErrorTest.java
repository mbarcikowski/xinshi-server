/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza.error;

import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.address.Addresses;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Element;

import static fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaError.Type;
import static fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorCondition.DefinedCondition;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;

public class StanzaErrorTest
{

    private StanzaError target_;

    private final ImmutableNode mockedImmutableNode_ = mock( ImmutableNode.class );

    private final int expectedElementIndex = 0;

    private final String expectedNamespaceURI = "expectedNamespace";

    private final String expectedPrefix = "prefix";

    private final Type expectedType = Type.CANCEL;

    private final Address expectedBy = Addresses.fromString( "to@Domain" );

    private final DefinedCondition expectedDefinedCondition_ = DefinedCondition.UNDEFINED_CONDITION;

    private final StanzaErrorConditionBuilder expectedStanzaErrorConditionBuilder_ =
        new StanzaErrorConditionBuilder().asDefinedCondition( expectedDefinedCondition_ );

    private final String expectedTextContent = "content";

    private final StanzaErrorTextBuilder expectedStanzaErrorTextBuilder_ =
        new StanzaErrorTextBuilder().withContent( expectedTextContent );

    private final String expectedApplicationConditionLocalName_ = "applicationCondtion";

    private final ImmutableChildElementBuilder applicationConditionBuilder_ =
        new ImmutableChildElementBuilder().named( expectedApplicationConditionLocalName_ );


    @Before
    public void setUp()
        throws Exception
    {
        target_ =
            new StanzaError( mockedImmutableNode_, expectedElementIndex, expectedNamespaceURI, expectedBy, expectedType,
                             expectedStanzaErrorConditionBuilder_, expectedStanzaErrorTextBuilder_,
                             applicationConditionBuilder_ );
    }

    @Test
    public void testGetBy()
        throws Exception
    {
        Address actual = target_.getBy();

        assertThat( actual, is( equalTo( expectedBy ) ) );
    }

    @Test
    public void testGetType()
        throws Exception
    {
        Type actual = target_.getType();

        assertThat( actual, is( equalTo( expectedType ) ) );
    }

    @Test
    public void testGetCondition()
        throws Exception
    {
        StanzaErrorCondition actual = target_.getCondition();

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getDefinedCondition(), is( equalTo( expectedDefinedCondition_ ) ) );
    }

    @Test
    public void testGetText()
        throws Exception
    {
        StanzaErrorText actual = target_.getText();

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getContent(), is( equalTo( expectedTextContent ) ) );

    }

    @Test
    public void testGetApplicationCondition()
        throws Exception
    {

        Element actual = target_.getApplicationCondition();

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getLocalName(), is( equalTo( expectedApplicationConditionLocalName_ ) ) );
    }

    @Test( expected = UnsupportedOperationException.class )
    public void testGetTextContent()
        throws Exception
    {
        target_.getTextContent();
    }

    @Test
    public void testConstructor_without_application_condition()
        throws Exception
    {
        target_ =
            new StanzaError( mockedImmutableNode_, expectedElementIndex, expectedNamespaceURI, expectedBy, expectedType,
                             expectedStanzaErrorConditionBuilder_, expectedStanzaErrorTextBuilder_, null );

        Element actual = target_.getApplicationCondition();

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testConstructor_without_text()
        throws Exception
    {
        target_ =
            new StanzaError( mockedImmutableNode_, expectedElementIndex, expectedNamespaceURI, expectedBy, expectedType,
                             expectedStanzaErrorConditionBuilder_, null, applicationConditionBuilder_ );

        Element actual = target_.getText();

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testConstructor_without_text_or_application_condition()
        throws Exception
    {
        target_ =
            new StanzaError( mockedImmutableNode_, expectedElementIndex, expectedNamespaceURI, expectedBy, expectedType,
                             expectedStanzaErrorConditionBuilder_, null, null );

        assertThat( target_.getText(), is( nullValue() ) );
        assertThat( target_.getApplicationCondition(), is( nullValue() ) );
    }

    public static class TypeTest
    {

        @Test
        public void testAUTHAsString()
            throws Exception
        {
            String actual = Type.AUTH.asString();

            assertThat( actual, is( equalTo( "auth" ) ) );
        }

        @Test
        public void testCANCELAsString()
            throws Exception
        {
            String actual = Type.CANCEL.asString();

            assertThat( actual, is( equalTo( "cancel" ) ) );
        }

        @Test
        public void testCONTINUEAsString()
            throws Exception
        {
            String actual = Type.CONTINUE.asString();

            assertThat( actual, is( equalTo( "continue" ) ) );
        }

        @Test
        public void testMODIFYAsString()
            throws Exception
        {
            String actual = Type.MODIFY.asString();

            assertThat( actual, is( equalTo( "modify" ) ) );
        }

        @Test
        public void testWAITAsString()
            throws Exception
        {
            String actual = Type.WAIT.asString();

            assertThat( actual, is( equalTo( "wait" ) ) );
        }

        @Test
        public void testFromString_in_enum()
            throws Exception
        {
            for ( Type type : Type.values() )
            {
                Type actual = Type.fromString( type.asString() );
                Type expected = type;
                assertThat( actual, is( equalTo( expected ) ) );
            }

        }

        @Test( expected = IllegalArgumentException.class )
        public void testFromString_not_in_enum()
            throws Exception
        {
            Type.fromString( null );
        }
    }


}
