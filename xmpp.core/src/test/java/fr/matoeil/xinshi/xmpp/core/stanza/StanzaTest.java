/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza;

import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.address.Addresses;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNodeList;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaError;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorCondition;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorConditionBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;

public class StanzaTest
{

    private Stanza<Message.Type> target_;

    private final String expectedNamespaceURI = "expectedNamespace";

    private final String expectedLocalName = "expectedLocalName";

    private final String expectedLanguage = "fr";

    private final Address expectedFrom = Addresses.fromString( "from@domain" );

    private final Address expectedTo = Addresses.fromString( "to@Domain" );

    private final String expectedId = "id";

    private final Message.Type expectedType = Message.Type.CHAT;

    private final ImmutableNodeList mockedChildNodes = mock( ImmutableNodeList.class );

    final ImmutableNode mockedParentNode = mock( ImmutableNode.class );

    final StanzaErrorConditionBuilder stanzaErrorConditionBuilder_ =
        new StanzaErrorConditionBuilder().asDefinedCondition(
            StanzaErrorCondition.DefinedCondition.UNDEFINED_CONDITION );

    StanzaErrorBuilder expectedStanzaError = new StanzaErrorBuilder().isChildOf( mockedParentNode ).atIndex( 0 ).ofType(
        StanzaError.Type.AUTH ).withCondition( stanzaErrorConditionBuilder_ );

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new Stanza<Message.Type>( expectedNamespaceURI, expectedLocalName, expectedLanguage, expectedFrom,
                                            expectedTo, expectedId, expectedType )
        {

            {
                initializeError( expectedNamespaceURI, expectedStanzaError, 0 );
                setImmutableChildNodes( mockedChildNodes );
            }
        };


    }

    @After
    public void tearDown()
        throws Exception
    {
    }

    @Test
    public void testGetLanguage()
        throws Exception
    {
        String actual = target_.getLanguage();

        assertThat( actual, is( equalTo( expectedLanguage ) ) );

    }

    @Test
    public void testGetFrom()
        throws Exception
    {
        Address actual = target_.getFrom();

        assertThat( actual, is( equalTo( expectedFrom ) ) );

    }

    @Test
    public void testGetTo()
        throws Exception
    {
        Address actual = target_.getTo();

        assertThat( actual, is( equalTo( expectedTo ) ) );
    }

    @Test
    public void testGetId()
        throws Exception
    {
        String actual = target_.getId();

        assertThat( actual, is( equalTo( expectedId ) ) );

    }

    @Test
    public void testGetType()
        throws Exception
    {
        Type actual = target_.getType();

        assertThat( actual, is( equalTo( (Type) expectedType ) ) );
    }

    @Test
    public void testGetError()
        throws Exception
    {
        StanzaError actual = target_.getStanzaError();

        assertThat( actual, is( notNullValue() ) );
    }

    @Test
    public void testGetAttribute_with_unknow_attribute()
        throws Exception
    {
        String actual = target_.getAttribute( "name" );

        assertThat( actual, isEmptyString() );
    }

    @Test
    public void testGetAttribute_with_know_attribute()
        throws Exception
    {
        String actual = target_.getAttribute( Stanza.FROM_ATTRIBUTE );

        assertThat( actual, is( equalTo( expectedFrom.fullAddress() ) ) );
    }


    @Test
    public void testGetAttributeNode_with_unknow_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNode( "name" );

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetAttributeNode_with_know_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNode( Stanza.FROM_ATTRIBUTE );

        assertThat( actual.getNodeName(), is( equalTo( Stanza.FROM_ATTRIBUTE ) ) );
        assertThat( actual.getNodeValue(), is( equalTo( expectedFrom.fullAddress() ) ) );
    }

    @Test
    public void testGetAttributeNS_with_unknow_attribute()
        throws Exception
    {
        String actual = target_.getAttributeNS( "namespace", "localName" );

        assertThat( actual, isEmptyString() );
    }

    @Test
    public void testGetAttributeNS_with_know_attribute()
        throws Exception
    {
        String actual = target_.getAttributeNS( null, Stanza.FROM_ATTRIBUTE );

        assertThat( actual, is( equalTo( expectedFrom.fullAddress() ) ) );
    }

    @Test
    public void testGetAttributeNodeNS_with_unknow_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNodeNS( "namespace", "localName" );

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetAttributeNodeNS_with_know_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNodeNS( null, Stanza.FROM_ATTRIBUTE );

        assertThat( actual.getNodeName(), is( equalTo( Stanza.FROM_ATTRIBUTE ) ) );
        assertThat( actual.getNodeValue(), is( equalTo( expectedFrom.fullAddress() ) ) );
    }


    @Test
    public void testHasAttribute_with_unknow_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttribute( "name" );

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Test
    public void testHasAttribute_with_know_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttribute( Stanza.FROM_ATTRIBUTE );

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testHasAttributeNS_with_unknow_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttributeNS( "namespace", "localName" );

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Test
    public void testHasAttributeNS_with_know_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttributeNS( null, Stanza.FROM_ATTRIBUTE );

        assertThat( actual, is( equalTo( true ) ) );
    }


    @Test
    public void testGetParentNode()
        throws Exception
    {
        Node actual = target_.getParentNode();

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetPreviousSibling()
        throws Exception
    {
        Node actual = target_.getPreviousSibling();

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetNextSibling()
        throws Exception
    {
        Node actual = target_.getNextSibling();

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetAttributes()
        throws Exception
    {

        NamedNodeMap actual = target_.getAttributes();

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getLength(), is( equalTo( 5 ) ) );
    }


    @Test
    public void testHasAttributes()
        throws Exception
    {
        boolean actual = target_.hasAttributes();

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testHasAttributes_with_stanza_without_any_attributes()
        throws Exception
    {
        Stanza target = new Stanza( expectedNamespaceURI, expectedLocalName, null, null, null, null, null )
        {
            {
                setImmutableChildNodes( mockedChildNodes );
            }
        };
        boolean actual = target.hasAttributes();

        assertThat( actual, is( equalTo( false ) ) );
    }


    @Test( expected = UnsupportedOperationException.class )
    public void testGetTextContent()
        throws Exception
    {
        String actual = target_.getTextContent();
    }
}
