/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.xmpp;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class XmppEnumMapTest
{

    private XmppEnumMap<TestXmppEnum> target_;

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new XmppEnumMap<>( TestXmppEnum.class );
    }

    @Test
    public void testFromString_with_valid_string()
        throws Exception
    {
        TestXmppEnum actual = target_.fromString( "test" );

        assertThat( actual, is( equalTo( TestXmppEnum.TEST ) ) );
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFromString_with_invalid_string()
        throws Exception
    {
        target_.fromString( "bad" );
    }

    private enum TestXmppEnum
        implements XmppEnum
    {
        TEST( "test" );

        private final String name_;

        TestXmppEnum( final String aName )
        {
            name_ = aName;
        }


        @Override
        public String asString()
        {
            return name_;
        }

    }
}
