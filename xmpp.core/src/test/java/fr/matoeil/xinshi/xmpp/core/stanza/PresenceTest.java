/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza;

import com.google.common.collect.ImmutableList;
import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.address.Addresses;
import fr.matoeil.xinshi.xmpp.core.internal.dom.EmptyImmutableNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaError;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorCondition;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorConditionBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.Priority;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.PriorityBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.Show;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.ShowBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.Status;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.StatusBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

import static fr.matoeil.xinshi.xmpp.core.stanza.Presence.Type.*;
import static fr.matoeil.xinshi.xmpp.core.stanza.presence.Show.State.AWAY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;

public class PresenceTest
{

    private Presence target_;

    private final ImmutableNode mockedImmutableNode_ = mock( ImmutableNode.class );

    private final String expectedNamespaceURI = "expectedNamespace";

    private final String expectedPrefix = "prefix";

    private final String expectedLocalName = "presence";

    private final String expectedLanguage = "fr";

    private final Address expectedFrom = Addresses.fromString( "from@domain" );

    private final Address expectedTo = Addresses.fromString( "to@Domain" );

    private final String expectedId = "id";

    private final int expectedShowElementIndex = 0;

    private final Presence.Type expectedType = Presence.Type.UNAVAILABLE;

    private final ShowBuilder expectedShow =
        new ShowBuilder().isChildOf( mockedImmutableNode_ ).atIndex( expectedShowElementIndex ).inNamespace(
            expectedNamespaceURI ).withState( AWAY );

    private final int expectedStatusElementIndex = 1;

    private final String expectedStatusContent = "a content";

    private final StatusBuilder expectedStatus =
        new StatusBuilder().isChildOf( mockedImmutableNode_ ).atIndex( expectedStatusElementIndex ).inNamespace(
            expectedNamespaceURI ).withContent( expectedStatusContent );

    private final List<StatusBuilder> expectedStatuses =
        new ImmutableList.Builder<StatusBuilder>().add( expectedStatus ).build();

    private final int expectedPriorityElementIndex = 2;

    private final PriorityBuilder expectedPriority =
        new PriorityBuilder().isChildOf( mockedImmutableNode_ ).atIndex( expectedPriorityElementIndex ).inNamespace(
            expectedNamespaceURI ).withValue( Byte.valueOf( "0" ) );


    private final int expectedErrorElementIndex = 3;

    private final Address expectedBy_ = Addresses.fromString( "service.domain.com" );

    private final StanzaError.Type expectedErrorType_ = StanzaError.Type.AUTH;

    private StanzaErrorConditionBuilder expeectedStanzaErrorConditionBuilder_ =
        new StanzaErrorConditionBuilder().asDefinedCondition(
            StanzaErrorCondition.DefinedCondition.UNDEFINED_CONDITION );

    private final StanzaErrorBuilder expectedError =
        new StanzaErrorBuilder().isChildOf( mockedImmutableNode_ ).atIndex( expectedErrorElementIndex ).inNamespace(
            expectedNamespaceURI ).by( expectedBy_ ).ofType( expectedErrorType_ ).withCondition(
            expeectedStanzaErrorConditionBuilder_ );

    private final ExtendedContentBuilder expectedExtendedContent =
        new ExtendedContentBuilder().inNamespace( "extension-namespace" ).named( "extension" );

    private final List<ExtendedContentBuilder> expectedExtendedContents =
        new ImmutableList.Builder<ExtendedContentBuilder>().add( expectedExtendedContent ).build();

    @Before
    public void setUp()
        throws Exception
    {
        target_ =
            new Presence( expectedNamespaceURI, expectedLanguage, expectedFrom, expectedTo, expectedId, expectedType,
                          expectedShow, expectedStatuses, expectedPriority, expectedError, expectedExtendedContents );
    }

    @After
    public void tearDown()
        throws Exception
    {
    }

    @Test
    public void testGetShow()
        throws Exception
    {
        Show actualShow = target_.getShow();

        assertThat( actualShow, is( notNullValue() ) );
    }

    @Test
    public void testGetStatuses()
        throws Exception
    {
        List<Status> actualStatuses = target_.getStatuses();

        assertThat( actualStatuses.size(), is( equalTo( expectedStatuses.size() ) ) );
    }

    @Test
    public void testGetPriority()
        throws Exception
    {
        Priority actualPriority = target_.getPriority();

        assertThat( actualPriority, is( notNullValue() ) );
    }

    @Test
    public void testGetError()
        throws Exception
    {
        StanzaError actualStanzaError = target_.getStanzaError();

        assertThat( actualStanzaError, is( notNullValue() ) );
    }

    @Test
    public void testGetExtendedContents()
        throws Exception
    {
        List<? extends Element> actualExtendedContents = target_.getExtendedContents();

        assertThat( actualExtendedContents.size(), is( equalTo( expectedExtendedContents.size() ) ) );
    }

    @Test
    public void testGetLocalName()
        throws Exception
    {
        String actual = target_.getLocalName();

        assertThat( actual, is( equalTo( expectedLocalName ) ) );
    }

    @Test
    public void testConstructor_with_no_show()
        throws Exception
    {
        target_ =
            new Presence( expectedNamespaceURI, expectedLanguage, expectedFrom, expectedTo, expectedId, expectedType,
                          null, expectedStatuses, expectedPriority, expectedError, expectedExtendedContents );

        Show actualShow = target_.getShow();
        assertThat( actualShow, is( nullValue() ) );

        int expectedLength = 0 + expectedStatuses.size() + 1 + 1 + expectedExtendedContents.size();
        NodeList actualChildNodes = target_.getChildNodes();
        assertThat( actualChildNodes.getLength(), is( equalTo( expectedLength ) ) );
    }

    @Test
    public void testConstructor_with_no_priority()
        throws Exception
    {
        target_ =
            new Presence( expectedNamespaceURI, expectedLanguage, expectedFrom, expectedTo, expectedId, expectedType,
                          expectedShow, expectedStatuses, null, expectedError, expectedExtendedContents );

        Priority actualPriority = target_.getPriority();
        assertThat( actualPriority, is( nullValue() ) );

        int expectedLength = 1 + expectedStatuses.size() + 0 + 1 + expectedExtendedContents.size();
        NodeList actualChildNodes = target_.getChildNodes();
        assertThat( actualChildNodes.getLength(), is( equalTo( expectedLength ) ) );
    }

    @Test
    public void testConstructor_with_no_error()
        throws Exception
    {
        target_ =
            new Presence( expectedNamespaceURI, expectedLanguage, expectedFrom, expectedTo, expectedId, expectedType,
                          expectedShow, expectedStatuses, expectedPriority, null, expectedExtendedContents );

        StanzaError actualStanzaError = target_.getStanzaError();
        assertThat( actualStanzaError, is( nullValue() ) );

        int expectedLength = 1 + expectedStatuses.size() + 1 + 0 + expectedExtendedContents.size();
        NodeList actualChildNodes = target_.getChildNodes();
        assertThat( actualChildNodes.getLength(), is( equalTo( expectedLength ) ) );
    }


    @Test
    public void testConstructor_with_no_child()
        throws Exception
    {
        List<StatusBuilder> statusBuilders = new ArrayList<>();
        List<ExtendedContentBuilder> extendedContentsBuilders = new ArrayList<>();

        target_ =
            new Presence( expectedNamespaceURI, expectedLanguage, expectedFrom, expectedTo, expectedId, expectedType,
                          null, statusBuilders, null, null, extendedContentsBuilders );

        Show actualShow = target_.getShow();
        assertThat( actualShow, is( nullValue() ) );

        List<Status> actualStatuses = target_.getStatuses();
        assertThat( actualStatuses.size(), is( equalTo( 0 ) ) );

        Priority actualPriority = target_.getPriority();
        assertThat( actualPriority, is( nullValue() ) );

        StanzaError actualStanzaError = target_.getStanzaError();
        assertThat( actualStanzaError, is( nullValue() ) );

        NodeList actualChildNodes = target_.getChildNodes();
        assertThat( actualChildNodes, is( instanceOf( EmptyImmutableNodeList.class ) ) );
    }

    public static class TypeTest
    {

        @Test
        public void testERRORAsString()
            throws Exception
        {
            String actual = ERROR.asString();

            assertThat( actual, is( equalTo( "error" ) ) );
        }

        @Test
        public void testPROBEAsString()
            throws Exception
        {
            String actual = PROBE.asString();

            assertThat( actual, is( equalTo( "probe" ) ) );
        }

        @Test
        public void testSUBSCRIBEAsString()
            throws Exception
        {
            String actual = SUBSCRIBE.asString();

            assertThat( actual, is( equalTo( "subscribe" ) ) );
        }

        @Test
        public void testSUBSCRIBEDAsString()
            throws Exception
        {
            String actual = SUBSCRIBED.asString();

            assertThat( actual, is( equalTo( "subscribed" ) ) );
        }

        @Test
        public void testUNAVAILABLEAsString()
            throws Exception
        {
            String actual = UNAVAILABLE.asString();

            assertThat( actual, is( equalTo( "unavailable" ) ) );
        }

        @Test
        public void testUNSUBSCRIBEAsString()
            throws Exception
        {
            String actual = UNSUBSCRIBE.asString();

            assertThat( actual, is( equalTo( "unsubscribe" ) ) );
        }

        @Test
        public void testUNSUBSCRIBEDAsString()
            throws Exception
        {
            String actual = UNSUBSCRIBED.asString();

            assertThat( actual, is( equalTo( "unsubscribed" ) ) );
        }

        @Test
        public void testFromString_in_enum()
            throws Exception
        {
            for ( Presence.Type type : Presence.Type.values() )
            {
                Presence.Type actual = Presence.Type.fromString( type.asString() );
                Type expected = type;
                assertThat( actual, is( equalTo( expected ) ) );
            }

        }

        @Test( expected = IllegalArgumentException.class )
        public void testFromString_not_in_enum()
            throws Exception
        {
            Presence.Type.fromString( null );
        }
    }
}
