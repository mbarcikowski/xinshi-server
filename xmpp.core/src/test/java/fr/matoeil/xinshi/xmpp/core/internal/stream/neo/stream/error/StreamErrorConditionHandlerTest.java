/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stream.error;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import fr.matoeil.xinshi.xmpp.core.stream.error.StreamErrorCondition;
import fr.matoeil.xinshi.xmpp.core.stream.error.StreamErrorConditionBuilder;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.Attributes;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class StreamErrorConditionHandlerTest
{
    private StreamErrorConditionHandler target_;

    private final String expectedDefinitionConditionName = "connection-timeout";

    private final String expectedNamespaceURI = "urn:ietf:params:xml:ns:xmpp-streams";

    private final ImmutableNode mockedParentNode_ = mock( ImmutableNode.class );

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new StreamErrorConditionHandler();

    }

    @Test
    public void testGetConditionBuilder()
        throws Exception
    {
        StreamErrorConditionBuilder actual = target_.getConditionBuilder();

        assertThat( actual, is( instanceOf( StreamErrorConditionBuilder.class ) ) );
    }

    @Test
    public void testStartElement_with_valid_name()
        throws Exception
    {

        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( eq( 0 ) ) ).thenReturn( "xmlns" );
        when( mockedAttributes.getValue( eq( 0 ) ) ).thenReturn( expectedNamespaceURI );
        target_.startElement( "namespace", expectedDefinitionConditionName, expectedDefinitionConditionName,
                              mockedAttributes );

        StreamErrorCondition actual = target_.getConditionBuilder().isChildOf( mockedParentNode_ ).atIndex( 0 ).build();
        assertThat( actual.getDefinedCondition(),
                    is( equalTo( StreamErrorCondition.DefinedCondition.CONNECTION_TIMEOUT ) ) );
    }

    @Test( expected = StreamException.class )
    public void testStartElement_with_unkown_name()
        throws Exception
    {

        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( eq( 0 ) ) ).thenReturn( "xmlns" );
        when( mockedAttributes.getValue( eq( 0 ) ) ).thenReturn( expectedNamespaceURI );
        target_.startElement( "namespace", "othername", "othername", mockedAttributes );
    }

    @Test
    public void testStartElement_with_correct_xmlns()
        throws Exception
    {
        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( eq( 0 ) ) ).thenReturn( "xmlns" );
        when( mockedAttributes.getValue( eq( 0 ) ) ).thenReturn( expectedNamespaceURI );
        target_.startElement( "namespace", expectedDefinitionConditionName, expectedDefinitionConditionName,
                              mockedAttributes );

        StreamErrorCondition actual = target_.getConditionBuilder().isChildOf( mockedParentNode_ ).atIndex( 0 ).build();
        assertThat( actual, is( notNullValue() ) );
    }

    @Test( expected = StreamException.class )
    public void testStartElement_with_incorrect_xmlns()
        throws Exception
    {
        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( eq( 0 ) ) ).thenReturn( "xmlns" );
        when( mockedAttributes.getValue( eq( 0 ) ) ).thenReturn( "other_namespace" );
        target_.startElement( "namespace", "name", "name", mockedAttributes );
    }


    @Test( expected = StreamException.class )
    public void testStartCDATA()
        throws Exception
    {
        target_.startCDATA();
    }

    @Test
    public void testCharacters()
        throws Exception
    {
        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( eq( 0 ) ) ).thenReturn( "xmlns" );
        when( mockedAttributes.getValue( eq( 0 ) ) ).thenReturn( expectedNamespaceURI );
        target_.startElement( "namespace", expectedDefinitionConditionName, expectedDefinitionConditionName,
                              mockedAttributes );

        String expectedContent = "content";
        char[] chars = expectedContent.toCharArray();

        target_.characters( chars, 0, chars.length );

        StreamErrorCondition actual = target_.getConditionBuilder().isChildOf( mockedParentNode_ ).atIndex( 0 ).build();
        assertThat( actual.getContent(), is( equalTo( expectedContent ) ) );
    }

    @Test( expected = StreamException.class )
    public void testEndCDATA()
        throws Exception
    {
        target_.endCDATA();
    }

    @Test
    public void testGetDefinedCondition()
        throws Exception
    {
        StreamErrorCondition.DefinedCondition actual = target_.getDefinedCondition( expectedDefinitionConditionName );

        assertThat( actual, is( instanceOf( StreamErrorCondition.DefinedCondition.class ) ) );
    }


}
