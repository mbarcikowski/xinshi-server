/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream;

import fr.matoeil.xinshi.xmpp.core.dom.ImmutableChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.stream.error.StreamErrorCondition;
import fr.matoeil.xinshi.xmpp.core.stream.error.StreamErrorConditionBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.error.StreamErrorTextBuilder;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Element;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;

public class StreamErrorBuilderTest
{
    private StreamErrorBuilder target_;

    private final ImmutableNode mockedImmutableNode = mock( ImmutableNode.class );

    private final String expectedNamespaceURI = "urn:ietf:params:xml:ns:xmpp-streams";

    private final String expectedLocalName = "error";

    private final StreamErrorCondition.DefinedCondition expectedCondition_ =
        StreamErrorCondition.DefinedCondition.UNDEFINED_CONDITION;

    private StreamErrorConditionBuilder expectedStreamErrorConditionBuilder_ =
        new StreamErrorConditionBuilder().asDefinedCondition( expectedCondition_ );

    private StreamErrorTextBuilder expectedStreamErrorTextBuilder_ = new StreamErrorTextBuilder();

    private ImmutableChildElementBuilder expectedApplicationConditionBuilder =
        new ImmutableChildElementBuilder().isChildOf( mockedImmutableNode ).atIndex( 0 ).named(
            "application-condition" );

    private final String expectedPrefix = "stream";

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new StreamErrorBuilder();

    }

    @Test( expected = IllegalArgumentException.class )
    public void testWithCondition_with_invalid_conditionBuilder()
    {
        target_.withCondition( null );
    }

    @Test( expected = IllegalArgumentException.class )
    public void testWithCondition_with_invalid_application_condition_builder()
    {
        target_.withApplicationCondition( null );
    }

    @Test( expected = IllegalStateException.class )
    public void testWithCondition_with_application_condition_builder_twice()
    {
        target_.withApplicationCondition( expectedApplicationConditionBuilder );
        target_.withApplicationCondition( expectedApplicationConditionBuilder );
    }

    @Test( expected = IllegalArgumentException.class )
    public void testWithCondition_with_text_builder()
    {
        target_.withText( null );
    }

    @Test( expected = IllegalStateException.class )
    public void testWithCondition_with_text_builder_twice()
    {
        target_.withText( expectedStreamErrorTextBuilder_ );
        target_.withText( expectedStreamErrorTextBuilder_ );
    }


    @Test( expected = IllegalStateException.class )
    public void testBuild_with_invalid_conditionBuilder()
    {
        target_.build();
    }

    @Test
    public void testBuild()
    {
        StreamError actual =
            target_.withPrefix( expectedPrefix ).withCondition( expectedStreamErrorConditionBuilder_ ).withText(
                expectedStreamErrorTextBuilder_ ).withApplicationCondition(
                expectedApplicationConditionBuilder ).build();

        assertThat( actual.getParentNode(), is( nullValue() ) );
        assertThat( actual.getNamespaceURI(), is( equalTo( expectedNamespaceURI ) ) );
        assertThat( actual.getLocalName(), is( equalTo( expectedLocalName ) ) );
        assertThat( actual.getNodeName(), is( equalTo( expectedPrefix + ":" + expectedLocalName ) ) );

        StreamErrorCondition streamErrorCondition = actual.getCondition();
        assertThat( streamErrorCondition.getDefinedCondition(), is( equalTo( expectedCondition_ ) ) );

        Element text = actual.getText();
        assertThat( text, is( notNullValue() ) );

        Element applicationCondition = actual.getApplicationCondition();
        assertThat( applicationCondition, is( notNullValue() ) );
    }
}
