/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream.feature.session;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.stream.feature.Feature;
import fr.matoeil.xinshi.xmpp.core.stream.feature.FeatureBuilder;
import org.junit.Test;

import java.lang.reflect.Constructor;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class SessionElementsTest
{
    @Test
    public void testConstructor()
        throws Exception
    {
        Constructor target = SessionElements.class.getDeclaredConstructor();

        assertThat( target.isAccessible(), is( equalTo( false ) ) );

        target.setAccessible( true );
        target.newInstance();
    }

    @Test
    public void testCreateBindFeatureBuilder()
        throws Exception
    {
        String expectedNamespaceUri = "urn:ietf:params:xml:ns:xmpp-session";

        FeatureBuilder builder = SessionElements.createSessionFeatureBuilder();

        Feature actual = builder.isChildOf( mock( ImmutableNode.class ) ).atIndex( 0 ).build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getNamespaceURI(), is( equalTo( expectedNamespaceUri ) ) );
        assertThat( actual.getLocalName(), is( equalTo( "session" ) ) );
        assertThat( actual.getChildNodes().getLength(), is( equalTo( 0 ) ) );
    }
}
