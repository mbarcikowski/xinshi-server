/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.xmpp;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;

import javax.annotation.Nullable;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class TextBuilderTest
{
    private TextBuilder target_;

    private final ImmutableNode mockedImmutableNode = mock( ImmutableNode.class );

    private final String expectedNamespaceURI = "urn:ietf:params:xml:ns:xmpp-stanzas";

    private final String expectedLocalName = "text";

    private final String expectedLanguage = "fr";

    private final String expectedContent = "a content";

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new TestTextBuilder();

    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsChildOf_with_invalid_parent()
    {
        target_.isChildOf( null );
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAtIndex_with_invalid_index()
    {
        target_.atIndex( -1 );
    }

    @Test(expected = IllegalStateException.class)
    public void testBuild_with_invalid_parent()
    {
        target_.build();
    }

    @Test(expected = IllegalStateException.class)
    public void testBuild_with_invalid_index()
    {
        target_.isChildOf( mockedImmutableNode ).build();
    }


    @Test
    public void testBuild()
    {

        Text actual = target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inLanguage( expectedLanguage ).withContent(
            expectedContent ).build();

        assertThat( actual.getParentNode(), is( equalTo( (Node) mockedImmutableNode ) ) );
        assertThat( actual.getNamespaceURI(), is( equalTo( expectedNamespaceURI ) ) );
        assertThat( actual.getLocalName(), is( equalTo( expectedLocalName ) ) );
        assertThat( actual.getLanguage(), is( equalTo( expectedLanguage ) ) );
        assertThat( actual.getContent(), is( equalTo( expectedContent ) ) );
    }

    private class TestText
        extends Text
    {
        public TestText( final ImmutableNode aParentNode, final Integer aElementIndex, final String aLanguage,
                         final String aContent )
        {
            super( aParentNode, aElementIndex, expectedNamespaceURI, aLanguage, aContent );
        }
    }

    private class TestTextBuilder
        extends TextBuilder
    {
        @Override
        protected Text newText( final ImmutableNode aParentNode, final Integer aElementIndex,
                                @Nullable final String aLanguage, @Nullable final String aContent )
        {
            return new TestText( aParentNode, aElementIndex, aLanguage, aContent );
        }


    }
}
