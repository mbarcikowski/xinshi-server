/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream.feature.tls;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.stream.NegotiationElement;
import fr.matoeil.xinshi.xmpp.core.stream.feature.Feature;
import fr.matoeil.xinshi.xmpp.core.stream.feature.FeatureBuilder;
import org.junit.Test;
import org.w3c.dom.Node;

import java.lang.reflect.Constructor;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class TlsElementsTest
{
    @Test
    public void testConstructor()
        throws Exception
    {
        Constructor target = TlsElements.class.getDeclaredConstructor();

        assertThat( target.isAccessible(), is( equalTo( false ) ) );

        target.setAccessible( true );
        target.newInstance();
    }

    @Test
    public void testTlsStartTls()
        throws Exception
    {
        NegotiationElement actual = TlsElements.TLS_STARTTLS;

        assertThat( actual, is( notNullValue() ) );
    }

    @Test
    public void testTlsFailure()
        throws Exception
    {
        NegotiationElement actual = TlsElements.TLS_FAILURE;

        assertThat( actual, is( notNullValue() ) );
    }

    @Test
    public void testTlsProceed()
        throws Exception
    {
        NegotiationElement actual = TlsElements.TLS_PROCEED;

        assertThat( actual, is( notNullValue() ) );
    }

    @Test
    public void testCreateTlsFeatureBuilder()
        throws Exception
    {
        String expectedNamespaceUri = "urn:ietf:params:xml:ns:xmpp-tls";

        FeatureBuilder builder = TlsElements.createTlsFeatureBuilder();

        Feature actual = builder.isChildOf( mock( ImmutableNode.class ) ).atIndex( 0 ).build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getNamespaceURI(), is( equalTo( expectedNamespaceUri ) ) );
        assertThat( actual.getLocalName(), is( equalTo( "starttls" ) ) );
        assertThat( actual.getChildNodes().getLength(), is( equalTo( 0 ) ) );
    }

    @Test
    public void testRequiredCreateTlsFeatureBuilder()
        throws Exception
    {
        String expectedNamespaceUri = "urn:ietf:params:xml:ns:xmpp-tls";

        FeatureBuilder builder = TlsElements.createRequiredTlsFeatureBuilder();

        Feature actual = builder.isChildOf( mock( ImmutableNode.class ) ).atIndex( 0 ).build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getNamespaceURI(), is( equalTo( expectedNamespaceUri ) ) );
        assertThat( actual.getLocalName(), is( equalTo( "starttls" ) ) );
        assertThat( actual.getChildNodes().getLength(), is( equalTo( 1 ) ) );

        Node requiredNode = actual.getChildNodes().item( 0 );
        assertThat( requiredNode.getNamespaceURI(), is( equalTo( expectedNamespaceUri ) ) );
        assertThat( requiredNode.getLocalName(), is( equalTo( "required" ) ) );
        assertThat( requiredNode.getChildNodes().getLength(), is( equalTo( 0 ) ) );
    }
}
