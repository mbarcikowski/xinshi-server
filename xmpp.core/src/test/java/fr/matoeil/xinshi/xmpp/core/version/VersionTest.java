/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.version;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class VersionTest
{

    private Version target_;

    private final int expectedMajor_ = 1;

    private final int expectedMinor_ = 1;

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new Version( expectedMajor_, expectedMinor_ );
    }

    @Test
    public void testGetMajor()
        throws Exception
    {
        int actual = target_.getMajor();

        assertThat( actual, is( equalTo( expectedMajor_ ) ) );
    }

    @Test
    public void testGetMinor()
        throws Exception
    {
        int actual = target_.getMinor();

        assertThat( actual, is( equalTo( expectedMinor_ ) ) );
    }

    @Test
    public void testEquals_with_same()
        throws Exception
    {
        boolean actual = target_.equals( target_ );

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testEquals_with_equals()
        throws Exception
    {
        final Version version = new Version( expectedMajor_, expectedMinor_ );
        boolean actual = target_.equals( version );

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testEquals_with_null()
        throws Exception
    {
        boolean actual = target_.equals( null );

        assertThat( actual, is( equalTo( false ) ) );
    }


    @Test
    public void testEquals_with_major_different()
        throws Exception
    {
        final Version version = new Version( expectedMajor_ + 1, expectedMinor_ );
        boolean actual = target_.equals( version );

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Test
    public void testEquals_with_minor_different()
        throws Exception
    {
        final Version version = new Version( expectedMajor_, expectedMinor_ + 1 );
        boolean actual = target_.equals( version );

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Test
    public void testHashCode()
        throws Exception
    {
        final Version version = new Version( expectedMajor_, expectedMinor_ );
        int expectedHashCode = version.hashCode();

        int actual = target_.hashCode();

        assertThat( actual, is( equalTo( expectedHashCode ) ) );
    }

    @Test
    public void testToString()
        throws Exception
    {
        String actual = target_.toString();

        assertThat( actual, is( notNullValue() ) );
    }
}
