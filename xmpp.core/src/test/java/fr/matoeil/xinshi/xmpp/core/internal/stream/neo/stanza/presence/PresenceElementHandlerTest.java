/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.presence;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableBaseChildElement;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableBaseChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableBaseElement;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.DOMException;
import org.xml.sax.Attributes;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PresenceElementHandlerTest
{

    private PresenceElementHandler target_;

    private final String expectedLocalName_ = "name";

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new TestPresenceElementHandler();
    }

    @Test
    public void testGetBuilder()
        throws Exception
    {
        ImmutableBaseChildElementBuilder actual = target_.getBuilder();

        assertThat( actual, is( instanceOf( TestImmutableBaseChildElementBuilder.class ) ) );
    }

    @Test(expected = StreamException.class)
    public void testHandleElementOpening()
        throws Exception
    {
        target_.handleElementOpening( "namespace", "name", "prefix;name" );
    }

    @Test(expected = StreamException.class)
    public void testStartPrefixMapping()
        throws Exception
    {
        target_.startPrefixMapping( "prefix", "namespace" );
    }

    @Test(expected = StreamException.class)
    public void testStartElement_with_attributes()
        throws Exception
    {
        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );

        target_.startElement( "namespace", "name", "prefix:name", mockedAttributes );
    }

    @Test
    public void testStartElement_without_attribute()
        throws Exception
    {
        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 0 );

        target_.startElement( "namespace", "name", "prefix:name", mockedAttributes );

        ImmutableBaseElement actual =
            target_.getBuilder().isChildOf( mock( ImmutableNode.class ) ).atIndex( 0 ).build();
        assertThat( actual, is( notNullValue() ) );
    }

    @Test(expected = StreamException.class)
    public void testStartCDATA()
        throws Exception
    {
        target_.startCDATA();
    }

    @Test(expected = StreamException.class)
    public void testEndCDATA()
        throws Exception
    {
        target_.endCDATA();
    }


    private class TestImmutableBaseChildElement
        extends ImmutableBaseChildElement
    {
        public TestImmutableBaseChildElement( final ImmutableNode aParentNode, final Integer aElementIndex,
                                              final String aNamespaceURI )
        {
            super( aParentNode, aElementIndex, aNamespaceURI, null,
                   PresenceElementHandlerTest.this.expectedLocalName_ );
        }

        @Override
        public String getTextContent()
            throws DOMException
        {
            throw new UnsupportedOperationException( "#getTextContent not implemented" );
        }
    }

    private class TestImmutableBaseChildElementBuilder
        extends ImmutableBaseChildElementBuilder<TestImmutableBaseChildElement, TestImmutableBaseChildElementBuilder>
    {
        @Override
        protected TestImmutableBaseChildElement newImmutableChildElement( final ImmutableNode aParentNode,
                                                                          final Integer aElementIndex,
                                                                          final String aNamespaceURI )
        {
            return new TestImmutableBaseChildElement( aParentNode, aElementIndex, aNamespaceURI );
        }
    }

    private class TestPresenceElementHandler
        extends PresenceElementHandler<TestImmutableBaseChildElement, TestImmutableBaseChildElementBuilder>
    {
        public TestPresenceElementHandler()
        {
            super( new TestImmutableBaseChildElementBuilder() );
        }

        @Override
        public void characters( final char[] aChars, final int aStart, final int aLength )
        {

        }
    }
}
