/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream;

import org.junit.Before;
import org.junit.Test;

import javax.annotation.Nullable;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class StreamElementBuilderTest
{

    private StreamElementBuilder target_;

    private final String expectedNamespaceURI = "namespace";

    private String expectedLocalName = "localName";

    private String expectedPrefix = "prefix";

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new TestStreamElementBuilder();
    }

    @Test
    public void testWithPrefix()
        throws Exception
    {

    }

    @Test
    public void testBuild()
        throws Exception
    {
        StreamElement actual = target_.withPrefix( expectedPrefix ).build();

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getNamespaceURI(), is( equalTo( expectedNamespaceURI ) ) );
        assertThat( actual.getLocalName(), is( equalTo( expectedLocalName ) ) );
        assertThat( actual.getNodeName(), is( equalTo( expectedPrefix + ":" + expectedLocalName ) ) );
    }

    private class TestStreamElement
        extends StreamElement
    {
        public TestStreamElement( final String aPrefix )
        {
            super( expectedNamespaceURI, aPrefix, expectedLocalName );
        }
    }

    private class TestStreamElementBuilder
        extends StreamElementBuilder
    {
        @Override
        protected StreamElement newStreamElement( @Nullable final String aPrefix )
        {
            return new TestStreamElement( aPrefix );
        }


    }
}
