/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza;

import com.google.common.collect.ImmutableList;
import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.address.Addresses;
import fr.matoeil.xinshi.xmpp.core.internal.dom.EmptyImmutableNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaError;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorCondition;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorConditionBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.message.Body;
import fr.matoeil.xinshi.xmpp.core.stanza.message.BodyBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.message.Subject;
import fr.matoeil.xinshi.xmpp.core.stanza.message.SubjectBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.message.Thread;
import fr.matoeil.xinshi.xmpp.core.stanza.message.ThreadBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

import static fr.matoeil.xinshi.xmpp.core.stanza.Message.Type.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;

public class MessageTest
{

    private Message target_;

    private final ImmutableNode mockedImmutableNode_ = mock( ImmutableNode.class );

    private final String expectedNamespaceURI = "expectedNamespace";

    private final String expectedPrefix = "prefix";

    private final String expectedLocalName = "message";

    private final String expectedLanguage = "fr";

    private final Address expectedFrom = Addresses.fromString( "from@domain" );

    private final Address expectedTo = Addresses.fromString( "to@Domain" );

    private final String expectedId = "id";

    private final Message.Type expectedType = Message.Type.CHAT;

    private final int expectedBodyElementIndex = 0;

    private final String expectedBodyContent = "a content";

    private final BodyBuilder expectedBody =
        new BodyBuilder().isChildOf( mockedImmutableNode_ ).atIndex( expectedBodyElementIndex ).inNamespace(
            expectedNamespaceURI ).withContent( expectedBodyContent );


    private final List<BodyBuilder> expectedBodies =
        new ImmutableList.Builder<BodyBuilder>().add( expectedBody ).build();

    private final int expectedSubjectElementIndex = 1;

    private final String expectedSubjectContent = "a content";

    private final SubjectBuilder expectedSubject =
        new SubjectBuilder().isChildOf( mockedImmutableNode_ ).atIndex( expectedSubjectElementIndex ).inNamespace(
            expectedNamespaceURI ).withContent( expectedSubjectContent );

    private final List<SubjectBuilder> expectedSubjects =
        new ImmutableList.Builder<SubjectBuilder>().add( expectedSubject ).build();

    private final int expectedThreadElementIndex = 2;

    private final String expectedThreadParentId = "parent_id";

    private final String expectedThreadId = "thread_id";

    private final ThreadBuilder expectedThread =
        new ThreadBuilder().isChildOf( mockedImmutableNode_ ).atIndex( expectedThreadElementIndex ).inNamespace(
            expectedNamespaceURI ).hasParentThread( expectedThreadParentId ).identifiedBy( expectedThreadId );

    private final int expectedErrorElementIndex = 3;

    private final Address expectedBy_ = Addresses.fromString( "service.domain.com" );

    private final StanzaError.Type expectedErrorType_ = StanzaError.Type.AUTH;

    private StanzaErrorConditionBuilder expectedStanzaErrorConditionBuilder_ =
        new StanzaErrorConditionBuilder().asDefinedCondition(
            StanzaErrorCondition.DefinedCondition.UNDEFINED_CONDITION );

    private final StanzaErrorBuilder expectedError =
        new StanzaErrorBuilder().isChildOf( mockedImmutableNode_ ).atIndex( expectedErrorElementIndex ).inNamespace(
            expectedNamespaceURI ).by( expectedBy_ ).ofType( expectedErrorType_ ).withCondition(
            expectedStanzaErrorConditionBuilder_ );

    private final ExtendedContentBuilder expectedExtendedContent =
        new ExtendedContentBuilder().inNamespace( "extension-namespace" ).named( "extension" );

    private final List<ExtendedContentBuilder> expectedExtendedContents =
        new ImmutableList.Builder<ExtendedContentBuilder>().add( expectedExtendedContent ).build();

    @Before
    public void setUp()
        throws Exception
    {
        target_ =
            new Message( expectedNamespaceURI, expectedLanguage, expectedFrom, expectedTo, expectedId, expectedType,
                         expectedBodies, expectedSubjects, expectedThread, expectedError, expectedExtendedContents );
    }

    @After
    public void tearDown()
        throws Exception
    {
    }

    @Test
    public void testGetBodies()
        throws Exception
    {
        List<Body> actualBodies = target_.getBodies();

        assertThat( actualBodies.size(), is( equalTo( expectedBodies.size() ) ) );
    }

    @Test
    public void testGetSubjects()
        throws Exception
    {
        List<Subject> actualSubjects = target_.getSubjects();

        assertThat( actualSubjects.size(), is( equalTo( expectedSubjects.size() ) ) );
    }

    @Test
    public void testGetThread()
        throws Exception
    {
        Thread actualThread = target_.getThread();

        assertThat( actualThread, is( notNullValue() ) );
    }

    @Test
    public void testGetError()
        throws Exception
    {
        StanzaError actualStanzaError = target_.getStanzaError();

        assertThat( actualStanzaError, is( notNullValue() ) );
    }

    @Test
    public void testGetExtendedContents()
        throws Exception
    {
        List<? extends Element> actualExtendedContents = target_.getExtendedContents();

        assertThat( actualExtendedContents.size(), is( equalTo( expectedExtendedContents.size() ) ) );
    }

    @Test
    public void testGetLocalName()
        throws Exception
    {
        String actual = target_.getLocalName();

        assertThat( actual, is( equalTo( expectedLocalName ) ) );
    }

    @Test
    public void testConstructor_with_no_thread()
        throws Exception
    {
        target_ =
            new Message( expectedNamespaceURI, expectedLanguage, expectedFrom, expectedTo, expectedId, expectedType,
                         expectedBodies, expectedSubjects, null, expectedError, expectedExtendedContents );

        Thread actualThread = target_.getThread();
        assertThat( actualThread, is( nullValue() ) );

        int expectedLength = expectedBodies.size() + expectedSubjects.size() + 0 + 1 + expectedExtendedContents.size();
        NodeList actualChildNodes = target_.getChildNodes();
        assertThat( actualChildNodes.getLength(), is( equalTo( expectedLength ) ) );
    }

    @Test
    public void testConstructor_with_no_error()
        throws Exception
    {
        target_ =
            new Message( expectedNamespaceURI, expectedLanguage, expectedFrom, expectedTo, expectedId, expectedType,
                         expectedBodies, expectedSubjects, expectedThread, null, expectedExtendedContents );

        StanzaError actualStanzaError = target_.getStanzaError();
        assertThat( actualStanzaError, is( nullValue() ) );

        int expectedLength = expectedBodies.size() + expectedSubjects.size() + 1 + 0 + expectedExtendedContents.size();
        NodeList actualChildNodes = target_.getChildNodes();
        assertThat( actualChildNodes.getLength(), is( equalTo( expectedLength ) ) );
    }

    @Test
    public void testConstructor_with_no_child()
        throws Exception
    {
        List<BodyBuilder> bodyBuilders = new ArrayList<>();
        List<SubjectBuilder> subjectBuilders = new ArrayList<>();
        List<ExtendedContentBuilder> extendedContentsBuilders = new ArrayList<>();

        target_ =
            new Message( expectedNamespaceURI, expectedLanguage, expectedFrom, expectedTo, expectedId, expectedType,
                         bodyBuilders, subjectBuilders, null, null, extendedContentsBuilders );

        List<Body> actualBodies = target_.getBodies();
        assertThat( actualBodies.size(), is( equalTo( 0 ) ) );

        List<Subject> actualSubjects = target_.getSubjects();
        assertThat( actualSubjects.size(), is( equalTo( 0 ) ) );

        Thread actualThread = target_.getThread();
        assertThat( actualThread, is( nullValue() ) );

        StanzaError actualStanzaError = target_.getStanzaError();
        assertThat( actualStanzaError, is( nullValue() ) );

        NodeList actualChildNodes = target_.getChildNodes();
        assertThat( actualChildNodes, is( instanceOf( EmptyImmutableNodeList.class ) ) );
    }

    public static class TypeTest
    {

        @Test
        public void testERRORAsString()
            throws Exception
        {
            String actual = ERROR.asString();

            assertThat( actual, is( equalTo( "error" ) ) );
        }

        @Test
        public void testCHATAsString()
            throws Exception
        {
            String actual = CHAT.asString();

            assertThat( actual, is( equalTo( "chat" ) ) );
        }

        @Test
        public void testGROUPCHATAsString()
            throws Exception
        {
            String actual = GROUPCHAT.asString();

            assertThat( actual, is( equalTo( "groupchat" ) ) );
        }

        @Test
        public void testHEADLINEAsString()
            throws Exception
        {
            String actual = HEADLINE.asString();

            assertThat( actual, is( equalTo( "headline" ) ) );
        }

        @Test
        public void testNORMALAsString()
            throws Exception
        {
            String actual = NORMAL.asString();

            assertThat( actual, is( equalTo( "normal" ) ) );
        }

        @Test
        public void testFromString_in_enum()
            throws Exception
        {
            for ( Message.Type type : Message.Type.values() )
            {
                Message.Type actual = Message.Type.fromString( type.asString() );
                Message.Type expected = type;
                assertThat( actual, is( equalTo( expected ) ) );
            }

        }

        @Test( expected = IllegalArgumentException.class )
        public void testFromString_not_in_enum()
            throws Exception
        {
            Message.Type.fromString( null );
        }
    }
}
