/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza.error;

import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.address.Addresses;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class StanzaErrorBuilderTest
{
    private StanzaErrorBuilder target_;

    private final ImmutableNode mockedImmutableNode = mock( ImmutableNode.class );

    private final String expectedNamespaceURI = "urn:ietf:params:xml:ns:xmpp-stanzas";

    private final String expectedLocalName = "error";

    private final StanzaError.Type expectedType_ = StanzaError.Type.AUTH;

    private final Address expectedBy_ = Addresses.fromString( "service.domain.com" );

    private final StanzaErrorCondition.DefinedCondition expectedCondition_ =
        StanzaErrorCondition.DefinedCondition.UNDEFINED_CONDITION;

    private StanzaErrorConditionBuilder expectedStanzaErrorConditionBuilder_ =
        new StanzaErrorConditionBuilder().asDefinedCondition( expectedCondition_ );

    private StanzaErrorTextBuilder expectedStanzaErrorTextBuilder_ = new StanzaErrorTextBuilder();

    private ImmutableChildElementBuilder expectedApplicationConditionBuilder =
        new ImmutableChildElementBuilder().isChildOf( mockedImmutableNode ).atIndex( 0 ).named(
            "application-condition" );

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new StanzaErrorBuilder();

    }

    @Test( expected = IllegalArgumentException.class )
    public void testBy_with_invalid_by()
    {
        target_.by( null );
    }

    @Test( expected = IllegalArgumentException.class )
    public void testOf_with_invalid_type()
    {
        target_.ofType( null );
    }

    @Test( expected = IllegalArgumentException.class )
    public void testWithCondition_with_invalid_conditionBuilder()
    {
        target_.withCondition( null );
    }

    @Test( expected = IllegalArgumentException.class )
    public void testWithCondition_with_invalid_application_condition_builder()
    {
        target_.withApplicationCondition( null );
    }

    @Test( expected = IllegalStateException.class )
    public void testWithCondition_with_application_condition_builder_twice()
    {
        target_.withApplicationCondition( expectedApplicationConditionBuilder );
        target_.withApplicationCondition( expectedApplicationConditionBuilder );
    }

    @Test( expected = IllegalArgumentException.class )
    public void testWithCondition_with_text_builder()
    {
        target_.withText( null );
    }

    @Test( expected = IllegalStateException.class )
    public void testWithCondition_with_text_builder_twice()
    {
        target_.withText( expectedStanzaErrorTextBuilder_ );
        target_.withText( expectedStanzaErrorTextBuilder_ );
    }


    @Test( expected = IllegalStateException.class )
    public void testBuild_with_invalid_type()
    {
        target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( "namespaceURI" ).build();
    }

    @Test( expected = IllegalStateException.class )
    public void testBuild_with_invalid_conditionBuilder()
    {
        target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( "namespaceURI" ).ofType(
            expectedType_ ).build();
    }

    @Test
    public void testBuild()
    {
        StanzaError actual =
            target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( expectedNamespaceURI ).by(
                expectedBy_ ).ofType( expectedType_ ).withCondition( expectedStanzaErrorConditionBuilder_ ).withText(
                expectedStanzaErrorTextBuilder_ ).withApplicationCondition(
                expectedApplicationConditionBuilder ).build();

        assertThat( actual.getParentNode(), is( equalTo( (Node) mockedImmutableNode ) ) );
        assertThat( actual.getNamespaceURI(), is( equalTo( expectedNamespaceURI ) ) );
        assertThat( actual.getLocalName(), is( equalTo( expectedLocalName ) ) );
        assertThat( actual.getBy(), is( equalTo( expectedBy_ ) ) );
        assertThat( actual.getType(), is( equalTo( expectedType_ ) ) );

        StanzaErrorCondition stanzaErrorCondition = actual.getCondition();
        assertThat( stanzaErrorCondition.getDefinedCondition(), is( equalTo( expectedCondition_ ) ) );

        Element text = actual.getText();
        assertThat( text, is( notNullValue() ) );

        Element applicationCondition = actual.getApplicationCondition();
        assertThat( applicationCondition, is( notNullValue() ) );
    }
}
