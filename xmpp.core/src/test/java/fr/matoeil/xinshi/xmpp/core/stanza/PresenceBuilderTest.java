/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza;

import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.address.Addresses;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaError;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorCondition;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorConditionBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorTextBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.PriorityBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.Show;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.ShowBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.StatusBuilder;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class PresenceBuilderTest
{
    private PresenceBuilder target_;

    private final String expectedNamespaceURI_ = "namespace";


    @Before
    public void setUp()
        throws Exception
    {
        target_ = new PresenceBuilder();
    }

    @Test
    public void testOfType_with_null_type()
        throws Exception
    {
        final Presence.Type expectedType = null;
        target_.inNamespace( expectedNamespaceURI_ );

        target_.ofType( (Presence.Type) null );

        target_.build();
        Presence actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getType(), is( equalTo( expectedType ) ) );
    }

    @Test
    public void testOfType_with_type()
        throws Exception
    {
        final Presence.Type expectedType = Presence.Type.PROBE;
        target_.inNamespace( expectedNamespaceURI_ );

        target_.ofType( expectedType );

        target_.build();
        Presence actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getType(), is( equalTo( expectedType ) ) );
    }

    @Test
    public void testWithStatus()
        throws Exception
    {
        StatusBuilder statusBuilder = new StatusBuilder();
        target_.inNamespace( expectedNamespaceURI_ );

        target_.withStatus( statusBuilder );

        target_.build();
        Presence actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getStatuses(), is( notNullValue() ) );
        assertThat( actual.getStatuses().size(), is( equalTo( 1 ) ) );
    }

    @Test
    public void testWithShow()
        throws Exception
    {
        ShowBuilder showBuilder = new ShowBuilder().withState( Show.State.DND );
        target_.inNamespace( expectedNamespaceURI_ );

        target_.withShow( showBuilder );

        target_.build();
        Presence actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getShow(), is( notNullValue() ) );
    }

    @Test( expected = IllegalStateException.class )
    public void testWithShow_twice()
        throws Exception
    {
        ShowBuilder showBuilder = new ShowBuilder().withState( Show.State.DND );
        target_.inNamespace( expectedNamespaceURI_ );

        target_.withShow( showBuilder );
        target_.withShow( showBuilder );
    }

    @Test
    public void testWithPriority()
        throws Exception
    {
        PriorityBuilder priorityBuilder = new PriorityBuilder().withValue( Byte.valueOf( "42" ) );
        target_.inNamespace( expectedNamespaceURI_ );

        target_.withPriority( priorityBuilder );

        target_.build();
        Presence actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getPriority(), is( notNullValue() ) );
    }

    @Test( expected = IllegalStateException.class )
    public void testWithPriority_twice()
        throws Exception
    {
        PriorityBuilder priorityBuilder = new PriorityBuilder().withValue( Byte.valueOf( "42" ) );
        target_.inNamespace( expectedNamespaceURI_ );

        target_.withPriority( priorityBuilder );
        target_.withPriority( priorityBuilder );
    }

    @Test
    public void testWithError_with_stanza_error()
        throws Exception
    {
        StanzaErrorBuilder stanzaErrorBuilder = newStanzaErrorBuilder();
        target_.inNamespace( expectedNamespaceURI_ );

        target_.withError( stanzaErrorBuilder );

        target_.build();
        Presence actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getStanzaError(), is( notNullValue() ) );
    }


    @Test( expected = IllegalStateException.class )
    public void testWithError_with_stanza_error_twice()
        throws Exception
    {
        StanzaErrorBuilder stanzaErrorBuilder = newStanzaErrorBuilder();
        target_.inNamespace( expectedNamespaceURI_ );

        target_.withError( stanzaErrorBuilder );
        target_.withError( stanzaErrorBuilder );
    }

    @Test
    public void testWithExtendedContent_with_extended_content()
        throws Exception
    {
        ExtendedContentBuilder extendedContentBuilder = newExtendedContentBuilder();
        target_.inNamespace( expectedNamespaceURI_ );

        target_.withExtendedContent( extendedContentBuilder );

        target_.build();
        Presence actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getExtendedContents(), is( notNullValue() ) );
        assertThat( actual.getExtendedContents().size(), is( equalTo( 1 ) ) );
    }

    @Test
    public void testWithExtendedContent_with_extended_content_twice()
        throws Exception
    {
        ExtendedContentBuilder extendedContentBuilder = newExtendedContentBuilder();
        target_.inNamespace( expectedNamespaceURI_ );

        target_.withExtendedContent( extendedContentBuilder );
        target_.withExtendedContent( extendedContentBuilder );

        target_.build();
        Presence actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getExtendedContents(), is( notNullValue() ) );
        assertThat( actual.getExtendedContents().size(), is( equalTo( 2 ) ) );
    }

    @Test
    public void testBuild()
        throws Exception
    {
        target_.inNamespace( expectedNamespaceURI_ );

        target_.build();

        Stanza actual = target_.build();
        assertThat( actual, is( instanceOf( Presence.class ) ) );
    }

    private StanzaErrorBuilder newStanzaErrorBuilder()
    {

        final String namespaceURI = "urn:ietf:params:xml:ns:xmpp-stanzas";

        final StanzaError.Type type = StanzaError.Type.AUTH;

        final Address address = Addresses.fromString( "service.domain.com" );

        final StanzaErrorCondition.DefinedCondition definedCondition =
            StanzaErrorCondition.DefinedCondition.UNDEFINED_CONDITION;

        StanzaErrorConditionBuilder stanzaErrorConditionBuilder =
            new StanzaErrorConditionBuilder().asDefinedCondition( definedCondition );

        StanzaErrorTextBuilder stanzaErrorTextBuilder = new StanzaErrorTextBuilder();

        ImmutableChildElementBuilder applicationConditionBuilder =
            new ImmutableChildElementBuilder().named( "application-condition" );

        StanzaErrorBuilder stanzaErrorBuilder =
            new StanzaErrorBuilder().inNamespace( namespaceURI ).by( address ).ofType( type ).withCondition(
                stanzaErrorConditionBuilder ).withText( stanzaErrorTextBuilder ).withApplicationCondition(
                applicationConditionBuilder );

        return stanzaErrorBuilder;
    }

    private ExtendedContentBuilder newExtendedContentBuilder()
    {

        final String namespaceURI = "namespace";
        final String prefix = "prefix";
        final String localName = "localName";

        ExtendedContentBuilder extendedContentBuilder =
            new ExtendedContentBuilder().inNamespace( namespaceURI ).withPrefix( prefix ).named( localName );
        return extendedContentBuilder;
    }
}
