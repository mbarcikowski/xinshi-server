/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo;

import fr.matoeil.xinshi.xmpp.core.stanza.Iq;
import fr.matoeil.xinshi.xmpp.core.stream.Features;
import fr.matoeil.xinshi.xmpp.core.stream.Header;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.Attributes;

import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.*;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class StreamHandlerTest
{

    private StreamHandler target_;

    private final StreamListener mockedStreamListener_ = mock( StreamListener.class );

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new StreamHandler( mockedStreamListener_ );
    }

    @Test
    public void testReset()
        throws Exception
    {
        target_.reset();

        verifyZeroInteractions( mockedStreamListener_ );
    }

    @Test
    public void testXmlDeclaration_with_utf_8_encoding()
        throws Exception
    {
        target_.xmlDeclaration( "1.0", "UTF-8", true );

        verifyZeroInteractions( mockedStreamListener_ );
    }

    @Test(expected = StreamException.class)
    public void testXmlDeclaration_with_other_encoding()
        throws Exception
    {
        target_.xmlDeclaration( "1.0", "UTF-16", true );

    }

    @Test
    public void testStartPrefixMapping_with_stream_namespace_and_stream_prefix()
        throws Exception
    {
        target_.startPrefixMapping( "stream", "http://etherx.jabber.org/streams" );

        verifyZeroInteractions( mockedStreamListener_ );
    }

    @Test(expected = StreamException.class)
    public void testStartPrefixMapping_with_stream_namespace_with_other_prefix()
        throws Exception
    {
        target_.startPrefixMapping( "any", "http://etherx.jabber.org/streams" );
    }

    @Test
    public void testStartPrefixMapping_after_receiving_stream_head()
        throws Exception
    {
        Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getValue( eq( "version" ) ) ).thenReturn( "1.0" );
        target_.startElement( "http://etherx.jabber.org/streams", "stream", "stream:stream", mockedAttributes );

        target_.startPrefixMapping( "any", "http://etherx.jabber.org/streams" );

        verify( mockedStreamListener_ ).handleStreamOpening( notNull( Header.class ) );
        verifyNoMoreInteractions( mockedStreamListener_ );
    }

    @Test
    public void testStartPrefixMapping_after_receiving_stream_element()
        throws Exception
    {
        Attributes mockedHeaderAttributes = mock( Attributes.class );
        when( mockedHeaderAttributes.getValue( eq( "version" ) ) ).thenReturn( "1.0" );
        target_.startElement( "http://etherx.jabber.org/streams", "stream", "stream:stream", mockedHeaderAttributes );
        target_.startElement( "jabber:client", "iq", "iq", mock( Attributes.class ) );

        target_.startPrefixMapping( "any", "http://etherx.jabber.org/streams" );

        verify( mockedStreamListener_ ).handleStreamOpening( notNull( Header.class ) );
        verifyNoMoreInteractions( mockedStreamListener_ );
    }

    @Test
    public void testStartElement_with_header()
        throws Exception
    {
        Attributes mockedHeaderAttributes = mock( Attributes.class );
        when( mockedHeaderAttributes.getValue( eq( "version" ) ) ).thenReturn( "1.0" );

        target_.startElement( "http://etherx.jabber.org/streams", "stream", "stream:stream", mockedHeaderAttributes );
        verify( mockedStreamListener_ ).handleStreamOpening( notNull( Header.class ) );
        verifyNoMoreInteractions( mockedStreamListener_ );
    }

    @Test(expected = StreamException.class)
    public void testStartElement_with_stream_element_without_namespace()
        throws Exception
    {
        Attributes mockedHeaderAttributes = mock( Attributes.class );
        when( mockedHeaderAttributes.getValue( eq( "version" ) ) ).thenReturn( "1.0" );
        target_.startElement( "http://etherx.jabber.org/streams", "stream", "stream:stream", mockedHeaderAttributes );

        target_.startElement( null, "name", "prefix:name", mock( Attributes.class ) );
    }

    @Test
    public void testStartElement_with_iq()
        throws Exception
    {
        Attributes mockedHeaderAttributes = mock( Attributes.class );
        when( mockedHeaderAttributes.getValue( eq( "version" ) ) ).thenReturn( "1.0" );
        target_.startElement( "http://etherx.jabber.org/streams", "stream", "stream:stream", mockedHeaderAttributes );

        target_.startElement( "jabber:client", "iq", "iq", mock( Attributes.class ) );

        verify( mockedStreamListener_ ).handleStreamOpening( notNull( Header.class ) );
        verifyNoMoreInteractions( mockedStreamListener_ );
    }

    @Test
    public void testStartElement_with_message()
        throws Exception
    {
        Attributes mockedHeaderAttributes = mock( Attributes.class );
        when( mockedHeaderAttributes.getValue( eq( "version" ) ) ).thenReturn( "1.0" );
        target_.startElement( "http://etherx.jabber.org/streams", "stream", "stream:stream", mockedHeaderAttributes );

        target_.startElement( "jabber:client", "message", "message", mock( Attributes.class ) );

        verify( mockedStreamListener_ ).handleStreamOpening( notNull( Header.class ) );
        verifyNoMoreInteractions( mockedStreamListener_ );
    }


    @Test
    public void testStartElement_with_presence()
        throws Exception
    {
        Attributes mockedHeaderAttributes = mock( Attributes.class );
        when( mockedHeaderAttributes.getValue( eq( "version" ) ) ).thenReturn( "1.0" );
        target_.startElement( "http://etherx.jabber.org/streams", "stream", "stream:stream", mockedHeaderAttributes );

        target_.startElement( "jabber:client", "presence", "presence", mock( Attributes.class ) );

        verify( mockedStreamListener_ ).handleStreamOpening( notNull( Header.class ) );
        verifyNoMoreInteractions( mockedStreamListener_ );
    }

    @Test(expected = StreamException.class)
    public void testStartElement_with_prefixed_stanza()
        throws Exception
    {
        Attributes mockedHeaderAttributes = mock( Attributes.class );
        when( mockedHeaderAttributes.getValue( eq( "version" ) ) ).thenReturn( "1.0" );
        target_.startElement( "http://etherx.jabber.org/streams", "stream", "stream:stream", mockedHeaderAttributes );

        target_.startElement( "jabber:client", "iq", "prefix:iq", mock( Attributes.class ) );
    }

    @Test(expected = StreamException.class)
    public void testStartElement_with_unknown_stanza()
        throws Exception
    {
        Attributes mockedHeaderAttributes = mock( Attributes.class );
        when( mockedHeaderAttributes.getValue( eq( "version" ) ) ).thenReturn( "1.0" );
        target_.startElement( "http://etherx.jabber.org/streams", "stream", "stream:stream", mockedHeaderAttributes );

        target_.startElement( "jabber:client", "name", "name", mock( Attributes.class ) );
    }

    @Test
    public void testStartElement_with_stream_error()
        throws Exception
    {
        Attributes mockedHeaderAttributes = mock( Attributes.class );
        when( mockedHeaderAttributes.getValue( eq( "version" ) ) ).thenReturn( "1.0" );
        target_.startElement( "http://etherx.jabber.org/streams", "stream", "stream:stream", mockedHeaderAttributes );

        target_.startElement( "http://etherx.jabber.org/streams", "error", "stream:error", mock( Attributes.class ) );

        verify( mockedStreamListener_ ).handleStreamOpening( notNull( Header.class ) );
        verifyNoMoreInteractions( mockedStreamListener_ );
    }

    @Test
    public void testStartElement_with_stream_features()
        throws Exception
    {
        Attributes mockedHeaderAttributes = mock( Attributes.class );
        when( mockedHeaderAttributes.getValue( eq( "version" ) ) ).thenReturn( "1.0" );
        target_.startElement( "http://etherx.jabber.org/streams", "stream", "stream:stream", mockedHeaderAttributes );

        target_.startElement( "http://etherx.jabber.org/streams", "features", "stream:features",
                              mock( Attributes.class ) );

        verify( mockedStreamListener_ ).handleStreamOpening( notNull( Header.class ) );
        verifyNoMoreInteractions( mockedStreamListener_ );
    }

    @Test
    public void testStartElement_with_stream_negotation_element()
        throws Exception
    {
        Attributes mockedHeaderAttributes = mock( Attributes.class );
        when( mockedHeaderAttributes.getValue( eq( "version" ) ) ).thenReturn( "1.0" );
        target_.startElement( "http://etherx.jabber.org/streams", "stream", "stream:stream", mockedHeaderAttributes );

        target_.startElement( "urn:ietf:params:xml:ns:xmpp-tls", "starttls", "starttls", mock( Attributes.class ) );

        verify( mockedStreamListener_ ).handleStreamOpening( notNull( Header.class ) );
        verifyNoMoreInteractions( mockedStreamListener_ );
    }

    @Test
    public void testStartElement_after_stanza()
        throws Exception
    {
        Attributes mockedHeaderAttributes = mock( Attributes.class );
        when( mockedHeaderAttributes.getValue( eq( "version" ) ) ).thenReturn( "1.0" );
        target_.startElement( "http://etherx.jabber.org/streams", "stream", "stream:stream", mockedHeaderAttributes );
        target_.startElement( "jabber:client", "iq", "iq", mock( Attributes.class ) );

        target_.startElement( "namespace", "name", "prefix:name", mock( Attributes.class ) );

        verify( mockedStreamListener_ ).handleStreamOpening( notNull( Header.class ) );
        verifyNoMoreInteractions( mockedStreamListener_ );
    }

    @Test(expected = StreamException.class)
    public void testStartCDATA()
        throws Exception
    {
        target_.startCDATA();
    }

    @Test(expected = StreamException.class)
    public void testStartCDATA_after_receive_stream_header()
        throws Exception
    {
        Attributes mockedHeaderAttributes = mock( Attributes.class );
        when( mockedHeaderAttributes.getValue( eq( "version" ) ) ).thenReturn( "1.0" );
        target_.startElement( "http://etherx.jabber.org/streams", "stream", "stream:stream", mockedHeaderAttributes );

        target_.startCDATA();
    }

    @Test(expected = StreamException.class)
    public void testStartCDATA_after_receive_stream_element()
        throws Exception
    {
        Attributes mockedHeaderAttributes = mock( Attributes.class );
        when( mockedHeaderAttributes.getValue( eq( "version" ) ) ).thenReturn( "1.0" );
        target_.startElement( "http://etherx.jabber.org/streams", "stream", "stream:stream", mockedHeaderAttributes );
        target_.startElement( "jabber:client", "iq", "iq", mock( Attributes.class ) );

        target_.startCDATA();

        verify( mockedStreamListener_ ).handleStreamOpening( notNull( Header.class ) );
        verifyNoMoreInteractions( mockedStreamListener_ );
    }

    @Test(expected = StreamException.class)
    public void testCharacters()
        throws Exception
    {
        target_.characters( new char[0], 0, 0 );
    }

    @Test
    public void testCharacters_after_receive_stream_header_with_whitespaces_only()
        throws Exception
    {
        Attributes mockedHeaderAttributes = mock( Attributes.class );
        when( mockedHeaderAttributes.getValue( eq( "version" ) ) ).thenReturn( "1.0" );
        target_.startElement( "http://etherx.jabber.org/streams", "stream", "stream:stream", mockedHeaderAttributes );
        char[] chars = "      ".toCharArray();

        target_.characters( chars, 0, chars.length );

        verify( mockedStreamListener_ ).handleStreamOpening( notNull( Header.class ) );
        verifyNoMoreInteractions( mockedStreamListener_ );
    }

    @Test(expected = StreamException.class)
    public void testCharacters_after_receive_stream_header_without_only_whitespaces()
        throws Exception
    {
        Attributes mockedHeaderAttributes = mock( Attributes.class );
        when( mockedHeaderAttributes.getValue( eq( "version" ) ) ).thenReturn( "1.0" );
        target_.startElement( "http://etherx.jabber.org/streams", "stream", "stream:stream", mockedHeaderAttributes );
        char[] chars = "   s    ".toCharArray();

        target_.characters( chars, 0, chars.length );

        verify( mockedStreamListener_ ).handleStreamOpening( notNull( Header.class ) );
        verifyNoMoreInteractions( mockedStreamListener_ );
    }

    @Test
    public void testCharacters_after_receive_stream_element()
        throws Exception
    {
        Attributes mockedHeaderAttributes = mock( Attributes.class );
        when( mockedHeaderAttributes.getValue( eq( "version" ) ) ).thenReturn( "1.0" );
        target_.startElement( "http://etherx.jabber.org/streams", "stream", "stream:stream", mockedHeaderAttributes );
        target_.startElement( "jabber:client", "iq", "iq", mock( Attributes.class ) );

        target_.characters( new char[0], 0, 0 );

        verify( mockedStreamListener_ ).handleStreamOpening( notNull( Header.class ) );
        verifyNoMoreInteractions( mockedStreamListener_ );
    }

    @Test(expected = StreamException.class)
    public void testEndCDATA()
        throws Exception
    {
        target_.endCDATA();
    }

    @Test(expected = StreamException.class)
    public void testEndCDATA_after_receive_stream_header()
        throws Exception
    {
        Attributes mockedHeaderAttributes = mock( Attributes.class );
        when( mockedHeaderAttributes.getValue( eq( "version" ) ) ).thenReturn( "1.0" );
        target_.startElement( "http://etherx.jabber.org/streams", "stream", "stream:stream", mockedHeaderAttributes );

        target_.endCDATA();
    }

    @Test(expected = StreamException.class)
    public void testEndCDATA_after_receive_stream_element()
        throws Exception
    {
        Attributes mockedHeaderAttributes = mock( Attributes.class );
        when( mockedHeaderAttributes.getValue( eq( "version" ) ) ).thenReturn( "1.0" );
        target_.startElement( "http://etherx.jabber.org/streams", "stream", "stream:stream", mockedHeaderAttributes );
        target_.startElement( "jabber:client", "iq", "iq", mock( Attributes.class ) );

        target_.endCDATA();

        verify( mockedStreamListener_ ).handleStreamOpening( notNull( Header.class ) );
        verifyNoMoreInteractions( mockedStreamListener_ );
    }

    @Test
    public void testEndElement_after_receive_stream_header()
        throws Exception
    {
        Attributes mockedHeaderAttributes = mock( Attributes.class );
        when( mockedHeaderAttributes.getValue( eq( "version" ) ) ).thenReturn( "1.0" );
        target_.startElement( "http://etherx.jabber.org/streams", "stream", "stream:stream", mockedHeaderAttributes );

        target_.endElement( "http://etherx.jabber.org/streams", "stream", "stream:stream" );

        verify( mockedStreamListener_ ).handleStreamOpening( notNull( Header.class ) );
        verify( mockedStreamListener_ ).handleStreamClosing();
        verifyNoMoreInteractions( mockedStreamListener_ );
    }

    @Test
    public void testEndElement_after_receive_stanza()
        throws Exception
    {
        Attributes mockedHeaderAttributes = mock( Attributes.class );
        when( mockedHeaderAttributes.getValue( eq( "version" ) ) ).thenReturn( "1.0" );
        target_.startElement( "http://etherx.jabber.org/streams", "stream", "stream:stream", mockedHeaderAttributes );
        target_.startElement( "jabber:client", "iq", "iq", mock( Attributes.class ) );

        target_.endElement( "jabber:client", "iq", "iq" );

        verify( mockedStreamListener_ ).handleStreamOpening( notNull( Header.class ) );
        verify( mockedStreamListener_ ).handleIncomingStanza( isA( Iq.class ) );
        verifyNoMoreInteractions( mockedStreamListener_ );
    }

    @Test
    public void testEndElement_after_receive_stream_element()
        throws Exception
    {
        Attributes mockedHeaderAttributes = mock( Attributes.class );
        when( mockedHeaderAttributes.getValue( eq( "version" ) ) ).thenReturn( "1.0" );
        target_.startElement( "http://etherx.jabber.org/streams", "stream", "stream:stream", mockedHeaderAttributes );
        target_.startElement( "http://etherx.jabber.org/streams", "features", "stream:features",
                              mock( Attributes.class ) );

        target_.endElement( "http://etherx.jabber.org/streams", "features", "stream:features" );

        verify( mockedStreamListener_ ).handleStreamOpening( notNull( Header.class ) );
        verify( mockedStreamListener_ ).handleIncomingStreamElement( isA( Features.class ) );
        verifyNoMoreInteractions( mockedStreamListener_ );
    }

    @Test
    public void testEndElement_for_child_element_of_stream_element()
        throws Exception
    {
        Attributes mockedHeaderAttributes = mock( Attributes.class );
        when( mockedHeaderAttributes.getValue( eq( "version" ) ) ).thenReturn( "1.0" );
        target_.startElement( "http://etherx.jabber.org/streams", "stream", "stream:stream", mockedHeaderAttributes );
        target_.startElement( "http://etherx.jabber.org/streams", "features", "stream:features",
                              mock( Attributes.class ) );
        target_.startElement( "namespace", "name", "prefix:name", mock( Attributes.class ) );

        target_.endElement( "namespace", "name", "prefix:name" );

        verify( mockedStreamListener_ ).handleStreamOpening( notNull( Header.class ) );
        verifyNoMoreInteractions( mockedStreamListener_ );
    }
}
