/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza;

import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.address.Addresses;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaError;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorCondition;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorConditionBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorTextBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.message.BodyBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.message.SubjectBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.message.ThreadBuilder;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class MessageBuilderTest
{
    private MessageBuilder target_;

    private final String expectedNamespaceURI_ = "namespace";


    @Before
    public void setUp()
        throws Exception
    {
        target_ = new MessageBuilder();
    }

    @Test
    public void testOfType_with_null_type()
        throws Exception
    {
        final Message.Type expectedType = null;
        target_.inNamespace( expectedNamespaceURI_ );

        target_.ofType( (Message.Type) null );

        target_.build();
        Message actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getType(), is( equalTo( expectedType ) ) );
    }

    @Test
    public void testOfType_with_type()
        throws Exception
    {
        final Message.Type expectedType = Message.Type.ERROR;
        target_.inNamespace( expectedNamespaceURI_ );

        target_.ofType( expectedType );

        target_.build();
        Message actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getType(), is( equalTo( expectedType ) ) );
    }

    @Test
    public void testWithBody()
        throws Exception
    {
        BodyBuilder bodyBuilder = new BodyBuilder();
        target_.inNamespace( expectedNamespaceURI_ );

        target_.withBody( bodyBuilder );

        target_.build();
        Message actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getBodies(), is( notNullValue() ) );
        assertThat( actual.getBodies().size(), is( equalTo( 1 ) ) );
    }

    @Test
    public void testWithSubject()
        throws Exception
    {
        SubjectBuilder subjectBuilder = new SubjectBuilder();
        target_.inNamespace( expectedNamespaceURI_ );

        target_.withSubject( subjectBuilder );

        target_.build();
        Message actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getSubjects(), is( notNullValue() ) );
        assertThat( actual.getSubjects().size(), is( equalTo( 1 ) ) );
    }

    @Test
    public void testWithThread()
        throws Exception
    {
        ThreadBuilder threadBuilder = new ThreadBuilder().identifiedBy( "id" );
        target_.inNamespace( expectedNamespaceURI_ );

        target_.withThread( threadBuilder );

        target_.build();
        Message actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getThread(), is( notNullValue() ) );
    }

    @Test
    public void testWithError_with_stanza_error()
        throws Exception
    {
        StanzaErrorBuilder stanzaErrorBuilder = newStanzaErrorBuilder();
        target_.inNamespace( expectedNamespaceURI_ );

        target_.withError( stanzaErrorBuilder );

        target_.build();
        Message actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getStanzaError(), is( notNullValue() ) );
    }


    @Test( expected = IllegalStateException.class )
    public void testWithError_with_stanza_error_twice()
        throws Exception
    {
        StanzaErrorBuilder stanzaErrorBuilder = newStanzaErrorBuilder();
        target_.inNamespace( expectedNamespaceURI_ );

        target_.withError( stanzaErrorBuilder );
        target_.withError( stanzaErrorBuilder );
    }

    @Test
    public void testWithExtendedContent_with_extended_content()
        throws Exception
    {
        ExtendedContentBuilder extendedContentBuilder = newExtendedContentBuilder();
        target_.inNamespace( expectedNamespaceURI_ );

        target_.withExtendedContent( extendedContentBuilder );

        target_.build();
        Message actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getExtendedContents(), is( notNullValue() ) );
        assertThat( actual.getExtendedContents().size(), is( equalTo( 1 ) ) );
    }

    @Test
    public void testWithExtendedContent_with_extended_content_twice()
        throws Exception
    {
        ExtendedContentBuilder extendedContentBuilder = newExtendedContentBuilder();
        target_.inNamespace( expectedNamespaceURI_ );

        target_.withExtendedContent( extendedContentBuilder );
        target_.withExtendedContent( extendedContentBuilder );

        target_.build();
        Message actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getExtendedContents(), is( notNullValue() ) );
        assertThat( actual.getExtendedContents().size(), is( equalTo( 2 ) ) );
    }

    @Test
    public void testBuild()
        throws Exception
    {
        target_.inNamespace( expectedNamespaceURI_ );

        target_.build();

        Stanza actual = target_.build();
        assertThat( actual, is( instanceOf( Message.class ) ) );
    }

    private StanzaErrorBuilder newStanzaErrorBuilder()
    {

        final String namespaceURI = "urn:ietf:params:xml:ns:xmpp-stanzas";

        final StanzaError.Type type = StanzaError.Type.AUTH;

        final Address address = Addresses.fromString( "service.domain.com" );

        final StanzaErrorCondition.DefinedCondition definedCondition =
            StanzaErrorCondition.DefinedCondition.UNDEFINED_CONDITION;

        StanzaErrorConditionBuilder stanzaErrorConditionBuilder =
            new StanzaErrorConditionBuilder().asDefinedCondition( definedCondition );

        StanzaErrorTextBuilder stanzaErrorTextBuilder = new StanzaErrorTextBuilder();

        ImmutableChildElementBuilder applicationConditionBuilder =
            new ImmutableChildElementBuilder().named( "application-condition" );

        StanzaErrorBuilder stanzaErrorBuilder =
            new StanzaErrorBuilder().inNamespace( namespaceURI ).by( address ).ofType( type ).withCondition(
                stanzaErrorConditionBuilder ).withText( stanzaErrorTextBuilder ).withApplicationCondition(
                applicationConditionBuilder );

        return stanzaErrorBuilder;
    }

    private ExtendedContentBuilder newExtendedContentBuilder()
    {

        final String namespaceURI = "namespace";
        final String prefix = "prefix";
        final String localName = "localName";

        ExtendedContentBuilder extendedContentBuilder =
            new ExtendedContentBuilder().inNamespace( namespaceURI ).withPrefix( prefix ).named( localName );
        return extendedContentBuilder;
    }
}
