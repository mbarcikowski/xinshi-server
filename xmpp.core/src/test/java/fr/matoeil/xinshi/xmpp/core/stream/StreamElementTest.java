/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream;

import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class StreamElementTest
{

    private StreamElement target_;

    private final String expectedNamespaceURI = "namespace";

    private final String expectedPrefix = "prefix";

    private final String expectedLocalName = "localName";

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new TestStreamElement();

    }

    @Test
    public void testGetLocalName()
        throws Exception
    {
        String actual = target_.getLocalName();

        assertThat( actual, is( equalTo( expectedLocalName ) ) );
    }

    @Test
    public void testGetNamespaceURI()
        throws Exception
    {
        String actual = target_.getNamespaceURI();

        assertThat( actual, is( equalTo( expectedNamespaceURI ) ) );
    }

    @Test
    public void testGetParentNode()
        throws Exception
    {
        Node actual = target_.getParentNode();

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetPreviousSibling()
        throws Exception
    {
        Node actual = target_.getPreviousSibling();

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetNextSibling()
        throws Exception
    {
        Node actual = target_.getNextSibling();

        assertThat( actual, is( nullValue() ) );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testGetTextContent()
        throws Exception
    {
        target_.getTextContent();
    }

    private class TestStreamElement
        extends StreamElement
    {
        public TestStreamElement()
        {
            super( StreamElementTest.this.expectedNamespaceURI, StreamElementTest.this.expectedPrefix,
                   StreamElementTest.this.expectedLocalName );
        }
    }

}
