/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream.feature.sasl;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.stream.NegotiationElement;
import fr.matoeil.xinshi.xmpp.core.stream.feature.Feature;
import fr.matoeil.xinshi.xmpp.core.stream.feature.FeatureBuilder;
import org.junit.Test;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

import java.lang.reflect.Constructor;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class SaslElementsTest
{
    @Test
    public void testConstructor()
        throws Exception
    {
        Constructor target = SaslElements.class.getDeclaredConstructor();

        assertThat( target.isAccessible(), is( equalTo( false ) ) );

        target.setAccessible( true );
        target.newInstance();
    }

    @Test
    public void testCreateSaslFeatureBuilderWithMecanisms()
        throws Exception
    {
        String expectedNamespaceUri = "urn:ietf:params:xml:ns:xmpp-sasl";
        final String[] expectedMecanisms = { "mech1", "mech2" };

        FeatureBuilder builder = SaslElements.createSaslFeatureBuilderWithMecanisms( expectedMecanisms );

        Feature actual = builder.isChildOf( mock( ImmutableNode.class ) ).atIndex( 0 ).build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getNamespaceURI(), is( equalTo( expectedNamespaceUri ) ) );
        assertThat( actual.getLocalName(), is( equalTo( "mechanisms" ) ) );
        assertThat( actual.getChildNodes().getLength(), is( equalTo( expectedMecanisms.length ) ) );

        for ( int index = 0, length = actual.getChildNodes().getLength(); index < length; index++ )
        {
            Element mechanismNode = (Element) actual.getChildNodes().item( index );
            assertThat( mechanismNode, is( notNullValue() ) );
            assertThat( mechanismNode.getNamespaceURI(), is( equalTo( expectedNamespaceUri ) ) );
            assertThat( mechanismNode.getNodeName(), is( equalTo( "mechanism" ) ) );
            assertThat( mechanismNode.getChildNodes().getLength(), is( equalTo( 1 ) ) );
            Node item = mechanismNode.getChildNodes().item( 0 );
            assertThat( item, is( notNullValue() ) );
            assertThat( item, is( instanceOf( Text.class ) ) );
            Text text = (Text) item;
            assertThat( text.getData(), is( equalTo( expectedMecanisms[index] ) ) );
        }


    }

    @Test
    public void testCreateSaslAuthNegotiationElement()
        throws Exception
    {
        String expectedNamespaceUri = "urn:ietf:params:xml:ns:xmpp-sasl";
        final String expectedMechanism = "mech";

        NegotiationElement actual = SaslElements.createSaslAuthNegotiationElement( expectedMechanism );

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getNamespaceURI(), is( equalTo( expectedNamespaceUri ) ) );
        assertThat( actual.getLocalName(), is( equalTo( "auth" ) ) );
        assertThat( actual.getAttributes().getLength(), is( equalTo( 1 ) ) );
        assertThat( actual.getAttribute( "mechanism" ), is( equalTo( expectedMechanism ) ) );
        assertThat( actual.getChildNodes().getLength(), is( equalTo( 0 ) ) );
    }

    @Test
    public void testCreateSaslAuthNegotiationElementWithInitialResponse()
        throws Exception
    {
        String expectedNamespaceUri = "urn:ietf:params:xml:ns:xmpp-sasl";
        final String expectedMechanism = "mech";
        final String expectedInitialResponse = "intialResponse=";
        NegotiationElement actual = SaslElements.createSaslAuthNegotiationElementWithInitialResponse( expectedMechanism,
                                                                                                      expectedInitialResponse );

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getNamespaceURI(), is( equalTo( expectedNamespaceUri ) ) );
        assertThat( actual.getLocalName(), is( equalTo( "auth" ) ) );
        assertThat( actual.getAttributes().getLength(), is( equalTo( 1 ) ) );
        assertThat( actual.getAttribute( "mechanism" ), is( equalTo( expectedMechanism ) ) );
        assertThat( actual.getChildNodes().getLength(), is( equalTo( 1 ) ) );

        Node contentNode = actual.getChildNodes().item( 0 );
        assertThat( contentNode, is( instanceOf( Text.class ) ) );
        Text text = (Text) contentNode;
        assertThat( text.getData(), is( equalTo( expectedInitialResponse ) ) );
    }

    @Test
    public void testSaslAbort()
        throws Exception
    {
        NegotiationElement actual = SaslElements.SASL_ABORT;

        assertThat( actual, is( notNullValue() ) );
    }

    @Test
    public void testSaslSuccess()
        throws Exception
    {
        NegotiationElement actual = SaslElements.SASL_SUCCESS;

        assertThat( actual, is( notNullValue() ) );
    }

    @Test
    public void testCreateSaslFailure()
        throws Exception
    {
        String expectedNamespaceUri = "urn:ietf:params:xml:ns:xmpp-sasl";
        final SaslElements.DefinedCondition expectedDefinedCondition =
            SaslElements.DefinedCondition.TEMPORARY_AUTH_FAILURE;

        NegotiationElement actual = SaslElements.createSaslFailure( expectedDefinedCondition );

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getNamespaceURI(), is( equalTo( expectedNamespaceUri ) ) );
        assertThat( actual.getLocalName(), is( equalTo( "failure" ) ) );
        assertThat( actual.getChildNodes().getLength(), is( equalTo( 1 ) ) );
        Element failureNode = (Element) actual.getChildNodes().item( 0 );
        assertThat( failureNode, is( notNullValue() ) );
        assertThat( failureNode.getNamespaceURI(), is( equalTo( expectedNamespaceUri ) ) );
        assertThat( failureNode.getNodeName(), is( equalTo( expectedDefinedCondition.asString() ) ) );
        assertThat( failureNode.getChildNodes().getLength(), is( equalTo( 0 ) ) );
    }

    @Test
    public void createSaslFailureWithDetails_without_lang()
        throws Exception
    {
        String expectedNamespaceUri = "urn:ietf:params:xml:ns:xmpp-sasl";
        final SaslElements.DefinedCondition expectedDefinedCondition =
            SaslElements.DefinedCondition.TEMPORARY_AUTH_FAILURE;
        final String expectedText = "text";

        NegotiationElement actual =
            SaslElements.createSaslFailureWithDetails( expectedDefinedCondition, null, expectedText );

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getNamespaceURI(), is( equalTo( expectedNamespaceUri ) ) );
        assertThat( actual.getLocalName(), is( equalTo( "failure" ) ) );
        assertThat( actual.getChildNodes().getLength(), is( equalTo( 2 ) ) );
        Element failureNode = (Element) actual.getChildNodes().item( 0 );
        assertThat( failureNode, is( notNullValue() ) );
        assertThat( failureNode.getNamespaceURI(), is( equalTo( expectedNamespaceUri ) ) );
        assertThat( failureNode.getNodeName(), is( equalTo( expectedDefinedCondition.asString() ) ) );
        assertThat( failureNode.getChildNodes().getLength(), is( equalTo( 0 ) ) );
        Element textNode = (Element) actual.getChildNodes().item( 1 );
        assertThat( textNode, is( notNullValue() ) );
        assertThat( textNode.getNamespaceURI(), is( equalTo( expectedNamespaceUri ) ) );
        assertThat( textNode.getNodeName(), is( equalTo( "text" ) ) );
        assertThat( textNode.getChildNodes().getLength(), is( equalTo( 1 ) ) );
        Text contentNode = (Text) textNode.getChildNodes().item( 0 );
        assertThat( contentNode, is( notNullValue() ) );
        assertThat( contentNode.getData(), is( equalTo( expectedText ) ) );
    }

    @Test
    public void createSaslFailureWithDetails_with_lang()
        throws Exception
    {
        String expectedNamespaceUri = "urn:ietf:params:xml:ns:xmpp-sasl";
        final SaslElements.DefinedCondition expectedDefinedCondition =
            SaslElements.DefinedCondition.TEMPORARY_AUTH_FAILURE;
        final String expectedText = "text";
        final String expectedLang = "fr";

        NegotiationElement actual =
            SaslElements.createSaslFailureWithDetails( expectedDefinedCondition, expectedLang, expectedText );

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getNamespaceURI(), is( equalTo( expectedNamespaceUri ) ) );
        assertThat( actual.getLocalName(), is( equalTo( "failure" ) ) );
        assertThat( actual.getChildNodes().getLength(), is( equalTo( 2 ) ) );

        Element failureNode = (Element) actual.getChildNodes().item( 0 );
        assertThat( failureNode, is( notNullValue() ) );
        assertThat( failureNode.getNamespaceURI(), is( equalTo( expectedNamespaceUri ) ) );
        assertThat( failureNode.getNodeName(), is( equalTo( expectedDefinedCondition.asString() ) ) );
        assertThat( failureNode.getChildNodes().getLength(), is( equalTo( 0 ) ) );

        Element textNode = (Element) actual.getChildNodes().item( 1 );
        assertThat( textNode, is( notNullValue() ) );
        assertThat( textNode.getNamespaceURI(), is( equalTo( expectedNamespaceUri ) ) );
        assertThat( textNode.getNodeName(), is( equalTo( "text" ) ) );
        assertThat( textNode.getChildNodes().getLength(), is( equalTo( 1 ) ) );

        Text contentNode = (Text) textNode.getChildNodes().item( 0 );
        assertThat( contentNode, is( notNullValue() ) );
        assertThat( contentNode.getData(), is( equalTo( expectedText ) ) );

        assertThat( textNode.getAttributes().getLength(), is( equalTo( 1 ) ) );
        assertThat( textNode.getAttribute( "xml:lang" ), is( equalTo( expectedLang ) ) );
    }

    @Test
    public void testCreateSaslChallenge()
        throws Exception
    {
        String expectedNamespaceUri = "urn:ietf:params:xml:ns:xmpp-sasl";
        final String expectedChallenge = "challenge";

        NegotiationElement actual = SaslElements.createSaslChallenge( expectedChallenge );

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getNamespaceURI(), is( equalTo( expectedNamespaceUri ) ) );
        assertThat( actual.getLocalName(), is( equalTo( "challenge" ) ) );
        assertThat( actual.getChildNodes().getLength(), is( equalTo( 1 ) ) );
        Node failureNode = actual.getChildNodes().item( 0 );
        assertThat( failureNode, is( notNullValue() ) );
        assertThat( failureNode, is( instanceOf( Text.class ) ) );
        Text text = (Text) failureNode;
        assertThat( text.getData(), is( equalTo( expectedChallenge ) ) );
    }

    public static class DefinedConditionTest
    {

        @Test
        public void testABORTEDAsString()
            throws Exception
        {
            String actual = SaslElements.DefinedCondition.ABORTED.asString();

            assertThat( actual, is( equalTo( "aborted" ) ) );
        }

        @Test
        public void testACCOUNT_DISABLEDAsString()
            throws Exception
        {
            String actual = SaslElements.DefinedCondition.ACCOUNT_DISABLED.asString();

            assertThat( actual, is( equalTo( "account-disabled" ) ) );
        }

        @Test
        public void testCREDENTIALS_EXPIREDAsString()
            throws Exception
        {
            String actual = SaslElements.DefinedCondition.CREDENTIALS_EXPIRED.asString();

            assertThat( actual, is( equalTo( "credentials-expired" ) ) );
        }

        @Test
        public void testENCRYPTION_REQUIREDAsString()
            throws Exception
        {
            String actual = SaslElements.DefinedCondition.ENCRYPTION_REQUIRED.asString();

            assertThat( actual, is( equalTo( "encryption-required" ) ) );
        }

        @Test
        public void testINCORRECT_ENCODINGAsString()
            throws Exception
        {
            String actual = SaslElements.DefinedCondition.INCORRECT_ENCODING.asString();

            assertThat( actual, is( equalTo( "incorrect-encoding" ) ) );
        }

        @Test
        public void testINVALID_AUTHZIDAsString()
            throws Exception
        {
            String actual = SaslElements.DefinedCondition.INVALID_AUTHZID.asString();

            assertThat( actual, is( equalTo( "invalid-authzid" ) ) );
        }

        @Test
        public void testINVALID_MECHANISMAsString()
            throws Exception
        {
            String actual = SaslElements.DefinedCondition.INVALID_MECHANISM.asString();

            assertThat( actual, is( equalTo( "invalid-mechanism" ) ) );
        }

        @Test
        public void testMALFORMED_REQUESTAsString()
            throws Exception
        {
            String actual = SaslElements.DefinedCondition.MALFORMED_REQUEST.asString();

            assertThat( actual, is( equalTo( "malformed-request" ) ) );
        }

        @Test
        public void testMECHANISM_TOO_WEAKAsString()
            throws Exception
        {
            String actual = SaslElements.DefinedCondition.MECHANISM_TOO_WEAK.asString();

            assertThat( actual, is( equalTo( "mechanism-too-weak" ) ) );
        }

        @Test
        public void testNOT_AUTHORIZEDAsString()
            throws Exception
        {
            String actual = SaslElements.DefinedCondition.NOT_AUTHORIZED.asString();

            assertThat( actual, is( equalTo( "not-authorized" ) ) );
        }

        @Test
        public void testTEMPORARY_AUTH_FAILUREAsString()
            throws Exception
        {
            String actual = SaslElements.DefinedCondition.TEMPORARY_AUTH_FAILURE.asString();

            assertThat( actual, is( equalTo( "temporary-auth-failure" ) ) );
        }


        @Test
        public void testFromString_in_enum()
            throws Exception
        {
            for ( SaslElements.DefinedCondition definedCondition : SaslElements.DefinedCondition.values() )
            {
                SaslElements.DefinedCondition actual =
                    SaslElements.DefinedCondition.fromString( definedCondition.asString() );
                SaslElements.DefinedCondition expected = definedCondition;
                assertThat( actual, is( equalTo( expected ) ) );
            }

        }

        @Test(expected = IllegalArgumentException.class)
        public void testFromString_not_in_enum()
            throws Exception
        {
            SaslElements.DefinedCondition.fromString( null );
        }
    }
}
