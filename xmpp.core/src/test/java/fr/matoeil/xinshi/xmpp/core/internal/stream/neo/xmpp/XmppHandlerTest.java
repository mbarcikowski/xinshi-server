/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.Attributes;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class XmppHandlerTest
{
    private XmppHandler target_;


    @Before
    public void setUp()
        throws Exception
    {
        target_ = new TestXmppHandler();
    }

    @Test
    public void testEndElement()
        throws Exception
    {
        target_.endElement( "namespace", "name", "prefix:name" );

        assertThat( target_, is( notNullValue() ) );
    }

    private static class TestXmppHandler
        extends XmppHandler
    {
        @Override
        public XmppHandler handleElementOpening( final String aNamespaceURI, final String aLocalName,
                                                 final String aQualifiedName )
        {
            throw new UnsupportedOperationException( "#handleElementOpening not implemented" );
        }

        @Override
        public void startPrefixMapping( final String aPrefix, final String aNamespaceURI )
        {
            throw new UnsupportedOperationException( "#startPrefixMapping not implemented" );
        }

        @Override
        public void startElement( final String aNamespaceURI, final String aLocalName, final String aQualifiedName,
                                  final Attributes aAttributes )
        {
            throw new UnsupportedOperationException( "#startElement not implemented" );
        }

        @Override
        public void startCDATA()
        {
            throw new UnsupportedOperationException( "#startCDATA not implemented" );
        }

        @Override
        public void characters( final char[] aChars, final int aStart, final int aLength )
        {
            throw new UnsupportedOperationException( "#characters not implemented" );
        }

        @Override
        public void endCDATA()
        {
            throw new UnsupportedOperationException( "#endCDATA not implemented" );
        }
    }
}
