/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo;

import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

import static org.mockito.Mockito.mock;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class HandlerTraitTest
{

    private HandlerTrait target_;

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new TestHandlerTrait();

    }

    @Test(expected = StreamException.class)
    public void testStartDTD()
        throws Exception
    {
        target_.startDTD( "name", "publicId", "systemId" );
    }

    @Test(expected = StreamException.class)
    public void testEndDTD()
        throws Exception
    {
        target_.endDTD();
    }

    @Test(expected = StreamException.class)
    public void testStartEntity()
        throws Exception
    {
        target_.startEntity( "name" );
    }

    @Test(expected = StreamException.class)
    public void testEndEntity()
        throws Exception
    {
        target_.endEntity( "name" );
    }

    @Test(expected = StreamException.class)
    public void testComment()
        throws Exception
    {
        target_.comment( new char[0], 0, 0 );
    }

    @Test(expected = StreamException.class)
    public void testProcessingInstruction()
        throws Exception
    {
        target_.processingInstruction( "target", "data" );
    }

    @Test(expected = StreamException.class)
    public void testSkippedEntity()
        throws Exception
    {
        target_.skippedEntity( "name" );
    }

    @Test
    public void testSetDocumentLocator()
        throws Exception
    {
        target_.setDocumentLocator( mock( Locator.class ) );
    }

    @Test
    public void testStartDocument()
        throws Exception
    {
        target_.startDocument();
    }

    @Test
    public void testEndDocument()
        throws Exception
    {
        target_.endDocument();
    }

    @Test
    public void testEndPrefixMapping()
        throws Exception
    {
        target_.endPrefixMapping( "prefix" );
    }

    @Test
    public void testIgnorableWhitespace()
        throws Exception
    {
        target_.ignorableWhitespace( new char[0], 0, 0 );
    }

    private static class TestHandlerTrait
        extends HandlerTrait
    {
        @Override
        public void startPrefixMapping( final String prefix, final String uri )
            throws SAXException
        {
            throw new UnsupportedOperationException( "#startPrefixMapping not implemented" );
        }

        @Override
        public void startElement( final String uri, final String localName, final String qName, final Attributes atts )
            throws SAXException
        {
            throw new UnsupportedOperationException( "#startElement not implemented" );
        }

        @Override
        public void endElement( final String uri, final String localName, final String qName )
            throws SAXException
        {
            throw new UnsupportedOperationException( "#endElement not implemented" );
        }

        @Override
        public void characters( final char[] ch, final int start, final int length )
            throws SAXException
        {
            throw new UnsupportedOperationException( "#characters not implemented" );
        }

        @Override
        public void startCDATA()
            throws SAXException
        {
            throw new UnsupportedOperationException( "#startCDATA not implemented" );
        }

        @Override
        public void endCDATA()
            throws SAXException
        {
            throw new UnsupportedOperationException( "#endCDATA not implemented" );
        }
    }
}
