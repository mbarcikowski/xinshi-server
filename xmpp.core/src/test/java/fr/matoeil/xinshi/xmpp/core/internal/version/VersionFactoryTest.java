package fr.matoeil.xinshi.xmpp.core.internal.version;

import fr.matoeil.xinshi.xmpp.core.version.Version;
import org.junit.Test;

import java.lang.reflect.Constructor;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class VersionFactoryTest
{
    @Test
    public void testConstructor()
        throws Exception
    {
        Constructor target = VersionFactory.class.getDeclaredConstructor();

        assertThat( target.isAccessible(), is( equalTo( false ) ) );

        target.setAccessible( true );
        target.newInstance();
    }

    @Test
    public void testFromString()
        throws Exception
    {
        Version expected = new Version( 1, 0 );

        Version actual = VersionFactory.fromString( "1.0" );

        assertThat( actual, is( equalTo( expected ) ) );
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFromString_with_invalid_version_string()
        throws Exception
    {
        Version expected = new Version( 1, 0 );

        Version actual = VersionFactory.fromString( "not.a.version" );

        assertThat( actual, is( equalTo( expected ) ) );
    }
}
