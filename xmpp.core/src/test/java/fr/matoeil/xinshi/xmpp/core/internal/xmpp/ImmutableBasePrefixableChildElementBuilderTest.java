/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.xmpp;

import com.google.common.collect.Lists;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableAttributeBuilder;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableCDATASectionBuilder;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableTextBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableBaseElement;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableCDATASection;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableChildElement;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableChildNodeBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableText;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Nullable;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

public class ImmutableBasePrefixableChildElementBuilderTest
{
    private final ImmutableNode mockedImmutableNode = mock( ImmutableNode.class );

    private final String expectedNamespaceURI = "namespace";

    private final String expectedPrefix = "prefix";

    private final String expectedLocalName = "localName";

    private TestImmutableBasePrefixableChildElementBuilder target_;

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new TestImmutableBasePrefixableChildElementBuilder();
    }

    @Test
    public void testWithPrefix()
        throws Exception
    {
        target_.withPrefix( expectedPrefix ).inNamespace( expectedNamespaceURI ).named( expectedLocalName );

        ImmutableBaseElement actual = target_.isChildOf( mock( ImmutableNode.class ) ).atIndex( 0 ).build();
        assertThat( actual.getNodeName(), is( equalTo( expectedPrefix + ":" + expectedLocalName ) ) );
    }


    @Test( expected = IllegalStateException.class )
    public void testBuild_without_namespace()
    {

        target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).build();
    }

    @Test( expected = IllegalStateException.class )
    public void testBuild_without_local_name()
    {

        target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( expectedNamespaceURI ).build();
    }

    @Test( expected = IllegalArgumentException.class )
    public void testNamed_with_invalid_name()
    {
        target_.named( null );
    }

    @Test
    public void testWithAttributes_list()
    {
        ImmutableChildElement actual =
            target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( expectedNamespaceURI ).named(
                expectedLocalName ).withAttributes( Lists.newArrayList( new ImmutableAttributeBuilder().named( "1" ),
                                                                        new ImmutableAttributeBuilder().named(
                                                                            "2" ) ) ).build();

        assertThat( actual.getAttributes().getLength(), is( equalTo( 2 ) ) );
    }

    @Test
    public void testWithAttributes_varargs()
    {
        ImmutableChildElement actual =
            target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( expectedNamespaceURI ).named(
                expectedLocalName ).withAttributes( new ImmutableAttributeBuilder().named( "1" ),
                                                    new ImmutableAttributeBuilder().named( "2" ) ).build();

        assertThat( actual.getAttributes().getLength(), is( equalTo( 2 ) ) );
    }

    @Test( expected = IllegalArgumentException.class )
    public void testWithAttribute_with_invalid_attribute()
    {
        target_.withAttribute( null );
    }

    @Test
    public void testWithAttribute()
    {
        ImmutableChildElement actual =
            target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( expectedNamespaceURI ).named(
                expectedLocalName ).withAttribute( new ImmutableAttributeBuilder().named( "name" ) ).build();

        assertThat( actual.getAttributes().getLength(), is( equalTo( 1 ) ) );
    }

    @Test
    public void testWithChildElements_list()
    {
        ImmutableChildElement actual =
            target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( expectedNamespaceURI ).named(
                expectedLocalName ).withChildElements(
                Lists.<ImmutableChildElementBuilder>newArrayList( createImmutableChildElementBuilderForTest(),
                                                                  createImmutableChildElementBuilderForTest() ) ).build();

        assertThat( actual.getChildNodes().getLength(), is( equalTo( 2 ) ) );
        assertThat( actual.getChildNodes().item( 0 ), is( instanceOf( ImmutableChildElement.class ) ) );
        assertThat( actual.getChildNodes().item( 1 ), is( instanceOf( ImmutableChildElement.class ) ) );
    }

    @Test
    public void testWithChildElements_varargs()
    {
        ImmutableChildElement actual =
            target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( expectedNamespaceURI ).named(
                expectedLocalName ).withChildElements( createImmutableChildElementBuilderForTest(),
                                                       createImmutableChildElementBuilderForTest() ).build();

        assertThat( actual.getChildNodes().getLength(), is( equalTo( 2 ) ) );
        assertThat( actual.getChildNodes().item( 0 ), is( instanceOf( ImmutableChildElement.class ) ) );
        assertThat( actual.getChildNodes().item( 1 ), is( instanceOf( ImmutableChildElement.class ) ) );
    }

    @Test( expected = IllegalArgumentException.class )
    public void testWithChildElement_with_invalid_child_node()
    {
        target_.withChildElement( null );
    }

    @Test
    public void testWithChildElement()
    {
        ImmutableChildElement actual =
            target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( expectedNamespaceURI ).named(
                expectedLocalName ).withChildElement( createImmutableChildElementBuilderForTest() ).build();

        assertThat( actual.getChildNodes().getLength(), is( equalTo( 1 ) ) );
        assertThat( actual.getChildNodes().item( 0 ), is( instanceOf( ImmutableChildElement.class ) ) );
    }

    @Test( expected = IllegalArgumentException.class )
    public void testWithText_with_null()
        throws Exception
    {
        ImmutableChildElement actual =
            target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( expectedNamespaceURI ).named(
                expectedLocalName ).withText( null ).build();

    }

    @Test
    public void testWithText()
        throws Exception
    {
        ImmutableChildElement actual =
            target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( expectedNamespaceURI ).named(
                expectedLocalName ).withText( new ImmutableTextBuilder() ).build();

        assertThat( actual.getChildNodes().getLength(), is( equalTo( 1 ) ) );
        assertThat( actual.getChildNodes().item( 0 ), is( instanceOf( ImmutableText.class ) ) );
    }

    @Test( expected = IllegalArgumentException.class )
    public void testWithCDATASection_with_null()
        throws Exception
    {
        ImmutableChildElement actual =
            target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( expectedNamespaceURI ).named(
                expectedLocalName ).withCDATASection( null ).build();

    }

    @Test
    public void testWithCDATASection()
        throws Exception
    {
        ImmutableChildElement actual =
            target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( expectedNamespaceURI ).named(
                expectedLocalName ).withCDATASection( new ImmutableCDATASectionBuilder() ).build();

        assertThat( actual.getChildNodes().getLength(), is( equalTo( 1 ) ) );
        assertThat( actual.getChildNodes().item( 0 ), is( instanceOf( ImmutableCDATASection.class ) ) );
    }

    private ImmutableChildElementBuilder createImmutableChildElementBuilderForTest()
    {
        return new ImmutableChildElementBuilder().named( "name" );
    }

    private class TestImmutableBasePrefixableChildElementBuilder
        extends
        ImmutableBasePrefixableChildElementBuilder<ImmutableChildElement, TestImmutableBasePrefixableChildElementBuilder>
    {


        @Override
        protected ImmutableChildElement newPrefixableImmutableChildElementBuilder( final ImmutableNode aParentNode,
                                                                                   final Integer aElementIndex,
                                                                                   @Nullable final String aNamespaceURI,
                                                                                   @Nullable final String aPrefix,
                                                                                   final String aLocalName,
                                                                                   final List<ImmutableAttributeBuilder> aAttributeBuilders,
                                                                                   final List<ImmutableChildNodeBuilder> aChildNodeBuilders )
        {
            return new ImmutableChildElement( aParentNode, aElementIndex, aNamespaceURI, aPrefix, aLocalName,
                                              aAttributeBuilders, aChildNodeBuilders );
        }


    }
}
