/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza.error;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableOneImmutableAttributeMap;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.Xmls;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;

import static fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorCondition.DefinedCondition;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class StanzaErrorConditionTest
{
    private StanzaErrorCondition target_;

    private final ImmutableNode mockedImmutableNode_ = mock( ImmutableNode.class );

    private final int expectedElementIndex = 0;

    private final String expectedNamespaceURI = "urn:ietf:params:xml:ns:xmpp-stanzas";

    private final DefinedCondition expectedDefinedCondition_ = DefinedCondition.UNDEFINED_CONDITION;

    private final String expectedLocalName = "undefined-condition";

    private final String expectedContent = "a content";

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new StanzaErrorCondition( mockedImmutableNode_, expectedElementIndex, expectedDefinedCondition_,
                                            expectedContent );
    }

    @Test
    public void testGetDefinedCondition()
        throws Exception
    {
        DefinedCondition actualDefinedCondition = target_.getDefinedCondition();

        assertThat( actualDefinedCondition, is( equalTo( expectedDefinedCondition_ ) ) );
    }

    @Test
    public void testGetContent()
        throws Exception
    {
        String actualContent = target_.getContent();

        assertThat( actualContent, is( equalTo( expectedContent ) ) );
    }

    @Test
    public void testGetTagName()
        throws Exception
    {
        String actual = target_.getTagName();

        assertThat( actual, is( equalTo( expectedLocalName ) ) );
    }


    @Test
    public void testGetNodeName()
        throws Exception
    {
        String actual = target_.getNodeName();

        assertThat( actual, is( equalTo( expectedLocalName ) ) );
    }

    @Test
    public void testGetNamespaceURI()
        throws Exception
    {
        String actual = target_.getNamespaceURI();

        assertThat( actual, is( equalTo( expectedNamespaceURI ) ) );
    }


    @Test
    public void testGetLocalName()
        throws Exception
    {
        String actual = target_.getLocalName();

        assertThat( actual, is( equalTo( expectedLocalName ) ) );
    }

    @Test
    public void testGetAttributes()
        throws Exception
    {
        NamedNodeMap actual = target_.getAttributes();

        assertThat( actual, is( instanceOf( ImmutableOneImmutableAttributeMap.class ) ) );
    }

    @Test
    public void testGetAttribute_with_unknow_attribute()
        throws Exception
    {
        String actual = target_.getAttribute( "name" );

        assertThat( actual, isEmptyString() );
    }

    @Test
    public void testGetAttribute_with_know_attribute()
        throws Exception
    {
        String actual = target_.getAttribute( Xmls.XMLNS_ATTRIBUTE );

        assertThat( actual, is( equalTo( Namespaces.STANZAS_NAMESPACE_URI ) ) );
    }


    @Test
    public void testGetAttributeNode_with_unknow_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNode( "name" );

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetAttributeNode_with_know_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNode( Xmls.XMLNS_ATTRIBUTE );

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getNodeName(), is( equalTo( Xmls.XMLNS_ATTRIBUTE ) ) );
        assertThat( actual.getNodeValue(), is( equalTo( Namespaces.STANZAS_NAMESPACE_URI ) ) );
    }

    @Test
    public void testGetAttributeNS_with_unknow_attribute()
        throws Exception
    {
        String actual = target_.getAttributeNS( "namespace", "localName" );

        assertThat( actual, isEmptyString() );
    }

    @Test
    public void testGetAttributeNS_with_know_attribute()
        throws Exception
    {
        String actual = target_.getAttributeNS( Xmls.XMLNS_ATTRIBUTE_NS_URI, Xmls.XMLNS_ATTRIBUTE );

        assertThat( actual, is( equalTo( Namespaces.STANZAS_NAMESPACE_URI ) ) );
    }

    @Test
    public void testGetAttributeNodeNS_with_unknow_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNodeNS( "namespace", "localName" );

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetAttributeNodeNS_with_know_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNodeNS( Xmls.XMLNS_ATTRIBUTE_NS_URI, Xmls.XMLNS_ATTRIBUTE );

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getNodeName(), is( equalTo( Xmls.XMLNS_ATTRIBUTE ) ) );
        assertThat( actual.getNodeValue(), is( equalTo( Namespaces.STANZAS_NAMESPACE_URI ) ) );
    }


    @Test
    public void testHasAttribute_with_unknow_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttribute( "name" );

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Test
    public void testHasAttribute_with_know_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttribute( Xmls.XMLNS_ATTRIBUTE );

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testHasAttributeNS_with_unknow_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttributeNS( "namespace", "localName" );

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Test
    public void testHasAttributeNS_with_know_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttributeNS( Xmls.XMLNS_ATTRIBUTE_NS_URI, Xmls.XMLNS_ATTRIBUTE );

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testGetTextContent()
        throws Exception
    {
        String actual = target_.getTextContent();

        assertThat( actual, is( equalTo( expectedContent ) ) );
    }

    public static class DefinedConditionTest
    {

        @Test
        public void testBAD_REQUESTAsString()
            throws Exception
        {
            String actual = DefinedCondition.BAD_REQUEST.asString();

            assertThat( actual, is( equalTo( "bad-request" ) ) );
        }


        @Test
        public void testCONFLICTAsString()
            throws Exception
        {
            String actual = DefinedCondition.CONFLICT.asString();

            assertThat( actual, is( equalTo( "conflict" ) ) );
        }


        @Test
        public void testFEATURE_NOT_IMPLEMENTEDAsString()
            throws Exception
        {
            String actual = DefinedCondition.FEATURE_NOT_IMPLEMENTED.asString();

            assertThat( actual, is( equalTo( "feature-not-implemented" ) ) );
        }


        @Test
        public void testFORBIDDENAsString()
            throws Exception
        {
            String actual = DefinedCondition.FORBIDDEN.asString();

            assertThat( actual, is( equalTo( "forbidden" ) ) );
        }


        @Test
        public void testGONEAsString()
            throws Exception
        {
            String actual = DefinedCondition.GONE.asString();

            assertThat( actual, is( equalTo( "gone" ) ) );
        }


        @Test
        public void testINTERNAL_SERVER_ERRORAsString()
            throws Exception
        {
            String actual = DefinedCondition.INTERNAL_SERVER_ERROR.asString();

            assertThat( actual, is( equalTo( "internal-server-error" ) ) );
        }


        @Test
        public void testITEM_NOT_FOUNDAsString()
            throws Exception
        {
            String actual = DefinedCondition.ITEM_NOT_FOUND.asString();

            assertThat( actual, is( equalTo( "item-not-found" ) ) );
        }


        @Test
        public void testJID_MALFORMEDAsString()
            throws Exception
        {
            String actual = DefinedCondition.JID_MALFORMED.asString();

            assertThat( actual, is( equalTo( "jid-malformed" ) ) );
        }


        @Test
        public void testNOT_ACCEPTABLEAsString()
            throws Exception
        {
            String actual = DefinedCondition.NOT_ACCEPTABLE.asString();

            assertThat( actual, is( equalTo( "not-acceptable" ) ) );
        }


        @Test
        public void testNOT_ALLOWEDAsString()
            throws Exception
        {
            String actual = DefinedCondition.NOT_ALLOWED.asString();

            assertThat( actual, is( equalTo( "not-allowed" ) ) );
        }


        @Test
        public void testNOT_AUTHORIZEDAsString()
            throws Exception
        {
            String actual = DefinedCondition.NOT_AUTHORIZED.asString();

            assertThat( actual, is( equalTo( "not-authorized" ) ) );
        }


        @Test
        public void testPOLICY_VIOLATIONAsString()
            throws Exception
        {
            String actual = DefinedCondition.POLICY_VIOLATION.asString();

            assertThat( actual, is( equalTo( "policy-violation" ) ) );
        }


        @Test
        public void testRECIPIENT_UNAVAILABLEAsString()
            throws Exception
        {
            String actual = DefinedCondition.RECIPIENT_UNAVAILABLE.asString();

            assertThat( actual, is( equalTo( "recipient-unavailable" ) ) );
        }


        @Test
        public void testREDIRECTAsString()
            throws Exception
        {
            String actual = DefinedCondition.REDIRECT.asString();

            assertThat( actual, is( equalTo( "redirect" ) ) );
        }


        @Test
        public void testREGISTRATION_REQUIREDAsString()
            throws Exception
        {
            String actual = DefinedCondition.REGISTRATION_REQUIRED.asString();

            assertThat( actual, is( equalTo( "registration-required" ) ) );
        }


        @Test
        public void testREMOTE_SERVER_NOT_FOUNDAsString()
            throws Exception
        {
            String actual = DefinedCondition.REMOTE_SERVER_NOT_FOUND.asString();

            assertThat( actual, is( equalTo( "remote-server-not-found" ) ) );
        }


        @Test
        public void testREMOTE_SERVER_TIMEOUTAsString()
            throws Exception
        {
            String actual = DefinedCondition.REMOTE_SERVER_TIMEOUT.asString();

            assertThat( actual, is( equalTo( "remote-server-timeout" ) ) );
        }


        @Test
        public void testRESOURCE_CONSTRAINTAsString()
            throws Exception
        {
            String actual = DefinedCondition.RESOURCE_CONSTRAINT.asString();

            assertThat( actual, is( equalTo( "resource-constraint" ) ) );
        }


        @Test
        public void testSERVICE_UNAVAILABLEAsString()
            throws Exception
        {
            String actual = DefinedCondition.SERVICE_UNAVAILABLE.asString();

            assertThat( actual, is( equalTo( "service-unavailable" ) ) );
        }


        @Test
        public void testSUBSCRIPTION_REQUIREDAsString()
            throws Exception
        {
            String actual = DefinedCondition.SUBSCRIPTION_REQUIRED.asString();

            assertThat( actual, is( equalTo( "subscription-required" ) ) );
        }


        @Test
        public void testUNDEFINED_CONDITIONAsString()
            throws Exception
        {
            String actual = DefinedCondition.UNDEFINED_CONDITION.asString();

            assertThat( actual, is( equalTo( "undefined-condition" ) ) );
        }


        @Test
        public void testUNEXPECTED_REQUESTAsString()
            throws Exception
        {
            String actual = DefinedCondition.UNEXPECTED_REQUEST.asString();

            assertThat( actual, is( equalTo( "unexpected-request" ) ) );
        }

        @Test
        public void testFromString_in_enum()
            throws Exception
        {
            for ( DefinedCondition definedCondition : DefinedCondition.values() )
            {
                DefinedCondition actual = DefinedCondition.fromString( definedCondition.asString() );
                DefinedCondition expected = definedCondition;
                assertThat( actual, is( equalTo( expected ) ) );
            }

        }

        @Test( expected = IllegalArgumentException.class )
        public void testFromString_not_in_enum()
            throws Exception
        {
            DefinedCondition.fromString( null );
        }
    }

}
