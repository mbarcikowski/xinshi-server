/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.dom;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableCDATASection;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class ImmutableCDATASectionBuilderTest
{
    private ImmutableCDATASectionBuilder target_;

    private final ImmutableNode mockedImmutableNode = mock( ImmutableNode.class );

    private final String expectedData = "data";

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new ImmutableCDATASectionBuilder();
    }


    @Test
    public void testBuild()
    {
        ImmutableCDATASection actual =
            target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).withData( expectedData ).build();

        assertThat( actual.getParentNode(), is( equalTo( (Node) mockedImmutableNode ) ) );
        assertThat( actual.getData(), is( equalTo( expectedData ) ) );
    }
}
