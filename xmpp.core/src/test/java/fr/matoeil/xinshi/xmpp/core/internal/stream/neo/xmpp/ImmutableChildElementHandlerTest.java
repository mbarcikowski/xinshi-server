/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp;

import fr.matoeil.xinshi.xmpp.core.dom.ImmutableChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableCDATASection;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableChildElement;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableText;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Attr;
import org.w3c.dom.Node;
import org.xml.sax.Attributes;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class ImmutableChildElementHandlerTest
{

    private ImmutableChildElementHandler target_;

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new ImmutableChildElementHandler();
    }

    @Test
    public void testGetBuilder()
        throws Exception
    {
        ImmutableChildElementBuilder actual = target_.getBuilder();

        assertThat( actual, is( instanceOf( ImmutableChildElementBuilder.class ) ) );
    }

    @Test
    public void testHandleElementOpening()
        throws Exception
    {
        XmppHandler xmppHandler = target_.handleElementOpening( "namespace", "name", "prefix:name" );

        xmppHandler.startElement( "namespace", "name", "prefix:name", mock( Attributes.class ) );
        ImmutableChildElement actual =
            target_.getBuilder().isChildOf( mock( ImmutableNode.class ) ).atIndex( 0 ).inNamespace( "namespace" ).named(
                "name" ).build();

        Assert.assertThat( actual, is( notNullValue() ) );
        Assert.assertThat( actual.getChildNodes().getLength(), is( equalTo( 1 ) ) );
    }


    @Test
    public void testStartPrefixMapping()
        throws Exception
    {
        target_.startPrefixMapping( "prefix", "namespace" );

        ImmutableChildElement actual =
            target_.getBuilder().isChildOf( mock( ImmutableNode.class ) ).atIndex( 0 ).named( "name" ).build();

        assertThat( actual, is( notNullValue() ) );

    }

    @Test
    public void testStartElement()
        throws Exception
    {
        final String expectedAttributeNamespaceURI = "namespace";
        final String expectedAttributePrefix = "prefix";
        final String expectedAttributeName = "name";
        final String expectedAttributeValue = "value";
        Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getURI( 0 ) ).thenReturn( expectedAttributeNamespaceURI );
        when( mockedAttributes.getQName( 0 ) ).thenReturn( expectedAttributePrefix + ":" + expectedAttributeName );
        when( mockedAttributes.getValue( 0 ) ).thenReturn( expectedAttributeValue );
        String expectedNamespaceURI = "namespace";
        String expectedName = "name";
        String expectedPrefix = "prefix";

        target_.startElement( expectedNamespaceURI, expectedName, expectedPrefix + ":" + expectedName,
                              mockedAttributes );

        ImmutableChildElement actual =
            target_.getBuilder().isChildOf( mock( ImmutableNode.class ) ).atIndex( 0 ).build();

        Assert.assertThat( actual, is( notNullValue() ) );
        Assert.assertThat( actual.getNamespaceURI(), is( equalTo( expectedNamespaceURI ) ) );
        Assert.assertThat( actual.getLocalName(), is( equalTo( expectedName ) ) );
        Assert.assertThat( actual.getNodeName(), is( equalTo( expectedPrefix + ":" + expectedName ) ) );
        Assert.assertThat( actual.getAttributes().getLength(), is( equalTo( 1 ) ) );
        Attr item = actual.getAttributeNodeNS( expectedAttributeNamespaceURI, expectedAttributeName );
        Assert.assertThat( item.getNodeName(), is( equalTo( expectedAttributePrefix + ":" + expectedAttributeName ) ) );
        Assert.assertThat( item.getValue(), is( equalTo( expectedAttributeValue ) ) );
    }

    @Test
    public void testStartCDATA()
        throws Exception
    {
        target_.startCDATA();

        ImmutableChildElement actual =
            target_.getBuilder().isChildOf( mock( ImmutableNode.class ) ).atIndex( 0 ).inNamespace( "namespace" ).named(
                "name" ).build();
        Assert.assertThat( actual, is( notNullValue() ) );
    }

    @Test
    public void testCharacters_not_in_cdata_section()
        throws Exception
    {

        char[] characters = "        ".toCharArray();

        target_.characters( characters, 0, characters.length );

        ImmutableChildElement actual =
            target_.getBuilder().isChildOf( mock( ImmutableNode.class ) ).atIndex( 0 ).inNamespace( "namespace" ).named(
                "name" ).build();
        Assert.assertThat( actual.getChildNodes().getLength(), is( equalTo( 1 ) ) );
        Node childNode = actual.getChildNodes().item( 0 );
        Assert.assertThat( childNode, instanceOf( ImmutableText.class ) );
    }

    @Test
    public void testCharacters_in_cdata_section()
        throws Exception
    {

        char[] characters = "        ".toCharArray();

        target_.startCDATA();
        target_.characters( characters, 0, characters.length );
        target_.endCDATA();

        ImmutableChildElement actual =
            target_.getBuilder().isChildOf( mock( ImmutableNode.class ) ).atIndex( 0 ).inNamespace( "namespace" ).named(
                "name" ).build();
        Assert.assertThat( actual.getChildNodes().getLength(), is( equalTo( 1 ) ) );
        Node childNode = actual.getChildNodes().item( 0 );
        Assert.assertThat( childNode, instanceOf( ImmutableCDATASection.class ) );
    }

    @Test
    public void testEndCDATA()
        throws Exception
    {
        target_.endCDATA();

        ImmutableChildElement actual =
            target_.getBuilder().isChildOf( mock( ImmutableNode.class ) ).atIndex( 0 ).inNamespace( "namespace" ).named(
                "name" ).build();
        Assert.assertThat( actual, is( notNullValue() ) );
    }

}
