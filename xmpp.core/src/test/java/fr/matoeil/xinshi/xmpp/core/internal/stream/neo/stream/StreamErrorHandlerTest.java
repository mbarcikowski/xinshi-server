/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stream;

import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stream.error.StreamErrorConditionHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stream.error.StreamErrorTextHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp.ImmutableChildElementHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp.XmppHandler;
import fr.matoeil.xinshi.xmpp.core.stream.StreamError;
import fr.matoeil.xinshi.xmpp.core.stream.StreamErrorBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import fr.matoeil.xinshi.xmpp.core.stream.error.StreamErrorCondition;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.Attributes;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StreamErrorHandlerTest
{
    private final String expectedNamespaceURI_ = "urn:ietf:params:xml:ns:xmpp-streams";

    private StreamErrorHandler target_;

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new StreamErrorHandler();
    }

    @Test
    public void testGetStanzaErrorBuilder()
        throws Exception
    {
        StreamErrorBuilder actual = target_.getBuilder();

        assertThat( actual, is( notNullValue() ) );
    }

    @Test(expected = StreamException.class)
    public void testHandleElementOpening_without_namespace()
        throws Exception
    {
        target_.handleElementOpening( null, "name", "name" );
    }

    @Test
    public void testHandleElementOpening_with_text()
        throws Exception
    {
        XmppHandler actual = target_.handleElementOpening( expectedNamespaceURI_, "text", "text" );

        assertThat( actual, is( instanceOf( StreamErrorTextHandler.class ) ) );
    }

    @Test
    public void testHandleElementOpening_with_condition()
        throws Exception
    {
        StreamErrorCondition.DefinedCondition expectedDefinedCondition =
            StreamErrorCondition.DefinedCondition.CONNECTION_TIMEOUT;

        XmppHandler actual = target_.handleElementOpening( expectedNamespaceURI_, expectedDefinedCondition.asString(),
                                                           expectedDefinedCondition.asString() );

        assertThat( actual, is( instanceOf( StreamErrorConditionHandler.class ) ) );
    }

    @Test
    public void testHandleElementOpening_with_element_not_in_stanza_namespace()
        throws Exception
    {
        XmppHandler actual = target_.handleElementOpening( "namespace", "name", "name" );

        assertThat( actual, is( instanceOf( ImmutableChildElementHandler.class ) ) );
    }

    @Test(expected = StreamException.class)
    public void testStartPrefixMapping_with_stream_namespace_prefixed()
        throws Exception
    {
        target_.startPrefixMapping( null, "http://etherx.jabber.org/streams" );
    }

    @Test
    public void testStartPrefixMapping_with_client_namespace_not_prefixed()
        throws Exception
    {
        target_.startPrefixMapping( null, "jabber:client" );

        assertStanzaErrorBuilt();
    }

    @Test
    public void testStartPrefixMapping_with_other_mapping()
        throws Exception
    {
        target_.startPrefixMapping( "prefix", "namespace" );

        assertStanzaErrorBuilt();
    }

    @Test(expected = StreamException.class)
    public void testStartElement_with_attribute()
        throws Exception
    {
        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( eq( 0 ) ) ).thenReturn( "attribtue" );
        when( mockedAttributes.getValue( eq( 0 ) ) ).thenReturn( "value" );

        target_.startElement( "http://etherx.jabber.org/streams", "error", "stream:error", mockedAttributes );
    }


    @Test
    public void testStartElement_without_attribute()
        throws Exception
    {
        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 0 );

        target_.startElement( "http://etherx.jabber.org/streams", "error", "stream:error", mockedAttributes );

        assertStanzaErrorBuilt();
    }


    @Test(expected = StreamException.class)
    public void testStartElement_with_other_attribute()
        throws Exception
    {
        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( eq( 0 ) ) ).thenReturn( "attribute" );
        when( mockedAttributes.getValue( eq( 0 ) ) ).thenReturn( "value" );
        target_.startElement( "jabber:client", "error", "error", mockedAttributes );
    }

    @Test(expected = StreamException.class)
    public void testStartCDATA()
        throws Exception
    {
        target_.startCDATA();
    }

    @Test
    public void testCharacters_with_whitespaces()
        throws Exception
    {
        char[] characters = "        ".toCharArray();

        target_.characters( characters, 0, characters.length );

        assertStanzaErrorBuilt();
    }

    @Test(expected = StreamException.class)
    public void testCharacters_without_whitespaces_only()
        throws Exception
    {
        char[] characters = "    a    ".toCharArray();
        target_.characters( characters, 0, characters.length );
    }

    @Test(expected = StreamException.class)
    public void testEndCDATA()
        throws Exception
    {
        target_.endCDATA();
    }

    private void assertStanzaErrorBuilt()
    {
        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 0 );
        target_.startElement( "http://etherx.jabber.org/streams", "error", "stream:error", mockedAttributes );
        StreamErrorCondition.DefinedCondition expectedDefinedCondition =
            StreamErrorCondition.DefinedCondition.CONNECTION_TIMEOUT;
        XmppHandler xmppHandler =
            target_.handleElementOpening( expectedNamespaceURI_, expectedDefinedCondition.asString(),
                                          expectedDefinedCondition.asString() );
        Attributes mockedConditionAttributes = mock( Attributes.class );
        when( mockedConditionAttributes.getLength() ).thenReturn( 1 );
        when( mockedConditionAttributes.getQName( eq( 0 ) ) ).thenReturn( "xmlns" );
        when( mockedConditionAttributes.getValue( eq( 0 ) ) ).thenReturn( expectedNamespaceURI_ );
        xmppHandler.startElement( expectedNamespaceURI_, expectedDefinedCondition.asString(),
                                  expectedDefinedCondition.asString(), mockedConditionAttributes );
        StreamError actual = target_.getBuilder().build();
        assertThat( actual, is( notNullValue() ) );
    }


}
