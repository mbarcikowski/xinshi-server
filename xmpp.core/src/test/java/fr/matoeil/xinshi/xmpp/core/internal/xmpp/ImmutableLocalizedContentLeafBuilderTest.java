/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.xmpp;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Nullable;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class ImmutableLocalizedContentLeafBuilderTest
{
    private TestBuilder target_;

    private final ImmutableNode mockedImmutableNode = mock( ImmutableNode.class );

    private final String expectedNamespaceURI = "namespace";

    private final String expectedLocalName = "localName";

    private final String expectedLanguage = "fr";

    private final String expectedContent = "content";


    @Before
    public void setUp()
        throws Exception
    {
        target_ = new TestBuilder();
    }

    @Test
    public void testBuild()
    {

        ImmutableLocalizedContentLeaf actual =
            target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( expectedNamespaceURI ).withContent(
                expectedContent ).inLanguage( expectedLanguage ).build();

        assertThat( actual.getLanguage(), is( equalTo( expectedLanguage ) ) );
        assertThat( actual.getContent(), is( equalTo( expectedContent ) ) );
    }

    private class TestBuilder
        extends ImmutableLocalizedContentLeafBuilder<ImmutableLocalizedContentLeaf, TestBuilder>
    {

        @Override
        protected ImmutableLocalizedContentLeaf newImmutableLocalizedContentLeaf( ImmutableNode aParentNode,
                                                                                  Integer aElementIndex,
                                                                                  String aNamespaceURI,
                                                                                  @Nullable String aLanguage,
                                                                                  @Nullable String aContent )
        {
            return new ImmutableLocalizedContentLeaf( aParentNode, aElementIndex, aNamespaceURI, expectedLocalName,
                                                      aLanguage, aContent )
            {
            };
        }
    }
}
