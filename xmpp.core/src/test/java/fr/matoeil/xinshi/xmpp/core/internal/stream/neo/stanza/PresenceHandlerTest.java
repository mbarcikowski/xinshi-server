/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza;

import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.error.StanzaErrorHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.presence.PriorityHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.presence.ShowHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.presence.StatusHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp.XmppHandler;
import fr.matoeil.xinshi.xmpp.core.stanza.Presence;
import fr.matoeil.xinshi.xmpp.core.stanza.PresenceBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.Attributes;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PresenceHandlerTest
{
    private PresenceHandler target_;

    private final String expectedNamespaceURI = "jabber:client";

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new PresenceHandler( expectedNamespaceURI );
    }

    @Test
    public void testGetBuilder()
        throws Exception
    {
        PresenceBuilder actual = target_.getBuilder();

        assertThat( actual, is( instanceOf( PresenceBuilder.class ) ) );
    }

    @Test
    public void testHandleElementOpening_other_configured_namespace()
        throws Exception
    {
        XmppHandler actual = target_.handleElementOpening( "namespace", "name", "prefix" );

        assertThat( actual, is( instanceOf( ExtendedContentHandler.class ) ) );
    }

    @Test
    public void testGetXmppHandler_with_show()
        throws Exception
    {
        XmppHandler actual = target_.getXmppHandler( "show" );

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual, is( instanceOf( ShowHandler.class ) ) );
    }

    @Test
    public void testGetXmppHandler_with_status()
        throws Exception
    {
        XmppHandler actual = target_.getXmppHandler( "status" );

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual, is( instanceOf( StatusHandler.class ) ) );
    }

    @Test
    public void testGetXmppHandler_with_priority()
        throws Exception
    {
        XmppHandler actual = target_.getXmppHandler( "priority" );

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual, is( instanceOf( PriorityHandler.class ) ) );
    }

    @Test
    public void testGetXmppHandler_with_error()
        throws Exception
    {
        XmppHandler actual = target_.getXmppHandler( "error" );

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual, is( instanceOf( StanzaErrorHandler.class ) ) );
    }

    @Test(expected = StreamException.class)
    public void testGetXmppHandler_with_invalid_name()
        throws Exception
    {
        target_.getXmppHandler( "name" );
    }

    @Test
    public void testStartElement_with_type_attribute()
        throws Exception
    {
        final Presence.Type expectedType = Presence.Type.ERROR;

        Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( 0 ) ).thenReturn( "type" );
        when( mockedAttributes.getValue( 0 ) ).thenReturn( expectedType.asString() );

        target_.startElement( "namespace", "name", "prefix:name", mockedAttributes );

        Presence actual = target_.build();
        assertThat( actual.getType(), is( equalTo( expectedType ) ) );
    }

    @Test( expected = StreamException.class )
    public void testStartElement_with_bad_type_attribute()
        throws Exception
    {

        Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( 0 ) ).thenReturn( "type" );
        when( mockedAttributes.getValue( 0 ) ).thenReturn( "" );

        target_.startElement( "namespace", "name", "prefix:name", mockedAttributes );

    }
}
