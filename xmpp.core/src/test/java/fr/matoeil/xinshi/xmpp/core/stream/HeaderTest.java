/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream;

import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.address.Addresses;
import fr.matoeil.xinshi.xmpp.core.version.Version;
import fr.matoeil.xinshi.xmpp.core.version.Versions;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class HeaderTest
{

    private Header target_;

    private final Version expectedVersion_ = Versions.fromString( "1.0" );

    private final Address expectedFrom_ = Addresses.fromString( "me@domain.com" );

    private final Address expectedTo_ = Addresses.fromString( "it@domain.com" );

    private final String expectedId_ = "id";

    private final String expectedLang_ = "lang";


    @Before
    public void setUp()
        throws Exception
    {
        target_ = new Header( expectedVersion_, expectedFrom_, expectedTo_, expectedId_, expectedLang_ );
    }

    @Test
    public void testVersion()
        throws Exception
    {
        Version actual = target_.version();

        assertThat( actual, is( equalTo( expectedVersion_ ) ) );
    }

    @Test
    public void testFrom()
        throws Exception
    {
        Address actual = target_.from();

        assertThat( actual, is( equalTo( expectedFrom_ ) ) );
    }

    @Test
    public void testTo()
        throws Exception
    {
        Address actual = target_.to();

        assertThat( actual, is( equalTo( expectedTo_ ) ) );
    }

    @Test
    public void testId()
        throws Exception
    {
        String actual = target_.id();

        assertThat( actual, is( equalTo( expectedId_ ) ) );
    }

    @Test
    public void testLang()
        throws Exception
    {
        String actual = target_.lang();

        assertThat( actual, is( equalTo( expectedLang_ ) ) );

    }
}
