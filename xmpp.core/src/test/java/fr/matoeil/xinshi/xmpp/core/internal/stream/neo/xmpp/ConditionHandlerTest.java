/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.Condition;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.ConditionBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.DefinedCondition;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.Attributes;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class ConditionHandlerTest
{
    private TestConditionHandler target_;

    private final TestConditionBuilder expectedConditionBuilder = new TestConditionBuilder();

    private final String expectedDefinitionConditionName = "name";

    private final String expectedNamespaceURI = "namespace";

    private final ImmutableNode mockedParentNode_ = mock( ImmutableNode.class );

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new TestConditionHandler();

    }

    @Test
    public void testGetConditionBuilder()
        throws Exception
    {
        ConditionBuilder actual = target_.getConditionBuilder();

        assertThat( actual, is( notNullValue() ) );
    }

    @Test(expected = StreamException.class)
    public void testHandleElementOpening()
        throws Exception
    {
        target_.handleElementOpening( "namespace", "name", "name" );
    }

    @Test(expected = StreamException.class)
    public void testStartPrefixMapping()
        throws Exception
    {
        target_.startPrefixMapping( "prefix", "namespace" );
    }

    @Test(expected = StreamException.class)
    public void testStartElement_without_attributes()
        throws Exception
    {
        final Attributes mockedAttributes = mock( Attributes.class );

        target_.startElement( "namespace", expectedDefinitionConditionName, expectedDefinitionConditionName,
                              mockedAttributes );


    }

    @Test
    public void testStartElement_with_valid_name()
        throws Exception
    {

        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( eq( 0 ) ) ).thenReturn( "xmlns" );
        when( mockedAttributes.getValue( eq( 0 ) ) ).thenReturn( expectedNamespaceURI );
        target_.startElement( "namespace", expectedDefinitionConditionName, expectedDefinitionConditionName,
                              mockedAttributes );

        TestCondition actual = target_.getConditionBuilder().isChildOf( mockedParentNode_ ).atIndex( 0 ).build();
        assertThat( actual.getDefinedCondition(), is( equalTo( TestDefinedCondition.NAME ) ) );
    }

    @Test(expected = StreamException.class)
    public void testStartElement_with_invalid_name()
        throws Exception
    {

        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( eq( 0 ) ) ).thenReturn( "xmlns" );
        when( mockedAttributes.getValue( eq( 0 ) ) ).thenReturn( expectedNamespaceURI );
        target_.startElement( "namespace", expectedDefinitionConditionName, "prefix:" + expectedDefinitionConditionName,
                              mockedAttributes );
    }

    @Test(expected = StreamException.class)
    public void testStartElement_with_unkown_name()
        throws Exception
    {

        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( eq( 0 ) ) ).thenReturn( "xmlns" );
        when( mockedAttributes.getValue( eq( 0 ) ) ).thenReturn( expectedNamespaceURI );
        target_.startElement( "namespace", "othername", "othername", mockedAttributes );
    }

    @Test
    public void testStartElement_with_correct_xmlns()
        throws Exception
    {
        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( eq( 0 ) ) ).thenReturn( "xmlns" );
        when( mockedAttributes.getValue( eq( 0 ) ) ).thenReturn( expectedNamespaceURI );
        target_.startElement( "namespace", expectedDefinitionConditionName, expectedDefinitionConditionName,
                              mockedAttributes );

        Condition actual = target_.getConditionBuilder().isChildOf( mockedParentNode_ ).atIndex( 0 ).build();
        assertThat( actual, is( notNullValue() ) );
    }

    @Test(expected = StreamException.class)
    public void testStartElement_with_incorrect_xmlns()
        throws Exception
    {
        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( eq( 0 ) ) ).thenReturn( "xmlns" );
        when( mockedAttributes.getValue( eq( 0 ) ) ).thenReturn( "other_namespace" );
        target_.startElement( "namespace", "name", "name", mockedAttributes );
    }

    @Test(expected = StreamException.class)
    public void testStartElement_with_other_attributes()
        throws Exception
    {
        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( eq( 0 ) ) ).thenReturn( "any" );
        target_.startElement( "namespace", "name", "name", mockedAttributes );
    }

    @Test(expected = StreamException.class)
    public void testStartCDATA()
        throws Exception
    {
        target_.startCDATA();
    }

    @Test
    public void testCharacters()
        throws Exception
    {
        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( eq( 0 ) ) ).thenReturn( "xmlns" );
        when( mockedAttributes.getValue( eq( 0 ) ) ).thenReturn( expectedNamespaceURI );
        target_.startElement( "namespace", expectedDefinitionConditionName, expectedDefinitionConditionName,
                              mockedAttributes );

        String expectedContent = "content";
        char[] chars = expectedContent.toCharArray();

        target_.characters( chars, 0, chars.length );

        TestCondition actual = target_.getConditionBuilder().isChildOf( mockedParentNode_ ).atIndex( 0 ).build();
        assertThat( actual.getContent(), is( equalTo( expectedContent ) ) );
    }

    @Test(expected = StreamException.class)
    public void testEndCDATA()
        throws Exception
    {
        target_.endCDATA();
    }

    @Test
    public void testGetDefinedCondition()
        throws Exception
    {
        DefinedCondition actual = target_.getDefinedCondition( expectedDefinitionConditionName );

        assertThat( actual, is( instanceOf( TestDefinedCondition.class ) ) );
    }


    private enum TestDefinedCondition
        implements DefinedCondition
    {
        NAME( "name" );

        private final String name_;

        TestDefinedCondition( final String aName )
        {
            name_ = aName;
        }

        @Override
        public String asString()
        {
            return name_;
        }
    }


    private class TestCondition
        extends Condition<TestDefinedCondition>
    {
        public TestCondition( final ImmutableNode aParentNode, final Integer aElementIndex,
                              final TestDefinedCondition aDefinedCondition, final String aContent )
        {
            super( aParentNode, aElementIndex, expectedNamespaceURI, aDefinedCondition, aContent );
        }
    }

    private class TestConditionBuilder
        extends ConditionBuilder<TestDefinedCondition, TestCondition, TestConditionBuilder>
    {
        @Override
        protected TestCondition newCondition( final ImmutableNode aParentNode, final Integer aElementIndex,
                                              final TestDefinedCondition aDefinedCondition, final String aContent )
        {
            return new TestCondition( aParentNode, aElementIndex, aDefinedCondition, aContent );
        }

    }

    private class TestConditionHandler
        extends ConditionHandler<TestDefinedCondition, TestCondition, TestConditionBuilder>
    {
        public TestConditionHandler()
        {
            super( ConditionHandlerTest.this.expectedConditionBuilder, ConditionHandlerTest.this.expectedNamespaceURI );
        }

        @Override
        protected TestDefinedCondition getDefinedCondition( final String aQualifiedName )
        {
            if ( TestDefinedCondition.NAME.asString().equals( aQualifiedName ) )
            {
                return TestDefinedCondition.NAME;
            }
            else
            {
                throw new IllegalArgumentException();
            }
        }


    }
}
