/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.dom;

import com.google.common.collect.Lists;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableCDATASection;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableChildElement;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableText;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class ImmutableChildElementBuilderTest
{
    private ImmutableChildElementBuilder target_;

    private final ImmutableNode mockedImmutableNode = mock( ImmutableNode.class );

    private final String expectedNamespaceURI = "namespace";

    private final String expectedPrefix = "prefix";

    private final String expectedLocalName = "localName";

    private final List<ImmutableAttributeBuilder> expectedAttributes = Collections.emptyList();

    private List<ImmutableChildElementBuilder> expectedChildNodes = Collections.emptyList();

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new ImmutableChildElementBuilder();
    }


    @Test
    public void testWithPrefix()
        throws Exception
    {
        final String expectedPrefix = "prefix";
        target_.withPrefix( expectedPrefix );

        ImmutableChildElement actual =
            target_.isChildOf( mock( ImmutableNode.class ) ).atIndex( 0 ).inNamespace( expectedNamespaceURI ).named(
                expectedLocalName ).build();
        assertThat( actual.getNodeName(), is( equalTo( expectedPrefix + ":" + expectedLocalName ) ) );
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNamed_with_invalid_name()
    {
        target_.named( null );
    }

    @Test(expected = IllegalStateException.class)
    public void testBuild_without_namespace()
    {

        target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).build();
    }

    @Test(expected = IllegalStateException.class)
    public void testBuild_without_local_name()
    {

        target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( expectedNamespaceURI ).build();
    }

    @Test
    public void testWithAttributes_list()
    {
        ImmutableChildElement actual =
            target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( expectedNamespaceURI ).named(
                expectedLocalName ).withAttributes( Lists.newArrayList( new ImmutableAttributeBuilder().named( "1" ),
                                                                        new ImmutableAttributeBuilder().named(
                                                                            "2" ) ) ).build();

        assertThat( actual.getAttributes().getLength(), is( equalTo( 2 ) ) );
    }

    @Test
    public void testWithAttributes_varargs()
    {
        ImmutableChildElement actual =
            target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( expectedNamespaceURI ).named(
                expectedLocalName ).withAttributes( new ImmutableAttributeBuilder().named( "1" ),
                                                    new ImmutableAttributeBuilder().named( "2" ) ).build();

        assertThat( actual.getAttributes().getLength(), is( equalTo( 2 ) ) );
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithAttribute_with_invalid_attribute()
    {
        target_.withAttribute( null );
    }

    @Test
    public void testWithAttribute()
    {
        ImmutableChildElement actual =
            target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( expectedNamespaceURI ).named(
                expectedLocalName ).withAttribute( new ImmutableAttributeBuilder().named( "name" ) ).build();

        assertThat( actual.getAttributes().getLength(), is( equalTo( 1 ) ) );
    }

    @Test
    public void testWithChildElements_list()
    {
        ImmutableChildElement actual =
            target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( expectedNamespaceURI ).named(
                expectedLocalName ).withChildElements(
                Lists.<ImmutableChildElementBuilder>newArrayList( createImmutableChildElementBuilderForTest(),
                                                                  createImmutableChildElementBuilderForTest() ) ).build();

        assertThat( actual.getChildNodes().getLength(), is( equalTo( 2 ) ) );
        assertThat( actual.getChildNodes().item( 0 ), is( instanceOf( ImmutableChildElement.class ) ) );
        assertThat( actual.getChildNodes().item( 1 ), is( instanceOf( ImmutableChildElement.class ) ) );
    }

    @Test
    public void testWithChildElements_varargs()
    {
        ImmutableChildElement actual =
            target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( expectedNamespaceURI ).named(
                expectedLocalName ).withChildElements( createImmutableChildElementBuilderForTest(),
                                                       createImmutableChildElementBuilderForTest() ).build();

        assertThat( actual.getChildNodes().getLength(), is( equalTo( 2 ) ) );
        assertThat( actual.getChildNodes().item( 0 ), is( instanceOf( ImmutableChildElement.class ) ) );
        assertThat( actual.getChildNodes().item( 1 ), is( instanceOf( ImmutableChildElement.class ) ) );
    }

    @Test( expected = IllegalArgumentException.class )
    public void testWithChildElement_with_invalid_child_node()
    {
        target_.withChildElement( null );
    }

    @Test
    public void testWithChildElement()
    {
        ImmutableChildElement actual =
            target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( expectedNamespaceURI ).named(
                expectedLocalName ).withChildElement( createImmutableChildElementBuilderForTest() ).build();

        assertThat( actual.getChildNodes().getLength(), is( equalTo( 1 ) ) );
        assertThat( actual.getChildNodes().item( 0 ), is( instanceOf( ImmutableChildElement.class ) ) );
    }

    @Test( expected = IllegalArgumentException.class )
    public void testWithText_with_null()
        throws Exception
    {
        ImmutableChildElement actual =
            target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( expectedNamespaceURI ).named(
                expectedLocalName ).withText( null ).build();

    }

    @Test
    public void testWithText()
        throws Exception
    {
        ImmutableChildElement actual =
            target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( expectedNamespaceURI ).named(
                expectedLocalName ).withText( new ImmutableTextBuilder() ).build();

        assertThat( actual.getChildNodes().getLength(), is( equalTo( 1 ) ) );
        assertThat( actual.getChildNodes().item( 0 ), is( instanceOf( ImmutableText.class ) ) );
    }

    @Test( expected = IllegalArgumentException.class )
    public void testWithCDATASection_with_null()
        throws Exception
    {
        ImmutableChildElement actual =
            target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( expectedNamespaceURI ).named(
                expectedLocalName ).withCDATASection( null ).build();

    }

    @Test
    public void testWithCDATASection()
        throws Exception
    {
        ImmutableChildElement actual =
            target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( expectedNamespaceURI ).named(
                expectedLocalName ).withCDATASection( new ImmutableCDATASectionBuilder() ).build();

        assertThat( actual.getChildNodes().getLength(), is( equalTo( 1 ) ) );
        assertThat( actual.getChildNodes().item( 0 ), is( instanceOf( ImmutableCDATASection.class ) ) );
    }

    @Test
    public void testBuild()
    {
        ImmutableChildElement actual =
            target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( expectedNamespaceURI ).withPrefix(
                expectedPrefix ).named( expectedLocalName ).withAttributes( expectedAttributes ).withChildElements(
                expectedChildNodes ).build();

        assertThat( actual.getParentNode(), is( equalTo( (Node) mockedImmutableNode ) ) );
        assertThat( actual.getNamespaceURI(), is( equalTo( expectedNamespaceURI ) ) );
        assertThat( actual.getPrefix(), is( equalTo( expectedPrefix ) ) );
        assertThat( actual.getLocalName(), is( equalTo( expectedLocalName ) ) );
        assertThat( actual.getNodeName(), is( equalTo( expectedPrefix + ":" + expectedLocalName ) ) );
    }

    private ImmutableChildElementBuilder createImmutableChildElementBuilderForTest()
    {
        return new ImmutableChildElementBuilder().named( "name" );
    }
}
