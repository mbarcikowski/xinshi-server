/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.dom;

import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.DOMException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ImmutableCharacterDataTest
{

    private ImmutableCharacterData target_;

    private final ImmutableNode mockedImmutableNode = mock( ImmutableNode.class );

    private final int expectedElementIndex = 0;

    private String expectedData = "data";

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new TestImmutableCharacterData();

        final NodeList mockedNodeList = mock( NodeList.class );
        when( mockedImmutableNode.getChildNodes() ).thenReturn( mockedNodeList );
    }

    @Test
    public void testGetParentNode()
        throws Exception
    {
        Node actual = target_.getParentNode();

        assertThat( actual, is( equalTo( (Node) mockedImmutableNode ) ) );
    }

    @Test
    public void testGetPreviousSibling()
        throws Exception
    {
        Node actual = target_.getPreviousSibling();

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetNextSibling()
        throws Exception
    {
        Node actual = target_.getNextSibling();

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetNodeValue()
        throws Exception
    {
        String actual = target_.getNodeValue();

        assertThat( actual, is( equalTo( expectedData ) ) );
    }

    @Test
    public void testGetData()
        throws Exception
    {
        String actual = target_.getData();

        assertThat( actual, is( equalTo( expectedData ) ) );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testSetData()
        throws Exception
    {
        String data = "new data";
        target_.setData( data );
    }

    @Test
    public void testGetLength()
        throws Exception
    {
        int actual = target_.getLength();

        assertThat( actual, is( equalTo( expectedData.length() ) ) );
    }

    @Test
    public void testSubstringData()
        throws Exception
    {
        String actual = target_.substringData( 0, 0 );

        assertThat( actual, is( equalTo( expectedData.substring( 0, 0 ) ) ) );
    }

    @Test(expected = DOMException.class)
    public void testSubstringData_with_offset_lower_than_0()
        throws Exception
    {
        target_.substringData( -1, 0 );
    }

    @Test(expected = DOMException.class)
    public void testSubstringData_with_count_lower_than_0()
        throws Exception
    {
        target_.substringData( 0, -1 );
    }

    @Test(expected = DOMException.class)
    public void testSubstringData_with_offset_greater_than_data_length()
        throws Exception
    {
        target_.substringData( expectedData.length(), 0 );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testAppendData()
        throws Exception
    {
        String data = "new data";
        target_.appendData( data );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testInsertData()
        throws Exception
    {
        String data = "new data";
        target_.insertData( 0, data );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testDeleteData()
        throws Exception
    {
        target_.deleteData( 0, 0 );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testReplaceData()
        throws Exception
    {
        String data = "new data";
        target_.replaceData( 0, 0, data );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testSetNodeValue()
        throws Exception
    {
        String data = "new data";
        target_.setNodeValue( data );
    }

    @Test
    public void testGetChildNodes()
        throws Exception
    {
        NodeList actual = target_.getChildNodes();

        assertThat( actual, is( instanceOf( EmptyImmutableNodeList.class ) ) );
    }

    @Test
    public void testGetFirstChild()
        throws Exception
    {
        Node actual = target_.getFirstChild();

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetLastChild()
        throws Exception
    {
        Node actual = target_.getLastChild();

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetAttributes()
        throws Exception
    {
        NamedNodeMap actual = target_.getAttributes();

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testHasChildNodes()
        throws Exception
    {
        boolean actual = target_.hasChildNodes();

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Test
    public void testGetNamespaceURI()
        throws Exception
    {
        String actual = target_.getNamespaceURI();

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetPrefix()
        throws Exception
    {
        String actual = target_.getPrefix();

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetLocalName()
        throws Exception
    {
        String actual = target_.getLocalName();

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testHasAttributes()
        throws Exception
    {
        boolean actual = target_.hasAttributes();

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Test
    public void testGetTextContent()
        throws Exception
    {
        String actual = target_.getTextContent();

        assertThat( actual, is( equalTo( expectedData ) ) );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testSplitText()
        throws Exception
    {
        target_.splitText( 0 );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testIsElementContentWhitespace()
        throws Exception
    {
        target_.isElementContentWhitespace();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testGetWholeText()
        throws Exception
    {
        target_.getWholeText();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testReplaceWholeText()
        throws Exception
    {
        String data = "new data";
        target_.replaceWholeText( data );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testCompareDocumentPosition()
        throws Exception
    {
        Node mockedNode = mock( Node.class );
        target_.compareDocumentPosition( mockedNode );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testLookupPrefix()
        throws Exception
    {
        String namespaceURI = "namespace";
        target_.lookupPrefix( namespaceURI );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testIsDefaultNamespace()
        throws Exception
    {
        String namespaceURI = "namespace";
        target_.isDefaultNamespace( namespaceURI );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testLookupNamespaceURI()
        throws Exception
    {
        String prefix = "prefix";
        target_.lookupNamespaceURI( prefix );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testIsEqualNode()
        throws Exception
    {
        Node mockedNode = mock( Node.class );
        target_.isEqualNode( mockedNode );
    }

    private class TestImmutableCharacterData
        extends ImmutableCharacterData
    {
        public TestImmutableCharacterData()
        {
            super( ImmutableCharacterDataTest.this.mockedImmutableNode,
                   ImmutableCharacterDataTest.this.expectedElementIndex, ImmutableCharacterDataTest.this.expectedData );
        }

        @Override
        public String getNodeName()
        {
            throw new UnsupportedOperationException( "#getNodeName not implemented" );
        }

        @Override
        public short getNodeType()
        {
            throw new UnsupportedOperationException( "#getNodeType not implemented" );
        }
    }
}
