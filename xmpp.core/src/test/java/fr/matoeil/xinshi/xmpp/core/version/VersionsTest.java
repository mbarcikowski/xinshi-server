package fr.matoeil.xinshi.xmpp.core.version;

import org.junit.Test;

import java.lang.reflect.Constructor;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class VersionsTest
{
    @Test
    public void testConstructor()
        throws Exception
    {
        Constructor target = Versions.class.getDeclaredConstructor();

        assertThat( target.isAccessible(), is( equalTo( false ) ) );

        target.setAccessible( true );
        target.newInstance();
    }

    @Test
    public void testFromString()
        throws Exception
    {
        Version expected = new Version( 1, 0 );

        Version actual = Versions.fromString( "1.0" );

        assertThat( actual, is( equalTo( expected ) ) );
    }
}
