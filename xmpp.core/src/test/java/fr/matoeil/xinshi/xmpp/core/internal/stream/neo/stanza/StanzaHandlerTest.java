/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza;

import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.address.Addresses;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.StanzaBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp.XmppHandler;
import fr.matoeil.xinshi.xmpp.core.stanza.ExtendedContentBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.Stanza;
import fr.matoeil.xinshi.xmpp.core.stanza.Type;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.Attributes;

import javax.annotation.Nullable;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StanzaHandlerTest
{
    private StanzaHandler target_;

    private final String expectedNamespaceURI_ = "jabber:client";

    private final String expectedLocalName_ = "name";

    private StanzaBuilder expectedStanzaBuilder_ = new TestStanzaBuilder();

    private XmppHandler mockedXmppHandler_ = mock( XmppHandler.class );

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new TestStanzaHandler();
    }

    @Test
    public void testGetNamespaceURI()
        throws Exception
    {
        String actual = target_.getNamespaceURI();

        assertThat( actual, is( equalTo( expectedNamespaceURI_ ) ) );
    }

    @Test
    public void testGetBuilder()
        throws Exception
    {
        StanzaBuilder actual = target_.getBuilder();

        assertThat( actual, is( equalTo( expectedStanzaBuilder_ ) ) );
    }

    @Test
    public void testBuild()
        throws Exception
    {
        Stanza actual = target_.build();

        assertThat( actual, is( notNullValue() ) );
    }

    @Test( expected = StreamException.class )
    public void testHandleElementOpening_without_namespace()
        throws Exception
    {
        target_.handleElementOpening( null, "name", "prefix" );
    }

    @Test
    public void testHandleElementOpening_with_configured_namespace()
        throws Exception
    {
        XmppHandler actual = target_.handleElementOpening( expectedNamespaceURI_, "name", "prefix" );

        assertThat( actual, is( equalTo( mockedXmppHandler_ ) ) );
    }


    @Test( expected = StreamException.class )
    public void testStartPrefixMapping_with_forbidden_prefix()
        throws Exception
    {
        target_.startPrefixMapping( "prefix", "jabber:client" );
    }

    @Test
    public void testStartPrefixMapping_with_allowed_prefix()
        throws Exception
    {
        target_.startPrefixMapping( "prefix", "namespace" );

        Stanza actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
    }

    @Test
    public void testStartElement_with_from_attribute()
        throws Exception
    {
        final Address expectedAddress = Addresses.fromString( "me@domain.com" );

        Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( 0 ) ).thenReturn( "from" );
        when( mockedAttributes.getValue( 0 ) ).thenReturn( expectedAddress.fullAddress() );

        target_.startElement( "namespace", "name", "prefix:name", mockedAttributes );

        Stanza actual = target_.build();
        assertThat( actual.getFrom(), is( equalTo( expectedAddress ) ) );
    }

    @Test( expected = StreamException.class )
    public void testStartElement_with_bad_from_attribute()
        throws Exception
    {
        Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( 0 ) ).thenReturn( "from" );
        when( mockedAttributes.getValue( 0 ) ).thenReturn( "" );

        target_.startElement( "namespace", "name", "prefix:name", mockedAttributes );
    }

    @Test
    public void testStartElement_with_to_attribute()
        throws Exception
    {
        final Address expectedAddress = Addresses.fromString( "me@domain.com" );

        Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( 0 ) ).thenReturn( "to" );
        when( mockedAttributes.getValue( 0 ) ).thenReturn( expectedAddress.fullAddress() );

        target_.startElement( "namespace", "name", "prefix:name", mockedAttributes );

        Stanza actual = target_.build();
        assertThat( actual.getTo(), is( equalTo( expectedAddress ) ) );
    }

    @Test( expected = StreamException.class )
    public void testStartElement_with_bad_to_attribute()
        throws Exception
    {
        Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( 0 ) ).thenReturn( "to" );
        when( mockedAttributes.getValue( 0 ) ).thenReturn( "" );

        target_.startElement( "namespace", "name", "prefix:name", mockedAttributes );
    }

    @Test
    public void testStartElement_with_id_attribute()
        throws Exception
    {
        final String expectedId = "id";

        Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( 0 ) ).thenReturn( "id" );
        when( mockedAttributes.getValue( 0 ) ).thenReturn( expectedId );

        target_.startElement( "namespace", "name", "prefix:name", mockedAttributes );

        Stanza actual = target_.build();
        assertThat( actual.getId(), is( equalTo( expectedId ) ) );
    }


    @Test
    public void testStartElement_with_xml_lang_attribute()
        throws Exception
    {
        final String expectedLanguage = "fr";

        Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( 0 ) ).thenReturn( "xml:lang" );
        when( mockedAttributes.getValue( 0 ) ).thenReturn( expectedLanguage );

        target_.startElement( "namespace", "name", "prefix:name", mockedAttributes );

        Stanza actual = target_.build();
        assertThat( actual.getAttribute( "xml:lang" ), is( equalTo( expectedLanguage ) ) );
    }

    @Test( expected = StreamException.class )
    public void testStartElement_with_other_attribute()
        throws Exception
    {
        Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( 0 ) ).thenReturn( "other" );

        target_.startElement( "namespace", "name", "prefix:name", mockedAttributes );
    }

    @Test( expected = StreamException.class )
    public void testStartCDATA()
        throws Exception
    {
        target_.startCDATA();
    }

    @Test
    public void testCharacters_with_whitespaces_only()
        throws Exception
    {
        char[] chars = "        ".toCharArray();
        target_.characters( chars, 0, chars.length );

        Stanza actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
    }

    @Test( expected = StreamException.class )
    public void testCharacters_without_whitespaces_only()
        throws Exception
    {
        char[] chars = "       a ".toCharArray();
        target_.characters( chars, 0, chars.length );
    }

    @Test( expected = StreamException.class )
    public void testEndCDATA()
        throws Exception
    {
        target_.endCDATA();
    }

    private class TestStanzaBuilder
        extends StanzaBuilder
    {

        @Override
        public StanzaBuilder withExtendedContent( final ExtendedContentBuilder aExtendedContentBuilder )
        {
            throw new UnsupportedOperationException( "#withExtendedContent not implemented" );
        }

        @Override
        protected Stanza newStanza( final String aNamespaceURI, @Nullable final Address aFrom,
                                    @Nullable final Address aTo, @Nullable final String aId, @Nullable final Type aType,
                                    @Nullable final String aLanguage )
        {
            return new Stanza( aNamespaceURI, expectedLocalName_, aLanguage, aFrom, aTo, aId, null )
            {
            };
        }
    }


    private class TestStanzaHandler
        extends StanzaHandler
    {
        public TestStanzaHandler()
        {
            super( StanzaHandlerTest.this.expectedNamespaceURI_, expectedStanzaBuilder_ );
        }

        @Override
        protected XmppHandler getXmppHandler( final String aQualifiedName )
        {
            return mockedXmppHandler_;
        }

        @Override
        protected Type getType( final String aType )
        {
            return null;
        }
    }


}
