/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza;

import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.address.Addresses;
import fr.matoeil.xinshi.xmpp.core.internal.dom.EmptyImmutableNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaError;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorCondition;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorConditionBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import static fr.matoeil.xinshi.xmpp.core.stanza.Iq.Type.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;

public class IqTest
{

    private Iq target_;

    private final ImmutableNode mockedImmutableNode_ = mock( ImmutableNode.class );

    private final String expectedNamespaceURI = "expectedNamespace";


    private final String expectedPrefix = "prefix";

    private final String expectedLocalName = "iq";


    private final String expectedLanguage = "fr";

    private final Address expectedFrom = Addresses.fromString( "from@domain" );

    private final Address expectedTo = Addresses.fromString( "to@Domain" );

    private final String expectedId = "id";

    private final Iq.Type expectedType = Iq.Type.GET;

    private final int expectedErrorElementIndex = 3;

    private final Address expectedBy_ = Addresses.fromString( "service.domain.com" );

    private final StanzaError.Type expectedErrorType_ = StanzaError.Type.AUTH;

    private StanzaErrorConditionBuilder expectedStanzaErrorConditionBuilder_ =
        new StanzaErrorConditionBuilder().asDefinedCondition(
            StanzaErrorCondition.DefinedCondition.UNDEFINED_CONDITION );

    private final StanzaErrorBuilder expectedError =
        new StanzaErrorBuilder().isChildOf( mockedImmutableNode_ ).atIndex( expectedErrorElementIndex ).inNamespace(
            expectedNamespaceURI ).by( expectedBy_ ).ofType( expectedErrorType_ ).withCondition(
            expectedStanzaErrorConditionBuilder_ );

    private final ExtendedContentBuilder expectedExtendedContent =
        new ExtendedContentBuilder().inNamespace( "extension-namespace" ).named( "extension" );

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new Iq( expectedNamespaceURI, expectedLanguage, expectedFrom, expectedTo, expectedId, expectedType,
                          expectedError, expectedExtendedContent );
    }

    @After
    public void tearDown()
        throws Exception
    {
    }

    @Test
    public void testGetError()
        throws Exception
    {
        StanzaError actualStanzaError = target_.getStanzaError();

        assertThat( actualStanzaError, is( notNullValue() ) );
    }

    @Test
    public void testGetChild()
        throws Exception
    {
        Element actualChild = target_.getChild();

        assertThat( actualChild, is( notNullValue() ) );

    }

    @Test
    public void testGetLocalName()
        throws Exception
    {
        String actual = target_.getLocalName();

        assertThat( actual, is( equalTo( expectedLocalName ) ) );
    }

    @Test
    public void testConstructor_with_no_error()
        throws Exception
    {
        target_ =
            new Iq( expectedNamespaceURI, expectedLanguage, expectedFrom, expectedTo, expectedId, expectedType, null,
                    expectedExtendedContent );

        StanzaError actualStanzaError = target_.getStanzaError();
        assertThat( actualStanzaError, is( nullValue() ) );

        int expectedLength = 1;
        NodeList actualChildNodes = target_.getChildNodes();
        assertThat( actualChildNodes.getLength(), is( equalTo( expectedLength ) ) );
    }

    @Test
    public void testConstructor_with_no_extended_content()
        throws Exception
    {
        target_ = new Iq( expectedNamespaceURI, expectedLanguage, expectedFrom, expectedTo, expectedId, expectedType,
                          expectedError, null );

        Element actualChild = target_.getChild();
        assertThat( actualChild, is( nullValue() ) );

        int expectedLength = 1;
        NodeList actualChildNodes = target_.getChildNodes();
        assertThat( actualChildNodes.getLength(), is( equalTo( expectedLength ) ) );
    }


    @Test
    public void testConstructor_with_no_child()
        throws Exception
    {
        target_ =
            new Iq( expectedNamespaceURI, expectedLanguage, expectedFrom, expectedTo, expectedId, expectedType, null,
                    null );

        StanzaError actualStanzaError = target_.getStanzaError();
        assertThat( actualStanzaError, is( nullValue() ) );

        NodeList actualChildNodes = target_.getChildNodes();
        assertThat( actualChildNodes, is( instanceOf( EmptyImmutableNodeList.class ) ) );
    }

    public static class TypeTest
    {

        @Test
        public void testERRORAsString()
            throws Exception
        {
            String actual = Iq.Type.ERROR.asString();

            assertThat( actual, is( equalTo( "error" ) ) );
        }

        @Test
        public void testGETAsString()
            throws Exception
        {
            String actual = GET.asString();

            assertThat( actual, is( equalTo( "get" ) ) );
        }

        @Test
        public void testSETAsString()
            throws Exception
        {
            String actual = SET.asString();

            assertThat( actual, is( equalTo( "set" ) ) );
        }

        @Test
        public void testRESULTAsString()
            throws Exception
        {
            String actual = RESULT.asString();

            assertThat( actual, is( equalTo( "result" ) ) );
        }

        @Test
        public void testFromString_in_enum()
            throws Exception
        {
            for ( Iq.Type type : Iq.Type.values() )
            {
                Iq.Type actual = Iq.Type.fromString( type.asString() );
                Iq.Type expected = type;
                assertThat( actual, is( equalTo( expected ) ) );
            }

        }

        @Test( expected = IllegalArgumentException.class )
        public void testFromString_not_in_enum()
            throws Exception
        {
            Iq.Type.fromString( null );
        }
    }
}
