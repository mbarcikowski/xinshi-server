/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.xmpp;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

public class ConditionBuilderTest
{
    private TestConditionBuilder target_;

    private final ImmutableNode mockedImmutableNode = mock( ImmutableNode.class );

    private final String expectedNamespaceURI = "namespace";

    private final String expectedLocalName = "test";

    private final TestDefinedCondition expectedDefinedCondition = TestDefinedCondition.TEST;

    private String expectedContent = "content";

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new TestConditionBuilder();

    }

    @Test(expected = IllegalArgumentException.class)
    public void testAsDefinedCondition_with_invalid_defined_condition()
    {
        target_.asDefinedCondition( null );
    }


    @Test(expected = IllegalStateException.class)
    public void testBuild_with_invalid_defined_condition()
    {
        target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( "namespaceURI" ).build();
    }

    @Test
    public void testBuild()
    {
        TestCondition actual = target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace(
            expectedNamespaceURI ).asDefinedCondition( expectedDefinedCondition ).withContent(
            expectedContent ).build();

        assertThat( actual.getParentNode(), is( equalTo( (Node) mockedImmutableNode ) ) );
        assertThat( actual.getNamespaceURI(), is( equalTo( expectedNamespaceURI ) ) );
        assertThat( actual.getLocalName(), is( equalTo( expectedLocalName ) ) );
        assertThat( actual.getDefinedCondition(), is( equalTo( expectedDefinedCondition ) ) );
        assertThat( actual.getContent(), is( equalTo( expectedContent ) ) );
    }

    private static enum TestDefinedCondition
        implements DefinedCondition
    {
        TEST
            {
                @Override
                public String asString()
                {
                    return "test";
                }
            }
    }

    private class TestCondition
        extends Condition<TestDefinedCondition>
    {
        public TestCondition( final ImmutableNode aParentNode, final Integer aElementIndex,
                              final TestDefinedCondition aDefinedCondition, final String aContent )
        {
            super( aParentNode, aElementIndex, expectedNamespaceURI, aDefinedCondition, aContent );
        }
    }

    private class TestConditionBuilder
        extends ConditionBuilder<TestDefinedCondition, TestCondition, TestConditionBuilder>
    {


        @Override
        protected TestCondition newCondition( final ImmutableNode aParentNode, final Integer aElementIndex,
                                              final TestDefinedCondition aDefinedCondition, final String aContent )
        {
            return new TestCondition( aParentNode, aElementIndex, aDefinedCondition, aContent );
        }

    }
}
