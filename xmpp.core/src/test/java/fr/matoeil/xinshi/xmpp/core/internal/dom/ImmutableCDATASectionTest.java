/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.dom;

import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ImmutableCDATASectionTest
{

    private final ImmutableNode mockedImmutableNode = mock( ImmutableNode.class );

    private final int expectedElementIndex = 0;

    private final String expectedData = "data";

    private ImmutableCDATASection target_;

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new ImmutableCDATASection( mockedImmutableNode, expectedElementIndex, expectedData );

        final NodeList mockedNodeList = mock( NodeList.class );
        when( mockedImmutableNode.getChildNodes() ).thenReturn( mockedNodeList );
    }


    @Test
    public void testGetNodeName()
        throws Exception
    {
        String actual = target_.getNodeName();

        assertThat( actual, is( equalTo( "#cdata-section" ) ) );
    }

    @Test
    public void testGetNodeType()
        throws Exception
    {
        short actual = target_.getNodeType();

        assertThat( actual, is( equalTo( Node.CDATA_SECTION_NODE ) ) );
    }


}
