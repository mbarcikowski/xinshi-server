/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream;

import com.google.common.collect.ImmutableList;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableAttributeBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableChildNodeBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.message.BodyBuilder;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.NodeList;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class NegotiationElementTest
{
    private NegotiationElement target_;

    private final String expectedNamespaceURI = "expectedNamespace";

    private final String expectedPrefix = "prefix";

    private final String expectedLocalName = "priority";

    private final String expectedAttributeNamespaceURI = "attribute-namespace";

    private final String expectedAttributePrefix = "attribute-prefix";

    private final String expectedAttributeLocalName = "attribute-local-name";

    private final String expectedAttributeValue = "attribute-value";

    private final ImmutableAttributeBuilder expectedAttributeBuilder_ =
        new ImmutableAttributeBuilder().inNamespace( expectedAttributeNamespaceURI ).withPrefix(
            expectedAttributePrefix ).named( expectedAttributeLocalName ).withValue( expectedAttributeValue );

    private final List<ImmutableAttributeBuilder> expectedAttributeBuilders_ =
        new ImmutableList.Builder<ImmutableAttributeBuilder>().add( expectedAttributeBuilder_ ).build();

    private final ImmutableChildNodeBuilder expectedChildNodeBuilder_ = new BodyBuilder();

    private final List<ImmutableChildNodeBuilder> expectedChildNodeBuilders_ =
        new ImmutableList.Builder<ImmutableChildNodeBuilder>().add( expectedChildNodeBuilder_ ).build();

    @Before
    public void setUp()
        throws Exception
    {
        target_ =
            new NegotiationElement( expectedNamespaceURI, expectedPrefix, expectedLocalName, expectedAttributeBuilders_,
                                    expectedChildNodeBuilders_ );
    }

    @Test
    public void testGetLocalName()
        throws Exception
    {
        String actual = target_.getLocalName();

        assertThat( actual, is( equalTo( expectedLocalName ) ) );
    }

    @Test
    public void testGetNamespaceURI()
        throws Exception
    {
        String actual = target_.getNamespaceURI();

        assertThat( actual, is( equalTo( expectedNamespaceURI ) ) );
    }

    @Test
    public void testGetChildNodes()
        throws Exception
    {
        NodeList actualChildNodes = target_.getChildNodes();

        assertThat( actualChildNodes, is( notNullValue() ) );
        assertThat( actualChildNodes.getLength(), is( equalTo( expectedChildNodeBuilders_.size() ) ) );
    }

}
