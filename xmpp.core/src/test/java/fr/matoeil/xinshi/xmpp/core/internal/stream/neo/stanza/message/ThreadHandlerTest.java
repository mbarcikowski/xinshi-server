/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.message;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.stanza.message.Thread;
import fr.matoeil.xinshi.xmpp.core.stanza.message.ThreadBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.Attributes;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ThreadHandlerTest
{
    private ThreadHandler target_;

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new ThreadHandler();
    }

    @Test
    public void testGetThreadBuilder()
        throws Exception
    {
        ThreadBuilder actual = target_.getThreadBuilder();

        assertThat( actual, is( notNullValue() ) );
    }

    @Test(expected = StreamException.class)
    public void testHandleElementOpening()
        throws Exception
    {
        target_.handleElementOpening( "namespace", "name", "prefix:name" );
    }

    @Test(expected = StreamException.class)
    public void testStartPrefixMapping()
        throws Exception
    {
        target_.startPrefixMapping( "prefix", "namespace" );
    }

    @Test(expected = StreamException.class)
    public void testStartElement_with_invalid_attribute()
        throws Exception
    {
        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( 0 ) ).thenReturn( "any" );

        target_.startElement( "jabber:client", "thread", "thread", mockedAttributes );
    }

    @Test
    public void testStartElement_with_parent_attribute()
        throws Exception
    {
        String expectedParentId = "01";

        char[] id = "02".toCharArray();
        target_.characters( id, 0, id.length );
        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( 0 ) ).thenReturn( "parent" );
        when( mockedAttributes.getValue( 0 ) ).thenReturn( expectedParentId );

        target_.startElement( "jabber:client", "thread", "thread", mockedAttributes );

        Thread actual = target_.getThreadBuilder().isChildOf( mock( ImmutableNode.class ) ).atIndex( 0 ).build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getParentId(), is( equalTo( expectedParentId ) ) );
    }

    @Test(expected = StreamException.class)
    public void testStartCDATA()
        throws Exception
    {
        target_.startCDATA();
    }

    @Test
    public void testCharacters()
        throws Exception
    {
        String expectedId = "01";

        char[] id = expectedId.toCharArray();

        target_.characters( id, 0, id.length );

        target_.startElement( "jabber:client", "thread", "thread", mock( Attributes.class ) );
        Thread actual = target_.getThreadBuilder().isChildOf( mock( ImmutableNode.class ) ).atIndex( 0 ).build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getId(), is( equalTo( expectedId ) ) );
    }

    @Test(expected = StreamException.class)
    public void testEndCDATA()
        throws Exception
    {
        target_.endCDATA();
    }
}
