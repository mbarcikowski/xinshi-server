/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream;

import fr.matoeil.xinshi.xmpp.core.dom.ImmutableChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.error.StreamErrorCondition;
import fr.matoeil.xinshi.xmpp.core.stream.error.StreamErrorConditionBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.error.StreamErrorText;
import fr.matoeil.xinshi.xmpp.core.stream.error.StreamErrorTextBuilder;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Element;

import static fr.matoeil.xinshi.xmpp.core.stream.error.StreamErrorCondition.DefinedCondition;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class StreamErrorTest
{

    private StreamError target_;

    private final String expectedPrefix = "prefix";

    private final DefinedCondition expectedDefinedCondition_ = DefinedCondition.UNDEFINED_CONDITION;

    private final StreamErrorConditionBuilder expectedStreamErrorConditionBuilder_ =
        new StreamErrorConditionBuilder().asDefinedCondition( expectedDefinedCondition_ );

    private final String expectedTextContent = "content";

    private final StreamErrorTextBuilder expectedStreamErrorTextBuilder_ =
        new StreamErrorTextBuilder().withContent( expectedTextContent );

    private final String expectedApplicationConditionLocalName_ = "applicationCondtion";

    private final ImmutableChildElementBuilder applicationConditionBuilder_ =
        new ImmutableChildElementBuilder().named( expectedApplicationConditionLocalName_ );


    @Before
    public void setUp()
        throws Exception
    {
        target_ =
            new StreamError( expectedPrefix, expectedStreamErrorConditionBuilder_, expectedStreamErrorTextBuilder_,
                             applicationConditionBuilder_ );
    }

    @Test
    public void testGetCondition()
        throws Exception
    {
        StreamErrorCondition actual = target_.getCondition();

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getDefinedCondition(), is( equalTo( expectedDefinedCondition_ ) ) );
    }

    @Test
    public void testGetText()
        throws Exception
    {
        StreamErrorText actual = target_.getText();

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getContent(), is( equalTo( expectedTextContent ) ) );

    }

    @Test
    public void testGetApplicationCondition()
        throws Exception
    {

        Element actual = target_.getApplicationCondition();

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getLocalName(), is( equalTo( expectedApplicationConditionLocalName_ ) ) );
    }

    @Test
    public void testConstructor_without_application_condition()
        throws Exception
    {
        target_ =
            new StreamError( expectedPrefix, expectedStreamErrorConditionBuilder_, expectedStreamErrorTextBuilder_,
                             null );

        Element actual = target_.getApplicationCondition();

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testConstructor_without_text()
        throws Exception
    {
        target_ =
            new StreamError( expectedPrefix, expectedStreamErrorConditionBuilder_, null, applicationConditionBuilder_ );

        Element actual = target_.getText();

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testConstructor_without_text_or_application_condition()
        throws Exception
    {
        target_ = new StreamError( expectedPrefix, expectedStreamErrorConditionBuilder_, null, null );

        assertThat( target_.getText(), is( nullValue() ) );
        assertThat( target_.getApplicationCondition(), is( nullValue() ) );
    }
}
