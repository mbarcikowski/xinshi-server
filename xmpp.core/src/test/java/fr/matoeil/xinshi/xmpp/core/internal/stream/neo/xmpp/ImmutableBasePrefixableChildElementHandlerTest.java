/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp;

import fr.matoeil.xinshi.xmpp.core.dom.ImmutableAttributeBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableCDATASection;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableChildElement;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableChildNodeBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableText;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.ImmutableBasePrefixableChildElementBuilder;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Attr;
import org.w3c.dom.Node;
import org.xml.sax.Attributes;

import javax.annotation.Nullable;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ImmutableBasePrefixableChildElementHandlerTest
{

    private TestImmutableBasePrefixableChildElementHandler target_;

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new TestImmutableBasePrefixableChildElementHandler();
    }

    @Test
    public void testGetBuilder()
        throws Exception
    {
        TestImmutableBaseChildElementBuilder actual = target_.getBuilder();

        assertThat( actual, is( instanceOf( TestImmutableBaseChildElementBuilder.class ) ) );
    }

    @Test
    public void testHandleElementOpening()
        throws Exception
    {
        XmppHandler xmppHandler = target_.handleElementOpening( "namespace", "name", "prefix:name" );

        xmppHandler.startElement( "namespace", "name", "prefix:name", mock( Attributes.class ) );
        ImmutableChildElement actual =
            target_.getBuilder().isChildOf( mock( ImmutableNode.class ) ).atIndex( 0 ).inNamespace( "namespace" ).named(
                "name" ).build();

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getChildNodes().getLength(), is( equalTo( 1 ) ) );
    }

    @Test
    public void testStartElement()
        throws Exception
    {
        final String expectedAttributeNamespaceURI = "namespace";
        final String expectedAttributePrefix = "prefix";
        final String expectedAttributeName = "name";
        final String expectedAttributeValue = "value";
        Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getURI( 0 ) ).thenReturn( expectedAttributeNamespaceURI );
        when( mockedAttributes.getQName( 0 ) ).thenReturn( expectedAttributePrefix + ":" + expectedAttributeName );
        when( mockedAttributes.getValue( 0 ) ).thenReturn( expectedAttributeValue );
        String expectedNamespaceURI = "namespace";
        String expectedName = "name";
        String expectedPrefix = "prefix";

        target_.startElement( expectedNamespaceURI, expectedName, expectedPrefix + ":" + expectedName,
                              mockedAttributes );

        ImmutableChildElement actual =
            target_.getBuilder().isChildOf( mock( ImmutableNode.class ) ).atIndex( 0 ).build();

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getNamespaceURI(), is( equalTo( expectedNamespaceURI ) ) );
        assertThat( actual.getLocalName(), is( equalTo( expectedName ) ) );
        assertThat( actual.getNodeName(), is( equalTo( expectedPrefix + ":" + expectedName ) ) );
        assertThat( actual.getAttributes().getLength(), is( equalTo( 1 ) ) );
        Attr item = actual.getAttributeNodeNS( expectedAttributeNamespaceURI, expectedAttributeName );
        assertThat( item.getNodeName(), is( equalTo( expectedAttributePrefix + ":" + expectedAttributeName ) ) );
        assertThat( item.getValue(), is( equalTo( expectedAttributeValue ) ) );
    }

    @Test
    public void testStartCDATA()
        throws Exception
    {
        target_.startCDATA();

        ImmutableChildElement actual =
            target_.getBuilder().isChildOf( mock( ImmutableNode.class ) ).atIndex( 0 ).inNamespace( "namespace" ).named(
                "name" ).build();
        assertThat( actual, is( notNullValue() ) );
    }

    @Test
    public void testCharacters_not_in_cdata_section()
        throws Exception
    {

        char[] characters = "        ".toCharArray();

        target_.characters( characters, 0, characters.length );

        ImmutableChildElement actual =
            target_.getBuilder().isChildOf( mock( ImmutableNode.class ) ).atIndex( 0 ).inNamespace( "namespace" ).named(
                "name" ).build();
        assertThat( actual.getChildNodes().getLength(), is( equalTo( 1 ) ) );
        Node childNode = actual.getChildNodes().item( 0 );
        assertThat( childNode, instanceOf( ImmutableText.class ) );
    }

    @Test
    public void testCharacters_in_cdata_section()
        throws Exception
    {

        char[] characters = "        ".toCharArray();

        target_.startCDATA();
        target_.characters( characters, 0, characters.length );
        target_.endCDATA();

        ImmutableChildElement actual =
            target_.getBuilder().isChildOf( mock( ImmutableNode.class ) ).atIndex( 0 ).inNamespace( "namespace" ).named(
                "name" ).build();
        assertThat( actual.getChildNodes().getLength(), is( equalTo( 1 ) ) );
        Node childNode = actual.getChildNodes().item( 0 );
        assertThat( childNode, instanceOf( ImmutableCDATASection.class ) );
    }

    @Test
    public void testEndCDATA()
        throws Exception
    {
        target_.endCDATA();

        ImmutableChildElement actual =
            target_.getBuilder().isChildOf( mock( ImmutableNode.class ) ).atIndex( 0 ).inNamespace( "namespace" ).named(
                "name" ).build();
        assertThat( actual, is( notNullValue() ) );
    }


    private class TestImmutableBaseChildElementBuilder
        extends ImmutableBasePrefixableChildElementBuilder<ImmutableChildElement, TestImmutableBaseChildElementBuilder>
    {


        @Override
        protected ImmutableChildElement newPrefixableImmutableChildElementBuilder( final ImmutableNode aParentNode,
                                                                                   final Integer aElementIndex,
                                                                                   final String aNamespaceURI,
                                                                                   @Nullable final String aPrefix,
                                                                                   final String aLocalName,
                                                                                   final List<ImmutableAttributeBuilder> aAttributeBuilders,
                                                                                   final List<ImmutableChildNodeBuilder> aChildNodeBuilders )
        {
            return new ImmutableChildElement( aParentNode, aElementIndex, aNamespaceURI, aPrefix, aLocalName,
                                              aAttributeBuilders, aChildNodeBuilders );
        }
    }

    private class TestImmutableBasePrefixableChildElementHandler
        extends ImmutableBasePrefixableChildElementHandler<ImmutableChildElement, TestImmutableBaseChildElementBuilder>
    {
        public TestImmutableBasePrefixableChildElementHandler()
        {
            super( new TestImmutableBaseChildElementBuilder() );
        }

        @Override
        public void startPrefixMapping( final String aPrefix, final String aNamespaceURI )
        {
            throw new UnsupportedOperationException( "#startPrefixMapping not implemented" );
        }
    }


}
