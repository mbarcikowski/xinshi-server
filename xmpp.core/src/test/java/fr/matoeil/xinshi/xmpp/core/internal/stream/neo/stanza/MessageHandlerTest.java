/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza;

import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.error.StanzaErrorHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.message.BodyHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.message.SubjectHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.message.ThreadHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp.XmppHandler;
import fr.matoeil.xinshi.xmpp.core.stanza.Message;
import fr.matoeil.xinshi.xmpp.core.stanza.MessageBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.Attributes;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MessageHandlerTest
{
    private MessageHandler target_;

    private final String expectedNamespaceURI = "jabber:client";

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new MessageHandler( expectedNamespaceURI );
    }

    @Test
    public void testGetBuilder()
        throws Exception
    {
        MessageBuilder actual = target_.getBuilder();

        assertThat( actual, is( instanceOf( MessageBuilder.class ) ) );
    }

    @Test
    public void testHandleElementOpening_other_configured_namespace()
        throws Exception
    {
        XmppHandler actual = target_.handleElementOpening( "namespace", "name", "prefix" );

        assertThat( actual, is( instanceOf( ExtendedContentHandler.class ) ) );
    }

    @Test
    public void testGetXmppHandler_with_body()
        throws Exception
    {
        XmppHandler actual = target_.getXmppHandler( "body" );

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual, is( instanceOf( BodyHandler.class ) ) );
    }

    @Test
    public void testGetXmppHandler_with_subject()
        throws Exception
    {
        XmppHandler actual = target_.getXmppHandler( "subject" );

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual, is( instanceOf( SubjectHandler.class ) ) );
    }

    @Test
    public void testGetXmppHandler_with_thread()
        throws Exception
    {
        XmppHandler actual = target_.getXmppHandler( "thread" );

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual, is( instanceOf( ThreadHandler.class ) ) );
    }

    @Test
    public void testGetXmppHandler_with_error()
        throws Exception
    {
        XmppHandler actual = target_.getXmppHandler( "error" );

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual, is( instanceOf( StanzaErrorHandler.class ) ) );
    }

    @Test(expected = StreamException.class)
    public void testGetXmppHandler_with_invalid_name()
        throws Exception
    {
        target_.getXmppHandler( "name" );
    }

    @Test
    public void testStartElement_with_type_attribute()
        throws Exception
    {
        final Message.Type expectedType = Message.Type.ERROR;

        Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( 0 ) ).thenReturn( "type" );
        when( mockedAttributes.getValue( 0 ) ).thenReturn( expectedType.asString() );

        target_.startElement( "namespace", "name", "prefix:name", mockedAttributes );

        Message actual = target_.build();
        assertThat( actual.getType(), is( equalTo( expectedType ) ) );
    }

    @Test( expected = StreamException.class )
    public void testStartElement_with_bad_type_attribute()
        throws Exception
    {

        Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( 0 ) ).thenReturn( "type" );
        when( mockedAttributes.getValue( 0 ) ).thenReturn( "" );

        target_.startElement( "namespace", "name", "prefix:name", mockedAttributes );

    }
}
