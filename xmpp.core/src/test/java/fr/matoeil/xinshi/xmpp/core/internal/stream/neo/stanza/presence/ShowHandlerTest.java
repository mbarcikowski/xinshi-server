/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.presence;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.Show;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.ShowBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

public class ShowHandlerTest
{
    private ShowHandler target_;

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new ShowHandler();

    }

    @Test
    public void testGetBuilder()
        throws Exception
    {
        ShowBuilder actual = target_.getBuilder();

        assertThat( actual, is( instanceOf( ShowBuilder.class ) ) );
    }

    @Test
    public void testCharacters_with_valid_state()
        throws Exception
    {
        Show.State expectedState = Show.State.DND;
        char[] chars = expectedState.asString().toCharArray();

        target_.characters( chars, 0, chars.length );

        Show actual = target_.getBuilder().isChildOf( mock( ImmutableNode.class ) ).atIndex( 0 ).build();

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getState(), is( equalTo( expectedState ) ) );
    }

    @Test(expected = StreamException.class)
    public void testCharacters_with_invalid_state()
        throws Exception
    {
        char[] chars = "nope".toCharArray();

        target_.characters( chars, 0, chars.length );
    }

}
