/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza.message;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class ThreadBuilderTest
{
    private ThreadBuilder target_;

    private final ImmutableNode mockedImmutableNode = mock( ImmutableNode.class );

    private final String expectedNamespaceURI = "namespace";

    private final String expectedLocalName = "thread";

    private final String expectedParentId = "parent_id";

    private final String expectedId = "id";

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new ThreadBuilder();

    }

    @Test(expected = IllegalArgumentException.class)
    public void testHasParentThread_with_invalid_parent_id()
    {
        target_.hasParentThread( null );
    }

    @Test(expected = IllegalArgumentException.class)
    public void testHasParentThread_with_malformed_parent_id()
    {
        target_.hasParentThread( "&" );
    }

    @Test(expected = IllegalArgumentException.class)
    public void testHasParentThread_with_invalid_id()
    {
        target_.identifiedBy( null );
    }

    @Test(expected = IllegalArgumentException.class)
    public void testHasParentThread_with_malformed_id()
    {
        target_.identifiedBy( "&" );
    }

    @Test(expected = IllegalStateException.class)
    public void testBuild_with_invalid_id()
    {
        target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( "namespaceURI" ).build();
    }

    @Test
    public void testBuild()
    {

        Thread actual =
            target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).inNamespace( expectedNamespaceURI ).hasParentThread(
                expectedParentId ).identifiedBy( expectedId ).build();

        assertThat( actual.getParentNode(), is( equalTo( (Node) mockedImmutableNode ) ) );
        assertThat( actual.getNamespaceURI(), is( equalTo( expectedNamespaceURI ) ) );
        assertThat( actual.getLocalName(), is( equalTo( expectedLocalName ) ) );
        assertThat( actual.getParentId(), is( equalTo( expectedParentId ) ) );
        assertThat( actual.getId(), is( equalTo( expectedId ) ) );
    }
}
