/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.error;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.Text;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorTextBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.Attributes;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class StanzaErrorTextHandlerTest
{
    private StanzaErrorTextHandler target_;

    private final String expectedNamespaceURI = "urn:ietf:params:xml:ns:xmpp-stanzas";

    private final ImmutableNode mockedParentNode_ = mock( ImmutableNode.class );

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new StanzaErrorTextHandler();
    }

    @Test
    public void testGetTextBuilder()
        throws Exception
    {
        StanzaErrorTextBuilder actual = target_.getTextBuilder();

        assertThat( actual, is( instanceOf( StanzaErrorTextBuilder.class ) ) );
    }

    @Test
    public void testStartElement_with_correct_xmlns()
        throws Exception
    {
        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( eq( 0 ) ) ).thenReturn( "xmlns" );
        when( mockedAttributes.getValue( eq( 0 ) ) ).thenReturn( expectedNamespaceURI );
        target_.startElement( "namespace", "name", "name", mockedAttributes );

        Text actual = target_.getTextBuilder().isChildOf( mockedParentNode_ ).atIndex( 0 ).build();
        assertThat( actual, is( notNullValue() ) );
    }

    @Test( expected = StreamException.class )
    public void testStartElement_with_incorrect_xmlns()
        throws Exception
    {
        final Attributes mockedAttributes = mock( Attributes.class );
        when( mockedAttributes.getLength() ).thenReturn( 1 );
        when( mockedAttributes.getQName( eq( 0 ) ) ).thenReturn( "xmlns" );
        when( mockedAttributes.getValue( eq( 0 ) ) ).thenReturn( "other_namespace" );
        target_.startElement( "namespace", "name", "name", mockedAttributes );
    }
}
