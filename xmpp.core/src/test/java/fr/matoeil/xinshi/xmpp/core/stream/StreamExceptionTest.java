/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream;

import fr.matoeil.xinshi.xmpp.core.old.stream.StreamErrorConstants;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class StreamExceptionTest
{

    private StreamException target_;

    private final StreamErrorConstants.Error expectedError_ = StreamErrorConstants.Error.INTERNAL_SERVER_ERROR;

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new StreamException( expectedError_ );
    }

    @Test
    public void testError()
        throws Exception
    {
        StreamErrorConstants.Error actual = target_.error();

        assertThat( actual, is( equalTo( expectedError_ ) ) );
    }

    @Test
    public void testConstructor_with_message()
        throws Exception
    {
        final String expectedMessage = "message";

        target_ = new StreamException( expectedError_, expectedMessage );

        assertThat( target_.error(), is( equalTo( expectedError_ ) ) );
        assertThat( target_.getMessage(), is( equalTo( expectedMessage ) ) );
    }

    @Test
    public void testConstructor_with_message_and_throwable()
        throws Exception
    {
        final String expectedMessage = "message";
        final Throwable expectedThrowable = new Throwable();

        target_ = new StreamException( expectedError_, expectedMessage, expectedThrowable );

        assertThat( target_.error(), is( equalTo( expectedError_ ) ) );
        assertThat( target_.getMessage(), is( equalTo( expectedMessage ) ) );
        assertThat( target_.getCause(), is( equalTo( expectedThrowable ) ) );
    }

    @Test
    public void testConstructor_with_throwable()
        throws Exception
    {
        final Throwable expectedThrowable = new Throwable();

        target_ = new StreamException( expectedError_, expectedThrowable );

        assertThat( target_.error(), is( equalTo( expectedError_ ) ) );
        assertThat( target_.getCause(), is( equalTo( expectedThrowable ) ) );

    }
}
