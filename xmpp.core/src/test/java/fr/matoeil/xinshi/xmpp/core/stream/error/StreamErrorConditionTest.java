/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream.error;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableOneImmutableAttributeMap;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.Xmls;
import fr.matoeil.xinshi.xmpp.core.stream.Namespaces;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;

import static fr.matoeil.xinshi.xmpp.core.stream.error.StreamErrorCondition.DefinedCondition;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class StreamErrorConditionTest
{
    private StreamErrorCondition target_;

    private final ImmutableNode mockedImmutableNode_ = mock( ImmutableNode.class );

    private final int expectedElementIndex = 0;

    private final String expectedNamespaceURI = "urn:ietf:params:xml:ns:xmpp-streams";

    private final DefinedCondition expectedDefinedCondition_ = DefinedCondition.UNDEFINED_CONDITION;

    private final String expectedLocalName = "undefined-condition";

    private final String expectedContent = "a content";

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new StreamErrorCondition( mockedImmutableNode_, expectedElementIndex, expectedDefinedCondition_,
                                            expectedContent );
    }

    @Test
    public void testGetDefinedCondition()
        throws Exception
    {
        DefinedCondition actualDefinedCondition = target_.getDefinedCondition();

        assertThat( actualDefinedCondition, is( equalTo( expectedDefinedCondition_ ) ) );
    }

    @Test
    public void testGetContent()
        throws Exception
    {
        String actualContent = target_.getContent();

        assertThat( actualContent, is( equalTo( expectedContent ) ) );
    }

    @Test
    public void testGetTagName()
        throws Exception
    {
        String actual = target_.getTagName();

        assertThat( actual, is( equalTo( expectedLocalName ) ) );
    }


    @Test
    public void testGetNodeName()
        throws Exception
    {
        String actual = target_.getNodeName();

        assertThat( actual, is( equalTo( expectedLocalName ) ) );
    }

    @Test
    public void testGetNamespaceURI()
        throws Exception
    {
        String actual = target_.getNamespaceURI();

        assertThat( actual, is( equalTo( expectedNamespaceURI ) ) );
    }


    @Test
    public void testGetLocalName()
        throws Exception
    {
        String actual = target_.getLocalName();

        assertThat( actual, is( equalTo( expectedLocalName ) ) );
    }

    @Test
    public void testGetAttributes()
        throws Exception
    {
        NamedNodeMap actual = target_.getAttributes();

        assertThat( actual, is( instanceOf( ImmutableOneImmutableAttributeMap.class ) ) );
    }

    @Test
    public void testGetAttribute_with_unknow_attribute()
        throws Exception
    {
        String actual = target_.getAttribute( "name" );

        assertThat( actual, isEmptyString() );
    }

    @Test
    public void testGetAttribute_with_know_attribute()
        throws Exception
    {
        String actual = target_.getAttribute( Xmls.XMLNS_ATTRIBUTE );

        assertThat( actual, is( equalTo( Namespaces.STREAM_ERROR_NAMESPACE_URI ) ) );
    }


    @Test
    public void testGetAttributeNode_with_unknow_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNode( "name" );

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetAttributeNode_with_know_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNode( Xmls.XMLNS_ATTRIBUTE );

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getNodeName(), is( equalTo( Xmls.XMLNS_ATTRIBUTE ) ) );
        assertThat( actual.getNodeValue(), is( equalTo( Namespaces.STREAM_ERROR_NAMESPACE_URI ) ) );
    }

    @Test
    public void testGetAttributeNS_with_unknow_attribute()
        throws Exception
    {
        String actual = target_.getAttributeNS( "namespace", "localName" );

        assertThat( actual, isEmptyString() );
    }

    @Test
    public void testGetAttributeNS_with_know_attribute()
        throws Exception
    {
        String actual = target_.getAttributeNS( Xmls.XMLNS_ATTRIBUTE_NS_URI, Xmls.XMLNS_ATTRIBUTE );

        assertThat( actual, is( equalTo( Namespaces.STREAM_ERROR_NAMESPACE_URI ) ) );
    }

    @Test
    public void testGetAttributeNodeNS_with_unknow_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNodeNS( "namespace", "localName" );

        assertThat( actual, is( nullValue() ) );
    }

    @Test
    public void testGetAttributeNodeNS_with_know_attribute()
        throws Exception
    {
        Attr actual = target_.getAttributeNodeNS( Xmls.XMLNS_ATTRIBUTE_NS_URI, Xmls.XMLNS_ATTRIBUTE );

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getNodeName(), is( equalTo( Xmls.XMLNS_ATTRIBUTE ) ) );
        assertThat( actual.getNodeValue(), is( equalTo( Namespaces.STREAM_ERROR_NAMESPACE_URI ) ) );
    }


    @Test
    public void testHasAttribute_with_unknow_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttribute( "name" );

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Test
    public void testHasAttribute_with_know_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttribute( Xmls.XMLNS_ATTRIBUTE );

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testHasAttributeNS_with_unknow_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttributeNS( "namespace", "localName" );

        assertThat( actual, is( equalTo( false ) ) );
    }

    @Test
    public void testHasAttributeNS_with_know_attribute()
        throws Exception
    {
        boolean actual = target_.hasAttributeNS( Xmls.XMLNS_ATTRIBUTE_NS_URI, Xmls.XMLNS_ATTRIBUTE );

        assertThat( actual, is( equalTo( true ) ) );
    }

    @Test
    public void testGetTextContent()
        throws Exception
    {
        String actual = target_.getTextContent();

        assertThat( actual, is( equalTo( expectedContent ) ) );
    }

    public static class DefinedConditionTest
    {

        @Test
        public void testBAD_FORMATAsString()
            throws Exception
        {
            String actual = DefinedCondition.BAD_FORMAT.asString();

            assertThat( actual, is( equalTo( "bad-format" ) ) );
        }


        @Test
        public void testBAD_NAMESPACE_PREFIXAsString()
            throws Exception
        {
            String actual = DefinedCondition.BAD_NAMESPACE_PREFIX.asString();

            assertThat( actual, is( equalTo( "bad-namespace-prefix" ) ) );
        }


        @Test
        public void testCONFLICTEDAsString()
            throws Exception
        {
            String actual = DefinedCondition.CONFLICT.asString();

            assertThat( actual, is( equalTo( "conflict" ) ) );
        }


        @Test
        public void testCONNECTION_TIMEOUTAsString()
            throws Exception
        {
            String actual = DefinedCondition.CONNECTION_TIMEOUT.asString();

            assertThat( actual, is( equalTo( "connection-timeout" ) ) );
        }


        @Test
        public void testHOST_GONEAsString()
            throws Exception
        {
            String actual = DefinedCondition.HOST_GONE.asString();

            assertThat( actual, is( equalTo( "host-gone" ) ) );
        }


        @Test
        public void testHOST_UNKNOWNAsString()
            throws Exception
        {
            String actual = DefinedCondition.HOST_UNKNOWN.asString();

            assertThat( actual, is( equalTo( "host-unknown" ) ) );
        }

        @Test
        public void testIMPROPER_ADDRESSINGAsString()
            throws Exception
        {
            String actual = DefinedCondition.IMPROPER_ADDRESSING.asString();

            assertThat( actual, is( equalTo( "improper-addressing" ) ) );
        }

        @Test
        public void testINTERNAL_SERVER_ERRORAsString()
            throws Exception
        {
            String actual = DefinedCondition.INTERNAL_SERVER_ERROR.asString();

            assertThat( actual, is( equalTo( "internal-server-error" ) ) );
        }


        @Test
        public void testINVALID_FROMAsString()
            throws Exception
        {
            String actual = DefinedCondition.INVALID_FROM.asString();

            assertThat( actual, is( equalTo( "invalid-from" ) ) );
        }


        @Test
        public void testINVALID_IDAsString()
            throws Exception
        {
            String actual = DefinedCondition.INVALID_ID.asString();

            assertThat( actual, is( equalTo( "invalid-id" ) ) );
        }


        @Test
        public void testINVALID_NAMESPACEAsString()
            throws Exception
        {
            String actual = DefinedCondition.INVALID_NAMESPACE.asString();

            assertThat( actual, is( equalTo( "invalid-namespace" ) ) );
        }


        @Test
        public void testINVALID_XMLAsString()
            throws Exception
        {
            String actual = DefinedCondition.INVALID_XML.asString();

            assertThat( actual, is( equalTo( "invalid-xml" ) ) );
        }


        @Test
        public void testNOT_AUTHORIZEDAsString()
            throws Exception
        {
            String actual = DefinedCondition.NOT_AUTHORIZED.asString();

            assertThat( actual, is( equalTo( "not-authorized" ) ) );
        }

        @Test
        public void testNOT_WELL_FORMEDAsString()
            throws Exception
        {
            String actual = DefinedCondition.NOT_WELL_FORMED.asString();

            assertThat( actual, is( equalTo( "not-well-formed" ) ) );
        }


        @Test
        public void testPOLICY_VIOLATIONAsString()
            throws Exception
        {
            String actual = DefinedCondition.POLICY_VIOLATION.asString();

            assertThat( actual, is( equalTo( "policy-violation" ) ) );
        }


        @Test
        public void testREMOTE_CONNECTION_FAILEDAsString()
            throws Exception
        {
            String actual = DefinedCondition.REMOTE_CONNECTION_FAILED.asString();

            assertThat( actual, is( equalTo( "remote-connection-failed" ) ) );
        }


        @Test
        public void testRESETAsString()
            throws Exception
        {
            String actual = DefinedCondition.RESET.asString();

            assertThat( actual, is( equalTo( "reset" ) ) );
        }


        @Test
        public void testRESOURCE_CONSTRAINTAsString()
            throws Exception
        {
            String actual = DefinedCondition.RESOURCE_CONSTRAINT.asString();

            assertThat( actual, is( equalTo( "resource-constraint" ) ) );
        }


        @Test
        public void testRESTRICTED_XMLAsString()
            throws Exception
        {
            String actual = DefinedCondition.RESTRICTED_XML.asString();

            assertThat( actual, is( equalTo( "restricted-xml" ) ) );
        }


        @Test
        public void testSEE_OTHER_HOSTAsString()
            throws Exception
        {
            String actual = DefinedCondition.SEE_OTHER_HOST.asString();

            assertThat( actual, is( equalTo( "see-other-host" ) ) );
        }


        @Test
        public void testSYSTEM_SHUTDOWNAsString()
            throws Exception
        {
            String actual = DefinedCondition.SYSTEM_SHUTDOWN.asString();

            assertThat( actual, is( equalTo( "system-shutdown" ) ) );
        }


        @Test
        public void testUNDEFINED_CONDITIONAsString()
            throws Exception
        {
            String actual = DefinedCondition.UNDEFINED_CONDITION.asString();

            assertThat( actual, is( equalTo( "undefined-condition" ) ) );
        }


        @Test
        public void testUNSUPPORTED_ENCODINGAsString()
            throws Exception
        {
            String actual = DefinedCondition.UNSUPPORTED_ENCODING.asString();

            assertThat( actual, is( equalTo( "unsupported-encoding" ) ) );
        }

        @Test
        public void testUNSUPPORTED_FEATUREAsString()
            throws Exception
        {
            String actual = DefinedCondition.UNSUPPORTED_FEATURE.asString();

            assertThat( actual, is( equalTo( "unsupported-feature" ) ) );
        }


        @Test
        public void testUNSUPPORTED_STANZA_TYPEAsString()
            throws Exception
        {
            String actual = DefinedCondition.UNSUPPORTED_STANZA_TYPE.asString();

            assertThat( actual, is( equalTo( "unsupported-stanza-type" ) ) );
        }


        @Test
        public void testUNSUPPORTED_VERSIONAsString()
            throws Exception
        {
            String actual = DefinedCondition.UNSUPPORTED_VERSION.asString();

            assertThat( actual, is( equalTo( "unsupported-version" ) ) );
        }

        @Test
        public void testFromString_in_enum()
            throws Exception
        {
            for ( DefinedCondition definedCondition : DefinedCondition.values() )
            {
                DefinedCondition actual = DefinedCondition.fromString( definedCondition.asString() );
                DefinedCondition expected = definedCondition;
                assertThat( actual, is( equalTo( expected ) ) );
            }

        }

        @Test( expected = IllegalArgumentException.class )
        public void testFromString_not_in_enum()
            throws Exception
        {
            DefinedCondition.fromString( null );
        }
    }

}
