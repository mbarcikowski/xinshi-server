/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream;

import fr.matoeil.xinshi.xmpp.core.stream.feature.FeatureBuilder;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class FeaturesBuilderTest
{

    private FeaturesBuilder target_;

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new FeaturesBuilder();
    }

    @Test
    public void testWithFeature()
        throws Exception
    {
        final FeatureBuilder expectedFeatureBuilder = new FeatureBuilder().inNamespace( "namespace" ).named( "name" );

        target_.withFeature( expectedFeatureBuilder );

        Features actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getFeatures().size(), is( equalTo( 1 ) ) );
    }

    @Test
    public void testBuild()
        throws Exception
    {
        Features actual = target_.build();

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getFeatures().size(), is( equalTo( 0 ) ) );
    }
}
