/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.dom;

import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.DOMException;
import org.w3c.dom.Node;

import javax.annotation.Nullable;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class ImmutableBaseElementBuilderTest
{
    private TestBuilder target_;

    private final String expectedNamespaceURI = "namespace";

    private final String expectedLocalName = "localName";

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new TestBuilder();
    }

    @Test
    public void testBuild()
    {
        ImmutableBaseElement actual = target_.inNamespace( expectedNamespaceURI ).build();

        assertThat( actual.getNamespaceURI(), is( equalTo( expectedNamespaceURI ) ) );
        assertThat( actual.getLocalName(), is( equalTo( expectedLocalName ) ) );
    }

    private class TestBuilder
        extends ImmutableBaseElementBuilder<ImmutableBaseElement, TestBuilder>
    {

        @Override
        protected ImmutableBaseElement newImmutableElement( @Nullable final String aNamespaceURI )
        {
            return new ImmutableBaseElement( aNamespaceURI, null, expectedLocalName )
            {
                @Override
                public Node getParentNode()
                {
                    throw new UnsupportedOperationException( "#getParentNode not implemented" );
                }

                @Override
                public Node getPreviousSibling()
                {
                    throw new UnsupportedOperationException( "#getPreviousSibling not implemented" );
                }

                @Override
                public Node getNextSibling()
                {
                    throw new UnsupportedOperationException( "#getNextSibling not implemented" );
                }

                @Override
                public String getTextContent()
                    throws DOMException
                {
                    throw new UnsupportedOperationException( "#getTextContent not implemented" );
                }
            };
        }
    }
}
