/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.dom;

import org.hamcrest.MatcherAssert;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;

import javax.annotation.Nullable;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class ImmutableCharacterDataBuilderTest
{

    private ImmutableCharacterDataBuilder target_;

    private final ImmutableNode mockedImmutableNode = mock( ImmutableNode.class );

    private final String expectedData = "data";

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new TestBuilder();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsChildOf_with_invalid_parent()
    {
        target_.isChildOf( null );
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAtIndex_with_invalid_index()
    {
        target_.atIndex( -1 );
    }

    @Test(expected = IllegalStateException.class)
    public void testBuild_with_invalid_parent()
    {
        target_.build();
    }

    @Test(expected = IllegalStateException.class)
    public void testBuild_with_invalid_index()
    {
        target_.isChildOf( mockedImmutableNode ).build();
    }

    @Test
    public void testBuild()
        throws Exception
    {
        ImmutableCharacterData actual =
            target_.isChildOf( mockedImmutableNode ).atIndex( 0 ).withData( expectedData ).build();

        MatcherAssert.assertThat( actual.getParentNode(), is( equalTo( (Node) mockedImmutableNode ) ) );
        assertThat( actual.getData(), is( equalTo( expectedData ) ) );
    }

    private static class TestBuilder
        extends ImmutableCharacterDataBuilder
    {

        @Override
        protected ImmutableCharacterData newImmutableCharacterData( final ImmutableNode aParentNode,
                                                                    final int aElementIndex,
                                                                    @Nullable final String aData )
        {
            return new ImmutableCharacterData( aParentNode, aElementIndex, aData )
            {
                @Override
                public String getNodeName()
                {
                    throw new UnsupportedOperationException( "#getNodeName not implemented" );
                }

                @Override
                public short getNodeType()
                {
                    throw new UnsupportedOperationException( "#getNodeType not implemented" );
                }
            };
        }
    }
}
