/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stanza;

import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.address.Addresses;
import fr.matoeil.xinshi.xmpp.core.stanza.ExtendedContentBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.Stanza;
import fr.matoeil.xinshi.xmpp.core.stanza.Type;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Nullable;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class StanzaBuilderTest
{
    private StanzaBuilder target_;

    private final String expectedPrefix_ = "prefix";

    private final String expectedLocalName_ = "name";

    private final TestType expectedType_ = new TestType();

    private final String expectedNamespaceURI_ = "namespace";

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new TestStanzaBuilder();
    }

    @Test( expected = IllegalArgumentException.class )
    public void testInNamespace_with_invalid_namespace()
        throws Exception
    {
        target_.inNamespace( null );
    }

    @Test
    public void testInNamespace_with_valid_namespace()
        throws Exception
    {
        target_.inNamespace( expectedNamespaceURI_ );

        Stanza actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getNamespaceURI(), is( equalTo( expectedNamespaceURI_ ) ) );
    }

    @Test
    public void testFrom_with_null_address()
        throws Exception
    {
        target_.inNamespace( expectedNamespaceURI_ );

        target_.from( (Address) null );

        Stanza actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getFrom(), is( nullValue() ) );
    }

    @Test
    public void testFrom_with_address()
        throws Exception
    {
        Address expectedAddress = Addresses.fromString( "me@domain.com" );
        target_.inNamespace( expectedNamespaceURI_ );

        target_.from( expectedAddress );

        Stanza actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getFrom(), is( equalTo( expectedAddress ) ) );
    }

    @Test
    public void testTo_with_null_address()
        throws Exception
    {
        target_.inNamespace( expectedNamespaceURI_ );

        target_.to( (Address) null );

        Stanza actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getTo(), is( nullValue() ) );
    }

    @Test
    public void testTo_with_address()
        throws Exception
    {
        Address expectedAddress = Addresses.fromString( "me@domain.com" );
        target_.inNamespace( expectedNamespaceURI_ );

        target_.to( expectedAddress );

        Stanza actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getTo(), is( equalTo( expectedAddress ) ) );
    }

    @Test
    public void testIdentifiedBy()
        throws Exception
    {
        String expectedId = "id";
        target_.inNamespace( expectedNamespaceURI_ );

        target_.identifiedBy( expectedId );

        Stanza actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getId(), is( equalTo( expectedId ) ) );
    }

    @Test
    public void testOfType()
        throws Exception
    {
        target_.inNamespace( expectedNamespaceURI_ );

        target_.ofType( expectedType_ );

        Stanza<TestType> actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getType(), is( equalTo( expectedType_ ) ) );
    }

    @Test
    public void testInLanguage()
        throws Exception
    {
        String expectedLanguage = "fr";
        target_.inNamespace( expectedNamespaceURI_ );

        target_.inLanguage( expectedLanguage );

        Stanza actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getLanguage(), is( equalTo( expectedLanguage ) ) );
    }

    @Test( expected = IllegalStateException.class )
    public void testBuild_without_namespace()
        throws Exception
    {
        target_.build();
    }

    @Test
    public void testBuild_with_namespace()
        throws Exception
    {
        target_.inNamespace( expectedNamespaceURI_ );

        target_.build();
        Stanza actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual, is( instanceOf( TestStanza.class ) ) );
    }

    private class TestType
        implements Type
    {
        @Override
        public String asString()
        {
            return "test";
        }
    }

    private class TestStanza
        extends Stanza<TestType>
    {
        TestStanza( final String aNamespaceURI, final String aLocalName, @Nullable final String aLanguage,
                    @Nullable final Address aFrom, @Nullable final Address aTo, @Nullable final String aId,
                    @Nullable final TestType aType )
        {
            super( aNamespaceURI, aLocalName, aLanguage, aFrom, aTo, aId, aType );
        }
    }

    private class TestStanzaBuilder
        extends StanzaBuilder<TestType, TestStanza, TestStanzaBuilder>
    {


        @Override
        public TestStanzaBuilder withExtendedContent( final ExtendedContentBuilder aExtendedContentBuilder )
        {
            throw new UnsupportedOperationException( "#withExtendedContent not implemented" );
        }

        @Override
        protected TestStanza newStanza( final String aNamespaceURI, @Nullable final Address aFrom,
                                        @Nullable final Address aTo, @Nullable final String aId,
                                        @Nullable final TestType aType, @Nullable final String aLanguage )
        {
            return new TestStanza( aNamespaceURI, expectedLocalName_, aLanguage, aFrom, aTo, aId, aType );
        }


    }


}
