/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream;

import com.google.common.collect.Lists;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableAttributeBuilder;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableCDATASectionBuilder;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableTextBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableCDATASection;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableChildElement;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableText;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public class NegotiationElementBuilderTest
{
    private final String expectedNamespaceURI_ = "namespace";

    private final String expectedLocalName_ = "name";

    private NegotiationElementBuilder target_;

    @Before
    public void setUp()
        throws Exception
    {
        target_ = new NegotiationElementBuilder();
    }

    @Test
    public void testInNamespace()
        throws Exception
    {
        target_.inNamespace( expectedNamespaceURI_ );

        NegotiationElement actual = target_.named( expectedLocalName_ ).build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getNamespaceURI(), is( equalTo( expectedNamespaceURI_ ) ) );
    }

    @Test( expected = IllegalArgumentException.class )
    public void testNamed_with_null()
        throws Exception
    {
        target_.inNamespace( expectedNamespaceURI_ );

        target_.named( null );
    }

    @Test
    public void testNamed_with_name()
        throws Exception
    {
        target_.inNamespace( expectedNamespaceURI_ );

        target_.named( expectedLocalName_ );

        NegotiationElement actual = target_.build();
        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getLocalName(), is( equalTo( expectedLocalName_ ) ) );
        assertThat( actual.getNodeName(), is( equalTo( expectedLocalName_ ) ) );
    }

    @Test( expected = IllegalStateException.class )
    public void testBuild_without_namespaceURI()
        throws Exception
    {
        target_.build();
    }

    @Test( expected = IllegalStateException.class )
    public void testBuild_without_localName()
        throws Exception
    {
        target_.inNamespace( expectedNamespaceURI_ ).build();
    }

    @Test
    public void testWithAttributes_list()
    {
        NegotiationElement actual =
            target_.inNamespace( expectedNamespaceURI_ ).named( expectedLocalName_ ).withAttributes(
                Lists.newArrayList( new ImmutableAttributeBuilder().named( "1" ),
                                    new ImmutableAttributeBuilder().named( "2" ) ) ).build();

        assertThat( actual.getAttributes().getLength(), is( equalTo( 2 ) ) );
    }

    @Test
    public void testWithAttributes_varargs()
    {
        NegotiationElement actual =
            target_.inNamespace( expectedNamespaceURI_ ).named( expectedLocalName_ ).withAttributes(
                new ImmutableAttributeBuilder().named( "1" ), new ImmutableAttributeBuilder().named( "2" ) ).build();

        assertThat( actual.getAttributes().getLength(), is( equalTo( 2 ) ) );
    }

    @Test( expected = IllegalArgumentException.class )
    public void testWithAttribute_with_invalid_attribute()
    {
        target_.inNamespace( expectedNamespaceURI_ ).withAttribute( null );
    }

    @Test
    public void testWithAttribute()
    {
        NegotiationElement actual =
            target_.inNamespace( expectedNamespaceURI_ ).named( expectedLocalName_ ).withAttribute(
                new ImmutableAttributeBuilder().named( expectedLocalName_ ) ).build();

        assertThat( actual.getAttributes().getLength(), is( equalTo( 1 ) ) );
    }

    @Test
    public void testWithChildElements_list()
    {
        NegotiationElement actual =
            target_.inNamespace( expectedNamespaceURI_ ).named( expectedLocalName_ ).withChildElements(
                Lists.<ImmutableChildElementBuilder>newArrayList( createImmutableChildElementBuilderForTest(),
                                                                  createImmutableChildElementBuilderForTest() ) ).build();

        assertThat( actual.getChildNodes().getLength(), is( equalTo( 2 ) ) );
        assertThat( actual.getChildNodes().item( 0 ), is( instanceOf( ImmutableChildElement.class ) ) );
        assertThat( actual.getChildNodes().item( 1 ), is( instanceOf( ImmutableChildElement.class ) ) );
    }

    @Test
    public void testWithChildElements_varargs()
    {
        NegotiationElement actual =
            target_.inNamespace( expectedNamespaceURI_ ).named( expectedLocalName_ ).withChildElements(
                createImmutableChildElementBuilderForTest(), createImmutableChildElementBuilderForTest() ).build();

        assertThat( actual.getChildNodes().getLength(), is( equalTo( 2 ) ) );
        assertThat( actual.getChildNodes().item( 0 ), is( instanceOf( ImmutableChildElement.class ) ) );
        assertThat( actual.getChildNodes().item( 1 ), is( instanceOf( ImmutableChildElement.class ) ) );
    }

    @Test( expected = IllegalArgumentException.class )
    public void testWithChildElement_with_invalid_child_node()
    {
        target_.withChildElement( null );
    }

    @Test
    public void testWithChildElement()
    {
        NegotiationElement actual =
            target_.inNamespace( expectedNamespaceURI_ ).named( expectedLocalName_ ).withChildElement(
                createImmutableChildElementBuilderForTest() ).build();

        assertThat( actual.getChildNodes().getLength(), is( equalTo( 1 ) ) );
        assertThat( actual.getChildNodes().item( 0 ), is( instanceOf( ImmutableChildElement.class ) ) );
    }

    @Test( expected = IllegalArgumentException.class )
    public void testWithText_with_null()
        throws Exception
    {
        NegotiationElement actual =
            target_.inNamespace( expectedNamespaceURI_ ).named( expectedLocalName_ ).withText( null ).build();

    }

    @Test
    public void testWithText()
        throws Exception
    {
        NegotiationElement actual = target_.inNamespace( expectedNamespaceURI_ ).named( expectedLocalName_ ).withText(
            new ImmutableTextBuilder() ).build();

        assertThat( actual.getChildNodes().getLength(), is( equalTo( 1 ) ) );
        assertThat( actual.getChildNodes().item( 0 ), is( instanceOf( ImmutableText.class ) ) );
    }

    @Test( expected = IllegalArgumentException.class )
    public void testWithCDATASection_with_null()
        throws Exception
    {
        NegotiationElement actual =
            target_.inNamespace( expectedNamespaceURI_ ).named( expectedLocalName_ ).withCDATASection( null ).build();

    }

    @Test
    public void testWithCDATASection()
        throws Exception
    {
        NegotiationElement actual =
            target_.inNamespace( expectedNamespaceURI_ ).named( expectedLocalName_ ).withCDATASection(
                new ImmutableCDATASectionBuilder() ).build();

        assertThat( actual.getChildNodes().getLength(), is( equalTo( 1 ) ) );
        assertThat( actual.getChildNodes().item( 0 ), is( instanceOf( ImmutableCDATASection.class ) ) );
    }

    @Test
    public void testBuild()
        throws Exception
    {
        NegotiationElement actual = target_.inNamespace( expectedNamespaceURI_ ).named( expectedLocalName_ ).build();

        assertThat( actual, is( notNullValue() ) );
        assertThat( actual.getNamespaceURI(), is( equalTo( expectedNamespaceURI_ ) ) );
        assertThat( actual.getLocalName(), is( equalTo( expectedLocalName_ ) ) );
    }

    private ImmutableChildElementBuilder createImmutableChildElementBuilderForTest()
    {
        return new ImmutableChildElementBuilder().named( "name" );
    }
}
