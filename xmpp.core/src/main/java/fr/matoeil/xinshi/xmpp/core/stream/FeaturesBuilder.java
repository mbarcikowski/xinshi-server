/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream;

import fr.matoeil.xinshi.xmpp.core.stream.feature.FeatureBuilder;

import javax.annotation.Nullable;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public final class FeaturesBuilder
    extends StreamElementBuilder<Features, FeaturesBuilder>
{
    private final List<FeatureBuilder> featureBuilders_ = newArrayList();


    public FeaturesBuilder withFeature( final FeatureBuilder aFeatureBuilder )
    {
        featureBuilders_.add( aFeatureBuilder );
        return this;
    }

    @Override
    protected Features newStreamElement( @Nullable final String aPrefix )
    {
        return new Features( aPrefix, featureBuilders_ );
    }
}
