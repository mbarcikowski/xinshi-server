/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream;

import fr.matoeil.xinshi.xmpp.core.dom.ImmutableChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.Elements;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableArrayNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableChildElement;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableChildNode;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableOneNodeList;
import fr.matoeil.xinshi.xmpp.core.stream.error.StreamErrorCondition;
import fr.matoeil.xinshi.xmpp.core.stream.error.StreamErrorConditionBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.error.StreamErrorText;
import fr.matoeil.xinshi.xmpp.core.stream.error.StreamErrorTextBuilder;
import org.w3c.dom.Element;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.ArrayList;
import java.util.List;

@Immutable
public final class StreamError
    extends StreamElement
    implements Element
{
    public static final String LOCAL_NAME = "error";

    private static final int INITIAL_CAPACITY = 3;

    private StreamErrorCondition streamErrorCondition_;

    @Nullable
    private StreamErrorText streamErrorText_;

    @Nullable
    private ImmutableChildElement applicationCondition_;

    //TODO prefix must be null

    StreamError( @Nullable final String aPrefix, StreamErrorConditionBuilder aStreamErrorConditionBuilder,
                 @Nullable StreamErrorTextBuilder aStreamErrorTextBuilder,
                 @Nullable ImmutableChildElementBuilder aApplicationConditionBuilder )
    {
        super( Namespaces.STREAM_ERROR_NAMESPACE_URI, aPrefix, LOCAL_NAME );
        generateAttributes();
        generateChildNodes( aStreamErrorConditionBuilder, aStreamErrorTextBuilder, aApplicationConditionBuilder );
    }

    public StreamErrorCondition getCondition()
    {
        return streamErrorCondition_;
    }

    @Nullable
    public StreamErrorText getText()
    {
        return streamErrorText_;
    }

    @Nullable
    public Element getApplicationCondition()
    {
        return applicationCondition_;
    }

    private void generateAttributes()
    {
        setImmutableAttributes( Elements.EMPTY_ATTRIBUTES );
    }

    private void generateChildNodes( final StreamErrorConditionBuilder aStreamErrorConditionBuilder,
                                     @Nullable StreamErrorTextBuilder aStreamErrorTextBuilder,
                                     @Nullable final ImmutableChildElementBuilder aApplicationConditionBuilder )
    {
        List<ImmutableChildNode> immutableChildNodes = new ArrayList<>( INITIAL_CAPACITY );

        int index = 0;
        streamErrorCondition_ = aStreamErrorConditionBuilder.isChildOf( this ).atIndex( index++ ).build();
        immutableChildNodes.add( streamErrorCondition_ );

        if ( aStreamErrorTextBuilder == null )
        {
            streamErrorText_ = null;
        }
        else
        {
            streamErrorText_ = aStreamErrorTextBuilder.isChildOf( this ).atIndex( index++ ).build();
            immutableChildNodes.add( streamErrorText_ );
        }

        if ( aApplicationConditionBuilder == null )
        {
            applicationCondition_ = null;
        }
        else
        {
            applicationCondition_ = aApplicationConditionBuilder.isChildOf( this ).atIndex( index ).build();
            immutableChildNodes.add( applicationCondition_ );
        }

        ImmutableNodeList immutableNodeList;
        if ( immutableChildNodes.size() == 1 )
        {
            immutableNodeList = new ImmutableOneNodeList( streamErrorCondition_ );
        }
        else
        {
            immutableNodeList = new ImmutableArrayNodeList( immutableChildNodes );
        }

        setImmutableChildNodes( immutableNodeList );
    }

}
