/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza.presence;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableBaseChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;

import static com.google.common.base.Preconditions.checkState;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public final class PriorityBuilder
    extends ImmutableBaseChildElementBuilder<Priority, PriorityBuilder>
{
    private static final String MUST_NOT_BE_NULL_ERROR_MESSAGE = "%s must not be null";

    private Byte value_;

    public PriorityBuilder withValue( byte aValue )
    {
        value_ = aValue;
        return this;
    }

    @Override
    protected Priority newImmutableChildElement( ImmutableNode aParentNode, Integer aElementIndex,
                                                 String aNamespaceURI )
    {
        checkState( value_ != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "value" );
        return new Priority( aParentNode, aElementIndex, aNamespaceURI, value_ );
    }
}
