/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza.presence;

import fr.matoeil.xinshi.xmpp.core.internal.dom.Elements;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableBaseChildElement;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableOneNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableText;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.XmppEnum;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.XmppEnumMap;
import org.w3c.dom.Element;

import javax.annotation.concurrent.Immutable;

/**
 * @author mathieu.barcikowski@gmail.com
 */
@Immutable
public final class Show
    extends ImmutableBaseChildElement
    implements Element
{
    public static final String LOCAL_NAME = "show";

    private final State state_;

    Show( final ImmutableNode aParentNode, final int aElementIndex, final String aNamespaceURI, final State aState )
    {
        super( aParentNode, aElementIndex, aNamespaceURI, null, LOCAL_NAME );
        state_ = aState;
        generateAttributes();
        generateChildNodes();
    }

    public State getState()
    {
        return state_;
    }

    @Override
    public String getTextContent()
    {
        return state_.asString();
    }

    private void generateAttributes()
    {
        setImmutableAttributes( Elements.EMPTY_ATTRIBUTES );
    }

    private void generateChildNodes()
    {
        ImmutableText text = new ImmutableText( this, 0, state_.asString() );
        setImmutableChildNodes( new ImmutableOneNodeList( text ) );
    }

    public static enum State
        implements XmppEnum
    {
        AWAY( "away" ),
        CHAT( "chat" ),
        DND( "dnd" ),
        XA( "xa" );

        private final String name_;

        State( final String aName )
        {
            name_ = aName;
        }

        public String asString()
        {
            return name_;
        }

        public static State fromString( String aType )
        {
            return XMPP_ENUM_MAP.fromString( aType );
        }

        private static final XmppEnumMap<State> XMPP_ENUM_MAP = new XmppEnumMap<>( State.class );
    }

}
