/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream.feature.tls;

import fr.matoeil.xinshi.xmpp.core.dom.ImmutableChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.NegotiationElement;
import fr.matoeil.xinshi.xmpp.core.stream.NegotiationElementBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.feature.FeatureBuilder;

import static fr.matoeil.xinshi.xmpp.core.stream.feature.tls.Namespaces.TLS_NAMESPACE_URI;

/**
 * @author mathieu.barcikowski@gmail.com
 * @see <a href="http://xmpp.org/rfcs/rfc6120.html#tls">http://xmpp.org/rfcs/rfc6120.html#tls</a>
 */
public final class TlsElements
{
    public static final NegotiationElement TLS_STARTTLS = createTlsStartTls();

    public static final NegotiationElement TLS_FAILURE = createTlsFailure();

    public static final NegotiationElement TLS_PROCEED = createTlsProceed();

    private static final String STARTTLS_LOCAL_NAME = "starttls";

    private static final String REQUIRED_LOCAL_NAME = "required";

    private static final String FAILURE_LOCAL_NAME = "failure";

    private static final String PROCEED_LOCAL_NAME = "proceed";

    private TlsElements()
    {
    }

    public static FeatureBuilder createTlsFeatureBuilder()
    {
        return new FeatureBuilder().inNamespace( TLS_NAMESPACE_URI ).named( STARTTLS_LOCAL_NAME );
    }

    public static FeatureBuilder createRequiredTlsFeatureBuilder()
    {
        return new FeatureBuilder().inNamespace( TLS_NAMESPACE_URI ).named( STARTTLS_LOCAL_NAME ).withChildElement(
            new ImmutableChildElementBuilder().inNamespace( TLS_NAMESPACE_URI ).named( REQUIRED_LOCAL_NAME ) );
    }

    private static NegotiationElement createTlsStartTls()
    {
        NegotiationElementBuilder builder =
            new NegotiationElementBuilder().inNamespace( TLS_NAMESPACE_URI ).named( STARTTLS_LOCAL_NAME );
        return builder.build();
    }

    private static NegotiationElement createTlsFailure()
    {
        NegotiationElementBuilder builder =
            new NegotiationElementBuilder().inNamespace( TLS_NAMESPACE_URI ).named( FAILURE_LOCAL_NAME );
        return builder.build();
    }

    private static NegotiationElement createTlsProceed()
    {
        NegotiationElementBuilder builder =
            new NegotiationElementBuilder().inNamespace( TLS_NAMESPACE_URI ).named( PROCEED_LOCAL_NAME );
        return builder.build();
    }
}
