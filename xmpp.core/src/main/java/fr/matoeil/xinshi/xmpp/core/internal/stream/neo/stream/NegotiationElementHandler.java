/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stream;

import fr.matoeil.xinshi.xmpp.core.dom.ImmutableAttributeBuilder;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableCDATASectionBuilder;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableTextBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.Nodes;
import fr.matoeil.xinshi.xmpp.core.internal.dom.QualifiedName;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp.ImmutableChildElementHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp.XmppHandler;
import fr.matoeil.xinshi.xmpp.core.stream.NegotiationElement;
import fr.matoeil.xinshi.xmpp.core.stream.NegotiationElementBuilder;
import org.xml.sax.Attributes;

import static java.lang.String.valueOf;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public final class NegotiationElementHandler
    extends StreamElementHandler<NegotiationElement, NegotiationElementBuilder>
{
    private boolean cDataStarted_ = false;

    public NegotiationElementHandler()
    {
        super( new NegotiationElementBuilder() );
    }

    @Override
    public XmppHandler handleElementOpening( final String aNamespaceURI, final String aLocalName,
                                             final String aQualifiedName )
    {
        ImmutableChildElementHandler immutableChildElementHandler = new ImmutableChildElementHandler();
        ImmutableChildElementBuilder immutableChildElementBuilder = immutableChildElementHandler.getBuilder();
        getBuilder().withChildElement( immutableChildElementBuilder );
        return immutableChildElementHandler;
    }


    @Override
    public void startPrefixMapping( final String aPrefix, final String aNamespaceURI )
    {
        checkStreamNamespaceURI( aNamespaceURI );
        //TODO implements FeatureHandler#startPrefixMapping
        //TODO anything to check here ?
    }

    @Override
    public void startElement( final String aNamespaceURI, final String aLocalName, final String aQualifiedName,
                              final Attributes aAttributes )
    {
        QualifiedName extendedContentQualifiedName = Nodes.splitQualifiedName( aQualifiedName );
        getBuilder().inNamespace( aNamespaceURI ).
            withPrefix( extendedContentQualifiedName.getPrefix() ).named( extendedContentQualifiedName.getLocalName() );

        for ( int index = 0, length = aAttributes.getLength(); index < length; index++ )
        {
            QualifiedName attributeQualifiedName = Nodes.splitQualifiedName( aAttributes.getQName( index ) );

            final ImmutableAttributeBuilder attributeBuilder =
                new ImmutableAttributeBuilder().inNamespace( aAttributes.getURI( index ) ).withPrefix(
                    attributeQualifiedName.getPrefix() ).named( attributeQualifiedName.getLocalName() ).withValue(
                    aAttributes.getValue( index ) );
            getBuilder().withAttribute( attributeBuilder );
        }
    }

    @Override
    public void startCDATA()
    {
        cDataStarted_ = true;
    }

    @Override
    public void characters( final char[] aChars, final int aStart, final int aLength )
    {
        if ( cDataStarted_ )
        {
            ImmutableCDATASectionBuilder immutableCDATASectionBuilder =
                new ImmutableCDATASectionBuilder().withData( valueOf( aChars, aStart, aLength ) );
            getBuilder().withCDATASection( immutableCDATASectionBuilder );
        }
        else
        {
            ImmutableTextBuilder immutableTextBuilder =
                new ImmutableTextBuilder().withData( valueOf( aChars, aStart, aLength ) );
            getBuilder().withText( immutableTextBuilder );
        }
    }

    @Override
    public void endCDATA()
    {
        cDataStarted_ = false;
    }
}
