/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableBaseElement;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.annotation.Nullable;

public abstract class StreamElement
    extends ImmutableBaseElement
    implements Element
{
    public StreamElement( @Nullable final String aNamespaceURI, @Nullable final String aPrefix,
                          final String aLocalName )
    {
        super( aNamespaceURI, aPrefix, aLocalName );
    }

    @Override
    public final Node getParentNode()
    {
        return null;
    }

    @Override
    public final Node getPreviousSibling()
    {
        return null;
    }

    @Override
    public final Node getNextSibling()
    {
        return null;
    }

    @Override
    public final String getTextContent()
    {
        //TODO implements NegotiationElement#getTextContent
        throw new UnsupportedOperationException( "NegotiationElement#getTextContent not implemented" );
    }

}
