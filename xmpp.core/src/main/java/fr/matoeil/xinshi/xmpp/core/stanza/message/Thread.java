/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza.message;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableArrayImmutableAttributeMap;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableAttribute;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableBaseChildElement;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableOneNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableText;
import org.w3c.dom.Element;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author mathieu.barcikowski@gmail.com
 */
@Immutable
public final class Thread
    extends ImmutableBaseChildElement
    implements Element
{
    public static final String LOCAL_NAME = "thread";

    public static final String PARENT_ATTRIBUTE_LOCAL_NAME = "parent";

    private static final int COMMON_ATTRIBUTES_COUNT = 1;

    private final String parentId_;

    private final String id_;

    Thread( final ImmutableNode aParentNode, final int aElementIndex, final String aNamespaceURI,
            @Nullable final String aParentId, final String aId )
    {
        super( aParentNode, aElementIndex, aNamespaceURI, null, LOCAL_NAME );
        parentId_ = aParentId;
        id_ = aId;
        generateAttributes();
        generateChildNodes();
    }

    @Nullable
    public String getParentId()
    {
        return parentId_;
    }

    public String getId()
    {
        return id_;
    }

    @Override
    public String getTextContent()
    {
        return id_;
    }

    private void generateAttributes()
    {
        List<ImmutableAttribute> attributes = new ArrayList<>( COMMON_ATTRIBUTES_COUNT );
        generateAttribute( attributes, PARENT_ATTRIBUTE_LOCAL_NAME, parentId_ );
        setImmutableAttributes( new ImmutableArrayImmutableAttributeMap( attributes ) );
    }

    private void generateAttribute( List<ImmutableAttribute> aAttributes, String aLocalName, Object aValue )
    {
        if ( aValue != null )
        {
            aAttributes.add( new ImmutableAttribute( this, null, null, aLocalName, aValue.toString() ) );
        }
    }


    private void generateChildNodes()
    {
        ImmutableText text = new ImmutableText( this, 0, id_ );
        setImmutableChildNodes( new ImmutableOneNodeList( text ) );
    }

}
