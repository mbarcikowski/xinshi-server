/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stream;

import com.google.common.base.CharMatcher;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stream.feature.FeatureHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp.XmppHandler;
import fr.matoeil.xinshi.xmpp.core.old.stream.StreamErrorConstants;
import fr.matoeil.xinshi.xmpp.core.stream.Features;
import fr.matoeil.xinshi.xmpp.core.stream.FeaturesBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import fr.matoeil.xinshi.xmpp.core.stream.feature.FeatureBuilder;
import org.xml.sax.Attributes;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public final class FeaturesHandler
    extends StreamElementHandler<Features, FeaturesBuilder>
{

    public FeaturesHandler()
    {
        super( new FeaturesBuilder() );
    }

    @Override
    public XmppHandler handleElementOpening( final String aNamespaceURI, final String aLocalName,
                                             final String aQualifiedName )
    {
        if ( aNamespaceURI == null )
        {
            //TODO correct StreamException error
            throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
        }
        else
        {
            FeatureHandler featureHandler = new FeatureHandler();
            FeatureBuilder featureBuilder = featureHandler.getBuilder();
            getBuilder().withFeature( featureBuilder );
            return featureHandler;
        }
    }

    @Override
    public void startPrefixMapping( final String aPrefix, final String aNamespaceURI )
    {
        checkStreamNamespaceURI( aNamespaceURI );
    }

    @Override
    public void startElement( final String aNamespaceURI, final String aLocalName, final String aQualifiedName,
                              final Attributes aAttributes )
    {
        initializeBuilderWithoutAttribute( aQualifiedName, aAttributes, getBuilder() );
    }

    @Override
    public void startCDATA()
    {
        //TODO correct StreamException error
        throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
    }

    @Override
    public void characters( final char[] aChars, final int aStart, final int aLength )
    {
        if ( !CharMatcher.WHITESPACE.matchesAllOf( String.valueOf( aChars, aStart, aLength ) ) )
        {
            //TODO correct StreamException error
            throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
        }
    }

    @Override
    public void endCDATA()
    {
        //TODO correct StreamException error
        throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
    }
}
