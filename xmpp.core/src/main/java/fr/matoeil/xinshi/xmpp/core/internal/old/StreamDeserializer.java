/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.old;

import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.address.Addresses;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.Xmls;
import fr.matoeil.xinshi.xmpp.core.old.stream.StreamErrorConstants;
import fr.matoeil.xinshi.xmpp.core.stream.Header;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import fr.matoeil.xinshi.xmpp.core.version.Version;
import fr.matoeil.xinshi.xmpp.core.version.Versions;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.xml.sax.Attributes;

import static fr.matoeil.xinshi.xmpp.core.old.stream.StreamConstants.Attributes.*;
import static fr.matoeil.xinshi.xmpp.core.old.stream.StreamConstants.STREAM;
import static fr.matoeil.xinshi.xmpp.core.old.stream.StreamConstants.STREAM_NAMESPACE_URI;

public final class StreamDeserializer
{

    private StreamDeserializer()
    {

    }

    public static Header decodeHeader( Element aElement )
    {
        Address from = null;
        String id = null;
        Address to = null;
        Version version = null;
        String xmlLang = null;

        String name = aElement.getLocalName();
        String namespaceURI = aElement.getNamespaceURI();
        if ( !STREAM_NAMESPACE_URI.equals( namespaceURI ) )
        {
            throw new StreamException( StreamErrorConstants.Error.INVALID_NAMESPACE );
        }
        if ( !STREAM.equals( name ) )
        {
            throw new StreamException( StreamErrorConstants.Error.INVALID_XML );
        }
        Attr fromAttribute = aElement.getAttributeNode( FROM );
        if ( fromAttribute != null )
        {
            try
            {
                from = Addresses.fromString( fromAttribute.getValue() );
            }
            catch ( Exception e )
            {
                throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT, e.getMessage(), e );
            }
        }
        Attr toAttribute = aElement.getAttributeNode( TO );
        if ( toAttribute != null )
        {
            try
            {
                to = Addresses.fromString( toAttribute.getValue() );
            }
            catch ( Exception e )
            {
                throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT, e.getMessage(), e );
            }
        }
        Attr versionAttribute = aElement.getAttributeNode( VERSION );
        if ( versionAttribute != null )
        {
            try
            {
                version = Versions.fromString( versionAttribute.getValue() );
            }
            catch ( Exception e )
            {
                throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT, e.getMessage(), e );
            }
        }
        Attr idAttribute = aElement.getAttributeNode( ID );
        if ( idAttribute != null )
        {
            id = idAttribute.getValue();
        }
        Attr xmlLangAttribute = aElement.getAttributeNodeNS( Xmls.XML_NS_URI, LANG );
        if ( xmlLangAttribute != null )
        {
            xmlLang = xmlLangAttribute.getValue();
        }
        if ( version == null )
        {
            throw new StreamException( StreamErrorConstants.Error.UNSUPPORTED_VERSION );
        }
        return new Header( version, from, to, id, xmlLang );
    }

    public static Header decodeHeader( final String aNamespaceURI, final String aLocalName, final String aQualifiedName,
                                       final Attributes aAttributes )
    {
        if ( !STREAM_NAMESPACE_URI.equals( aNamespaceURI ) )
        {
            throw new StreamException( StreamErrorConstants.Error.INVALID_NAMESPACE );
        }
        if ( !STREAM.equals( aLocalName ) )
        {
            throw new StreamException( StreamErrorConstants.Error.INVALID_XML );
        }

        final Address fromAddress = getFrom( aAttributes );

        final Address toAddress = getTo( aAttributes );

        final Version version = getVersion( aAttributes );

        final String id = aAttributes.getValue( ID );

        final String xmlLang = aAttributes.getValue( Xmls.XML_NS_URI, LANG );

        return new Header( version, fromAddress, toAddress, id, xmlLang );
    }

    private static Address getFrom( final Attributes aAttributes )
    {
        String from = aAttributes.getValue( FROM );
        if ( from != null )
        {
            try
            {
                return Addresses.fromString( from );
            }
            catch ( Exception e )
            {
                throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT, e.getMessage(), e );
            }
        }
        else
        {
            return null;
        }
    }

    private static Address getTo( final Attributes aAttributes )
    {
        String to = aAttributes.getValue( TO );
        if ( to != null )
        {
            try
            {
                return Addresses.fromString( to );
            }
            catch ( Exception e )
            {
                throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT, e.getMessage(), e );
            }
        }
        else
        {
            return null;
        }
    }

    private static Version getVersion( final Attributes aAttributes )
    {
        String versionAttribute = aAttributes.getValue( VERSION );
        if ( versionAttribute != null )
        {
            try
            {
                return Versions.fromString( versionAttribute );
            }
            catch ( Exception e )
            {
                throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT, e.getMessage(), e );
            }
        }
        else
        {
            throw new StreamException( StreamErrorConstants.Error.UNSUPPORTED_VERSION );
        }
    }
}
