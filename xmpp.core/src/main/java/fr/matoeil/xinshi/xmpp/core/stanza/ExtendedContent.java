/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza;

import fr.matoeil.xinshi.xmpp.core.dom.ImmutableAttributeBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.Elements;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableBaseChildElement;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableChildNodeBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.List;

/**
 * @author mathieu.barcikowski@gmail.com
 */
@Immutable
public final class ExtendedContent
    extends ImmutableBaseChildElement
{

    ExtendedContent( final ImmutableNode aParentNode, final int aElementIndex, @Nullable final String aNamespaceURI,
                     @Nullable final String aPrefix, final String aLocalName,
                     List<ImmutableAttributeBuilder> aAttributeBuilders,
                     List<ImmutableChildNodeBuilder> aChildNodeBuilders )
    {
        super( aParentNode, aElementIndex, aNamespaceURI, aPrefix, aLocalName );
        setImmutableAttributes( Elements.generateAttributes( this, aAttributeBuilders ) );
        setImmutableChildNodes( Elements.generateChildNodes( this, aChildNodeBuilders ) );
    }

    @Override
    public String getTextContent()
    {
        //TODO implements ExtendedContent#getTextContent
        throw new UnsupportedOperationException( "ExtendedContent#getTextContent not implemented" );
    }

}
