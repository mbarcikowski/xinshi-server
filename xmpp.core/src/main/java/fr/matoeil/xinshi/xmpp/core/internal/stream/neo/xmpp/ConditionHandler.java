/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp;

import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.Xmls;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.Condition;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.ConditionBuilder;
import fr.matoeil.xinshi.xmpp.core.old.stream.StreamErrorConstants;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import org.xml.sax.Attributes;

import static java.lang.String.valueOf;

public abstract class ConditionHandler<C extends fr.matoeil.xinshi.xmpp.core.internal.xmpp.DefinedCondition, T extends Condition<C>, B extends ConditionBuilder<C, T, B>>
    extends XmppHandler
{
    private final B conditionBuilder_;

    private final String namespaceURI_;

    protected ConditionHandler( B aConditionBuilder, final String aNamespaceURI )
    {
        conditionBuilder_ = aConditionBuilder;
        namespaceURI_ = aNamespaceURI;
    }

    public final B getConditionBuilder()
    {
        return conditionBuilder_;
    }

    @Override
    public final XmppHandler handleElementOpening( final String aNamespaceURI, final String aLocalName,
                                                   final String aQualifiedName )
    {
        //TODO correct StreamException error
        throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
    }

    @Override
    public final void startPrefixMapping( final String aPrefix, final String aNamespaceURI )
    {
        //TODO correct StreamException error
        throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
    }

    @Override
    public final void startElement( final String aNamespaceURI, final String aLocalName, final String aQualifiedName,
                                    final Attributes aAttributes )
    {
        if ( !aLocalName.equals( aQualifiedName ) )
        {
            //TODO correct StreamException error
            throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
        }

        try
        {
            conditionBuilder_.asDefinedCondition( getDefinedCondition( aQualifiedName ) );
        }
        catch ( IllegalArgumentException e )
        {
            //TODO correct StreamException error
            throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT, e );
        }

        if ( aAttributes.getLength() != 1 || !Xmls.XMLNS_ATTRIBUTE.equals( aAttributes.getQName( 0 ) )
            || !namespaceURI_.equals( aAttributes.getValue( 0 ) ) )
        {
            //TODO correct StreamException error
            throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
        }
    }

    @Override
    public final void startCDATA()
    {
        //TODO correct StreamException error
        throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
    }

    @Override
    public final void characters( final char[] aChars, final int aStart, final int aLength )
    {
        conditionBuilder_.withContent( valueOf( aChars, aStart, aLength ) );
    }

    @Override
    public final void endCDATA()
    {
        //TODO correct StreamException error
        throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
    }

    protected abstract C getDefinedCondition( final String aQualifiedName );
}
