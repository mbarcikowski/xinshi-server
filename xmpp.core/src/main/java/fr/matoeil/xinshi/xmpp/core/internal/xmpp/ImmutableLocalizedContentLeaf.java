/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.xmpp;

import fr.matoeil.xinshi.xmpp.core.internal.dom.Elements;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableAttribute;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableAttributeMap;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableBaseChildElement;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableOneImmutableAttributeMap;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableOneNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableText;
import fr.matoeil.xinshi.xmpp.core.internal.dom.Nodes;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.Xmls;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

/**
 * @author mathieu.barcikowski@gmail.com
 */
@Immutable
public abstract class ImmutableLocalizedContentLeaf
    extends ImmutableBaseChildElement
{
    private final String language_;

    private final String content_;

    public ImmutableLocalizedContentLeaf( final ImmutableNode aParentNode, final int aElementIndex,
                                          final String aNamespaceURI, final String aLocalName,
                                          @Nullable final String aLanguage, @Nullable final String aContent )
    {
        super( aParentNode, aElementIndex, aNamespaceURI, null, aLocalName );
        language_ = aLanguage;
        content_ = aContent;
        generateAttributes();
        generateChildNodes();
    }

    @Nullable
    public final String getLanguage()
    {
        return language_;
    }

    @Nullable
    public final String getContent()
    {
        return content_;
    }

    @Override
    public final String getTextContent()
    {
        return content_;
    }

    private void generateAttributes()
    {
        ImmutableAttributeMap immutableAttributes;
        if ( language_ == null )
        {
            immutableAttributes = Elements.EMPTY_ATTRIBUTES;
        }
        else
        {
            immutableAttributes = new ImmutableOneImmutableAttributeMap(
                new ImmutableAttribute( this, Xmls.XML_NS_URI, Xmls.XML_NS_PREFIX, Xmls.LANG, language_ ) );
        }
        setImmutableAttributes( immutableAttributes );
    }

    private void generateChildNodes()
    {
        ImmutableNodeList childNodes;
        if ( content_ == null )
        {
            childNodes = Nodes.EMPTY_CHILD_NODES;
        }
        else
        {
            ImmutableText text = new ImmutableText( this, 0, content_ );
            childNodes = new ImmutableOneNodeList( text );
        }
        setImmutableChildNodes( childNodes );
    }

}
