/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo;

public final class Xmls
{
    public static final String LANG = "lang";

    public static final String XML_NS_URI = "http://www.w3.org/XML/1998/namespace";

    public static final String XML_NS_PREFIX = "xml";

    public static final String XML_LANG_ATTRIBUTE = XML_NS_PREFIX + ":" + LANG;

    public static final String XMLNS_ATTRIBUTE_NS_URI = "http://www.w3.org/2000/xmlns/";

    public static final String XMLNS_ATTRIBUTE = "xmlns";

    private Xmls()
    {
    }
}
