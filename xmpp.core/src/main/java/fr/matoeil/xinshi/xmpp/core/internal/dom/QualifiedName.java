/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.dom;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

/**
 * @author mathieu.barcikowski@gmail.com
 */
@Immutable
public final class QualifiedName
{
    @Nullable
    private final String prefix_;

    private final String localName_;

    public QualifiedName( @Nullable final String aPrefix, final String aLocalName )
    {
        prefix_ = aPrefix;
        localName_ = aLocalName;
    }

    @Nullable
    public String getPrefix()
    {
        return prefix_;
    }

    public String getLocalName()
    {
        return localName_;
    }
}
