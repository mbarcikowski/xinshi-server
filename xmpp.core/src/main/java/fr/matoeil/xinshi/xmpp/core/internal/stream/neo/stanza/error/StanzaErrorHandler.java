/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.error;

import com.google.common.base.CharMatcher;
import fr.matoeil.xinshi.xmpp.address.Addresses;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp.ImmutableChildElementHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp.XmppHandler;
import fr.matoeil.xinshi.xmpp.core.old.stream.StreamErrorConstants;
import fr.matoeil.xinshi.xmpp.core.stanza.error.Namespaces;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorConditionBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorText;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorTextBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import org.xml.sax.Attributes;

import static fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaError.*;

public class StanzaErrorHandler
    extends XmppHandler
{
    private final StanzaErrorBuilder stanzaErrorBuilder_;

    public StanzaErrorHandler()
    {
        stanzaErrorBuilder_ = new StanzaErrorBuilder();
    }

    public StanzaErrorBuilder getStanzaErrorBuilder()
    {
        return stanzaErrorBuilder_;
    }

    @Override
    public XmppHandler handleElementOpening( final String aNamespaceURI, final String aLocalName,
                                             final String aQualifiedName )
    {
        if ( aNamespaceURI == null )
        {
            //TODO correct StreamException error
            throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
        }
        else
        {
            switch ( aNamespaceURI )
            {
                case Namespaces.STANZAS_NAMESPACE_URI:
                {

                    if ( StanzaErrorText.LOCAL_NAME.equals( aQualifiedName ) )
                    {
                        StanzaErrorTextHandler stanzaErrorTextHandler = new StanzaErrorTextHandler();
                        StanzaErrorTextBuilder stanzaErrorTextBuilder = stanzaErrorTextHandler.getTextBuilder();
                        stanzaErrorBuilder_.withText( stanzaErrorTextBuilder );
                        return stanzaErrorTextHandler;
                    }
                    else
                    {
                        StanzaErrorConditionHandler stanzaErrorConditionHandler = new StanzaErrorConditionHandler();
                        StanzaErrorConditionBuilder stanzaErrorConditionBuilder =
                            stanzaErrorConditionHandler.getConditionBuilder();
                        stanzaErrorBuilder_.withCondition( stanzaErrorConditionBuilder );
                        return stanzaErrorConditionHandler;
                    }

                }
                default:
                {
                    ImmutableChildElementHandler immutableChildElementHandler = new ImmutableChildElementHandler();
                    ImmutableChildElementBuilder immutableChildElementBuilder =
                        immutableChildElementHandler.getBuilder();
                    stanzaErrorBuilder_.withApplicationCondition( immutableChildElementBuilder );
                    return immutableChildElementHandler;
                }
            }
        }
    }

    @Override
    public void startPrefixMapping( final String aPrefix, final String aNamespaceURI )
    {
        //TODO other namespace (server,....)
        switch ( aNamespaceURI )
        {
            case fr.matoeil.xinshi.xmpp.core.stanza.Namespaces.CLIENT_NAMESPACE_URI:
            {
                if ( aPrefix != null )
                {
                    //TODO correct StreamException error
                    throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
                }
                break;
            }
            default:
            {
                //no op
            }
        }
    }


    @Override
    public void startElement( final String aNamespaceURI, final String aLocalName, final String aQualifiedName,
                              final Attributes aAttributes )
    {
        for ( int index = 0, length = aAttributes.getLength(); index < length; index++ )
        {
            switch ( aAttributes.getQName( index ) )
            {
                case BY_ATTRIBUTE_LOCAL_NAME:
                {
                    //TODO try catch
                    stanzaErrorBuilder_.by( Addresses.fromString( aAttributes.getValue( index ) ) );
                    break;
                }
                case TYPE_ATTRIBUTE_LOCAL_NAME:
                {
                    //TODO try catch
                    stanzaErrorBuilder_.ofType( Type.fromString( aAttributes.getValue( index ) ) );
                    break;
                }
                default:
                {
                    //TODO correct StreamException error
                    throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
                }
            }
        }
    }

    @Override
    public void startCDATA()
    {
        //TODO correct StreamException error
        throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
    }

    @Override
    public void characters( final char[] aChars, final int aStart, final int aLength )
    {
        if ( !CharMatcher.WHITESPACE.matchesAllOf( String.valueOf( aChars, aStart, aLength ) ) )
        {
            //TODO correct StreamException error
            throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
        }
    }

    @Override
    public void endCDATA()
    {
        //TODO correct StreamException error
        throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
    }
}
