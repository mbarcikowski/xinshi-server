/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp;

import fr.matoeil.xinshi.xmpp.core.dom.ImmutableAttributeBuilder;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableCDATASectionBuilder;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableTextBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableBaseChildElement;
import fr.matoeil.xinshi.xmpp.core.internal.dom.Nodes;
import fr.matoeil.xinshi.xmpp.core.internal.dom.QualifiedName;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.ImmutableBasePrefixableChildElementBuilder;
import org.xml.sax.Attributes;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static java.lang.String.valueOf;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public abstract class ImmutableBasePrefixableChildElementHandler<C extends ImmutableBaseChildElement, B extends ImmutableBasePrefixableChildElementBuilder<C, B>>
    extends XmppHandler
{
    private final B builder_;

    private boolean cDataStarted_ = false;

    protected ImmutableBasePrefixableChildElementHandler( final B aBuilder )
    {
        builder_ = aBuilder;
    }

    public final B getBuilder()
    {
        return builder_;
    }

    @Override
    public final XmppHandler handleElementOpening( final String aNamespaceURI, final String aLocalName,
                                                   final String aQualifiedName )
    {
        ImmutableChildElementHandler immutableChildElementHandler = new ImmutableChildElementHandler();
        ImmutableChildElementBuilder immutableChildElementBuilder = immutableChildElementHandler.getBuilder();
        builder_.withChildElement( immutableChildElementBuilder );
        return immutableChildElementHandler;
    }

    @Override
    public final void startElement( final String aNamespaceURI, final String aLocalName, final String aQualifiedName,
                                    final Attributes aAttributes )
    {
        QualifiedName qualifiedName = Nodes.splitQualifiedName( aQualifiedName );
        getBuilder().inNamespace( aNamespaceURI ).
            withPrefix( qualifiedName.getPrefix() ).named( qualifiedName.getLocalName() );

        List<ImmutableAttributeBuilder> attributeBuilders = newArrayList();
        for ( int index = 0, length = aAttributes.getLength(); index < length; index++ )
        {
            QualifiedName attributeQualifiedName = Nodes.splitQualifiedName( aAttributes.getQName( index ) );

            final ImmutableAttributeBuilder attributeBuilder =
                new ImmutableAttributeBuilder().inNamespace( aAttributes.getURI( index ) ).withPrefix(
                    attributeQualifiedName.getPrefix() ).named( attributeQualifiedName.getLocalName() ).withValue(
                    aAttributes.getValue( index ) );
            attributeBuilders.add( attributeBuilder );
        }
        getBuilder().withAttributes( attributeBuilders );
    }

    @Override
    public final void startCDATA()
    {
        cDataStarted_ = true;
    }

    @Override
    public final void characters( final char[] aChars, final int aStart, final int aLength )
    {
        if ( cDataStarted_ )
        {
            ImmutableCDATASectionBuilder immutableCDATASectionBuilder =
                new ImmutableCDATASectionBuilder().withData( valueOf( aChars, aStart, aLength ) );
            builder_.withCDATASection( immutableCDATASectionBuilder );
        }
        else
        {
            ImmutableTextBuilder immutableTextBuilder =
                new ImmutableTextBuilder().withData( valueOf( aChars, aStart, aLength ) );
            builder_.withText( immutableTextBuilder );
        }
    }

    @Override
    public final void endCDATA()
    {
        cDataStarted_ = false;
    }
}
