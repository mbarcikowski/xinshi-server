/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.xmpp;

import fr.matoeil.xinshi.xmpp.core.dom.ImmutableAttributeBuilder;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableCDATASectionBuilder;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableTextBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableBaseChildElement;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableBaseChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableChildNodeBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public abstract class ImmutableBasePrefixableChildElementBuilder<T extends ImmutableBaseChildElement, B extends ImmutableBasePrefixableChildElementBuilder<T, B>>
    extends ImmutableBaseChildElementBuilder<T, B>
{
    private static final String MUST_NOT_BE_NULL_ERROR_MESSAGE = "%s must not be null";

    @Nullable
    private String prefix_;

    public final B withPrefix( @Nullable String aPrefix )
    {
        prefix_ = aPrefix;
        return (B) this;
    }

    private String localName_;

    private final List<ImmutableAttributeBuilder> attributeBuilders_ = new ArrayList<>();

    private final List<ImmutableChildNodeBuilder> childNodeBuilders_ = new ArrayList<>();

    public final B named( String aLocalName )
    {
        checkArgument( aLocalName != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "localName" );
        localName_ = aLocalName;
        return (B) this;
    }

    public final B withAttributes( List<ImmutableAttributeBuilder> aAttributeBuilders )
    {
        checkArgument( aAttributeBuilders != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "attributeBuilders" );
        attributeBuilders_.addAll( aAttributeBuilders );
        return (B) this;
    }

    public final B withAttributes( final ImmutableAttributeBuilder... aAttributeBuilders )
    {
        checkArgument( aAttributeBuilders != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "attributeBuilders" );
        Collections.addAll( attributeBuilders_, aAttributeBuilders );
        return (B) this;
    }

    public final B withAttribute( final ImmutableAttributeBuilder aAttributeBuilder )
    {
        checkArgument( aAttributeBuilder != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "attributeBuilder" );
        attributeBuilders_.add( aAttributeBuilder );
        return (B) this;
    }

    public final B withChildElements( List<ImmutableChildElementBuilder> aChildElementBuilders )
    {
        checkArgument( aChildElementBuilders != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "childElementBuilders" );
        childNodeBuilders_.addAll( aChildElementBuilders );
        return (B) this;
    }

    public final B withChildElements( final ImmutableChildElementBuilder... aChildElementBuilders )
    {
        checkArgument( aChildElementBuilders != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "childElementBuilders" );
        Collections.addAll( childNodeBuilders_, aChildElementBuilders );
        return (B) this;
    }

    public final B withChildElement( final ImmutableChildElementBuilder aChildElementBuilder )
    {
        checkArgument( aChildElementBuilder != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "childElementBuilder" );
        childNodeBuilders_.add( aChildElementBuilder );
        return (B) this;
    }

    public final B withText( final ImmutableTextBuilder aTextBuilder )
    {
        checkArgument( aTextBuilder != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "textBuilder" );
        childNodeBuilders_.add( aTextBuilder );
        return (B) this;
    }

    public final B withCDATASection( final ImmutableCDATASectionBuilder aCDATASectionBuilder )
    {
        checkArgument( aCDATASectionBuilder != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "CDATASectionBuilder" );
        childNodeBuilders_.add( aCDATASectionBuilder );
        return (B) this;
    }

    @Override
    protected final T newImmutableChildElement( final ImmutableNode aParentNode, final Integer aElementIndex,
                                                @Nullable final String aNamespaceURI )
    {
        checkState( localName_ != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "localName" );
        checkState( aNamespaceURI != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "namespaceURI" );
        return newPrefixableImmutableChildElementBuilder( aParentNode, aElementIndex, aNamespaceURI, prefix_,
                                                          localName_, attributeBuilders_, childNodeBuilders_ );
    }


    protected abstract T newPrefixableImmutableChildElementBuilder( final ImmutableNode aParentNode,
                                                                    final Integer aElementIndex,
                                                                    final String aNamespaceURI,
                                                                    @Nullable final String aPrefix,
                                                                    final String aLocalName,
                                                                    final List<ImmutableAttributeBuilder> aAttributeBuilders,
                                                                    final List<ImmutableChildNodeBuilder> aChildNodeBuilders );


}
