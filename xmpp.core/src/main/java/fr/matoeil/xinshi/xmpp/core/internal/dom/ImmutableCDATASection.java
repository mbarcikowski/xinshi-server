/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.dom;

import org.w3c.dom.CDATASection;
import org.w3c.dom.Node;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public final class ImmutableCDATASection
    extends ImmutableCharacterData
    implements CDATASection
{

    private static final String NODE_NAME = "#cdata-section";

    public ImmutableCDATASection( final ImmutableNode aParent, final int aElementIndex, final String aData )
    {
        super( aParent, aElementIndex, aData );
    }

    @Override
    public String getNodeName()
    {
        return NODE_NAME;
    }

    @Override
    public short getNodeType()
    {
        return Node.CDATA_SECTION_NODE;
    }
}