/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo;

import com.google.common.base.CharMatcher;
import fr.matoeil.xinshi.xmpp.core.internal.old.StreamDeserializer;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.IqHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.MessageHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.PresenceHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.StanzaHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stream.FeaturesHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stream.NegotiationElementHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stream.StreamElementHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stream.StreamErrorHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp.XmppHandler;
import fr.matoeil.xinshi.xmpp.core.old.stream.StreamConstants;
import fr.matoeil.xinshi.xmpp.core.old.stream.StreamErrorConstants;
import fr.matoeil.xinshi.xmpp.core.stanza.Iq;
import fr.matoeil.xinshi.xmpp.core.stanza.Message;
import fr.matoeil.xinshi.xmpp.core.stanza.Presence;
import fr.matoeil.xinshi.xmpp.core.stanza.Stanza;
import fr.matoeil.xinshi.xmpp.core.stream.Header;
import fr.matoeil.xinshi.xmpp.core.stream.StreamElement;
import fr.matoeil.xinshi.xmpp.core.stream.StreamError;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import org.xml.sax.Attributes;
import uk.org.retep.niosax.NioSaxParserHandler;

import java.util.ArrayDeque;
import java.util.Deque;

//TODO verify xmlns='' attribute have a namespace uri
public final class StreamHandler
    extends HandlerTrait
    implements NioSaxParserHandler
{

    private final StreamListener streamListener_;

    private int depth_ = 0;

    private Deque<XmppHandler> handlers_ = new ArrayDeque<>();

    public StreamHandler( final StreamListener aStreamListener )
    {
        streamListener_ = aStreamListener;
    }

    public void reset()
    {
        depth_ = 0;
        handlers_ = new ArrayDeque<>();
    }

    @Override
    public void xmlDeclaration( String versionInfo, String encoding, boolean standalone )
    {
        if ( encoding != null && !StreamConstants.XML_ENCODING.equals( encoding ) )
        {
            throw new StreamException( StreamErrorConstants.Error.UNSUPPORTED_ENCODING );
        }
    }

    @Override
    public void startPrefixMapping( String aPrefix, String aNamespaceURI )
    {
        switch ( depth_ )
        {
            case 0:
            {
                //TODO replace by a header handler ?
                //TODO see rfc 6120#4.8
                if ( StreamConstants.STREAM_NAMESPACE_URI.equals( aNamespaceURI ) && ( aPrefix.length() > 0
                    && !StreamConstants.STREAM_PREFIX.equals( aPrefix ) ) )
                {
                    throw new StreamException( StreamErrorConstants.Error.BAD_NAMESPACE_PREFIX );
                }
                break;
            }
            case 1:
            {
                //TODO verify if content namespace wihout prefix
                break;
            }
            default:
            {
                XmppHandler xmppHandler = handlers_.peekFirst();
                xmppHandler.startPrefixMapping( aPrefix, aNamespaceURI );
            }
        }
    }

    @Override
    public void startElement( final String aNamespaceURI, final String aLocalName, final String aQualifiedName,
                              final Attributes aAttributes )
    {
        //TODO try/catch IllegalArgument & IllegalState and wrap it with stream exception ?
        switch ( depth_++ )
        {
            case 0:
            {
                //TODO replace by a header handler ?
                Header header =
                    StreamDeserializer.decodeHeader( aNamespaceURI, aLocalName, aQualifiedName, aAttributes );
                streamListener_.handleStreamOpening( header );
                break;
            }
            case 1:
            {
                if ( aNamespaceURI == null )
                {
                    //TODO correct StreamException error
                    throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
                }
                else
                {
                    switch ( aNamespaceURI )
                    {
                        //TODO server namaspace api
                        case fr.matoeil.xinshi.xmpp.core.stanza.Namespaces.CLIENT_NAMESPACE_URI:
                        {
                            XmppHandler newHandler = handleStanzaOpening( aNamespaceURI, aLocalName, aQualifiedName );
                            newHandler.startElement( aNamespaceURI, aLocalName, aQualifiedName, aAttributes );
                            handlers_.addFirst( newHandler );
                            break;
                        }
                        case fr.matoeil.xinshi.xmpp.core.stream.Namespaces.STREAM_NAMESPACE_URI:
                        {

                            if ( StreamError.LOCAL_NAME.equals( aLocalName ) )
                            {
                                StreamElementHandler newHandler = new StreamErrorHandler();
                                newHandler.startElement( aNamespaceURI, aLocalName, aQualifiedName, aAttributes );
                                handlers_.addFirst( newHandler );
                            }
                            else
                            {

                                StreamElementHandler newHandler = new FeaturesHandler();
                                newHandler.startElement( aNamespaceURI, aLocalName, aQualifiedName, aAttributes );
                                handlers_.addFirst( newHandler );
                            }
                            break;
                        }
                        default:
                        {
                            StreamElementHandler newHandler = new NegotiationElementHandler();
                            newHandler.startElement( aNamespaceURI, aLocalName, aQualifiedName, aAttributes );
                            handlers_.addFirst( newHandler );
                        }
                    }
                }
                break;
            }
            default:
            {
                XmppHandler xmppHandler = handlers_.peekFirst();
                XmppHandler newHandler = xmppHandler.handleElementOpening( aNamespaceURI, aLocalName, aQualifiedName );
                newHandler.startElement( aNamespaceURI, aLocalName, aQualifiedName, aAttributes );
                handlers_.addFirst( newHandler );
            }
        }
    }


    @Override
    public void startCDATA()
    {
        switch ( depth_ )
        {
            case 0:
            {
                //TODO replace by a header handler ?
                //TODO correct StreamException error
                throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
            }
            case 1:
            {
                //TODO replace by a xmpp handler ?
                //TODO correct StreamException error
                throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
            }
            default:
            {
                XmppHandler xmppHandler = handlers_.peekFirst();
                xmppHandler.startCDATA();
            }
        }

    }


    @Override
    public void characters( final char[] aChars, final int aStart, final int aLength )
    {
        switch ( depth_ )
        {
            case 0:
            {
                //TODO replace by a header handler ?
                //TODO correct StreamException error
                throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
            }
            case 1:
            {
                //TODO replace by a xmpp handler ?
                if ( !CharMatcher.WHITESPACE.matchesAllOf( String.valueOf( aChars, aStart, aLength ) ) )
                {
                    //TODO correct StreamException error
                    throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
                }
                break;
            }
            default:
            {
                XmppHandler xmppHandler = handlers_.peekFirst();
                xmppHandler.characters( aChars, aStart, aLength );
            }
        }
    }

    @Override
    public void endCDATA()
    {
        switch ( depth_ )
        {
            case 0:
            {
                //TODO replace by a header handler ?
                //TODO correct StreamException error
                throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
            }
            case 1:
            {
                //TODO replace by a xmpp handler ?
                //TODO correct StreamException error
                throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
            }
            default:
            {
                XmppHandler xmppHandler = handlers_.peekFirst();
                xmppHandler.endCDATA();
            }
        }
    }

    @Override
    public void endElement( final String aNamespaceURI, final String aLocalName, final String aQualifiedName )
    {
        switch ( --depth_ )
        {
            case 0:
            {
                //TODO replace by a header handler ?
                streamListener_.handleStreamClosing();
                break;
            }
            case 1:
            {
                XmppHandler xmppHandler = handlers_.pollFirst();
                if ( xmppHandler instanceof StanzaHandler )
                {
                    StanzaHandler stanzaHandler = (StanzaHandler) xmppHandler;
                    stanzaHandler.endElement( aNamespaceURI, aLocalName, aQualifiedName );
                    Stanza stanza = stanzaHandler.build();
                    streamListener_.handleIncomingStanza( stanza );
                }
                else
                {
                    StreamElementHandler streamElementHandler = (StreamElementHandler) xmppHandler;
                    streamElementHandler.endElement( aNamespaceURI, aLocalName, aQualifiedName );
                    StreamElement streamElement = streamElementHandler.build();
                    streamListener_.handleIncomingStreamElement( streamElement );
                }
                break;
            }
            default:
            {
                XmppHandler xmppHandler = handlers_.pollFirst();
                xmppHandler.endElement( aNamespaceURI, aLocalName, aQualifiedName );
            }
        }
    }

    private StanzaHandler handleStanzaOpening( final String aNamespaceURI, final String aLocalName,
                                               final String aQualifiedName )
    {
        if ( aLocalName.length() != aQualifiedName.length() )
        {
            //TODO correct StreamException error
            throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
        }
        StanzaHandler stanzaHandler;

        switch ( aLocalName )
        {
            case Iq.LOCAL_NAME:
            {
                stanzaHandler = new IqHandler( aNamespaceURI );
                break;
            }
            case Message.LOCAL_NAME:
            {
                stanzaHandler = new MessageHandler( aNamespaceURI );
                break;
            }
            case Presence.LOCAL_NAME:
            {
                stanzaHandler = new PresenceHandler( aNamespaceURI );
                break;
            }
            default:
            {
                //TODO correct StreamException error
                throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
            }
        }
        return stanzaHandler;
    }
}
