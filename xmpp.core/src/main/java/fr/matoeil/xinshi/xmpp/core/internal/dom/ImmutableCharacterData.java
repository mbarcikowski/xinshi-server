/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.dom;

import org.w3c.dom.CharacterData;
import org.w3c.dom.DOMException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import static fr.matoeil.xinshi.xmpp.core.internal.dom.Nodes.EMPTY_CHILD_NODES;

@Immutable
public abstract class ImmutableCharacterData
    extends ImmutableNodeTrait
    implements ImmutableChildNode, CharacterData
{

    //TODO create ImmutableCDATASection & ImmutableCDATASection.Builder
    private final ImmutableNode parentNode_;

    private final int elementIndex_;

    private final String data_;

    public ImmutableCharacterData( final ImmutableNode aParentNode, final int aElementIndex, final String aData )
    {
        parentNode_ = aParentNode;
        elementIndex_ = aElementIndex;
        data_ = aData;
    }

    @Override
    public final Node getParentNode()
    {
        return parentNode_;
    }

    @Nullable
    @Override
    public final Node getPreviousSibling()
    {
        return parentNode_.getChildNodes().item( elementIndex_ - 1 );
    }

    @Nullable
    @Override
    public final Node getNextSibling()
    {
        return parentNode_.getChildNodes().item( elementIndex_ + 1 );
    }


    @Override
    public final String getNodeValue()
    {
        return data_;
    }

    @Override
    public final String getData()
    {
        return data_;
    }

    @Override
    public final void setData( final String data )
    {
        throw new UnsupportedOperationException( "ImmutableCharacterData#setData not supported" );
    }

    @Override
    public final int getLength()
    {
        return data_.length();
    }

    @Override
    public final String substringData( final int offset, final int count )
    {
        int length = data_.length();
        if ( count < 0 || offset < 0 || offset > length - 1 )
        {
            throw new DOMException( DOMException.INDEX_SIZE_ERR,
                                    "The index or size is negative, or greater than the allowed value." );
        }

        int tailIndex = Math.min( offset + count, length );

        return data_.substring( offset, tailIndex );
    }

    @Override
    public final void appendData( final String arg )
    {
        throw new UnsupportedOperationException( "ImmutableCharacterData#appendData not supported" );
    }

    @Override
    public final void insertData( final int offset, final String arg )
    {
        throw new UnsupportedOperationException( "ImmutableCharacterData#insertData not supported" );
    }

    @Override
    public final void deleteData( final int offset, final int count )
    {
        throw new UnsupportedOperationException( "ImmutableCharacterData#deleteData not supported" );
    }

    @Override
    public final void replaceData( final int offset, final int count, final String arg )
    {
        throw new UnsupportedOperationException( "ImmutableCharacterData#replaceData not supported" );
    }


    @Override
    public final void setNodeValue( final String nodeValue )
    {
        throw new UnsupportedOperationException( "ImmutableCharacterData#setNodeValue not supported" );
    }


    @Override
    public final NodeList getChildNodes()
    {
        return EMPTY_CHILD_NODES;
    }

    @Override
    public final Node getFirstChild()
    {
        return null;
    }

    @Override
    public final Node getLastChild()
    {
        return null;
    }

    @Override
    public final NamedNodeMap getAttributes()
    {
        return null;
    }

    @Override
    public final boolean hasChildNodes()
    {
        return false;
    }

    @Override
    public final String getNamespaceURI()
    {
        return null;
    }

    @Override
    public final String getPrefix()
    {
        return null;
    }

    @Override
    public final String getLocalName()
    {
        return null;
    }

    @Override
    public final boolean hasAttributes()
    {
        return false;
    }

    @Override
    public final String getTextContent()
    {
        return data_;
    }

    public final Text splitText( final int offset )
    {
        throw new UnsupportedOperationException( "ImmutableText#splitText not supported" );
    }

    public final boolean isElementContentWhitespace()
    {
        //TODO ImmutableText#isElementContentWhitespace not implemented
        throw new UnsupportedOperationException( "ImmutableText#isElementContentWhitespace not implemented" );
    }

    public final String getWholeText()
    {
        //TODO ImmutableText#getWholeText not implemented
        throw new UnsupportedOperationException( "ImmutableText#getWholeText not implemented" );
    }

    public final Text replaceWholeText( final String content )
    {
        throw new UnsupportedOperationException( "ImmutableText#replaceWholeText not supported" );
    }

    @Override
    public final short compareDocumentPosition( final Node other )
    {
        //TODO ImmutableText#compareDocumentPosition not implemented
        throw new UnsupportedOperationException( "ImmutableText#compareDocumentPosition not implemented" );
    }

    @Override
    public final String lookupPrefix( final String namespaceURI )
    {
        //TODO ImmutableText#lookupPrefix not implemented
        throw new UnsupportedOperationException( "ImmutableText#lookupPrefix not implemented" );
    }

    @Override
    public final boolean isDefaultNamespace( final String namespaceURI )
    {
        //TODO ImmutableText#isDefaultNamespace not implemented
        throw new UnsupportedOperationException( "ImmutableText#isDefaultNamespace not implemented" );
    }

    @Override
    public final String lookupNamespaceURI( final String prefix )
    {
        //TODO ImmutableText#lookupNamespaceURI not implemented
        throw new UnsupportedOperationException( "ImmutableText#lookupNamespaceURI not implemented" );
    }

    @Override
    public final boolean isEqualNode( final Node arg )
    {
        //TODO implements ImmutableCharacterData#isEqualNode
        throw new UnsupportedOperationException( "ImmutableCharacterData#isEqualNode not implemented" );
    }
}
