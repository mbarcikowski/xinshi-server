/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.core.internal.dom.Elements;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableArrayNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableBaseChildElement;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.dom.Nodes;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.XmppEnumMap;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaError;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.message.Body;
import fr.matoeil.xinshi.xmpp.core.stanza.message.BodyBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.message.Subject;
import fr.matoeil.xinshi.xmpp.core.stanza.message.SubjectBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.message.Thread;
import fr.matoeil.xinshi.xmpp.core.stanza.message.ThreadBuilder;
import org.w3c.dom.Element;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.collect.Lists.transform;

@Immutable
public final class Message
    extends Stanza<Message.Type>
{

    public static final String LOCAL_NAME = "message";

    private static final Function<BodyBuilder, Body> BUILD_BODY = Elements.build();

    private static final Function<SubjectBuilder, Subject> BUILD_SUBJECT = Elements.build();

    private static final Function<ExtendedContentBuilder, ExtendedContent> BUILD_EXTENDED_CONTENT = Elements.build();

    private static final Function<ThreadBuilder, Thread> BUILD_THREAD = Elements.build();

    private List<Body> bodies_;

    private List<Subject> subjects_;

    private Thread thread_;

    private List<ExtendedContent> extendedContents_;

    Message( final String aNamespaceURI, @Nullable final String aLanguage, @Nullable final Address aFrom,
             @Nullable final Address aTo, @Nullable final String aId, @Nullable final Type aType,
             List<BodyBuilder> aBodyBuilders, List<SubjectBuilder> aSubjectBuilders,
             @Nullable final ThreadBuilder aThreadBuilder, @Nullable final StanzaErrorBuilder aStanzaErrorBuilder,
             final List<ExtendedContentBuilder> aExtendedContentBuilders )
    {
        super( aNamespaceURI, LOCAL_NAME, aLanguage, aFrom, aTo, aId, aType );
        generateChildNodes( aNamespaceURI, aBodyBuilders, aSubjectBuilders, aThreadBuilder, aStanzaErrorBuilder,
                            aExtendedContentBuilders );
    }

    public List<Body> getBodies()
    {
        return bodies_;
    }


    public List<Subject> getSubjects()
    {
        return subjects_;
    }

    @Nullable
    public Thread getThread()
    {
        return thread_;
    }

    public List<? extends Element> getExtendedContents()
    {
        return extendedContents_;
    }

    private void generateChildNodes( final String aNamespaceURI, final List<BodyBuilder> aBodyBuilders,
                                     final List<SubjectBuilder> aSubjectBuilders,
                                     @Nullable final ThreadBuilder aThreadBuilder,
                                     @Nullable final StanzaErrorBuilder aStanzaErrorBuilder,
                                     final List<ExtendedContentBuilder> aExtendedContentBuilders )
    {
        int index = 0;

        index = initializeBodies( aNamespaceURI, aBodyBuilders, index );

        index = initializeSubjects( aNamespaceURI, aSubjectBuilders, index );

        index = initializeThread( aNamespaceURI, aThreadBuilder, index );

        index = initializeError( aNamespaceURI, aStanzaErrorBuilder, index );

        initializeExtendedContents( aExtendedContentBuilders, index );

        initializeChildNodes();
    }

    private int initializeBodies( final String aNamespaceURI, final List<BodyBuilder> aBodyBuilders, final int aIndex )
    {
        int index = aIndex;
        for ( BodyBuilder builder : aBodyBuilders )
        {
            builder.isChildOf( this ).atIndex( index++ ).inNamespace( aNamespaceURI );
        }
        bodies_ = new ImmutableList.Builder<Body>().addAll( transform( aBodyBuilders, BUILD_BODY ) ).build();
        return index;
    }

    private int initializeSubjects( final String aNamespaceURI, final List<SubjectBuilder> aSubjectBuilders,
                                    final int aIndex )
    {
        int index = aIndex;
        for ( SubjectBuilder builder : aSubjectBuilders )
        {
            builder.isChildOf( this ).atIndex( index++ ).inNamespace( aNamespaceURI );
        }
        subjects_ = new ImmutableList.Builder<Subject>().addAll( transform( aSubjectBuilders, BUILD_SUBJECT ) ).build();
        return index;
    }

    private int initializeThread( final String aNamespaceURI, final ThreadBuilder aThreadBuilder, final int aIndex )
    {
        int index = aIndex;
        if ( aThreadBuilder != null )
        {
            aThreadBuilder.isChildOf( this ).atIndex( index++ ).inNamespace( aNamespaceURI );
        }
        thread_ = BUILD_THREAD.apply( aThreadBuilder );
        return index;
    }


    private void initializeExtendedContents( final List<ExtendedContentBuilder> aExtendedContentBuilders,
                                             final int aIndex )
    {
        int index = aIndex;
        for ( ExtendedContentBuilder builder : aExtendedContentBuilders )
        {
            builder.isChildOf( this ).atIndex( index++ );
        }
        extendedContents_ = new ImmutableList.Builder<ExtendedContent>().addAll(
            transform( aExtendedContentBuilders, BUILD_EXTENDED_CONTENT ) ).build();
    }

    private void initializeChildNodes()
    {
        StanzaError aStanzaError = getStanzaError();
        ImmutableNodeList childNodes;
        int initialCapacity =
            bodies_.size() + subjects_.size() + ( thread_ == null ? 0 : 1 ) + ( aStanzaError == null ? 0 : 1 )
                + extendedContents_.size();
        if ( initialCapacity == 0 )
        {
            childNodes = Nodes.EMPTY_CHILD_NODES;
        }
        else
        {
            List<ImmutableBaseChildElement> childElements = new ArrayList<>( initialCapacity );
            childElements.addAll( bodies_ );
            childElements.addAll( subjects_ );
            if ( thread_ != null )
            {
                childElements.add( thread_ );
            }
            if ( aStanzaError != null )
            {
                childElements.add( aStanzaError );
            }
            childElements.addAll( extendedContents_ );
            childNodes = new ImmutableArrayNodeList( childElements );
        }
        setImmutableChildNodes( childNodes );
    }

    public static enum Type
        implements fr.matoeil.xinshi.xmpp.core.stanza.Type
    {
        ERROR( "error" ),
        CHAT( "chat" ),
        GROUPCHAT( "groupchat" ),
        HEADLINE( "headline" ),
        NORMAL( "normal" );

        private final String name_;

        Type( final String aName )
        {
            name_ = aName;
        }

        public String asString()
        {
            return name_;
        }

        public static Type fromString( String aType )
        {
            return XMPP_ENUM_MAP.fromString( aType );
        }

        private static final XmppEnumMap<Type> XMPP_ENUM_MAP = new XmppEnumMap<>( Type.class );

    }

}
