package fr.matoeil.xinshi.xmpp.core.stanza.error;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.Condition;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.XmppEnumMap;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

/**
 * @author mathieu.barcikowski@gmail.com
 */
@Immutable
public final class StanzaErrorCondition
    extends Condition<StanzaErrorCondition.DefinedCondition>
{

    StanzaErrorCondition( final ImmutableNode aParentNode, final int aElementIndex,
                          final DefinedCondition aDefinedCondition, @Nullable final String aContent )
    {
        super( aParentNode, aElementIndex, Namespaces.STANZAS_NAMESPACE_URI, aDefinedCondition, aContent );

    }

    public static enum DefinedCondition
        implements fr.matoeil.xinshi.xmpp.core.internal.xmpp.DefinedCondition
    {
        BAD_REQUEST( "bad-request" ),
        CONFLICT( "conflict" ),
        FEATURE_NOT_IMPLEMENTED( "feature-not-implemented" ),
        FORBIDDEN( "forbidden" ),
        GONE( "gone" ),
        INTERNAL_SERVER_ERROR( "internal-server-error" ),
        ITEM_NOT_FOUND( "item-not-found" ),
        JID_MALFORMED( "jid-malformed" ),
        NOT_ACCEPTABLE( "not-acceptable" ),
        NOT_ALLOWED( "not-allowed" ),
        NOT_AUTHORIZED( "not-authorized" ),
        POLICY_VIOLATION( "policy-violation" ),
        RECIPIENT_UNAVAILABLE( "recipient-unavailable" ),
        REDIRECT( "redirect" ),
        REGISTRATION_REQUIRED( "registration-required" ),
        REMOTE_SERVER_NOT_FOUND( "remote-server-not-found" ),
        REMOTE_SERVER_TIMEOUT( "remote-server-timeout" ),
        RESOURCE_CONSTRAINT( "resource-constraint" ),
        SERVICE_UNAVAILABLE( "service-unavailable" ),
        SUBSCRIPTION_REQUIRED( "subscription-required" ),
        UNDEFINED_CONDITION( "undefined-condition" ),
        UNEXPECTED_REQUEST( "unexpected-request" );

        private final String localName_;

        DefinedCondition( final String aLocalName )
        {
            localName_ = aLocalName;
        }

        public String asString()
        {
            return localName_;
        }

        public static DefinedCondition fromString( String aType )
        {
            return XMPP_ENUM_MAP.fromString( aType );
        }

        private static final XmppEnumMap<DefinedCondition> XMPP_ENUM_MAP = new XmppEnumMap<>( DefinedCondition.class );

    }

}
