/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza.error;

import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableArrayImmutableAttributeMap;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableArrayNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableAttribute;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableBaseChildElement;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableChildElement;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableChildNode;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableOneNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.XmppEnum;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.XmppEnumMap;
import org.w3c.dom.Element;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author mathieu.barcikowski@gmail.com
 */
@Immutable
public final class StanzaError
    extends ImmutableBaseChildElement
{
    public static final String LOCAL_NAME = "error";

    public static final String BY_ATTRIBUTE_LOCAL_NAME = "by";

    public static final String TYPE_ATTRIBUTE_LOCAL_NAME = "type";

    private static final int COMMON_ATTRIBUTES_COUNT = 2;

    private static final int INITIAL_CAPACITY = 3;

    private final Address by_;

    private final Type type_;

    private StanzaErrorCondition stanzaErrorCondition_;

    @Nullable
    private StanzaErrorText stanzaErrorText_;

    @Nullable
    private ImmutableChildElement applicationCondition_;

    //TODO prefix must be null

    StanzaError( final ImmutableNode aParentNode, final int aElementIndex, @Nullable final String aNamespaceURI,
                 @Nullable Address aBy, Type aType, StanzaErrorConditionBuilder aStanzaErrorConditionBuilder,
                 @Nullable StanzaErrorTextBuilder aStanzaErrorTextBuilder,
                 @Nullable ImmutableChildElementBuilder aApplicationConditionBuilder )
    {
        super( aParentNode, aElementIndex, aNamespaceURI, null, LOCAL_NAME );
        by_ = aBy;
        type_ = aType;
        generateAttributes();
        generateChildNodes( aStanzaErrorConditionBuilder, aStanzaErrorTextBuilder, aApplicationConditionBuilder );
    }

    public Address getBy()
    {
        return by_;
    }


    public Type getType()
    {
        return type_;
    }

    public StanzaErrorCondition getCondition()
    {
        return stanzaErrorCondition_;
    }

    @Nullable
    public StanzaErrorText getText()
    {
        return stanzaErrorText_;
    }

    @Nullable
    public Element getApplicationCondition()
    {
        return applicationCondition_;
    }

    @Override
    public String getTextContent()
    {
        //TODO implements StanzaError#getTextContent
        throw new UnsupportedOperationException( "Error#getTextContent not implemented" );
    }

    private void generateAttributes()
    {
        List<ImmutableAttribute> attributes = new ArrayList<>( COMMON_ATTRIBUTES_COUNT );
        generateAttribute( attributes, BY_ATTRIBUTE_LOCAL_NAME, by_ );
        generateAttribute( attributes, TYPE_ATTRIBUTE_LOCAL_NAME, type_ );
        setImmutableAttributes( new ImmutableArrayImmutableAttributeMap( attributes ) );
    }

    private void generateAttribute( List<ImmutableAttribute> aAttributes, String aLocalName, Object aValue )
    {
        if ( aValue != null )
        {
            aAttributes.add( new ImmutableAttribute( this, null, null, aLocalName, aValue.toString() ) );
        }
    }

    private void generateChildNodes( final StanzaErrorConditionBuilder aStanzaErrorConditionBuilder,
                                     @Nullable StanzaErrorTextBuilder aStanzaErrorTextBuilder,
                                     @Nullable final ImmutableChildElementBuilder aApplicationConditionBuilder )
    {
        List<ImmutableChildNode> immutableChildNodes = new ArrayList<>( INITIAL_CAPACITY );

        int index = 0;
        stanzaErrorCondition_ = aStanzaErrorConditionBuilder.isChildOf( this ).atIndex( index++ ).build();
        immutableChildNodes.add( stanzaErrorCondition_ );

        if ( aStanzaErrorTextBuilder == null )
        {
            stanzaErrorText_ = null;
        }
        else
        {
            stanzaErrorText_ = aStanzaErrorTextBuilder.isChildOf( this ).atIndex( index++ ).build();
            immutableChildNodes.add( stanzaErrorText_ );
        }

        if ( aApplicationConditionBuilder == null )
        {
            applicationCondition_ = null;
        }
        else
        {
            applicationCondition_ = aApplicationConditionBuilder.isChildOf( this ).atIndex( index ).build();
            immutableChildNodes.add( applicationCondition_ );
        }

        ImmutableNodeList immutableNodeList;
        if ( immutableChildNodes.size() == 1 )
        {
            immutableNodeList = new ImmutableOneNodeList( stanzaErrorCondition_ );
        }
        else
        {
            immutableNodeList = new ImmutableArrayNodeList( immutableChildNodes );
        }

        setImmutableChildNodes( immutableNodeList );
    }

    public static enum Type
        implements XmppEnum
    {
        AUTH( "auth" ),
        CANCEL( "cancel" ),
        CONTINUE( "continue" ),
        MODIFY( "modify" ),
        WAIT( "wait" );

        private final String name_;

        Type( final String aName )
        {
            name_ = aName;
        }

        public String asString()
        {
            return name_;
        }

        public static Type fromString( String aType )
        {
            return XMPP_ENUM_MAP.fromString( aType );
        }

        private static final XmppEnumMap<Type> XMPP_ENUM_MAP = new XmppEnumMap<>( Type.class );
    }


}
