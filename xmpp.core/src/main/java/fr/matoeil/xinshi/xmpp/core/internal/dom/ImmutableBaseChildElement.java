/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.dom;

import org.w3c.dom.Node;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

/**
 * @author mathieu.barcikowski@gmail.com
 */
@Immutable
public abstract class ImmutableBaseChildElement
    extends ImmutableBaseElement
    implements ImmutableChildNode
{
    private final ImmutableNode parentNode_;

    private final int elementIndex_;

    public ImmutableBaseChildElement( final ImmutableNode aParentNode, final int aElementIndex,
                                      @Nullable final String aNamespaceURI, @Nullable final String aPrefix,
                                      final String aLocalName )
    {
        super( aNamespaceURI, aPrefix, aLocalName );
        parentNode_ = aParentNode;
        elementIndex_ = aElementIndex;
    }


    @Override
    public final Node getParentNode()
    {
        return parentNode_;
    }

    @Nullable
    @Override
    public final Node getPreviousSibling()
    {
        return parentNode_.getChildNodes().item( elementIndex_ - 1 );
    }

    @Nullable
    @Override
    public final Node getNextSibling()
    {
        return parentNode_.getChildNodes().item( elementIndex_ + 1 );
    }

}
