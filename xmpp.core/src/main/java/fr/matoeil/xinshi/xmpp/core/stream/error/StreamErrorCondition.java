/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream.error;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.Condition;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.XmppEnumMap;
import fr.matoeil.xinshi.xmpp.core.stream.Namespaces;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

/**
 * @author mathieu.barcikowski@gmail.com
 */
@Immutable
public final class StreamErrorCondition
    extends Condition<StreamErrorCondition.DefinedCondition>
{

    StreamErrorCondition( final ImmutableNode aParentNode, final int aElementIndex,
                          final DefinedCondition aDefinedCondition, @Nullable final String aContent )
    {
        super( aParentNode, aElementIndex, Namespaces.STREAM_ERROR_NAMESPACE_URI, aDefinedCondition, aContent );
    }

    public static enum DefinedCondition
        implements fr.matoeil.xinshi.xmpp.core.internal.xmpp.DefinedCondition
    {
        BAD_FORMAT( "bad-format" ),
        BAD_NAMESPACE_PREFIX( "bad-namespace-prefix" ),
        CONFLICT( "conflict" ),
        CONNECTION_TIMEOUT( "connection-timeout" ),
        HOST_GONE( "host-gone" ),
        HOST_UNKNOWN( "host-unknown" ),
        IMPROPER_ADDRESSING( "improper-addressing" ),
        INTERNAL_SERVER_ERROR( "internal-server-error" ),
        INVALID_FROM( "invalid-from" ),
        INVALID_ID( "invalid-id" ),
        INVALID_NAMESPACE( "invalid-namespace" ),
        INVALID_XML( "invalid-xml" ),
        NOT_AUTHORIZED( "not-authorized" ),
        NOT_WELL_FORMED( "not-well-formed" ),
        POLICY_VIOLATION( "policy-violation" ),
        REMOTE_CONNECTION_FAILED( "remote-connection-failed" ),
        RESET( "reset" ),
        RESOURCE_CONSTRAINT( "resource-constraint" ),
        RESTRICTED_XML( "restricted-xml" ),
        SEE_OTHER_HOST( "see-other-host" ),
        SYSTEM_SHUTDOWN( "system-shutdown" ),
        UNDEFINED_CONDITION( "undefined-condition" ),
        UNSUPPORTED_ENCODING( "unsupported-encoding" ),
        UNSUPPORTED_FEATURE( "unsupported-feature" ),
        UNSUPPORTED_STANZA_TYPE( "unsupported-stanza-type" ),
        UNSUPPORTED_VERSION( "unsupported-version" );

        private final String localName_;

        DefinedCondition( final String aLocalName )
        {
            localName_ = aLocalName;
        }

        public String asString()
        {
            return localName_;
        }

        public static DefinedCondition fromString( String aType )
        {
            return XMPP_ENUM_MAP.fromString( aType );
        }

        private static final XmppEnumMap<DefinedCondition> XMPP_ENUM_MAP = new XmppEnumMap<>( DefinedCondition.class );

    }

}
