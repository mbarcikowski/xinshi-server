/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza;

import com.google.common.base.Function;
import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.core.internal.dom.Elements;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableArrayImmutableAttributeMap;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableAttribute;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableBaseElement;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.Xmls;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaError;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorBuilder;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.ArrayList;
import java.util.List;

@Immutable
public abstract class Stanza<T extends Type>
    extends ImmutableBaseElement
    implements Element
{

    protected static final Function<StanzaErrorBuilder, StanzaError> BUILD_ERROR = Elements.build();

    private static final int COMMON_ATTRIBUTES_COUNT = 5;

    public static final String FROM_ATTRIBUTE = "from";

    public static final String TO_ATTRIBUTE = "to";

    public static final String ID_ATTRIBUTE = "id";

    public static final String TYPE_ATTRIBUTE = "type";

    private final String language_;

    private final Address from_;

    private final Address to_;

    private final String id_;

    private final T type_;

    private StanzaError stanzaError_;

    protected Stanza( final String aNamespaceURI, final String aLocalName, @Nullable final String aLanguage,
                      @Nullable final Address aFrom, @Nullable final Address aTo, @Nullable final String aId,
                      @Nullable final T aType )
    {
        super( aNamespaceURI, null, aLocalName );
        language_ = aLanguage;
        from_ = aFrom;
        to_ = aTo;
        id_ = aId;
        type_ = aType;
        generateAttributes();
    }

    @Nullable
    public final StanzaError getStanzaError()
    {
        return stanzaError_;
    }

    @Nullable
    public final String getLanguage()
    {
        return language_;
    }

    @Nullable
    public final Address getFrom()
    {
        return from_;
    }

    @Nullable
    public final Address getTo()
    {
        return to_;
    }

    @Nullable
    public final String getId()
    {
        return id_;
    }

    @Nullable
    public final T getType()
    {
        return type_;
    }

    @Override
    public final Node getParentNode()
    {
        return null;
    }

    @Override
    public final Node getPreviousSibling()
    {
        return null;
    }

    @Override
    public final Node getNextSibling()
    {
        return null;
    }

    @Override
    public final String getTextContent()
    {
        //TODO Stanza#getTextContent not implemented
        throw new UnsupportedOperationException( "Stanza#getTextContent not implemented" );
    }

    //TODO set xmlns attribute if stream root is not prefixed
    private void generateAttributes()
    {
        List<ImmutableAttribute> attributes = new ArrayList<>( COMMON_ATTRIBUTES_COUNT );
        generateAttribute( attributes, Xmls.XML_NS_URI, Xmls.XML_NS_PREFIX, Xmls.LANG, language_ );
        generateAttribute( attributes, FROM_ATTRIBUTE, from_ );
        generateAttribute( attributes, TO_ATTRIBUTE, to_ );
        generateAttribute( attributes, ID_ATTRIBUTE, id_ );
        generateAttribute( attributes, TYPE_ATTRIBUTE, type_ );
        setImmutableAttributes( new ImmutableArrayImmutableAttributeMap( attributes ) );
    }

    private void generateAttribute( List<ImmutableAttribute> aAttributes, String aNamespaceURI, String aPrefix,
                                    String aLocalName, Object aValue )
    {
        if ( aValue != null )
        {
            aAttributes.add( new ImmutableAttribute( this, aNamespaceURI, aPrefix, aLocalName, aValue.toString() ) );
        }
    }

    private void generateAttribute( List<ImmutableAttribute> aAttributes, String aLocalName, Object aValue )
    {
        generateAttribute( aAttributes, null, null, aLocalName, aValue );
    }

    protected final int initializeError( final String aNamespaceURI, final StanzaErrorBuilder aStanzaErrorBuilder,
                                         final int aIndex )
    {
        int index = aIndex;
        if ( aStanzaErrorBuilder != null )
        {
            aStanzaErrorBuilder.isChildOf( this ).atIndex( index ).inNamespace( aNamespaceURI );
            index++;
        }
        stanzaError_ = BUILD_ERROR.apply( aStanzaErrorBuilder );
        return index;
    }


}
