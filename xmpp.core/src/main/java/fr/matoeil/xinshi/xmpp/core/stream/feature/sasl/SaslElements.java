/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream.feature.sasl;

import fr.matoeil.xinshi.xmpp.core.dom.ImmutableAttributeBuilder;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableTextBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.Xmls;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.XmppEnumMap;
import fr.matoeil.xinshi.xmpp.core.stream.NegotiationElement;
import fr.matoeil.xinshi.xmpp.core.stream.NegotiationElementBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.feature.FeatureBuilder;

import javax.annotation.Nullable;

import static fr.matoeil.xinshi.xmpp.core.stream.feature.sasl.Namespaces.SASL_NAMESPACE_URI;

/**
 * @author mathieu.barcikowski@gmail.com
 * @see <a href="http://xmpp.org/rfcs/rfc6120.html#sasl">http://xmpp.org/rfcs/rfc6120.html#sasl</a>
 */
public final class SaslElements
{


    public static final NegotiationElement SASL_ABORT = createSaslAbort();

    public static final NegotiationElement SASL_SUCCESS = createSaslSuccess();

    private static final String SUCCESS_LOCAL_NAME = "success";

    private static final String FAILURE_LOCAL_NAME = "failure";

    private static final String CHALLENGE_LOCAL_NAME = "challenge";

    private static final String MECHANISMS_LOCAL_NAME = "mechanisms";

    private static final String MECHANISM_LOCAL_NAME = "mechanism";

    private static final String AUTH_LOCAL_NAME = "auth";

    private static final String ABORT_LOCAL_NAME = "abort";

    private static final String TEXT_LOCALNAME = "text";

    private SaslElements()
    {
    }

    public static FeatureBuilder createSaslFeatureBuilderWithMecanisms( String... aMechanisms )
    {
        FeatureBuilder builder = new FeatureBuilder().inNamespace( SASL_NAMESPACE_URI ).named( MECHANISMS_LOCAL_NAME );
        for ( String mechanism : aMechanisms )
        {
            builder.withChildElement( new ImmutableChildElementBuilder().inNamespace( SASL_NAMESPACE_URI ).named(
                MECHANISM_LOCAL_NAME ).withText( new ImmutableTextBuilder().withData( mechanism ) ) );
        }
        return builder;
    }

    public static NegotiationElement createSaslAuthNegotiationElement( String aMechanism )
    {
        NegotiationElementBuilder builder =
            new NegotiationElementBuilder().inNamespace( SASL_NAMESPACE_URI ).named( AUTH_LOCAL_NAME ).withAttribute(
                new ImmutableAttributeBuilder().named( MECHANISM_LOCAL_NAME ).withValue( aMechanism ) );
        return builder.build();
    }

    public static NegotiationElement createSaslAuthNegotiationElementWithInitialResponse( String aMechanism,
                                                                                          String aInitialResponse )
    {
        NegotiationElementBuilder builder =
            new NegotiationElementBuilder().inNamespace( SASL_NAMESPACE_URI ).named( AUTH_LOCAL_NAME ).withAttribute(
                new ImmutableAttributeBuilder().named( MECHANISM_LOCAL_NAME ).withValue( aMechanism ) ).withText(
                new ImmutableTextBuilder().withData( aInitialResponse ) );
        return builder.build();
    }

    public static NegotiationElement createSaslFailureWithDetails( DefinedCondition aDefinedCondition,
                                                                   @Nullable String aLanguage, String aText )
    {
        ImmutableChildElementBuilder textBuilder = new ImmutableChildElementBuilder();

        NegotiationElementBuilder builder = new NegotiationElementBuilder().inNamespace( SASL_NAMESPACE_URI ).named(
            FAILURE_LOCAL_NAME ).withChildElement(
            new ImmutableChildElementBuilder().inNamespace( SASL_NAMESPACE_URI ).named(
                aDefinedCondition.asString() ) ).withChildElement(
            textBuilder.inNamespace( SASL_NAMESPACE_URI ).named( TEXT_LOCALNAME ).withText(
                new ImmutableTextBuilder().withData( aText ) ) );
        if ( aLanguage != null )
        {
            textBuilder.withAttribute(
                new ImmutableAttributeBuilder().inNamespace( Xmls.XML_NS_URI ).withPrefix( Xmls.XML_NS_PREFIX ).named(
                    Xmls.LANG ).withValue( aLanguage ) );
        }
        return builder.build();
    }

    public static NegotiationElement createSaslFailure( DefinedCondition aDefinedCondition )
    {
        NegotiationElementBuilder builder = new NegotiationElementBuilder().inNamespace( SASL_NAMESPACE_URI ).named(
            FAILURE_LOCAL_NAME ).withChildElement(
            new ImmutableChildElementBuilder().inNamespace( SASL_NAMESPACE_URI ).named(
                aDefinedCondition.asString() ) );
        return builder.build();
    }

    public static NegotiationElement createSaslChallenge( String aChallenge )
    {
        NegotiationElementBuilder builder =
            new NegotiationElementBuilder().inNamespace( SASL_NAMESPACE_URI ).named( CHALLENGE_LOCAL_NAME ).withText(
                new ImmutableTextBuilder().withData( aChallenge ) );
        return builder.build();
    }

    private static NegotiationElement createSaslAbort()
    {
        NegotiationElementBuilder builder =
            new NegotiationElementBuilder().inNamespace( SASL_NAMESPACE_URI ).named( ABORT_LOCAL_NAME );
        return builder.build();
    }

    private static NegotiationElement createSaslSuccess()
    {
        NegotiationElementBuilder builder =
            new NegotiationElementBuilder().inNamespace( SASL_NAMESPACE_URI ).named( SUCCESS_LOCAL_NAME );
        return builder.build();
    }

    public static enum DefinedCondition
        implements fr.matoeil.xinshi.xmpp.core.internal.xmpp.DefinedCondition
    {
        ABORTED( "aborted" ),
        ACCOUNT_DISABLED( "account-disabled" ),
        CREDENTIALS_EXPIRED( "credentials-expired" ),
        ENCRYPTION_REQUIRED( "encryption-required" ),
        INCORRECT_ENCODING( "incorrect-encoding" ),
        INVALID_AUTHZID( "invalid-authzid" ),
        INVALID_MECHANISM( "invalid-mechanism" ),
        MALFORMED_REQUEST( "malformed-request" ),
        MECHANISM_TOO_WEAK( "mechanism-too-weak" ),
        NOT_AUTHORIZED( "not-authorized" ),
        TEMPORARY_AUTH_FAILURE( "temporary-auth-failure" );

        private final String name_;

        DefinedCondition( final String aName )
        {
            name_ = aName;
        }

        public String asString()
        {
            return name_;
        }

        public static DefinedCondition fromString( String aType )
        {
            return XMPP_ENUM_MAP.fromString( aType );
        }

        private static final XmppEnumMap<DefinedCondition> XMPP_ENUM_MAP = new XmppEnumMap<>( DefinedCondition.class );
    }
}
