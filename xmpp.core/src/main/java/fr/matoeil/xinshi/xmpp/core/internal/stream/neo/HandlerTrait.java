/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo;

import fr.matoeil.xinshi.xmpp.core.old.stream.StreamErrorConstants;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.ext.LexicalHandler;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public abstract class HandlerTrait
    implements ContentHandler, LexicalHandler
{


    @Override
    public final void startDTD( String name, String publicId, String systemId )
    {
        throw new StreamException( StreamErrorConstants.Error.RESTRICTED_XML );
    }

    @Override
    public final void endDTD()
    {
        throw new StreamException( StreamErrorConstants.Error.RESTRICTED_XML );
    }

    @Override
    public final void startEntity( String name )
    {
        throw new StreamException( StreamErrorConstants.Error.RESTRICTED_XML );
    }

    @Override
    public final void endEntity( String name )
    {
        throw new StreamException( StreamErrorConstants.Error.RESTRICTED_XML );
    }

    @Override
    public final void comment( char[] ch, int start, int length )
    {
        throw new StreamException( StreamErrorConstants.Error.RESTRICTED_XML );
    }

    @Override
    public final void processingInstruction( String target, String data )
    {
        throw new StreamException( StreamErrorConstants.Error.RESTRICTED_XML );
    }

    @Override
    public final void skippedEntity( String name )
    {
        throw new StreamException( StreamErrorConstants.Error.RESTRICTED_XML );
    }

    @Override
    public final void setDocumentLocator( Locator locator )
    {
        //no op
    }

    @Override
    public final void startDocument()
    {
        //no op
    }

    @Override
    public final void endDocument()
    {
        //no op
    }

    @Override
    public final void endPrefixMapping( String prefix )
    {
        //no op
    }

    @Override
    public final void ignorableWhitespace( char[] ch, int start, int length )
    {
        //no op
    }
}
