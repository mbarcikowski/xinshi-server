/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream.feature;

import fr.matoeil.xinshi.xmpp.core.dom.ImmutableAttributeBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableChildNodeBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.ImmutableBasePrefixableChildElementBuilder;

import javax.annotation.Nullable;
import java.util.List;


/**
 * @author mathieu.barcikowski@gmail.com
 */
public final class FeatureBuilder
    extends ImmutableBasePrefixableChildElementBuilder<Feature, FeatureBuilder>
{

    @Override
    protected Feature newPrefixableImmutableChildElementBuilder( final ImmutableNode aParentNode,
                                                                 final Integer aElementIndex,
                                                                 final String aNamespaceURI,
                                                                 @Nullable final String aPrefix,
                                                                 final String aLocalName,
                                                                 final List<ImmutableAttributeBuilder> aAttributeBuilders,
                                                                 final List<ImmutableChildNodeBuilder> aChildNodeBuilders )
    {
        return new Feature( aParentNode, aElementIndex, aNamespaceURI, aPrefix, aLocalName, aAttributeBuilders,
                            aChildNodeBuilders );
    }
}
