/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.xmpp;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableChildNodeBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;

import javax.annotation.Nullable;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public abstract class TextBuilder<T extends Text, B extends TextBuilder<T, B>>
    implements ImmutableChildNodeBuilder<T, TextBuilder<T, B>>
{

    private static final String MUST_NOT_BE_NULL_ERROR_MESSAGE = "%s must not be null";

    private ImmutableNode parentNode_;

    private Integer elementIndex_;

    private String language_;

    private String content_;

    public final B isChildOf( ImmutableNode aParentNode )
    {
        checkArgument( aParentNode != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "parentNode" );
        parentNode_ = aParentNode;
        return (B) this;
    }

    public final B atIndex( int aElementIndex )
    {
        checkArgument( aElementIndex >= 0, "must not be negative: %s", aElementIndex );
        elementIndex_ = aElementIndex;
        return (B) this;
    }

    public final B inLanguage( String aLanguage )
    {
        language_ = aLanguage;
        return (B) this;
    }


    public final B withContent( String aContent )
    {
        content_ = aContent;
        return (B) this;
    }

    @Override
    public final T build()
    {
        checkState( parentNode_ != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "parentNode" );
        checkState( elementIndex_ != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "elementIndex" );
        return newText( parentNode_, elementIndex_, language_, content_ );
    }

    protected abstract T newText( final ImmutableNode aParentNode, final Integer aElementIndex,
                                  @Nullable final String aLanguage, @Nullable final String aContent );
}
