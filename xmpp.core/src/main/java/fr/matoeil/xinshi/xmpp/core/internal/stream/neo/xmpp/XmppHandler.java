/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp;

import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.HandlerTrait;
import org.xml.sax.Attributes;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public abstract class XmppHandler
    extends HandlerTrait
{


    public abstract XmppHandler handleElementOpening( final String aNamespaceURI, final String aLocalName,
                                                      final String aQualifiedName );


    @Override
    public abstract void startPrefixMapping( final String aPrefix, final String aNamespaceURI );

    @Override
    public abstract void startElement( final String aNamespaceURI, final String aLocalName, final String aQualifiedName,
                                       final Attributes aAttributes );


    @Override
    public abstract void startCDATA();

    @Override
    public abstract void characters( final char[] aChars, final int aStart, final int aLength );

    @Override
    public abstract void endCDATA();

    @Override
    public final void endElement( final String aNamespaceURI, final String aLocalName, final String aQualifiedName )
    {
        //no op
    }
}
