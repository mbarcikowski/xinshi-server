/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza;

import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp.ImmutableBasePrefixableChildElementHandler;
import fr.matoeil.xinshi.xmpp.core.old.stream.StreamErrorConstants;
import fr.matoeil.xinshi.xmpp.core.stanza.ExtendedContent;
import fr.matoeil.xinshi.xmpp.core.stanza.ExtendedContentBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.Namespaces;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;

public class ExtendedContentHandler
    extends ImmutableBasePrefixableChildElementHandler<ExtendedContent, ExtendedContentBuilder>
{

    public ExtendedContentHandler()
    {
        super( new ExtendedContentBuilder() );
    }

    @Override
    public void startPrefixMapping( final String aPrefix, final String aNamespaceURI )
    {
        //TODO other namespace (server,....)
        if ( Namespaces.CLIENT_NAMESPACE_URI.equals( aNamespaceURI ) )
        {
            //TODO correct StreamException error
            throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
        }
    }

}
