/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.dom;

import com.google.common.base.Function;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableAttributeBuilder;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public final class Elements
{

    private Elements()
    {

    }

    public static final ImmutableAttributeMap EMPTY_ATTRIBUTES = new EmptyImmutableAttributeMap();

    public static <T extends ImmutableBaseChildElement, B extends ImmutableBaseChildElementBuilder<T, B>> Function<B, T> build()
    {
        return new Function<B, T>()
        {
            @Nullable
            @Override
            public T apply( @Nullable B input )
            {
                if ( input == null )
                {
                    return null;
                }
                else
                {
                    return input.build();
                }
            }
        };
    }

    public static ImmutableAttributeMap generateAttributes( final ImmutableBaseElement aOwnerElement,
                                                            final List<ImmutableAttributeBuilder> aAttributeBuilders )
    {
        ImmutableAttributeMap immutableAttributes;
        int size = aAttributeBuilders.size();
        switch ( size )
        {
            case 0:
            {
                immutableAttributes = Elements.EMPTY_ATTRIBUTES;
                break;
            }
            case 1:
            {
                final ImmutableAttribute immutableAttribute =
                    aAttributeBuilders.get( 0 ).isOwnedBy( aOwnerElement ).build();
                immutableAttributes = new ImmutableOneImmutableAttributeMap( immutableAttribute );
                break;
            }
            default:
            {
                List<ImmutableAttribute> attributes = new ArrayList<>( size );
                for ( ImmutableAttributeBuilder builder : aAttributeBuilders )
                {
                    attributes.add( builder.isOwnedBy( aOwnerElement ).build() );
                }
                immutableAttributes = new ImmutableArrayImmutableAttributeMap( attributes );
            }
        }
        return immutableAttributes;
    }

    public static ImmutableNodeList generateChildNodes( final ImmutableNode aParentNode,
                                                        final List<ImmutableChildNodeBuilder> aChildNodeBuilders )
    {
        int index = 0;
        ImmutableNodeList immutableNodeList;
        int size = aChildNodeBuilders.size();
        switch ( size )
        {
            case 0:
            {
                immutableNodeList = Nodes.EMPTY_CHILD_NODES;
                break;
            }
            case 1:
            {
                final ImmutableChildNode immutableChildNode =
                    aChildNodeBuilders.get( 0 ).isChildOf( aParentNode ).atIndex( index ).build();
                immutableNodeList = new ImmutableOneNodeList( immutableChildNode );
                break;
            }
            default:
            {
                List<ImmutableChildNode> immutableChildNodes = new ArrayList<>( size );
                for ( ImmutableChildNodeBuilder builder : aChildNodeBuilders )
                {
                    immutableChildNodes.add( builder.isChildOf( aParentNode ).atIndex( index++ ).build() );
                }
                immutableNodeList = new ImmutableArrayNodeList( immutableChildNodes );
            }
        }

        return immutableNodeList;
    }

}
