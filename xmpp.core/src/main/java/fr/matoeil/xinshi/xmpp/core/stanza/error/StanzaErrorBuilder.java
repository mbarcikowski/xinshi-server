/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza.error;

import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableBaseChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public final class StanzaErrorBuilder
    extends ImmutableBaseChildElementBuilder<StanzaError, StanzaErrorBuilder>
{

    private static final String MUST_NOT_BE_NULL_ERROR_MESSAGE = "%s must not be null";

    private Address by_;

    private StanzaError.Type type_;

    private StanzaErrorConditionBuilder stanzaErrorConditionBuilder_;

    private StanzaErrorTextBuilder stanzaErrorTextBuilder_;

    private ImmutableChildElementBuilder applicationConditionBuilder_;

    public StanzaErrorBuilder by( Address aBy )
    {
        checkArgument( aBy != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "by" );
        by_ = aBy;
        return this;
    }

    public StanzaErrorBuilder ofType( StanzaError.Type aType )
    {
        checkArgument( aType != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "type" );
        type_ = aType;
        return this;
    }

    public StanzaErrorBuilder withCondition( StanzaErrorConditionBuilder aStanzaErrorConditionBuilder )
    {
        checkArgument( aStanzaErrorConditionBuilder != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "conditionBuilder" );
        stanzaErrorConditionBuilder_ = aStanzaErrorConditionBuilder;
        return this;
    }

    public StanzaErrorBuilder withText( final StanzaErrorTextBuilder aStanzaErrorTextBuilder )
    {
        checkArgument( aStanzaErrorTextBuilder != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "textBuilder" );
        checkState( stanzaErrorTextBuilder_ == null, "must have only one text" );
        stanzaErrorTextBuilder_ = aStanzaErrorTextBuilder;
        return this;
    }

    public StanzaErrorBuilder withApplicationCondition(
        final ImmutableChildElementBuilder aApplicationConditionBuilder )
    {
        checkArgument( aApplicationConditionBuilder != null, MUST_NOT_BE_NULL_ERROR_MESSAGE,
                       "applicationConditionBuilder" );
        checkState( applicationConditionBuilder_ == null, "must have only one application condition" );
        applicationConditionBuilder_ = aApplicationConditionBuilder;
        return this;
    }


    @Override
    protected StanzaError newImmutableChildElement( ImmutableNode aParentNode, Integer aElementIndex,
                                                    String aNamespaceURI )
    {
        checkState( type_ != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "type" );
        checkState( stanzaErrorConditionBuilder_ != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "conditionBuilder" );
        return new StanzaError( aParentNode, aElementIndex, aNamespaceURI, by_, type_, stanzaErrorConditionBuilder_,
                                stanzaErrorTextBuilder_, applicationConditionBuilder_ );
    }
}
