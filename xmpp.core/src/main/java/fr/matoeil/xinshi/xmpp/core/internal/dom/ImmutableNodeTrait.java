/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.dom;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.UserDataHandler;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public abstract class ImmutableNodeTrait
    implements ImmutableNode
{
    //Node
    public final Node insertBefore( final Node newChild, final Node refChild )
    {
        throw new UnsupportedOperationException( "ImmutableNode#insertBefore not supported" );
    }

    public final Node replaceChild( final Node newChild, final Node oldChild )
    {
        throw new UnsupportedOperationException( "ImmutableNode#replaceChild not supported" );
    }

    public final Node removeChild( final Node oldChild )
    {
        throw new UnsupportedOperationException( "ImmutableNode#removeChild not supported" );
    }

    public final Node appendChild( final Node newChild )
    {
        throw new UnsupportedOperationException( "ImmutableNode#appendChild not supported" );
    }

    /**
     * Not supported since Node is an immutable object, we don't need to clone id
     *
     * @param deep
     * @return nothing
     * @throws UnsupportedOperationException
     */
    public final Node cloneNode( final boolean deep )
    {
        throw new UnsupportedOperationException( "ImmutableNode#cloneNode not supported" );
    }

    public final void normalize()
    {
        throw new UnsupportedOperationException( "ImmutableNode#normalize not supported" );
    }

    public final boolean isSupported( final String feature, final String version )
    {
        return false;
    }

    public final void setPrefix( final String prefix )
    {
        throw new UnsupportedOperationException( "ImmutableNode#setPrefix not supported" );
    }

    public final String getBaseURI()
    {
        return null;
    }

    public final void setTextContent( final String textContent )
    {
        throw new UnsupportedOperationException( "ImmutableNode#setTextContent not supported" );
    }

    public final boolean isSameNode( final Node other )
    {
        return this == other;
    }

    public final Object getFeature( final String feature, final String version )
    {
        return null;
    }

    public final Object setUserData( final String key, final Object data, final UserDataHandler handler )
    {
        throw new UnsupportedOperationException( "ImmutableNode#setUserData not supported" );
    }

    public final Object getUserData( final String key )
    {
        return null;
    }

    @Override
    public final Document getOwnerDocument()
    {
        return null;
    }
}
