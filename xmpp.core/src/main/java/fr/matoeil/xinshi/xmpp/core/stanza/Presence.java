/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.core.internal.dom.Elements;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableArrayNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableBaseChildElement;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.dom.Nodes;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.XmppEnumMap;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaError;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.Priority;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.PriorityBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.Show;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.ShowBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.Status;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.StatusBuilder;
import org.w3c.dom.Element;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.collect.Lists.transform;

@Immutable
public final class Presence
    extends Stanza<Presence.Type>
{

    public static final String LOCAL_NAME = "presence";

    private static final Function<ShowBuilder, Show> BUILD_SHOW = Elements.build();

    private static final Function<StatusBuilder, Status> BUILD_STATUS = Elements.build();

    private static final Function<PriorityBuilder, Priority> BUILD_PRIORITY = Elements.build();

    private static final Function<ExtendedContentBuilder, ExtendedContent> BUILD_EXTENDED_CONTENT = Elements.build();

    private Show show_;

    private List<Status> statuses_;

    private Priority priority_;

    private List<ExtendedContent> extendedContents_;

    Presence( final String aNamespaceURI, @Nullable final String aLanguage, @Nullable final Address aFrom,
              @Nullable final Address aTo, @Nullable final String aId, @Nullable final Type aType,
              @Nullable final ShowBuilder aShowBuilder, List<StatusBuilder> aStatusBuilders,
              @Nullable final PriorityBuilder aPriorityBuilder, @Nullable final StanzaErrorBuilder aStanzaErrorBuilder,
              final List<ExtendedContentBuilder> aExtendedContentBuilders )
    {
        super( aNamespaceURI, LOCAL_NAME, aLanguage, aFrom, aTo, aId, aType );
        generateChildNodes( aNamespaceURI, aShowBuilder, aStatusBuilders, aPriorityBuilder, aStanzaErrorBuilder,
                            aExtendedContentBuilders );
    }


    @Nullable
    public Show getShow()
    {
        return show_;
    }

    public List<Status> getStatuses()
    {
        return statuses_;
    }

    @Nullable
    public Priority getPriority()
    {
        return priority_;
    }

    public List<? extends Element> getExtendedContents()
    {
        return extendedContents_;
    }

    private void generateChildNodes( final String aNamespaceURI, @Nullable final ShowBuilder aShowBuilder,
                                     final List<StatusBuilder> aStatusBuilders,
                                     @Nullable final PriorityBuilder aPriorityBuilder,
                                     @Nullable final StanzaErrorBuilder aStanzaErrorBuilder,
                                     final List<ExtendedContentBuilder> aExtendedContentBuilders )
    {
        int index = 0;

        index = initializeShow( aNamespaceURI, aShowBuilder, index );

        index = initializeStatuses( aNamespaceURI, aStatusBuilders, index );

        index = initializePriority( aNamespaceURI, aPriorityBuilder, index );

        index = initializeError( aNamespaceURI, aStanzaErrorBuilder, index );

        initializeExtendedContents( aExtendedContentBuilders, index );

        initializeChildNodes();
    }

    private int initializeShow( final String aNamespaceURI, final ShowBuilder aShowBuilder, final int aIndex )
    {
        int index = aIndex;
        if ( aShowBuilder != null )
        {
            aShowBuilder.isChildOf( this ).atIndex( index++ ).inNamespace( aNamespaceURI );
        }
        show_ = BUILD_SHOW.apply( aShowBuilder );
        return index;
    }

    private int initializeStatuses( final String aNamespaceURI, final List<StatusBuilder> aStatusBuilders,
                                    final int aIndex )
    {
        int index = aIndex;
        for ( StatusBuilder builder : aStatusBuilders )
        {
            builder.isChildOf( this ).atIndex( index++ ).inNamespace( aNamespaceURI );
        }
        statuses_ = new ImmutableList.Builder<Status>().addAll( transform( aStatusBuilders, BUILD_STATUS ) ).build();
        return index;
    }

    private int initializePriority( final String aNamespaceURI, final PriorityBuilder aPriorityBuilder,
                                    final int aIndex )
    {
        int index = aIndex;
        if ( aPriorityBuilder != null )
        {
            aPriorityBuilder.isChildOf( this ).atIndex( index++ ).inNamespace( aNamespaceURI );
        }
        priority_ = BUILD_PRIORITY.apply( aPriorityBuilder );
        return index;
    }


    private void initializeExtendedContents( final List<ExtendedContentBuilder> aExtendedContentBuilders,
                                             final int aIndex )
    {
        int index = aIndex;
        for ( ExtendedContentBuilder builder : aExtendedContentBuilders )
        {
            builder.isChildOf( this ).atIndex( index++ );
        }
        extendedContents_ = new ImmutableList.Builder<ExtendedContent>().addAll(
            transform( aExtendedContentBuilders, BUILD_EXTENDED_CONTENT ) ).build();
    }

    private void initializeChildNodes()
    {
        StanzaError aStanzaError = getStanzaError();
        ImmutableNodeList childNodes;
        int initialCapacity = ( show_ == null ? 0 : 1 ) + statuses_.size() + ( priority_ == null ? 0 : 1 ) + (
            aStanzaError == null
                ? 0
                : 1 );
        if ( initialCapacity == 0 )
        {
            childNodes = Nodes.EMPTY_CHILD_NODES;
        }
        else
        {
            List<ImmutableBaseChildElement> childElements = new ArrayList<>( initialCapacity );
            if ( show_ != null )
            {
                childElements.add( show_ );
            }
            childElements.addAll( statuses_ );
            if ( priority_ != null )
            {
                childElements.add( priority_ );
            }
            if ( aStanzaError != null )
            {
                childElements.add( aStanzaError );
            }
            childElements.addAll( extendedContents_ );
            childNodes = new ImmutableArrayNodeList( childElements );
        }
        setImmutableChildNodes( childNodes );
    }

    public static enum Type
        implements fr.matoeil.xinshi.xmpp.core.stanza.Type
    {
        ERROR( "error" ),
        PROBE( "probe" ),
        SUBSCRIBE( "subscribe" ),
        SUBSCRIBED( "subscribed" ),
        UNAVAILABLE( "unavailable" ),
        UNSUBSCRIBE( "unsubscribe" ),
        UNSUBSCRIBED( "unsubscribed" );

        private final String name_;

        Type( final String aName )
        {
            name_ = aName;
        }

        public String asString()
        {
            return name_;
        }

        public static Type fromString( String aType )
        {
            return XMPP_ENUM_MAP.fromString( aType );
        }

        private static final XmppEnumMap<Type> XMPP_ENUM_MAP = new XmppEnumMap<>( Type.class );
    }

}
