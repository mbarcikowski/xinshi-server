/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza;

import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.error.StanzaErrorHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.message.BodyHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.message.SubjectHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.message.ThreadHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp.XmppHandler;
import fr.matoeil.xinshi.xmpp.core.old.stream.StreamErrorConstants;
import fr.matoeil.xinshi.xmpp.core.stanza.Message;
import fr.matoeil.xinshi.xmpp.core.stanza.MessageBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaError;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.message.Body;
import fr.matoeil.xinshi.xmpp.core.stanza.message.BodyBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.message.Subject;
import fr.matoeil.xinshi.xmpp.core.stanza.message.SubjectBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.message.Thread;
import fr.matoeil.xinshi.xmpp.core.stanza.message.ThreadBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public final class MessageHandler
    extends StanzaHandler<Message.Type, Message, MessageBuilder>
{

    public MessageHandler( final String aNamespaceURI )
    {
        super( aNamespaceURI, new MessageBuilder() );
    }

    @Override
    protected XmppHandler getXmppHandler( final String aQualifiedName )
    {
        switch ( aQualifiedName )
        {
            case Body.LOCAL_NAME:
            {
                BodyHandler bodyHandler = new BodyHandler();
                BodyBuilder bodyBuilder = bodyHandler.getBuilder();
                getBuilder().withBody( bodyBuilder );
                return bodyHandler;
            }
            case Subject.LOCAL_NAME:
            {
                SubjectHandler subjectHandler = new SubjectHandler();
                SubjectBuilder subjectBuilder = subjectHandler.getBuilder();
                getBuilder().withSubject( subjectBuilder );
                return subjectHandler;
            }
            case Thread.LOCAL_NAME:
            {
                ThreadHandler threadHandler = new ThreadHandler();
                ThreadBuilder threadBuilder = threadHandler.getThreadBuilder();
                getBuilder().withThread( threadBuilder );
                return threadHandler;
            }
            case StanzaError.LOCAL_NAME:
            {
                StanzaErrorHandler stanzaErrorHandler = new StanzaErrorHandler();
                StanzaErrorBuilder stanzaErrorBuilder = stanzaErrorHandler.getStanzaErrorBuilder();
                getBuilder().withError( stanzaErrorBuilder );
                return stanzaErrorHandler;
            }
            default:
            {
                //TODO correct StreamException error
                throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
            }
        }
    }

    @Override
    protected Message.Type getType( final String aType )
    {
        return Message.Type.fromString( aType );
    }
}
