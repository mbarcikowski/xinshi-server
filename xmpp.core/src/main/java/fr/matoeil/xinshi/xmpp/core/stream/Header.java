/*
 * Copyright 2012 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream;

import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.core.version.Version;

import javax.annotation.Nullable;

public final class Header
{
    private final Version version_;

    private final Address from_;

    private final Address to_;

    private final String id_;

    private final String lang_;

    public Header( Version aVersion, @Nullable Address aFrom, @Nullable Address aTo, @Nullable String aId,
                   @Nullable String aLang )
    {
        version_ = aVersion;
        from_ = aFrom;
        to_ = aTo;
        id_ = aId;
        lang_ = aLang;
    }

    public Version version()
    {
        return version_;
    }

    @Nullable
    public Address from()
    {
        return from_;
    }

    @Nullable
    public Address to()
    {
        return to_;
    }

    @Nullable
    public String id()
    {
        return id_;
    }

    @Nullable
    public String lang()
    {
        return lang_;
    }
}
