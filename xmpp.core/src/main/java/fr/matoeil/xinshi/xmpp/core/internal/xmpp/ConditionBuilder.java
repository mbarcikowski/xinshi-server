/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.xmpp;


import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableBaseChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public abstract class ConditionBuilder<C extends DefinedCondition, T extends Condition<C>, B extends ConditionBuilder<C, T, B>>
    extends ImmutableBaseChildElementBuilder<T, B>
{
    private static final String MUST_NOT_BE_NULL_ERROR_MESSAGE = "%s must not be null";

    private C definedCondition_;

    private String content_;

    public final B asDefinedCondition( C aDefinedCondition )
    {
        checkArgument( aDefinedCondition != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "definedCondition" );
        definedCondition_ = aDefinedCondition;
        return (B) this;
    }

    public final B withContent( String aContent )
    {
        content_ = aContent;
        return (B) this;
    }

    @Override
    protected final T newImmutableChildElement( final ImmutableNode aParentNode, final Integer aElementIndex,
                                                final String aNamespaceURI )
    {
        checkState( definedCondition_ != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "definedCondition" );
        return newCondition( aParentNode, aElementIndex, definedCondition_, content_ );
    }

    protected abstract T newCondition( final ImmutableNode aParentNode, final Integer aElementIndex,
                                       final C aDefinedCondition, final String aContent );

}
