/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.presence;

import fr.matoeil.xinshi.xmpp.core.old.stream.StreamErrorConstants;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.Priority;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.PriorityBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;

import static java.lang.String.valueOf;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public final class PriorityHandler
    extends PresenceElementHandler<Priority, PriorityBuilder>
{

    public PriorityHandler()
    {
        super( new PriorityBuilder() );
    }


    @Override
    public void characters( final char[] aChars, final int aStart, final int aLength )
    {
        try
        {
            getBuilder().withValue( Byte.parseByte( valueOf( aChars, aStart, aLength ) ) );
        }
        catch ( IllegalArgumentException e )
        {
            //TODO correct StreamException error
            throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT, e );
        }
    }

}
