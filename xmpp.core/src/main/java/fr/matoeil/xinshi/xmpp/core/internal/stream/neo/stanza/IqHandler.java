/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza;

import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.error.StanzaErrorHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp.XmppHandler;
import fr.matoeil.xinshi.xmpp.core.old.stream.StreamErrorConstants;
import fr.matoeil.xinshi.xmpp.core.stanza.Iq;
import fr.matoeil.xinshi.xmpp.core.stanza.IqBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaError;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public final class IqHandler
    extends StanzaHandler<Iq.Type, Iq, IqBuilder>
{

    public IqHandler( final String aNamespaceURI )
    {
        super( aNamespaceURI, new IqBuilder() );
    }


    @Override
    protected XmppHandler getXmppHandler( final String aQualifiedName )
    {
        switch ( aQualifiedName )
        {
            case StanzaError.LOCAL_NAME:
            {
                StanzaErrorHandler stanzaErrorHandler = new StanzaErrorHandler();
                StanzaErrorBuilder stanzaErrorBuilder = stanzaErrorHandler.getStanzaErrorBuilder();
                getBuilder().withError( stanzaErrorBuilder );
                return stanzaErrorHandler;
            }
            default:
            {
                //TODO correct StreamException error
                throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
            }
        }
    }

    @Override
    protected Iq.Type getType( final String aType )
    {
        return Iq.Type.fromString( aType );
    }
}
