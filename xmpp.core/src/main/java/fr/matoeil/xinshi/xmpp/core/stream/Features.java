/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream;

import com.google.common.collect.ImmutableList;
import fr.matoeil.xinshi.xmpp.core.internal.dom.Elements;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableArrayNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableOneNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.dom.Nodes;
import fr.matoeil.xinshi.xmpp.core.stream.feature.Feature;
import fr.matoeil.xinshi.xmpp.core.stream.feature.FeatureBuilder;
import org.w3c.dom.Element;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.List;

/**
 * @author mathieu.barcikowski@gmail.com
 */
@Immutable
public final class Features
    extends StreamElement
    implements Element
{

    public static final String LOCAL_NAME = "features";

    private List<Feature> features_;

    Features( @Nullable final String aPrefix, final List<FeatureBuilder> aFeatureBuilders )
    {
        super( Namespaces.STREAM_NAMESPACE_URI, aPrefix, LOCAL_NAME );
        generateAttributes();
        generateChildNodes( aFeatureBuilders );
    }

    public List<Feature> getFeatures()
    {
        return features_;
    }

    private void generateAttributes()
    {
        setImmutableAttributes( Elements.EMPTY_ATTRIBUTES );
    }

    private void generateChildNodes( final List<FeatureBuilder> aFeatureBuilders )
    {
        int index = 0;
        ImmutableNodeList immutableNodeList;
        int size = aFeatureBuilders.size();
        switch ( size )
        {
            case 0:
            {
                immutableNodeList = Nodes.EMPTY_CHILD_NODES;
                features_ = ImmutableList.of();
                break;
            }
            case 1:
            {
                final Feature feature = aFeatureBuilders.get( 0 ).isChildOf( this ).atIndex( index ).build();
                features_ = ImmutableList.of( feature );
                immutableNodeList = new ImmutableOneNodeList( feature );
                break;
            }
            default:
            {
                ImmutableList.Builder<Feature> featuresBuilder = new ImmutableList.Builder<>();
                index = 0;
                for ( FeatureBuilder builder : aFeatureBuilders )
                {
                    featuresBuilder.add( builder.isChildOf( this ).atIndex( index++ ).build() );
                }
                features_ = featuresBuilder.build();
                immutableNodeList = new ImmutableArrayNodeList( features_ );
            }
        }

        setImmutableChildNodes( immutableNodeList );
    }

}
