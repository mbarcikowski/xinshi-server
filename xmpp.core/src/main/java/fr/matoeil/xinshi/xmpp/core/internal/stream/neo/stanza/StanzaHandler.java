/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza;

import com.google.common.base.CharMatcher;
import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.address.Addresses;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.StanzaBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp.XmppHandler;
import fr.matoeil.xinshi.xmpp.core.old.stream.StreamErrorConstants;
import fr.matoeil.xinshi.xmpp.core.stanza.ExtendedContentBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.Namespaces;
import fr.matoeil.xinshi.xmpp.core.stanza.Stanza;
import fr.matoeil.xinshi.xmpp.core.stanza.Type;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import org.xml.sax.Attributes;

import static fr.matoeil.xinshi.xmpp.core.internal.stream.neo.Xmls.XML_LANG_ATTRIBUTE;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public abstract class StanzaHandler<T extends Type, S extends Stanza<T>, B extends StanzaBuilder<T, S, B>>
    extends XmppHandler
{

    private final String namespaceURI_;

    private final B builder_;

    protected StanzaHandler( final String aNamespaceURI, final B aBuilder )
    {
        namespaceURI_ = aNamespaceURI;
        builder_ = aBuilder.inNamespace( aNamespaceURI );
    }

    public String getNamespaceURI()
    {
        return namespaceURI_;
    }

    public final B getBuilder()
    {
        return builder_;
    }

    public final S build()
    {
        return builder_.build();
    }

    @Override
    public final XmppHandler handleElementOpening( final String aNamespaceURI, final String aLocalName,
                                                   final String aQualifiedName )
    {
        if ( aNamespaceURI == null )
        {
            //TODO correct StreamException error
            throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
        }
        else if ( getNamespaceURI().equals( aNamespaceURI ) )
        {
            return getXmppHandler( aQualifiedName );
        }
        else
        {
            ExtendedContentHandler extendedContentHandler = new ExtendedContentHandler();
            ExtendedContentBuilder extendedContentBuilder = extendedContentHandler.getBuilder();
            getBuilder().withExtendedContent( extendedContentBuilder );
            return extendedContentHandler;
        }
    }

    @Override
    public final void startPrefixMapping( final String aPrefix, final String aNamespaceURI )
    {
        //TODO other namespace (server,....)
        if ( Namespaces.CLIENT_NAMESPACE_URI.equals( aNamespaceURI ) && aPrefix != null )
        {
            //TODO correct StreamException error
            throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
        }
    }

    @Override
    public final void startElement( final String aNamespaceURI, final String aLocalName, final String aQualifiedName,
                                    final Attributes aAttributes )
    {
        final StanzaBuilder aStanzaBuilder = getBuilder();

        for ( int index = 0, length = aAttributes.getLength(); index < length; index++ )
        {
            switch ( aAttributes.getQName( index ) )
            {
                case Stanza.FROM_ATTRIBUTE:
                {
                    try
                    {
                        Address fromAddress = Addresses.fromString( aAttributes.getValue( index ) );
                        aStanzaBuilder.from( fromAddress );
                    }
                    catch ( IllegalArgumentException e )
                    {
                        //TODO correct StreamException error
                        throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
                    }
                    break;
                }
                case Stanza.TO_ATTRIBUTE:
                {
                    try
                    {
                        Address toAddress = Addresses.fromString( aAttributes.getValue( index ) );
                        aStanzaBuilder.to( toAddress );
                    }
                    catch ( IllegalArgumentException e )
                    {
                        //TODO correct StreamException error
                        throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
                    }
                    break;
                }
                case Stanza.ID_ATTRIBUTE:
                {
                    aStanzaBuilder.identifiedBy( aAttributes.getValue( index ) );
                    break;
                }
                case Stanza.TYPE_ATTRIBUTE:
                {
                    //TODO try catch
                    try
                    {
                        T type = getType( aAttributes.getValue( index ) );
                        aStanzaBuilder.ofType( type );
                    }
                    catch ( IllegalArgumentException e )
                    {
                        //TODO correct StreamException error
                        throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
                    }

                    break;
                }
                case XML_LANG_ATTRIBUTE:
                {
                    aStanzaBuilder.inLanguage( aAttributes.getValue( index ) );
                    break;
                }
                default:
                {
                    //TODO if xmlns='{content namespace}'
                    //TODO correct StreamException error
                    throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
                }
            }
        }
    }

    @Override
    public final void startCDATA()
    {
        //TODO correct StreamException error
        throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
    }

    @Override
    public final void characters( final char[] aChars, final int aStart, final int aLength )
    {
        if ( !CharMatcher.WHITESPACE.matchesAllOf( String.valueOf( aChars, aStart, aLength ) ) )
        {
            //TODO correct StreamException error
            throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
        }
    }


    @Override
    public final void endCDATA()
    {
        //TODO correct StreamException error
        throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
    }

    protected abstract XmppHandler getXmppHandler( String aQualifiedName );


    protected abstract T getType( final String aType );
}
