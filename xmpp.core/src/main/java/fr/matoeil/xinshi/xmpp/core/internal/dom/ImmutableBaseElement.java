/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.dom;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.TypeInfo;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

@Immutable
public abstract class ImmutableBaseElement
    extends ImmutableNodeTrait
    implements Element
{
    private final String localName_;

    private final String namespaceURI_;

    private final String prefix_;

    private ImmutableAttributeMap attributes_;

    private ImmutableNodeList childNodes_;

    public ImmutableBaseElement( @Nullable final String aNamespaceURI, @Nullable final String aPrefix,
                                 final String aLocalName )
    {
        namespaceURI_ = aNamespaceURI;
        prefix_ = aPrefix;
        localName_ = aLocalName;
    }

    protected final ImmutableAttributeMap getImmutableAttributes()
    {
        return attributes_;
    }

    protected final void setImmutableAttributes( ImmutableAttributeMap aImmutableAttributes )
    {
        attributes_ = aImmutableAttributes;
    }

    protected final ImmutableNodeList getImmutableChildNodes()
    {
        return childNodes_;
    }

    protected final void setImmutableChildNodes( ImmutableNodeList aImmutableChildNodes )
    {
        childNodes_ = aImmutableChildNodes;
    }

    @Override
    public final void setAttribute( final String name, final String value )
    {
        throw new UnsupportedOperationException( "ImmutableBaseElement#setAttribute not supported" );
    }

    @Override
    public final void removeAttribute( final String name )
    {
        throw new UnsupportedOperationException( "ImmutableBaseElement#removeAttribute not supported" );
    }

    @Override
    public final Attr setAttributeNode( final Attr newAttr )
    {
        throw new UnsupportedOperationException( "ImmutableBaseElement#setAttributeNode not supported" );
    }

    @Override
    public final Attr removeAttributeNode( final Attr oldAttr )
    {
        throw new UnsupportedOperationException( "ImmutableBaseElement#removeAttributeNode not supported" );
    }


    @Override
    public final void setAttributeNS( final String namespaceURI, final String qualifiedName, final String value )
    {
        throw new UnsupportedOperationException( "ImmutableBaseElement#setAttributeNS not implemented" );
    }

    @Override
    public final void removeAttributeNS( final String namespaceURI, final String localName )
    {
        throw new UnsupportedOperationException( "ImmutableBaseElement#removeAttributeNS not supported" );
    }

    @Override
    public final Attr setAttributeNodeNS( final Attr newAttr )
    {
        throw new UnsupportedOperationException( "ImmutableBaseElement#setAttributeNodeNS not supported" );
    }

    @Override
    public final TypeInfo getSchemaTypeInfo()
    {
        throw new UnsupportedOperationException( "ImmutableBaseElement#setIdAttribute not supported" );
    }

    @Override
    public final void setIdAttribute( final String name, final boolean isId )
    {
        throw new UnsupportedOperationException( "ImmutableBaseElement#setIdAttribute not supported" );
    }

    @Override
    public final void setIdAttributeNS( final String namespaceURI, final String localName, final boolean isId )
    {
        throw new UnsupportedOperationException( "ImmutableBaseElement#setIdAttributeNS not supported" );
    }

    @Override
    public final void setIdAttributeNode( final Attr idAttr, final boolean isId )
    {
        throw new UnsupportedOperationException( "ImmutableBaseElement#setIdAttributeNode not supported" );
    }

    @Override
    public final void setNodeValue( final String nodeValue )
    {
        throw new UnsupportedOperationException( "ImmutableBaseElement#setNodeValue not supported" );
    }

    @Override
    public final String getNodeValue()
    {
        return null;
    }

    @Override
    public final short getNodeType()
    {
        return Node.ELEMENT_NODE;
    }

    @Override
    public final String getTagName()
    {
        return Nodes.getQualifiedName( prefix_, localName_ );
    }

    @Override
    public final String getAttribute( final String aName )
    {
        Attr attribute = attributes_.getNamedItem( aName );
        if ( attribute == null )
        {
            return Attributes.ATTRIBUTE_NOT_DEFINED_VALUE;
        }
        else
        {
            return attribute.getNodeValue();
        }
    }

    @Override
    public final Attr getAttributeNode( final String aName )
    {
        return attributes_.getNamedItem( aName );
    }

    @Override
    public final NodeList getElementsByTagName( final String name )
    {
        //TODO implements Body#getElementsByTagName
        throw new UnsupportedOperationException( "Body#getElementsByTagName not implemented" );
    }

    @Override
    public final String getAttributeNS( final String aNamespaceURI, final String aLocalName )
    {
        Attr attribute = attributes_.getNamedItemNS( aNamespaceURI, aLocalName );
        if ( attribute == null )
        {
            return Attributes.ATTRIBUTE_NOT_DEFINED_VALUE;
        }
        else
        {
            return attribute.getNodeValue();
        }
    }

    @Override
    public final Attr getAttributeNodeNS( final String aNamespaceURI, final String aLocalName )
    {
        return attributes_.getNamedItemNS( aNamespaceURI, aLocalName );
    }

    @Override
    public final NodeList getElementsByTagNameNS( final String namespaceURI, final String localName )
    {
        //TODO implements Body#getElementsByTagNameNS
        throw new UnsupportedOperationException( "Body#getElementsByTagNameNS not implemented" );
    }

    @Override
    public final boolean hasAttribute( final String aName )
    {
        return getAttributeNode( aName ) != null;
    }

    @Override
    public final boolean hasAttributeNS( final String aNamespaceURI, final String aLocalName )
    {
        return getAttributeNodeNS( aNamespaceURI, aLocalName ) != null;
    }

    @Override
    public final String getNodeName()
    {
        return Nodes.getQualifiedName( prefix_, localName_ );
    }

    @Override
    public final NodeList getChildNodes()
    {
        return childNodes_;
    }

    @Nullable
    @Override
    public final Node getFirstChild()
    {
        return childNodes_.item( 0 );
    }

    @Nullable
    @Override
    public final Node getLastChild()
    {
        return childNodes_.item( childNodes_.getLength() - 1 );
    }

    @Override
    public final NamedNodeMap getAttributes()
    {
        return attributes_;
    }

    @Override
    public final boolean hasChildNodes()
    {
        return childNodes_.getLength() > 0;
    }

    @Nullable
    @Override
    public final String getNamespaceURI()
    {
        return namespaceURI_;
    }

    @Nullable
    @Override
    public final String getPrefix()
    {
        return prefix_;
    }

    @Override
    public final String getLocalName()
    {
        return localName_;
    }

    @Override
    public final boolean hasAttributes()
    {
        return attributes_.getLength() > 0;
    }

    @Override
    public final short compareDocumentPosition( final Node other )
    {
        //TODO implements Body#compareDocumentPosition
        throw new UnsupportedOperationException( "Body#compareDocumentPosition not implemented" );
    }

    @Override
    public final String lookupPrefix( final String namespaceURI )
    {
        //TODO implements Body#lookupPrefix
        throw new UnsupportedOperationException( "Body#lookupPrefix not implemented" );
    }

    @Override
    public final boolean isDefaultNamespace( final String aNamespaceURI )
    {
        //TODO implements Body#isDefaultNamespace
        throw new UnsupportedOperationException( "Body#isDefaultNamespace not implemented" );
    }

    @Override
    public final String lookupNamespaceURI( final String aPrefix )
    {
        //TODO implements Body#lookupNamespaceURI
        throw new UnsupportedOperationException( "Body#lookupNamespaceURI not implemented" );
    }

    @Override
    public final boolean isEqualNode( final Node arg )
    {
        //TODO implements Body#isEqualNode
        throw new UnsupportedOperationException( "Body#isEqualNode not implemented" );
    }


}
