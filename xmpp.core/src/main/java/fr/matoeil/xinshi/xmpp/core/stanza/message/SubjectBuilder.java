/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza.message;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.ImmutableLocalizedContentLeafBuilder;

import javax.annotation.Nullable;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public final class SubjectBuilder
    extends ImmutableLocalizedContentLeafBuilder<Subject, SubjectBuilder>
{

    @Override
    protected Subject newImmutableLocalizedContentLeaf( ImmutableNode aParentNode, Integer aElementIndex,
                                                        final String aNamespaceURI, @Nullable final String aLanguage,
                                                        @Nullable final String aContent )
    {
        return new Subject( aParentNode, aElementIndex, aNamespaceURI, aLanguage, aContent );
    }
}
