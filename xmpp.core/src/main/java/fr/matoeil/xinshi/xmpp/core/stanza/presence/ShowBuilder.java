/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza.presence;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableBaseChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public final class ShowBuilder
    extends ImmutableBaseChildElementBuilder<Show, ShowBuilder>
{
    private static final String MUST_NOT_BE_NULL_ERROR_MESSAGE = "%s must not be null";

    private Show.State state_;

    public ShowBuilder withState( Show.State aState )
    {
        checkArgument( aState != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "state" );
        state_ = aState;
        return this;
    }

    @Override
    protected Show newImmutableChildElement( ImmutableNode aParentNode, Integer aElementIndex, String aNamespaceURI )
    {
        checkState( state_ != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "state" );
        return new Show( aParentNode, aElementIndex, aNamespaceURI, state_ );
    }


}
