/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza.error;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.TextBuilder;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public final class StanzaErrorTextBuilder
    extends TextBuilder<StanzaErrorText, StanzaErrorTextBuilder>
{

    @Override
    protected StanzaErrorText newText( final ImmutableNode aParentNode, final Integer aElementIndex,
                                       final String aLanguage, final String aContent )
    {
        return new StanzaErrorText( aParentNode, aElementIndex, aLanguage, aContent );
    }

}
