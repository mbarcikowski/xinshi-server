/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.dom;

import javax.annotation.Nullable;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public abstract class ImmutableBaseElementBuilder<T extends ImmutableBaseElement, B extends ImmutableBaseElementBuilder<T, B>>
    implements ImmutableNodeBuilder<T>
{

    private String namespaceURI_;


    public final B inNamespace( String aNamespaceURI )
    {
        namespaceURI_ = aNamespaceURI;
        return (B) this;
    }

    @Override
    public final T build()
    {
        return newImmutableElement( namespaceURI_ );
    }

    protected abstract T newImmutableElement( @Nullable String aNamespaceURI );
}
