/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.presence;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableBaseChildElement;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableBaseChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp.XmppHandler;
import fr.matoeil.xinshi.xmpp.core.old.stream.StreamErrorConstants;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import org.xml.sax.Attributes;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public abstract class PresenceElementHandler<C extends ImmutableBaseChildElement, B extends ImmutableBaseChildElementBuilder<C, B>>
    extends XmppHandler
{
    private final B builder_;

    public PresenceElementHandler( final B aBuilder )
    {
        builder_ = aBuilder;
    }

    public final B getBuilder()
    {
        return builder_;
    }

    @Override
    public final XmppHandler handleElementOpening( final String aNamespaceURI, final String aLocalName,
                                                   final String aQualifiedName )
    {
        //TODO correct StreamException error
        throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
    }

    @Override
    public final void startPrefixMapping( final String aPrefix, final String aNamespaceURI )
    {
        //TODO correct StreamException error
        throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
    }

    @Override
    public final void startElement( final String aNamespaceURI, final String aLocalName, final String aQualifiedName,
                                    final Attributes aAttributes )
    {
        if ( aAttributes.getLength() != 0 )
        {
            //TODO correct StreamException error
            throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
        }
    }

    @Override
    public final void startCDATA()
    {
        //TODO correct StreamException error
        throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
    }

    @Override
    public final void endCDATA()
    {
        //TODO correct StreamException error
        throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
    }
}
