/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream.error;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.ConditionBuilder;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public final class StreamErrorConditionBuilder
    extends ConditionBuilder<StreamErrorCondition.DefinedCondition, StreamErrorCondition, StreamErrorConditionBuilder>
{

    @Override
    protected StreamErrorCondition newCondition( final ImmutableNode aParentNode, final Integer aElementIndex,
                                                 final StreamErrorCondition.DefinedCondition aDefinedCondition,
                                                 final String aContent )
    {
        return new StreamErrorCondition( aParentNode, aElementIndex, aDefinedCondition, aContent );
    }

}
