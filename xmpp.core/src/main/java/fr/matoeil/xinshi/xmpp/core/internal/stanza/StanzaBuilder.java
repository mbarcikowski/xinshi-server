/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stanza;

import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.core.stanza.ExtendedContentBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.Stanza;
import fr.matoeil.xinshi.xmpp.core.stanza.Type;

import javax.annotation.Nullable;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public abstract class StanzaBuilder<T extends Type, S extends Stanza<T>, B extends StanzaBuilder<T, S, B>>
{

    private static final String MUST_NOT_BE_NULL_ERROR_MESSAGE = "%s must not be null";

    private String namespaceURI_;

    private Address from_;

    private Address to_;

    private String id_;

    private T type_;

    private String language_;

    public final B inNamespace( final String aNamespaceURI )
    {
        checkArgument( aNamespaceURI != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "namespaceURI" );
        namespaceURI_ = aNamespaceURI;
        return (B) this;
    }

    public final B from( @Nullable Address aFrom )
    {
        from_ = aFrom;
        return (B) this;
    }

    public final B to( @Nullable final Address aTo )
    {
        to_ = aTo;
        return (B) this;
    }

    public final B identifiedBy( @Nullable final String aId )
    {
        id_ = aId;
        return (B) this;
    }

    public final B ofType( @Nullable final T aType )
    {
        type_ = aType;
        return (B) this;
    }

    public B inLanguage( @Nullable final String aLanguage )
    {
        language_ = aLanguage;
        return (B) this;
    }

    public final S build()
    {
        checkState( namespaceURI_ != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "namespaceURI" );
        return newStanza( namespaceURI_, from_, to_, id_, type_, language_ );
    }

    public abstract B withExtendedContent( final ExtendedContentBuilder aExtendedContentBuilder );


    protected abstract S newStanza( final String aNamespaceURI, @Nullable final Address aFrom,
                                    @Nullable final Address aTo, @Nullable final String aId, @Nullable final T aType,
                                    @Nullable final String aLanguage );
}
