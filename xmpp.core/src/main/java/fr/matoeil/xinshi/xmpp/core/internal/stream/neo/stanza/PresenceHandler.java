/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza;

import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.error.StanzaErrorHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.presence.PriorityHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.presence.ShowHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.presence.StatusHandler;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp.XmppHandler;
import fr.matoeil.xinshi.xmpp.core.old.stream.StreamErrorConstants;
import fr.matoeil.xinshi.xmpp.core.stanza.Presence;
import fr.matoeil.xinshi.xmpp.core.stanza.PresenceBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaError;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.Priority;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.PriorityBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.Show;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.ShowBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.Status;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.StatusBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public final class PresenceHandler
    extends StanzaHandler<Presence.Type, Presence, PresenceBuilder>
{


    public PresenceHandler( final String aNamespaceURI )
    {
        super( aNamespaceURI, new PresenceBuilder() );

    }

    @Override
    protected XmppHandler getXmppHandler( final String aQualifiedName )
    {
        switch ( aQualifiedName )
        {

            case Show.LOCAL_NAME:
            {
                ShowHandler showHandler = new ShowHandler();
                ShowBuilder showBuilder = showHandler.getBuilder();
                getBuilder().withShow( showBuilder );
                return showHandler;
            }
            case Status.LOCAL_NAME:
            {
                StatusHandler statusHandler = new StatusHandler();
                StatusBuilder statusBuilder = statusHandler.getBuilder();
                getBuilder().withStatus( statusBuilder );
                return statusHandler;
            }
            case Priority.LOCAL_NAME:
            {
                PriorityHandler priorityHandler = new PriorityHandler();
                PriorityBuilder priorityBuilder = priorityHandler.getBuilder();
                getBuilder().withPriority( priorityBuilder );
                return priorityHandler;
            }
            case StanzaError.LOCAL_NAME:
            {
                StanzaErrorHandler stanzaErrorHandler = new StanzaErrorHandler();
                StanzaErrorBuilder stanzaErrorBuilder = stanzaErrorHandler.getStanzaErrorBuilder();
                getBuilder().withError( stanzaErrorBuilder );
                return stanzaErrorHandler;
            }
            default:
            {
                //TODO correct StreamException error
                throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
            }
        }
    }

    @Override
    protected Presence.Type getType( final String aType )
    {
        return Presence.Type.fromString( aType );
    }
}
