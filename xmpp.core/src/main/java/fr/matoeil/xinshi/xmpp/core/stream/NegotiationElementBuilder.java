/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream;

import fr.matoeil.xinshi.xmpp.core.dom.ImmutableAttributeBuilder;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableCDATASectionBuilder;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableTextBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableChildNodeBuilder;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public final class NegotiationElementBuilder
    extends StreamElementBuilder<NegotiationElement, NegotiationElementBuilder>
{
    private static final String MUST_NOT_BE_NULL_ERROR_MESSAGE = "%s must not be null";

    private String namespaceURI_;

    private String localName_;

    private final List<ImmutableAttributeBuilder> attributeBuilders_ = new ArrayList<>();

    private final List<ImmutableChildNodeBuilder> childNodeBuilders_ = new ArrayList<>();


    public NegotiationElementBuilder inNamespace( String aNamespaceURI )
    {
        checkArgument( aNamespaceURI != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "namespaceURI" );
        namespaceURI_ = aNamespaceURI;
        return this;
    }


    public NegotiationElementBuilder named( String aLocalName )
    {
        checkArgument( aLocalName != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "localName" );
        localName_ = aLocalName;
        return this;
    }

    public NegotiationElementBuilder withAttributes( List<ImmutableAttributeBuilder> aAttributeBuilders )
    {
        checkArgument( aAttributeBuilders != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "attributeBuilders" );
        attributeBuilders_.addAll( aAttributeBuilders );
        return this;
    }

    public NegotiationElementBuilder withAttributes( ImmutableAttributeBuilder... aAttributeBuilders )
    {
        checkArgument( aAttributeBuilders != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "attributeBuilders" );
        Collections.addAll( attributeBuilders_, aAttributeBuilders );
        return this;
    }

    public NegotiationElementBuilder withAttribute( ImmutableAttributeBuilder aAttributeBuilder )
    {
        checkArgument( aAttributeBuilder != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "attributeBuilder" );
        attributeBuilders_.add( aAttributeBuilder );
        return this;
    }

    public NegotiationElementBuilder withChildElements( List<ImmutableChildElementBuilder> aChildElementBuilders )
    {
        checkArgument( aChildElementBuilders != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "childElementBuilders" );
        childNodeBuilders_.addAll( aChildElementBuilders );
        return this;
    }

    public NegotiationElementBuilder withChildElements( final ImmutableChildElementBuilder... aChildElementBuilders )
    {
        checkArgument( aChildElementBuilders != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "childElementBuilders" );
        Collections.addAll( childNodeBuilders_, aChildElementBuilders );
        return this;
    }

    public NegotiationElementBuilder withChildElement( final ImmutableChildElementBuilder aChildElementBuilder )
    {
        checkArgument( aChildElementBuilder != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "childElementBuilder" );
        childNodeBuilders_.add( aChildElementBuilder );
        return this;
    }

    public NegotiationElementBuilder withText( final ImmutableTextBuilder aTextBuilder )
    {
        checkArgument( aTextBuilder != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "textBuilder" );
        childNodeBuilders_.add( aTextBuilder );
        return this;
    }

    public NegotiationElementBuilder withCDATASection( final ImmutableCDATASectionBuilder aCDATASectionBuilder )
    {
        checkArgument( aCDATASectionBuilder != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "CDATASectionBuilder" );
        childNodeBuilders_.add( aCDATASectionBuilder );
        return this;
    }

    @Override
    protected NegotiationElement newStreamElement( @Nullable final String aPrefix )
    {
        checkState( namespaceURI_ != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "namespaceURI" );
        checkState( localName_ != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "localName" );
        return new NegotiationElement( namespaceURI_, aPrefix, localName_, attributeBuilders_, childNodeBuilders_ );
    }
}
