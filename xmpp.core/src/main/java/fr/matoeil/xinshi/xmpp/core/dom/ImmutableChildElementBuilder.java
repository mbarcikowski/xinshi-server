/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.dom;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableBaseChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableChildElement;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableChildNodeBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public final class ImmutableChildElementBuilder
    extends ImmutableBaseChildElementBuilder<ImmutableChildElement, ImmutableChildElementBuilder>
{

    private static final String MUST_NOT_BE_NULL_ERROR_MESSAGE = "%s must not be null";

    private String localName_;

    private final List<ImmutableAttributeBuilder> attributeBuilders_ = new ArrayList<>();

    private final List<ImmutableChildNodeBuilder> childNodeBuilders_ = new ArrayList<>();

    @Nullable
    private String prefix_;

    public ImmutableChildElementBuilder withPrefix( @Nullable String aPrefix )
    {
        prefix_ = aPrefix;
        return this;
    }

    public ImmutableChildElementBuilder named( String aLocalName )
    {
        checkArgument( aLocalName != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "localName" );
        localName_ = aLocalName;
        return this;
    }

    public ImmutableChildElementBuilder withAttributes( List<ImmutableAttributeBuilder> aAttributeBuilders )
    {
        checkArgument( aAttributeBuilders != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "attributeBuilders" );
        attributeBuilders_.addAll( aAttributeBuilders );
        return this;
    }

    public ImmutableChildElementBuilder withAttributes( final ImmutableAttributeBuilder... aAttributeBuilders )
    {
        checkArgument( aAttributeBuilders != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "attributeBuilders" );
        Collections.addAll( attributeBuilders_, aAttributeBuilders );
        return this;
    }

    public ImmutableChildElementBuilder withAttribute( final ImmutableAttributeBuilder aAttributeBuilder )
    {
        checkArgument( aAttributeBuilder != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "attributeBuilder" );
        attributeBuilders_.add( aAttributeBuilder );
        return this;
    }

    public ImmutableChildElementBuilder withChildElements( List<ImmutableChildElementBuilder> aChildElementBuilders )
    {
        checkArgument( aChildElementBuilders != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "childElementBuilders" );
        childNodeBuilders_.addAll( aChildElementBuilders );
        return this;
    }

    public ImmutableChildElementBuilder withChildElements( final ImmutableChildElementBuilder... aChildElementBuilders )
    {
        checkArgument( aChildElementBuilders != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "childElementBuilders" );
        Collections.addAll( childNodeBuilders_, aChildElementBuilders );
        return this;
    }

    public ImmutableChildElementBuilder withChildElement( final ImmutableChildElementBuilder aChildElementBuilder )
    {
        checkArgument( aChildElementBuilder != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "childElementBuilder" );
        childNodeBuilders_.add( aChildElementBuilder );
        return this;
    }

    public ImmutableChildElementBuilder withText( final ImmutableTextBuilder aTextBuilder )
    {
        checkArgument( aTextBuilder != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "textBuilder" );
        childNodeBuilders_.add( aTextBuilder );
        return this;
    }

    public ImmutableChildElementBuilder withCDATASection( final ImmutableCDATASectionBuilder aCDATASectionBuilder )
    {
        checkArgument( aCDATASectionBuilder != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "CDATASectionBuilder" );
        childNodeBuilders_.add( aCDATASectionBuilder );
        return this;
    }

    @Override
    protected ImmutableChildElement newImmutableChildElement( final ImmutableNode aParentNode,
                                                              final Integer aElementIndex, final String aNamespaceURI )
    {
        checkState( localName_ != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "localName" );
        return new ImmutableChildElement( aParentNode, aElementIndex, aNamespaceURI, prefix_, localName_,
                                          attributeBuilders_, childNodeBuilders_ );
    }
}
