/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream.feature.bind;

import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.dom.ImmutableTextBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.ExtendedContentBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.feature.FeatureBuilder;

import static fr.matoeil.xinshi.xmpp.core.stream.feature.bind.Namespaces.BIND_NAMESPACE_URI;

/**
 * @author mathieu.barcikowski@gmail.com
 * @see <a href="http://xmpp.org/rfcs/rfc6120.html#bind">http://xmpp.org/rfcs/rfc6120.html#bind</a>
 */
public final class BindElements
{
    private static final String BIND_LOCAL_NAME = "bind";

    private static final String JID_LOCAL_NAME = "jid";

    public static ExtendedContentBuilder createBindResultExtendedContentBuilder( Address aAddress )
    {
        return new ExtendedContentBuilder().inNamespace( BIND_NAMESPACE_URI ).named( BIND_LOCAL_NAME ).withChildElement(
            new ImmutableChildElementBuilder().inNamespace( BIND_NAMESPACE_URI ).named( JID_LOCAL_NAME ).withText(
                new ImmutableTextBuilder().withData( aAddress.fullAddress() ) ) );

    }

    public static FeatureBuilder createBindFeatureBuilder()
    {
        return new FeatureBuilder().inNamespace( BIND_NAMESPACE_URI ).named( BIND_LOCAL_NAME );
    }


    private BindElements()
    {
    }
}
