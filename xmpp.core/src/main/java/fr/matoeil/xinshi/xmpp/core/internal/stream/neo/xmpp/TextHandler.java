/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp;

import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.Xmls;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.Text;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.TextBuilder;
import fr.matoeil.xinshi.xmpp.core.old.stream.StreamErrorConstants;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import org.xml.sax.Attributes;

import static java.lang.String.valueOf;

public abstract class TextHandler<T extends Text, B extends TextBuilder<T, B>>
    extends XmppHandler
{
    private final B textBuilder_;

    private final String namespaceUri_;

    protected TextHandler( B aTextBuilder, final String aNamespaceUri )
    {
        textBuilder_ = aTextBuilder;
        namespaceUri_ = aNamespaceUri;
    }

    public final B getTextBuilder()
    {
        return textBuilder_;
    }

    @Override
    public final XmppHandler handleElementOpening( final String aNamespaceURI, final String aLocalName,
                                                   final String aQualifiedName )
    {
        //TODO correct StreamException error
        throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
    }

    @Override
    public final void startPrefixMapping( final String aPrefix, final String aNamespaceURI )
    {
        //TODO TextHandler#startPrefixMapping not implemented
        throw new UnsupportedOperationException( "TextHandler#startPrefixMapping not implemented" );
    }

    @Override
    public final void startElement( final String aNamespaceURI, final String aLocalName, final String aQualifiedName,
                                    final Attributes aAttributes )
    {
        for ( int index = 0, length = aAttributes.getLength(); index < length; index++ )
        {
            switch ( aAttributes.getQName( index ) )
            {
                case Xmls.XML_LANG_ATTRIBUTE:
                {
                    textBuilder_.inLanguage( aAttributes.getValue( index ) );
                    break;
                }
                case Xmls.XMLNS_ATTRIBUTE:
                {
                    String attributeNamespaceURI = aAttributes.getValue( index );
                    if ( !namespaceUri_.equals( attributeNamespaceURI ) )
                    {
                        //TODO correct StreamException error
                        throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
                    }
                    break;
                }
                default:
                {
                    //TODO correct StreamException error
                    throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
                }
            }
        }
    }

    @Override
    public final void startCDATA()
    {
        //TODO correct StreamException error
        throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
    }

    @Override
    public final void characters( final char[] aChars, final int aStart, final int aLength )
    {
        textBuilder_.withContent( valueOf( aChars, aStart, aLength ) );
    }

    @Override
    public final void endCDATA()
    {
        //TODO correct StreamException error
        throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
    }
}
