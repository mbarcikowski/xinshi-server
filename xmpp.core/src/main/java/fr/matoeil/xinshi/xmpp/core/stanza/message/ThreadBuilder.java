/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza.message;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableBaseChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNode;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.base.Strings.isNullOrEmpty;
import static fr.matoeil.xinshi.xmpp.core.internal.xerces.XMLChar.isValidNmtoken;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public final class ThreadBuilder
    extends ImmutableBaseChildElementBuilder<Thread, ThreadBuilder>
{
    private static final String MUST_NOT_BE_NULL_ERROR_MESSAGE = "%s must not be null";

    private String parentId_;

    private String id_;

    public ThreadBuilder hasParentThread( String aParentId )
    {
        checkArgument( !isNullOrEmpty( aParentId ), "must not be null or empty" );
        checkArgument( isValidNmtoken( aParentId ), "must be a NMTOKEN" );
        parentId_ = aParentId;
        return this;
    }

    public ThreadBuilder identifiedBy( String aId )
    {
        checkArgument( !isNullOrEmpty( aId ), "must not be null or empty" );
        checkArgument( isValidNmtoken( aId ), "must be a NMTOKEN" );
        id_ = aId;
        return this;
    }

    @Override
    protected Thread newImmutableChildElement( ImmutableNode aParentNode, Integer aElementIndex, String aNamespaceURI )
    {
        checkState( id_ != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "id" );
        return new Thread( aParentNode, aElementIndex, aNamespaceURI, parentId_, id_ );
    }

}
