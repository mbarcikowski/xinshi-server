/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.dom;

import org.w3c.dom.NamedNodeMap;

import javax.annotation.concurrent.Immutable;

/**
 * @author mathieu.barcikowski@gmail.com
 */
@Immutable
public interface ImmutableAttributeMap
    extends NamedNodeMap
{
    /**
     * Retrieves a immutable attribute specified by name.
     *
     * @param aName The <code>aName</code> of a node to retrieve.
     * @return A <code>ImmutableAttribute</code> (of any type) with the specified
     *         <code>aName</code>, or <code>null</code> if it does not identify
     *         any immutable attribute in this map.
     */
    @Override
    ImmutableAttribute getNamedItem( String aName );

    /**
     * Returns the <code>index</code>th item in the map. If <code>index</code>
     * is greater than or equal to the number of immutable attributes in this map, this
     * returns <code>null</code>.
     *
     * @param aIndex Index into this map.
     * @return The immutable attribute at the <code>aIndex</code>th position in the map, or
     *         <code>null</code> if that is not a valid index.
     */
    @Override
    ImmutableAttribute item( int aIndex );

    /**
     * Retrieves a immutable attribute specified by local name and namespace URI.
     * <br>Per [<a href='http://www.w3.org/TR/1999/REC-xml-names-19990114/'>XML Namespaces</a>]
     * , applications must use the value null as the namespaceURI parameter
     * for methods if they wish to have no namespace.
     *
     * @param aNamespaceURI The namespace URI of the immutable attribute to retrieve.
     * @param aLocalName    The local name of the immutable attribute to retrieve.
     * @return A <code>ImmutableAttribute</code> (of any type) with the specified local
     *         name and namespace URI, or <code>null</code> if they do not
     *         identify any immutable attribute in this map.
     */
    @Override
    ImmutableAttribute getNamedItemNS( String aNamespaceURI, String aLocalName );
}