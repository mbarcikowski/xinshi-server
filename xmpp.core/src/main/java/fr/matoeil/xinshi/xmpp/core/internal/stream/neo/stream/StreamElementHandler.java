/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stream;

import fr.matoeil.xinshi.xmpp.core.internal.dom.Nodes;
import fr.matoeil.xinshi.xmpp.core.internal.dom.QualifiedName;
import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp.XmppHandler;
import fr.matoeil.xinshi.xmpp.core.old.stream.StreamErrorConstants;
import fr.matoeil.xinshi.xmpp.core.stream.Namespaces;
import fr.matoeil.xinshi.xmpp.core.stream.StreamElement;
import fr.matoeil.xinshi.xmpp.core.stream.StreamElementBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import org.xml.sax.Attributes;

public abstract class StreamElementHandler<S extends StreamElement, B extends StreamElementBuilder<S, B>>
    extends XmppHandler
{
    private final B builder_;

    protected StreamElementHandler( final B aBuilder )
    {
        builder_ = aBuilder;
    }

    public B getBuilder()
    {
        return builder_;
    }

    public final S build()
    {
        return builder_.build();
    }

    static void checkStreamNamespaceURI( final String aNamespaceURI )
    {
        if ( Namespaces.STREAM_NAMESPACE_URI.equals( aNamespaceURI ) )
        {
            //TODO correct StreamException error
            throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
        }
    }

    static <S extends StreamElement, B extends StreamElementBuilder<S, B>> void initializeBuilderWithoutAttribute(
        final String aQualifiedName, final Attributes aAttributes, final B aBuilder )
    {
        QualifiedName featuresQualifiedName = Nodes.splitQualifiedName( aQualifiedName );
        aBuilder.withPrefix( featuresQualifiedName.getPrefix() );

        if ( aAttributes.getLength() != 0 )
        {
            //TODO correct StreamException error
            throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
        }
    }


}
