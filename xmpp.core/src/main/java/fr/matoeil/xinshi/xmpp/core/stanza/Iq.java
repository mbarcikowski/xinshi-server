/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.core.internal.dom.Elements;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableArrayNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableOneNodeList;
import fr.matoeil.xinshi.xmpp.core.internal.dom.Nodes;
import fr.matoeil.xinshi.xmpp.core.internal.xmpp.XmppEnumMap;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaError;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorBuilder;
import org.w3c.dom.Element;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

@Immutable
public final class Iq
    extends Stanza<Iq.Type>
{

    private static final Function<ExtendedContentBuilder, ExtendedContent> BUILD_EXTENDED_CONTENT = Elements.build();

    public static final String LOCAL_NAME = "iq";

    private ExtendedContent extendedContent_;

    Iq( final String aNamespaceURI, @Nullable final String aLanguage, @Nullable final Address aFrom,
        @Nullable final Address aTo, @Nullable final String aId, @Nullable final Type aType,
        @Nullable final StanzaErrorBuilder aStanzaErrorBuilder,
        @Nullable final ExtendedContentBuilder aExtendedContentBuilder )
    {
        super( aNamespaceURI, LOCAL_NAME, aLanguage, aFrom, aTo, aId, aType );
        generateChildNodes( aNamespaceURI, aStanzaErrorBuilder, aExtendedContentBuilder );
    }

    @Nullable
    public Element getChild()
    {
        return extendedContent_;
    }

    private void generateChildNodes( final String aNamespaceURI, @Nullable final StanzaErrorBuilder aStanzaErrorBuilder,
                                     @Nullable final ExtendedContentBuilder aExtendedContentBuilder )
    {
        int index = 0;

        index = initializeError( aNamespaceURI, aStanzaErrorBuilder, index );

        initializeExtendedContent( aExtendedContentBuilder, index );

        initializeChildNodes();
    }

    private void initializeExtendedContent( final ExtendedContentBuilder aExtendedContentBuilder, final int aIndex )
    {
        if ( aExtendedContentBuilder != null )
        {
            aExtendedContentBuilder.isChildOf( this ).atIndex( aIndex );
            extendedContent_ = aExtendedContentBuilder.build();
        }
        extendedContent_ = BUILD_EXTENDED_CONTENT.apply( aExtendedContentBuilder );
    }

    private void initializeChildNodes()
    {
        StanzaError aStanzaError = getStanzaError();
        ImmutableNodeList childNodes;
        if ( extendedContent_ == null )
        {
            if ( aStanzaError == null )
            {
                childNodes = Nodes.EMPTY_CHILD_NODES;
            }
            else
            {
                childNodes = new ImmutableOneNodeList( aStanzaError );
            }

        }
        else
        {
            if ( aStanzaError == null )
            {
                childNodes = new ImmutableOneNodeList( extendedContent_ );
            }
            else
            {
                childNodes = new ImmutableArrayNodeList( ImmutableList.of( aStanzaError, extendedContent_ ) );
            }
        }

        setImmutableChildNodes( childNodes );
    }

    public static enum Type
        implements fr.matoeil.xinshi.xmpp.core.stanza.Type
    {
        ERROR( "error" ),
        GET( "get" ),
        RESULT( "result" ),
        SET( "set" );

        private final String name_;

        Type( final String aName )
        {
            name_ = aName;
        }

        public String asString()
        {
            return name_;
        }

        public static Type fromString( String aType )
        {
            return XMPP_ENUM_MAP.fromString( aType );
        }

        private static final XmppEnumMap<Type> XMPP_ENUM_MAP = new XmppEnumMap<>( Type.class );
    }

}
