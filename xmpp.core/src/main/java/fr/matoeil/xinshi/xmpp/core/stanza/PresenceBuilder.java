/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza;

import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.StanzaBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.PriorityBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.ShowBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.presence.StatusBuilder;

import javax.annotation.Nullable;
import java.util.List;

import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Lists.newArrayList;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public final class PresenceBuilder
    extends StanzaBuilder<Presence.Type, Presence, PresenceBuilder>
{
    private final List<StatusBuilder> statusBuilders_ = newArrayList();

    private ShowBuilder showBuilder_;

    private PriorityBuilder priorityBuilder_;

    private StanzaErrorBuilder stanzaErrorBuilder_;

    private final List<ExtendedContentBuilder> extendedContentBuilders_ = newArrayList();

    public PresenceBuilder withStatus( final StatusBuilder aStatusBuilder )
    {
        statusBuilders_.add( aStatusBuilder );
        return this;
    }

    public PresenceBuilder withShow( final ShowBuilder aShowBuilder )
    {
        checkState( showBuilder_ == null, "must have only one show" );
        showBuilder_ = aShowBuilder;
        return this;
    }

    public PresenceBuilder withPriority( final PriorityBuilder aPriorityBuilder )
    {
        checkState( priorityBuilder_ == null, "must have only one priority" );
        priorityBuilder_ = aPriorityBuilder;
        return this;
    }

    public PresenceBuilder withError( final StanzaErrorBuilder aStanzaErrorBuilder )
    {
        checkState( stanzaErrorBuilder_ == null, "must have only one error" );
        stanzaErrorBuilder_ = aStanzaErrorBuilder;
        return this;
    }

    public PresenceBuilder withExtendedContent( final ExtendedContentBuilder aExtendedContentBuilder )
    {
        extendedContentBuilders_.add( aExtendedContentBuilder );
        return this;
    }

    @Override
    protected Presence newStanza( final String aNamespaceURI, @Nullable final Address aFrom,
                                  @Nullable final Address aTo, @Nullable final String aId,
                                  @Nullable final Presence.Type aType, @Nullable final String aLanguage )
    {
        //TODO show, status, priority, error & extendedContent
        return new Presence( aNamespaceURI, aLanguage, aFrom, aTo, aId, aType, showBuilder_, statusBuilders_,
                             priorityBuilder_, stanzaErrorBuilder_, extendedContentBuilders_ );
    }
}
