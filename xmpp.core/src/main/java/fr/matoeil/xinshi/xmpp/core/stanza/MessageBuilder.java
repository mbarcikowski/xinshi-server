/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza;

import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.StanzaBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.message.BodyBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.message.SubjectBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.message.ThreadBuilder;

import javax.annotation.Nullable;
import java.util.List;

import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Lists.newArrayList;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public final class MessageBuilder
    extends StanzaBuilder<Message.Type, Message, MessageBuilder>
{
    private final List<BodyBuilder> bodyBuilders_ = newArrayList();

    private final List<SubjectBuilder> subjectsBuilders_ = newArrayList();

    private ThreadBuilder threadBuilder_;

    private StanzaErrorBuilder stanzaErrorBuilder_;

    private final List<ExtendedContentBuilder> extendedContentBuilders_ = newArrayList();


    public MessageBuilder withBody( final BodyBuilder aBodyBuilder )
    {
        bodyBuilders_.add( aBodyBuilder );
        return this;
    }

    public MessageBuilder withSubject( final SubjectBuilder aSubjectBuilder )
    {
        subjectsBuilders_.add( aSubjectBuilder );
        return this;
    }

    public MessageBuilder withThread( final ThreadBuilder aThreadBuilder )
    {
        checkState( threadBuilder_ == null, "must have only one thread" );
        threadBuilder_ = aThreadBuilder;
        return this;
    }

    public MessageBuilder withError( final StanzaErrorBuilder aStanzaErrorBuilder )
    {
        checkState( stanzaErrorBuilder_ == null, "must have only one error" );
        stanzaErrorBuilder_ = aStanzaErrorBuilder;
        return this;
    }

    public MessageBuilder withExtendedContent( final ExtendedContentBuilder aExtendedContentBuilder )
    {
        extendedContentBuilders_.add( aExtendedContentBuilder );
        return this;
    }

    @Override
    protected Message newStanza( final String aNamespaceURI, @Nullable final Address aFrom, @Nullable final Address aTo,
                                 @Nullable final String aId, @Nullable final Message.Type aType,
                                 @Nullable final String aLanguage )
    {
        //TODO body, subject, priority, error & extendedContent
        return new Message( aNamespaceURI, aLanguage, aFrom, aTo, aId, aType, bodyBuilders_, subjectsBuilders_,
                            threadBuilder_, stanzaErrorBuilder_, extendedContentBuilders_ );
    }
}
