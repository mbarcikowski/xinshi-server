/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.dom;

import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableAttribute;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableBaseElement;
import fr.matoeil.xinshi.xmpp.core.internal.dom.ImmutableNodeBuilder;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public final class ImmutableAttributeBuilder
    implements ImmutableNodeBuilder<ImmutableAttribute>
{
    private static final String MUST_NOT_BE_NULL_ERROR_MESSAGE = "%s must not be null";

    private ImmutableBaseElement ownerElement_;

    private String namespaceURI_;

    private String prefix_;

    private String localName_;

    private String value_;

    public ImmutableAttributeBuilder isOwnedBy( ImmutableBaseElement aOwnerElement )
    {
        checkArgument( aOwnerElement != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "ownerElement" );
        ownerElement_ = aOwnerElement;
        return this;
    }


    public ImmutableAttributeBuilder withPrefix( String aPrefix )
    {
        prefix_ = aPrefix;
        return this;
    }

    public ImmutableAttributeBuilder inNamespace( String aNamespaceURI )
    {
        namespaceURI_ = aNamespaceURI;
        return this;
    }

    public ImmutableAttributeBuilder named( String aLocalName )
    {
        checkArgument( aLocalName != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "localName" );
        localName_ = aLocalName;
        return this;
    }

    public ImmutableAttributeBuilder withValue( String aValue )
    {
        value_ = aValue;
        return this;
    }

    @Override
    public ImmutableAttribute build()
    {
        checkState( ownerElement_ != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "ownerElement" );
        checkState( localName_ != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "localName" );
        return new ImmutableAttribute( ownerElement_, namespaceURI_, prefix_, localName_, value_ );
    }
}
