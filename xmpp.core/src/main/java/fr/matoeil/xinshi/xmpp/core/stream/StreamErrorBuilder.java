/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stream;

import fr.matoeil.xinshi.xmpp.core.dom.ImmutableChildElementBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.error.StreamErrorConditionBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.error.StreamErrorTextBuilder;

import javax.annotation.Nullable;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

public final class StreamErrorBuilder
    extends StreamElementBuilder<StreamError, StreamErrorBuilder>
{

    private static final String MUST_NOT_BE_NULL_ERROR_MESSAGE = "%s must not be null";

    private StreamErrorConditionBuilder streamErrorConditionBuilder_;

    private StreamErrorTextBuilder streamErrorTextBuilder_;

    private ImmutableChildElementBuilder applicationConditionBuilder_;

    public StreamErrorBuilder withCondition( StreamErrorConditionBuilder aStreamErrorConditionBuilder )
    {
        checkArgument( aStreamErrorConditionBuilder != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "conditionBuilder" );
        streamErrorConditionBuilder_ = aStreamErrorConditionBuilder;
        return this;
    }

    public StreamErrorBuilder withText( final StreamErrorTextBuilder aStreamErrorTextBuilder )
    {
        checkArgument( aStreamErrorTextBuilder != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "textBuilder" );
        checkState( streamErrorTextBuilder_ == null, "must have only one text" );
        streamErrorTextBuilder_ = aStreamErrorTextBuilder;
        return this;
    }

    public StreamErrorBuilder withApplicationCondition(
        final ImmutableChildElementBuilder aApplicationConditionBuilder )
    {
        checkArgument( aApplicationConditionBuilder != null, MUST_NOT_BE_NULL_ERROR_MESSAGE,
                       "applicationConditionBuilder" );
        checkState( applicationConditionBuilder_ == null, "must have only one application condition" );
        applicationConditionBuilder_ = aApplicationConditionBuilder;
        return this;
    }

    @Override
    protected StreamError newStreamElement( @Nullable final String aPrefix )
    {
        checkState( streamErrorConditionBuilder_ != null, MUST_NOT_BE_NULL_ERROR_MESSAGE, "conditionBuilder" );
        return new StreamError( aPrefix, streamErrorConditionBuilder_, streamErrorTextBuilder_,
                                applicationConditionBuilder_ );
    }
}
