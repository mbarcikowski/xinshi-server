/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.stanza;

import fr.matoeil.xinshi.xmpp.address.Address;
import fr.matoeil.xinshi.xmpp.core.internal.stanza.StanzaBuilder;
import fr.matoeil.xinshi.xmpp.core.stanza.error.StanzaErrorBuilder;

import javax.annotation.Nullable;

import static com.google.common.base.Preconditions.checkState;

/**
 * @author mathieu.barcikowski@gmail.com
 */
public final class IqBuilder
    extends StanzaBuilder<Iq.Type, Iq, IqBuilder>
{
    private StanzaErrorBuilder stanzaErrorBuilder_;

    private ExtendedContentBuilder extendedContentBuilder_;

    public IqBuilder withError( final StanzaErrorBuilder aStanzaErrorBuilder )
    {
        checkState( stanzaErrorBuilder_ == null, "must have only one error" );
        stanzaErrorBuilder_ = aStanzaErrorBuilder;
        return this;
    }

    public IqBuilder withExtendedContent( final ExtendedContentBuilder aExtendedContentBuilder )
    {
        checkState( extendedContentBuilder_ == null, "must have only one extended content" );
        extendedContentBuilder_ = aExtendedContentBuilder;
        return this;
    }

    @Override
    protected Iq newStanza( final String aNamespaceURI, @Nullable final Address aFrom, @Nullable final Address aTo,
                            @Nullable final String aId, @Nullable final Iq.Type aType,
                            @Nullable final String aLanguage )
    {
        return new Iq( aNamespaceURI, aLanguage, aFrom, aTo, aId, aType, stanzaErrorBuilder_, extendedContentBuilder_ );
    }
}
