/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo.stanza.message;

import fr.matoeil.xinshi.xmpp.core.internal.stream.neo.xmpp.XmppHandler;
import fr.matoeil.xinshi.xmpp.core.old.stream.StreamErrorConstants;
import fr.matoeil.xinshi.xmpp.core.stanza.message.ThreadBuilder;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import org.xml.sax.Attributes;

import static fr.matoeil.xinshi.xmpp.core.stanza.message.Thread.PARENT_ATTRIBUTE_LOCAL_NAME;
import static java.lang.String.valueOf;

public class ThreadHandler
    extends XmppHandler
{
    private final ThreadBuilder threadBuilder_;

    public ThreadHandler()
    {
        threadBuilder_ = new ThreadBuilder();
    }


    public ThreadBuilder getThreadBuilder()
    {
        return threadBuilder_;
    }

    @Override
    public XmppHandler handleElementOpening( final String aNamespaceURI, final String aLocalName,
                                             final String aQualifiedName )
    {
        //TODO correct StreamException error
        throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
    }

    @Override
    public void startPrefixMapping( final String aPrefix, final String aNamespaceURI )
    {
        //TODO correct StreamException error
        throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
    }

    @Override
    public void startElement( final String aNamespaceURI, final String aLocalName, final String aQualifiedName,
                              final Attributes aAttributes )
    {
        for ( int index = 0, length = aAttributes.getLength(); index < length; index++ )
        {
            switch ( aAttributes.getQName( index ) )
            {
                case PARENT_ATTRIBUTE_LOCAL_NAME:
                {
                    threadBuilder_.hasParentThread( aAttributes.getValue( index ) );
                    break;
                }
                default:
                {
                    //TODO correct StreamException error
                    throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
                }
            }
        }
    }

    @Override
    public void startCDATA()
    {
        //TODO correct StreamException error
        throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
    }

    @Override
    public void characters( final char[] aChars, final int aStart, final int aLength )
    {
        threadBuilder_.identifiedBy( valueOf( aChars, aStart, aLength ) );
    }

    @Override
    public void endCDATA()
    {
        //TODO correct StreamException error
        throw new StreamException( StreamErrorConstants.Error.BAD_FORMAT );
    }
}
