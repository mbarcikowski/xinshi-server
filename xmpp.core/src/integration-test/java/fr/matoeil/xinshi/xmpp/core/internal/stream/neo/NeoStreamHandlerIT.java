/*
 * Copyright 2012-2013 Mathieu Barcikowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.matoeil.xinshi.xmpp.core.internal.stream.neo;

import fr.matoeil.xinshi.xmpp.core.old.stream.StreamErrorConstants;
import fr.matoeil.xinshi.xmpp.core.stanza.Stanza;
import fr.matoeil.xinshi.xmpp.core.stream.Header;
import fr.matoeil.xinshi.xmpp.core.stream.StreamElement;
import fr.matoeil.xinshi.xmpp.core.stream.StreamException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import uk.org.retep.niosax.NioSaxParser;
import uk.org.retep.niosax.NioSaxParserFactory;
import uk.org.retep.niosax.NioSaxSource;
import uk.org.retep.niosax.charset.UTF_8;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.nio.ByteBuffer;

public class NeoStreamHandlerIT
{

    private String xml_;

    @Before
    public void setUp()
        throws Exception
    {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append( "<?xml version='1.0'?>" );
        stringBuilder.append(
            " <stream:stream from='juliet@im.example.com'  to='im.example.com'  version='1.0'  xmlns='jabber:client'  xmlns:stream='http://etherx.jabber.org/streams'>" );

        stringBuilder.append( "<stream:features>" );
        stringBuilder.append( "     <compression xmlns='http://jabber.org/features/compress'>" );
        stringBuilder.append( "       <method>zlib</method>" );
        stringBuilder.append( "       <method>lzw</method>" );
        stringBuilder.append( "     </compression>" );
        stringBuilder.append( "   </stream:features>" );

        stringBuilder.append(
            "        <message from='juliet@example.com/balcony' id='z94nb37h'  to='romeo@example.net' type='chat' xml:lang='en'>" );
        stringBuilder.append( "<subject>I implore you!</subject>" );
        stringBuilder.append( "<subject xml:lang='fr'>Je t'implore!</subject>" );
        //stringBuilder.append( "<subject xml:lang='cs'>&#x00DA;p&#x011B;nliv&#x011B; pros&#x00ED;m!</subject>" );
        stringBuilder.append( "  <body>Wherefore art thou, Romeo?</body>" );
        stringBuilder.append( "  <body xml:lang='fr'>Où es-tu, mon Romeo?</body>" );
        //stringBuilder.append( "  <body xml:lang='cs'>Pro&#x010D;e&#x017E; jsi ty, Romeo?</body>" );
        stringBuilder.append(
            " <thread parent='e0ffe42b28561960c6b12b944a092794b9683a38'>0e3141cd80894871a68e6fe6b1ec56fa</thread>" );
        stringBuilder.append( "<html xmlns='http://jabber.org/protocol/xhtml-im'>" );
        stringBuilder.append( "     <body xmlns='http://www.w3.org/1999/xhtml'>" );
        stringBuilder.append(
            "      <p>Wherefore <span style='font-style: italic'>art</span> thou, <span style='color:red'>Romeo</span>?</p>" );
        stringBuilder.append( "     </body>" );
        stringBuilder.append( "  </html>" );
        stringBuilder.append( "</message>" );

        stringBuilder.append( "<presence xml:lang='en'>" );
        stringBuilder.append( "  <show>dnd</show>" );
        stringBuilder.append( "  <status>Wooing Juliet</status>" );
        stringBuilder.append( "  <status xml:lang='fr'>Courtise Juliette</status>" );
        //stringBuilder.append( "  <status xml:lang='cs'>Dvo&#x0159;&#x00ED;m se Julii</status>" );
        stringBuilder.append( "  <priority>1</priority>" );
        stringBuilder.append(
            "   <c xmlns='http://jabber.org/protocol/caps' hash='sha-1' node='http://psi-im.org' ver='q07IKJEyjvHSyhy//CH0CxmKi8w='/>" );
        stringBuilder.append( "</presence>" );

        stringBuilder.append( "<iq from='romeo@example.net/orchard'  id='hf61v3n7' type='get'>" );
        stringBuilder.append( "      <query xmlns='jabber:iq:roster' />" );
        stringBuilder.append( "    </iq>" );

        stringBuilder.append( "<iq id='hf61v3n7' to='romeo@example.net/orchard' type='result'>" );
        stringBuilder.append( "      <query xmlns='jabber:iq:roster'>" );
        stringBuilder.append( "        <item jid='juliet@example.com' name='Juliet' subscription='both'>" );
        stringBuilder.append( "          <group>Friends</group>" );
        stringBuilder.append( "        </item>" );
        stringBuilder.append( "        <item jid='benvolio@example.org' name='Benvolio' subscription='to'/>" );
        stringBuilder.append( "        <item jid='mercutio@example.org' name='Mercutio' subscription='from'/>" );
        stringBuilder.append( "      </query>" );
        stringBuilder.append( "    </iq>" );

        stringBuilder.append( "<iq from='romeo@example.net/orchard'  id='hf61v3n7' type='get'>" );
        stringBuilder.append(
            "      <rest:rest xmlns:rest='fr:matoeil:rest' rest:method='POST' resource='MyResource/1'>postbody</rest:rest>" );
        stringBuilder.append( "</iq>" );

        stringBuilder.append( "<iq from='romeo@example.net/orchard'  id='df14b5d4' type='get'>" );
        stringBuilder.append(
            "      <rest:rest xmlns:rest='fr:matoeil:rest' rest:method='POST' resource='MyResource/2'><![CDATA[postbody]]></rest:rest>" );
        stringBuilder.append( "</iq>" );
        stringBuilder.append( "<iq from='romeo@example.net/orchard'  id='df14b5d4' type='get'>" );
        stringBuilder.append(
            "      <rest:rest xmlns:rest='fr:matoeil:rest' rest:method='GET' resource='MyResource/2'>" );
        stringBuilder.append( "<parameter name='parameter_1'>value_1</parameter>" );
        stringBuilder.append( "<parameter name='parameter_2'><![CDATA[value_2]]></parameter>" );
        stringBuilder.append( "</rest:rest>" );
        stringBuilder.append( "</iq>" );

        stringBuilder.append( "<iq from='romeo@example.net/orchard'  id='hf61v3n7' type='error'>" );
        stringBuilder.append( "      <query xmlns='jabber:iq:roster' />" );
        stringBuilder.append( " <error by='roster@domain.com' type='cancel'>" );
        stringBuilder.append(
            "   <redirect xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'>xmpp:characters@conference.example.org</redirect>" );
        stringBuilder.append( "<failed-rules xmlns='http://jabber.org/protocol/amp#errors'>" );
        stringBuilder.append( "         <rule action='error' condition='deliver'  value='stored'/>" );
        stringBuilder.append( "       </failed-rules>" );
        stringBuilder.append( "</error>" );
        stringBuilder.append( "    </iq>" );

        stringBuilder.append( "<presence type='error'>" );
        stringBuilder.append( " <error by='roster@domain.com' type='cancel'>" );
        stringBuilder.append(
            "   <redirect xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'>xmpp:characters@conference.example.org</redirect>" );
        stringBuilder.append( "<failed-rules xmlns='http://jabber.org/protocol/amp#errors'>" );
        stringBuilder.append( "         <rule action='error' condition='deliver'  value='stored'/>" );
        stringBuilder.append( "       </failed-rules>" );
        stringBuilder.append( "</error>" );
        stringBuilder.append( "</presence>" );

        stringBuilder.append(
            "        <message from='juliet@example.com/balcony' id='z94nb37h'  to='romeo@example.net' type='error' xml:lang='en'>" );
        stringBuilder.append( " <error by='roster@domain.com' type='cancel'>" );
        stringBuilder.append(
            "   <redirect xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'>xmpp:characters@conference.example.org</redirect>" );
        stringBuilder.append( "<failed-rules xmlns='http://jabber.org/protocol/amp#errors'>" );
        stringBuilder.append( "         <rule action='error' condition='deliver'  value='stored'/>" );
        stringBuilder.append( "       </failed-rules>" );
        stringBuilder.append( "</error>" );
        stringBuilder.append( "</message>" );

        stringBuilder.append( "<message type='error' id='7h3baci9'>" );
        stringBuilder.append( "  <error type='modify'>" );
        stringBuilder.append( "    <undefined-condition xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>" );
        stringBuilder.append( "    <text xml:lang='en'  xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'>" );
        stringBuilder.append( "      [ ... application-specific information ... ]" );
        stringBuilder.append( "    </text>" );
        stringBuilder.append( "    <too-many-parameters xmlns='http://example.org/ns'/>" );
        stringBuilder.append( "  </error>" );
        stringBuilder.append( "</message>" );

        stringBuilder.append( " <stream:error>" );
        stringBuilder.append(
            "     <see-other-host xmlns='urn:ietf:params:xml:ns:xmpp-streams'>[2001:41D0:1:A49b::1]:9222</see-other-host>" );
        stringBuilder.append( "   </stream:error>" );

        stringBuilder.append( "<stream:error>" );
        stringBuilder.append( "     <unsupported-feature xmlns='urn:ietf:params:xml:ns:xmpp-streams'/>" );
        stringBuilder.append( "   </stream:error>" );

        stringBuilder.append( "<stream:error>" );
        stringBuilder.append( "     <not-well-formed xmlns='urn:ietf:params:xml:ns:xmpp-streams'/>" );
        stringBuilder.append(
            "     <text xml:lang='en' xmlns='urn:ietf:params:xml:ns:xmpp-streams'>Some special application diagnostic information!</text>" );
        stringBuilder.append( "     <escape-your-data xmlns='http://example.org/ns'/>" );
        stringBuilder.append( "   </stream:error>" );

        //TODO stream without prefix
        //stringBuilder.append( "<presence xml:lang='en' xmlns='jabber:client'>" );

        stringBuilder.append( "<starttls xmlns='urn:ietf:params:xml:ns:xmpp-tls' xml:lang='toto' />" );

        stringBuilder.append( "<proceed xmlns='urn:ietf:params:xml:ns:xmpp-tls'/>" );

        stringBuilder.append(
            "<auth xmlns='urn:ietf:params:xml:ns:xmpp-sasl' mechanism='PLAIN'>AGp1bGlldAByMG0zMG15cjBtMzA=</auth>" );

        stringBuilder.append( "<abort xmlns='urn:ietf:params:xml:ns:xmpp-sasl'/>" );

        stringBuilder.append( "<failure xmlns='urn:ietf:params:xml:ns:xmpp-sasl' >" );
        stringBuilder.append( "     <aborted/>" );
        stringBuilder.append( "   </failure>" );

        stringBuilder.append( "</stream:stream>" );

        xml_ = stringBuilder.toString();

    }

    //TODO make it a integration test
    @Ignore
    @Test
    public void testTouille()
        throws Exception
    {

        NioSaxParserFactory NIO_SAX_PARSER_FACTORY = NioSaxParserFactory.getInstance();
        NioSaxParser nioSaxParser = NIO_SAX_PARSER_FACTORY.newInstance();
        nioSaxParser.setHandler( new StreamHandler( new StreamListener()
        {
            @Override
            public void handleStreamOpening( final Header aHeader )
            {
                System.out.println( aHeader );
            }

            @Override
            public void handleIncomingStreamElement( final StreamElement aElement )
            {
                System.out.println( aElement );
            }

            @Override
            public void handleIncomingStanza( final Stanza aStanza )
            {
                System.out.println( aStanza );
            }

            @Override
            public void handleStreamClosing()
            {
                System.out.println( "Footer" );
            }
        } ) );
        NioSaxSource nioSaxSource = new NioSaxSource( new UTF_8() );
        try
        {
            nioSaxParser.startDocument();
        }
        catch ( SAXException e )
        {
            throw new StreamException( StreamErrorConstants.Error.INTERNAL_SERVER_ERROR, e.getMessage(), e );
        }
        final ByteBuffer byteBuffer = ByteBuffer.wrap( xml_.getBytes( "UTF-8" ) );
        nioSaxSource.setByteBuffer( byteBuffer );
        nioSaxParser.parse( nioSaxSource );
        nioSaxSource.compact();


    }

    @Test
    public void testSax()
        throws Exception
    {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setNamespaceAware( true );
        SAXParser saxParser = factory.newSAXParser();

        DefaultHandler handler = new DefaultHandler()
        {
            @Override
            public void startElement( final String uri, final String localName, final String qName,
                                      final Attributes attributes )
                throws SAXException
            {
                super.startElement( uri, localName, qName, attributes );
            }
        };

        ByteArrayInputStream inputStream = new ByteArrayInputStream( xml_.getBytes( "UTF-8" ) );
        saxParser.parse( inputStream, handler );

    }

    @Test
    public void testDom()
        throws Exception
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware( true );
        DocumentBuilder builder = factory.newDocumentBuilder();

        ByteArrayInputStream inputStream = new ByteArrayInputStream( xml_.getBytes( "UTF-8" ) );
        Document doc = builder.parse( inputStream );

        doc.getDocumentElement();

        //optional, but recommended
        //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
        doc.getDocumentElement().normalize();
    }
}
